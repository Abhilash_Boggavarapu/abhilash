﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Client.master" AutoEventWireup="true" CodeBehind="Compose.aspx.cs" Inherits="ETaxFilling.MailBox.Compose" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%-- <script type="text/javascript">
    function chkVal() {
        var drpHA = document.getElementById('<%=ddlSub.ClientID %>').value;
        var trHA = document.getElementById('<%=trSub.ClientID %>');
        if (drpHA == '7') {
            trHA.style.display = ''; // this is for showing


        }
        else {
            trHA.style.display = 'none'; // this is for hiding
        }
    }
 
</script> 
--%>

    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding-right:10px;" id="TABLE1" class="box">
  <tr>
    <td align="left"><asp:Label ID="lblTitle" runat="server" Width="100%" Text="Compose" CssClass="divider top20 bottom10"></asp:Label></td>
  </tr>
  <tr>
  <td><asp:Label ID="lblmessage" runat="server" 
          CssClass="error" Text="Message" Visible="False" ></asp:Label></td>
  </tr>
  <tr>
    <td class="content" align="center"><table width="80%" border="0" cellpadding="0" cellspacing="8">
	 
	  <tr id="trTomember" runat="server" visible="false"> 
        <td  style="width:45%; text-align:right;" class="l_text">To  :</td>
        <td class="formtext"><asp:TextBox ID="txtbtoUserName" runat="server"  CssClass="usertext" Width="384px" Text="Admin@dtptax.com" ></asp:TextBox></td>
      </tr>
	 
	  <tr id="trSub" runat="server">
        <td style="width:45%; text-align:right;" class="l_text">Subject :</td>
        <td class="formtext"><asp:TextBox ID="txtbSub" runat="server" Width="384px" CssClass="textbox"></asp:TextBox></td>
      </tr>
	  <tr>
        <td style="width:45%; text-align:right;" class="l_text">Message :</td>
        <td class="formtext" style=" padding-left :25px" ><asp:TextBox ID="txtbMessage" runat="server"  CssClass="textbox" Height="150px" Width="384px" TextMode="MultiLine" MaxLength="10"></asp:TextBox></td>
      </tr>
	  <tr>
        <td style="width:45%; text-align:right;"></td>
        <td class="formtext"><span id="lblCharCount" ></span></td>
      </tr>
	  <tr>
        <td style="width:45%; text-align:right;"></td>
        <td class="formtext">
            <asp:Button ID="btnSubmit" runat="server" Text="Send " 
                ForeColor="black" CssClass="button" Height="32px" Width="72px" onclick="btnSubmit_Click1" 
                /></td>
      </tr>
	  <tr>
        <td style="width:45%; text-align:right;"></td>
        <td class="label" align="left">Notes:</td>
      </tr>
	  <tr>
        <td style="width:45%; text-align:right;"></td>
        <td style="font-size:11px; color:#999999;" align="left">1)Maximum length of the message is 800 characters</td>
      </tr>
	  </table></td>
  </tr>
  <script type="text/javascript" language="javascript">
      var txtMessage = document.getElementById('ContentPlaceHolder1_txtbMessage');
      var lblCharCount = document.getElementById('lblCharCount');
      txtMessage.onkeyup = function () {
          var msg = txtMessage.value;

          lblCharCount.innerHTML = "Total characters entered :" + (msg.length);
          if (msg.length >= 800) {
              alert('Maximum 800 Characters Only')
          }
      }
                </script>
</table>
</asp:Content>
