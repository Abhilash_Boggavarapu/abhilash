﻿<%@ Page Language="C#" MasterPageFile="~/Client.Master" AutoEventWireup="true" CodeBehind="BankInformation.aspx.cs"
    Inherits="ETaxFilling.BankInformation1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table align="center" cellpadding="0" cellspacing="8" border="0" width="100%" class="box">
        <tr class="box">
            <td colspan="2">
                <h2 class="divider top20 bottom10">
                    Bank Information</h2>
                <div id="divBIerr" style="color: #FF0000" runat="server">
                </div>
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 40%;">
                <span style="color:red;">*</span> Account No :
             
            </td>
            <td style="width: 232px">
                <asp:TextBox ID="txtAcNo" runat="server" CssClass="ValidationRequired digits" MaxLength="50"
                    Height="20px" Width="124px" TabIndex="1"></asp:TextBox>
            </td>
        </tr>
         <tr>
            <td align="right" style="width: 40%;">
                <span style="color:red;">*</span> Routing No :
            </td>
            <td style="width: 232px">
                <asp:TextBox ID="txtRoutingNo" runat="server" CssClass="ValidationRequired digits" MaxLength="50"
                    Height="20px" Width="124px" TabIndex="2"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                <span style="color:red;">*</span> Bank Name :
            
            </td>
            <td style="width: 232px; height: 39px;">
                <asp:TextBox ID="txtBName" runat="server" CssClass="ValidationRequired" MaxLength="100"
                    Height="20px" Width="124px" TabIndex="3"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                <span style="color:red;">*</span> Account Holder Name :
           
            </td>
            <td style="width: 232px">
                <asp:TextBox ID="txtAName" runat="server" CssClass="ValidationRequired" MaxLength="100"
                    Height="20px" Width="124px" TabIndex="4"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                <span style="color:red;">*</span> Account Type :
             
            </td>
            <td class="field" style="padding-left: 25px;">
                <asp:DropDownList ID="ddlAType" runat="server" CssClass="ValidationRequired"
                    Height="22px" Width="124px" TabIndex="5">
                    <asp:ListItem Value="">Select</asp:ListItem>
                    <asp:ListItem Value="Savings">Savings</asp:ListItem>
                    <asp:ListItem>Checkings</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
            </td>
            <td style="width: 232px">
                &nbsp;&nbsp;
                <asp:Button ID="btnCDetails" runat="server" Font-Bold="True" Text="Save" CssClass="button"
                    OnClick="btnCDetails_Click" Height="29px" Width="96px" TabIndex="6" />
            </td>
        </tr>
        <tr>
            <td class="style3" colspan="2">
                <table>
                    <asp:Label ID="lblStatus" runat="server" CssClass="error"></asp:Label>
                </table>
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr align="center">
            <asp:Label Text="" ID="lblDisplay" runat="server" />
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GrdBank" runat="server" AlternatingRowStyle-CssClass="alt" margin="5%"
                    AutoGenerateColumns="False" PagerStyle-CssClass="pgr" Width="100%" HorizontalAlign="Center"
                    CssClass="mGrid" OnPageIndexChanging="GrdBank_PageIndexChanging" AllowPaging="True">
                    <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
                    <Columns>
                        <asp:TemplateField HeaderText="S.No.">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %><br />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="BankDetailsID" HeaderText="Id" Visible="false" />
                        <asp:BoundField DataField="AccountNo" HeaderText="Account No" />
                         <asp:BoundField DataField="RoutingNo" HeaderText="Routing No" />
                          <asp:BoundField DataField="AccountHolderName" HeaderText="AccountHolder Name" />
                        <asp:BoundField DataField="AccounTtype" HeaderText="Account Type" />
                        <asp:BoundField DataField="BankName" HeaderText="Bank Name" />
                    </Columns>
                    <PagerSettings FirstPageText="First" LastPageText="Last" />
                    <PagerStyle CssClass="pgr"></PagerStyle>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
