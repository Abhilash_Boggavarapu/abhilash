﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ETaxDal;
using ETaxBO;
namespace ETaxFilling
{
    public partial class ChangePasword : System.Web.UI.Page
    { 
        ChangeSettingsDAL objChangeSettingDal = new ChangeSettingsDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Utility.Utility.checkLoginSession())
            {
                Response.Redirect("userlogin.aspx");
            }
            lblmsg.Text = string.Empty;
            lblStatus.Text = string.Empty;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            lblmsg.Text = string.Empty;
            if (string.IsNullOrEmpty(TxtPwd.Text))
            {
                divCpasserr.InnerText = Utility.Utility.ReqPwd;
            }
            else if (!TxtPwd.Text.IsStrongPassword())
            {
                divCpasserr.InnerText = Utility.Utility.ValPwd;
            }
             else  if (string.IsNullOrEmpty(TxtNPwd.Text))
            {
                divCpasserr.InnerText = Utility.Utility.ReqNPwd;
            }
            else if (!TxtNPwd.Text.IsStrongPassword())
            {
                divCpasserr.InnerText = Utility.Utility.ValNPwd;
                TxtNPwd.Text = string.Empty;
                TxtNPwd.Focus();
            }
            else if (string.IsNullOrEmpty(TxtCPwd.Text))
            {
                divCpasserr.InnerText = Utility.Utility.ReqCPwd;
            }         
            else if (!TxtNPwd.Text.Equals(TxtCPwd.Text))
            {
                divCpasserr.InnerText = Utility.Utility.MatchPwd;
                TxtCPwd.Text = string.Empty;
                TxtCPwd.Focus();
            }
            else
            {
                BO_tblUser objUser = new BO_tblUser();
                divCpasserr.InnerText = string.Empty;
                objUser.Password = TxtNPwd.Text;
                string Password = objChangeSettingDal.GetPassword(Utility.Utility.getLoginSession().UserID);
                if (TxtPwd.Text == Password)
                {
                    int Status = objChangeSettingDal.UpdateNewPassword(Utility.Utility.getLoginSession().UserID, objUser);
                    if (Status == 1)
                    {
                        lblStatus.Text = "Your Password Updated Successfully";
                        lblStatus.ForeColor = System.Drawing.Color.Green;
                    }
                    else
                    {
                        lblStatus.Text = "Error occured while Updating Password";
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                    }
                    TxtCPwd.Text = TxtNPwd.Text = TxtPwd.Text = string.Empty;
                    //TxtPwd.Text = Password;
                    //lblmsg.Text = "Your Password Matches";
                    //lblmsg.ForeColor = System.Drawing.Color.Green;
                }
                else
                {
                    lblmsg.Text = "The old password you entered is wrong";
                    lblmsg.ForeColor = System.Drawing.Color.Red;
                }               
            }
        }
    }
}