﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ETaxDal;
using ETaxBO;
using System.IO;

namespace ETaxFilling
{
    public partial class ConfirmTaxDocument : System.Web.UI.Page
    {
        UploadDocumentsDAL objDocumentDal = new UploadDocumentsDAL();
        long taxPayerID;
        string Remarks;
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMsg.Text = string.Empty;
            lblGrid.Text =string.Empty;
            taxPayerID = Utility.Utility.getLoginSession().TaxPayerID;
            if (!Utility.Utility.checkLoginSession())
            {
                Response.Redirect("userlogin.aspx");
            }
            if (!IsPostBack)
            { 
                List<BO_tblTaxSheets> objTaxSheets = objDocumentDal.getReturnTaxpayerInformation(Utility.Utility.getLoginSession().TaxPayerID, Convert.ToInt32(MasterItems.TaxSheetSType.ConfirmTaxDocuments),(MasterItems.TaxSheetSource.Admin).ToString());
                if (objTaxSheets.Count != 0)
                {
                    GdVDownloadConfirmDocuments.DataSource = objTaxSheets;
                    GdVDownloadConfirmDocuments.DataBind();
                }
                else
                {
                    lblStus.Text = "No Form 8879 Form Found";
                    lblStus.ForeColor = System.Drawing.Color.Red;
                    pnlConfirm.Enabled = false;
                    FileUpload.Enabled = false;
                }
            }
        }
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            Remarks = txtRemarks.Text;
            fileExtension();         
        }
        private void fileExtension()
        {
            if (FileUpload.PostedFile.FileName == "")
            {
                lblMsg.ForeColor = System.Drawing.Color.Red;
                lblMsg.Text = "No File selected To Upload";
            }
            else
            {
                string[] validFileTypes = { "zip", "rar", "doc", "docx", "pdf", "jpeg", "jpg", "xlsx" };
                string ext = System.IO.Path.GetExtension(FileUpload.PostedFile.FileName);
                bool isValidFile = false;
                for (int i = 0; i < validFileTypes.Length; i++)
                {
                    if (ext == "." + validFileTypes[i])
                    {
                        isValidFile = true;
                        break;
                    }
                }
                if (!isValidFile)
                {
                    lblMsg.ForeColor = System.Drawing.Color.Red;
                    lblMsg.Text = "Invalid File. Please upload a File with extension " +
                    string.Join(",", validFileTypes);
                }
                else
                {
                    SaveDocuments();
                    lblMsg.ForeColor = System.Drawing.Color.Green;
                    lblMsg.Text = "File uploaded successfully.";
                    txtRemarks.Text = string.Empty;
                }
            }
        }
        private void SaveDocuments()
        {
            try
            {
                BO_tblTaxSheets objSheets = new BO_tblTaxSheets();
                HttpFileCollection hfc = Request.Files;
                for (int i = 0; i < hfc.Count; i++)
                {
                    HttpPostedFile hpf = hfc[i];
                    if (hpf.ContentLength == 0)
                    {
                        lblMsg.ForeColor = System.Drawing.Color.Red;
                        lblMsg.Text = "Can't  Upload Empty Files";
                    }
                    if (hpf.ContentLength > 0)
                    {
                        string fileName = hpf.FileName;
                        int fileSize = hpf.ContentLength;
                        byte[] documentBinary = new byte[fileSize];
                        hpf.InputStream.Read(documentBinary, 0, fileSize);
                        objSheets.Remarks = Remarks;
                        objSheets.SheetName = fileName;
                        objSheets.SheetsType = Convert.ToInt32(MasterItems.TaxSheetSType.ConfirmTaxDocuments);
                        objSheets.TaxSheet = documentBinary;
                        objSheets.TaxPayerID = taxPayerID;
                        if (cb.Checked == true)
                        objSheets.EligibleForEfilingStatus = false;
                        objSheets.EligibleForEfilingStatus = true;
                        objSheets.TaxSheetSource = MasterItems.TaxSheetSource.TaxPayer.ToString();
                        Session["ConfirmDocument"] = objSheets;                    
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnConfirmTaxDocument_Click(object sender, EventArgs e)
        {

            BO_tblTaxSheets sessionObjSheets = Session["ConfirmDocument"] as BO_tblTaxSheets;
            if (sessionObjSheets != null)
            {
                sessionObjSheets.ConfirmTaxDocumentStatus = true;
                objDocumentDal.SaveConfirmTaxSheet(sessionObjSheets);
                lblConfirm.Text = "Sucessfully Confirmed";
                lblConfirm.ForeColor = System.Drawing.Color.Green;
                Session["ConfirmDocument"] = null;
            }
        }

        protected void btnClnDocView_Click(object sender, EventArgs e)
        {
             UploadDocumentsDAL objDocuMentDal = new UploadDocumentsDAL();
            List<BO_tblTaxSheets> objTaxSheets = objDocuMentDal.getReturnTaxpayerInformation(Utility.Utility.getLoginSession().TaxPayerID, Convert.ToInt32(MasterItems.TaxSheetSType.ConfirmTaxDocuments),(MasterItems.TaxSheetSource.TaxPayer).ToString());
            if (objTaxSheets.Count!=0)
            {
                grdClintDocDisplay.DataSource = objTaxSheets;
                grdClintDocDisplay.DataBind();
            }
            else
            {
                lblGrid.Text = "No Records Found";
                lblGrid.ForeColor = System.Drawing.Color.Red;
            }
        }
        protected void grdClintDocDisplay_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Download")
            {
                BO_tblTaxSheets objTaxSheets = objDocumentDal.GetTaxPayerSheets(Convert.ToInt32(e.CommandArgument.ToString()));
                byte[] fileData = objTaxSheets.TaxSheet;
                Response.ClearContent();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + objTaxSheets.SheetName);
                BinaryWriter bw = new BinaryWriter(Response.OutputStream);
                bw.Write(fileData);
                bw.Close();
                Response.ContentType = "application";
                Response.End();
            }
        }

        protected void GdVDownloadConfirmDocuments_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Download")
            {
                BO_tblTaxSheets objTaxSheets = objDocumentDal.GetTaxPayerSheets(Convert.ToInt32(e.CommandArgument.ToString()));
                byte[] fileData = objTaxSheets.TaxSheet;
                Response.ClearContent();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + objTaxSheets.SheetName);
                BinaryWriter bw = new BinaryWriter(Response.OutputStream);
                bw.Write(fileData);
                bw.Close();
                Response.ContentType = "application/pdf";
                Response.End();
            }
        }
    }
}