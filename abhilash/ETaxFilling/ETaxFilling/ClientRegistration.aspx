﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClientRegistration.aspx.cs" Inherits="ETaxFilling.ClientRegistration" %>
     <%@ Register TagPrefix="recaptcha" Namespace="Recaptcha" Assembly="Recaptcha" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Delta Tax Planners : Client Registration</title>
    <meta name="title" content="" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="alexaVerifyID" content="Ga2U3YWa-o9hB-cZ4ob6KoXwqZA" />
    <link rel="icon" type="image/png" href="images/favicon.png" />
    <link href="styles/front_style_home.css" media="screen" rel="stylesheet" type="text/css" />
    <script src="B_Scripts/jquery.js" type="text/javascript"></script>
    <script src="B_Scripts/jquery.maskedinput.js" type="text/javascript"></script>
    <script src="B_Scripts/jquery.validate.js" type="text/javascript"></script>
    <script src="B_Scripts/mktSignup.js" type="text/javascript"></script>
    <link href="B_Styles/stylesheet.css" rel="stylesheet" type="text/css" />
  
</head>
    

<body class='innerbody'>
    <form id="form1" runat="server">
      <asp:ScriptManager runat="server" />
    <div>
        <!--wrapper start here -->
        <div class="wrapper">
            <!--header start here -->
            <!--header start here -->
            <div class="header clearfix">
                <div class="top_navigation">
                    <!--container start here -->
                    <div class="container">
                        <h1 class="logo alignleft">
                            <a href="index.html" title="Delta Tax Planners">Delta Tax Planners</a>
                        </h1>
                        <div class="top_link alignright">
                            <div class="top_linkrcor alignright">
                                <div class="alignleft">
                                    <a href="index.html">
                                        <img src="images/home-icon.gif" /></a> | <a href="Faq.html">FAQ</a> | <a href="Links.html">
                                            Links</a></div>
                                <div class="alignleft">
                                    | <a href="../../UserLogIn.aspx">Login</a></div>
                                | <a href="#">Sign Up</a>
                                <div class="alignnone">
                                </div>
                            </div>
                            <div class="alignright toplinklcor">
                            </div>
                            <div class="alignnone">
                            </div>
                        </div>
                    </div>
                    <!--container end here -->
                </div>
                <div class="alignnone">
                </div>
                <div class="mid-nav">
                    <!--container start here -->
                    <div class="container">
                        <div class="navigation_home">
                            <ul>
                                <li class="home"><a href="index.html" title="Home" class="home">Home</a></li>
                                <li class="aboutus"><a href="aboutus.html" title="About us" class="aboutus active">About
                                    us</a></li>
                                <li class="view"><a href="Services.html" title="Services" class="view ">Services</a></li>
                                <li class="myaccount"><a href="whydtp.html" title="Why DTP ?" class="myaccount ">Why
                                    DTP ?</a></li>
                                <li class="share"><a href="contactus.html" title="Contact Us" class="share ">Contact
                                    Us </a></li>
                                <li class="rewards last"><a href="faq.html" title="FAQ" class="rewards">FAQ </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!--container end here -->
                </div>
            </div>
            <!--header end here -->
            <!--header end here -->
            <!--middle start here -->
            <div class="inner_middle">
                <div class="container">
                    <div class="white_roundbdr_box">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <h2 class="text_header">
                                        <img src="images/success-story.png" />
                                        Client Registration :</h2>
                                </td>
                                <td width="10">
                                </td>
                                <td width="200" valign="middle" align="right">
                                    <a href="#">
                                        </a>
                                &nbsp;</td>
                            </tr>
                        </table>
                    </div>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td valign="top">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="white_roundbdr_box">
                                    <tr>
                                        <td class="sp_re_top">
                                            <span></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="sp_re_middle">
                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td align="left">
                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                                <td align="center">
                                                                    <table class="cms" cellpadding="0" cellspacing="0" border="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                    <div id="divCREGerror" style="color:#FF0000" runat="server"></div>
    <div align ="center">
          <table cellpadding="0" cellspacing="0" border="0" width="100%" >
                <tr>
                    <td align ="right" style=" width:40%;">
                        <span style="color: #FF0000">* </span> Email ID (User Name) :</td>
                    <td align="left" >
                        <asp:TextBox ID="txtUserName" runat="server" 
                            CssClass="ValidationRequired email" Width="124px" 
                            ontextchanged="txtUserName_TextChanged" AutoPostBack="True"
                            MaxLength="50" TabIndex="1"></asp:TextBox>
                            </td>
                              </tr>
                              <tr>
                           <td></td>
                            <td align="left">
                        <asp:Label ID="lblmsg" CssClass="error" runat="server"></asp:Label>
                        </td>
                            </tr>
                <tr>
                    <td align ="right" >
                    <span style="color: #FF0000">*</span> Password :</td>
                    <td align="left" >
                        <asp:TextBox ID="txtPassword"  runat="server" 
                            CssClass="ValidationRequired" Width="124px" TextMode="Password" 
                            MaxLength="15" TabIndex="2"></asp:TextBox>
                        </td>
                </tr>
                <tr>
                    <td align="right"  >
                        <span style="color: #FF0000">*</span>  Confirm Password :</td>
                    <td align="left" >
                     <asp:TextBox ID="txtConformPassword"  CssClass="ValidationRequired" runat="server" Width="124px" TextMode="Password" 
                            MaxLength="15" TabIndex="3"></asp:TextBox>
                     </td>
                </tr>
                <tr>
            <td>
            </td>
            <td style="font-size:9px; color:Gray;" align="left">
                  Note: Password length must be 6 Characters(should contain one alphabets )
                </td></tr>
                <tr>
                    <td align="right"  >
                    <span style="color: #FF0000">*</span> First Name :</td>
                    <td align="left" >
                        <asp:TextBox ID="txtFirstname" runat="server" CssClass="ValidationRequired" 
                            MaxLength="50" Width="123px" TabIndex="4"></asp:TextBox>
                       </td>
                </tr>
                <tr>
                    <td align="right"  >
                        <span style="color: #FF0000">*</span> Last Name :</td>
                    <td  align ="left" >
                        <asp:TextBox ID="txtLastname" runat="server" CssClass="ValidationRequired" 
                            MaxLength="50" Width="123px" TabIndex="5"></asp:TextBox>
                       </td>
                </tr>
                <tr>
                    <td align="right"  > 
                        <span style="color: #FF0000">*</span> Gender :</td>
                    <td  align="left" >
                        <asp:DropDownList ID="ddlGender" runat="server" Height="20px" Width="135px" 
                            TabIndex="6">
                            <asp:ListItem>--Select--</asp:ListItem>
                            <asp:ListItem>Male</asp:ListItem>
                            <asp:ListItem>Female</asp:ListItem>
                            
                        </asp:DropDownList>
                     </td>
                    
                </tr>
              
                <tr>
                    <td align="right" >
                        <span style="color: #FF0000">*</span> Country :</td>
                    <td  align="left" >
                        <asp:UpdatePanel ID="up1" runat="server">
                            <ContentTemplate>
                           
                       <asp:DropDownList ID="ddlCountry" runat="server"  Width="135px"  
                        AutoPostBack="True" Height="20px" 
                        onselectedindexchanged="ddlCountry_SelectedIndexChanged" TabIndex="7">
                    </asp:DropDownList>
                     </ContentTemplate>
                        </asp:UpdatePanel>
                
                    </td>
                </tr>
                <tr>
                    <td align="right"  >
                       <span style="color: #FF0000">*</span> State :</td>
                    <td align="left">
                     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                        <asp:DropDownList ID="ddlState" runat="server" 
                             Height="20px"  Width="135px" 
                            AutoPostBack="True" onselectedindexchanged="ddlState_SelectedIndexChanged" 
                                    TabIndex="8">
                          
                        </asp:DropDownList>
                        </ContentTemplate></asp:UpdatePanel>
                     </td>
                </tr>
                <tr>
                    <td align ="right">
                      <span style="color: #FF0000">*</span> City :</td>
                    <td align="left"  >
                     <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                       <asp:DropDownList ID="ddlCity" runat="server" 
                             Height="20px" Width="135px"  TabIndex="9" 
                                    AutoPostBack="True" onselectedindexchanged="ddlCity_SelectedIndexChanged">
                           
                        </asp:DropDownList>
                        </ContentTemplate></asp:UpdatePanel>
                        </td>
                </tr>
                 <tr id="trcity" runat="server">
                <td></td>
                <td align="left">
                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>
              <asp:TextBox ID="txtCity"   Width="124px" runat="server" />
               </ContentTemplate></asp:UpdatePanel>
                   
                </td>
                </tr>
                <tr>
                    <td align ="right">
                      <span style="color: #FF0000"></span> ZipCode :</td>
                    <td align="left">
                        <asp:TextBox ID="txtZip" runat="server"  
                            Width="124px"  MaxLength="10" TabIndex="10"></asp:TextBox>
                        </td>
                </tr>
                <tr>
                    <td align ="right">
                        <span style="color: #FF0000">*</span> Mobile Number :</td>
                    <td align ="left">
                        <asp:TextBox ID="txtMobileNo" runat="server" 
                            CssClass="ValidationRequired " Width="124px"  MaxLength="20" 
                            TabIndex="11"></asp:TextBox>
                        </td>
                </tr>
                <tr>
                    <td align ="right" >
                         <span style="color: #FF0000"></span> Work Number :</td>
                    <td align ="left" >
                        <asp:TextBox ID="txtWorkNo" runat="server"
                            Width="124px"  MaxLength="20" TabIndex="12"></asp:TextBox>
                       </td>
                </tr>
                <tr>
                    <td align ="right" >
                       Home Number :</td>
                    <td align ="left">
                        <asp:TextBox ID="txtHomeNo" runat="server"  
                            Width="124px"  MaxLength="20" TabIndex="13"></asp:TextBox>
                        </td>
                </tr>
                <tr>
                    <td align ="right" >
                         Employer Name :</td>
                    <td align ="left">
                        <asp:TextBox ID="txtEmployName" runat="server" 
                            Width="124px" MaxLength="50" TabIndex="14"></asp:TextBox>
                        </td>
                </tr>
                <tr>
                    <td align ="right" >
                        Reffered By:</td>
                    <td align="left">
                        <asp:TextBox ID="txtRefferdBy" runat="server" 
                            Width="124px" MaxLength="50" TabIndex="15"></asp:TextBox>
                       </td>
                </tr>
                <tr>
            
                      <td align="center" colspan="2" >
                     <recaptcha:RecaptchaControl
                     ID="recaptcha" Height="20" BackColor="Azure" Font-Italic="true"
                     runat="server"
                     PublicKey="6LeP7tgSAAAAAEv3wa40m3isYO75O63uMzc7oSWp"
                     PrivateKey="6LeP7tgSAAAAADsKH9sLMAy04o6XGiTkkdHi0Wn_" TabIndex="16" />
                 
                  </td>
                </tr>
                <tr>
                <td></td>
                <td align="left">
                    <asp:Label Text="" ID="lblRc" runat="server" /></td>
                </tr>
                <tr>
                <td colspan="2" align="center"> 
                        <asp:CheckBox   ID ="ckbTMCN"  Text ="Please Check Terms and Conditions"
                            runat="server" TabIndex="17" style="font-weight: 700"  />  
                          <a href="Terms.html" target="_blank">Click here to view</a>
                    </td>
                </tr>
                <tr><td> <asp:HyperLink ID="HyperLink1" runat="server" CssClass="buttonSubmit" 
                        NavigateUrl="~/index.html" TabIndex="19">Back</asp:HyperLink></td>
                                     <td align="left">                      
                        <asp:Button ID="btnSave" runat="server" onclick="btnSave_Click" 
                            Text="Register" Width="100px" CssClass ="button" TabIndex="18" />  </td>                    
                    
                </tr>
                </table>
  
         

        
    </div> 

                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;
                                                                    
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="sp_re_bottom">
                                            <span></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="15px;">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <!--middle end here -->
            <!--footer start here -->
            <div>
                <!--footer start here -->
                <script language="javascript">

                    var winPop = null;

                    function Open_win_bottom(id, page_rank, total_record, is_login) {
                        if (parseInt(is_login)) {
                            url = 'http://www.viewbestads.com/public/default/account/viewadvdetail/id/' + id + '/page_rank/' + page_rank + '/total_record/' + total_record + '/adv_flag/' + 'N';
                        }
                        else {
                            url = 'http://www.viewbestads.com/public/default/news/viewadvnewsdetail/id/' + id + '/page_rank/' + page_rank + '/total_record/' + total_record + '/news_flag/' + 'N';

                        }


                        if (winPop && !winPop.closed) { return alert("Page in Use!"); }

                        else { winPop = window.open(url, 'add_win', 'menubar=no,fullscreen=yes,resizable=yes,scrollbars=yes,location=0'); }



                    }
                </script>
                <div class="footer clearfix">
                    <div class="container">
                        <div class="footerbox alignleft">
                            <div class="footerbox-title latestnews">
                                Latest Positive News</div>
                            <div class="fzbox">
                                <ul>
                                    <li>
                                        <p>
                                            <a href="http://www.irs.gov/uac/2013-Standard-Mileage-Rates-Up-1-Cent-per-Mile-for-Business,-Medical-and-Moving"
                                                target="_blank">Standard Mileage Rates for 2013</a></p>
                                    </li>
                                    <li>
                                        <p>
                                            <a href="http://www.irs.gov/uac/Newsroom/Tax-Relief-for-Victims-of-Hurricane-Sandy-in-Maryland"
                                                target="_blank">Tax Relief for Victims of Hurricane Sandy in Maryland</a></p>
                                    </li>
                                </ul>
                            </div>
                            <div class="viewall">
                                <a href="news/viewadvnews/news_flag/T-2.html" title="View All" class="alignright">View
                                    All </a>
                            </div>
                        </div>
                        <div class="footerbox alignleft">
                            <div class="footerbox-title ourblog">
                                Our Blog</div>
                            <div class="fzbox">
                                <ul class="nopad">
                                    <li>
                                        <p>
                                            <a href="http://www.irs.gov/Tax-Professionals/PTIN-Requirements-for-Tax-Return-Preparers"
                                                target="_blank">Renew or Register Your PTIN for 2013</a> <span class="clear">&nbsp;</span>
                                            <span class="alignleft dateicon"><a href="blog/index6213.html?p=86" target="_blank">
                                                2012-12-18</a></span> <span class="alignright commenticon"><a href="blog/index6213.html?p=86"
                                                    target="_blank">0</a></span> <span class="clear">&nbsp;</span>
                                        </p>
                                    </li>
                                    <li>
                                        <p>
                                            <a href="http://www.irs.gov/uac/Free-File:-Do-Your-Federal-Taxes-for-Free" target="_blank">
                                                Free File: It's Fast, Easy & Secure</a> <span class="clear">&nbsp;</span> <span class="alignleft dateicon">
                                                    <a href="#" target="_blank">2012-12-15</a></span> <span class="alignright commenticon">
                                                        <a href="#" target="_blank">0</a></span> <span class="clear">&nbsp;</span>
                                        </p>
                                    </li>
                                </ul>
                            </div>
                            <div class="clear">
                            </div>
                            <div class="viewall">
                                <a href="#" title="View All" class="alignright" target="_blank">View All</a></div>
                        </div>
                        <div class="footerbox alignleft nomargin">
                            <div class="footerbox-title followontwitter">
                                Follow us on Twitter!</div>
                            <div class="social-bottom">
                                <a href="#" target="_blank" class="facebook-bot">Facebook</a><a href="#" target="_blank"
                                    class="twitter-bot">Twitter</a>
                            </div>
                            <br>
                            <font style="color: #FFFFFF; font-size: 11px; line-height: 42px;">Get Social (trust
                                us, it's healthy!) </font>
                        </div>
                    </div>
                    <!--<div class="viewall"><a href="http://twitter.com/ViewBestAds" title="View All" class="alignright">View All</a></div>-->
                </div>
                <div class="clear">
                </div>
            </div>
        </div>
        <!--footer end here -->
        <div class="copyright">
            <div class="container">
                <div class="alignleft">
                    Copyright &copy; 2012 Delta Tax Planners. All Rights Reserved.</div>
                <div class="alignright developbox">
                    <span class="alignleft">Designed and Developed by : &nbsp;</span><a href="http://www.venhan.com/"
                        target="_blank" title="Venhan Technologies Pvt Ltd." style="font-weight: bold;
                        color: #FFFFFF;"> Venhan Pvt. Ltd.</a></div>
                <div class="clear">
                </div>
            </div>
        </div>
        <script type="text/javascript">
            var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
            document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
        </script>
        <script type="text/javascript">
            try {
                var pageTracker = _gat._getTracker("UA-10012798-1");
                pageTracker._trackPageview();
            } catch (err) { }</script>
        <!-- vzSense -->
        <!--
<script type="text/javascript" src="http://vizisense.net/pixel.js"></script>
<script type="text/javascript">_vz_accountID="7Q2XBJRRTuk+IeIprwmt2w=="; if(typeof(_vz_sense) == "function") _vz_sense();</script>
<noscript>
  <img src="http://vizisense.net/cgi-bin/trail.fcgi?accountID=7Q2XBJRRTuk+IeIprwmt2w==&amp;isScript=false" style="display: none;" border="0" height="1" width="1" alt="vzSense"/>
</noscript>-->
    </div>
    </form>
</body>
<script src="../ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js" type="text/javascript"></script>
<script src="js/fancybox/jquery.fancybox-1.3.1.js" type="text/javascript"></script>
<script type="text/javascript">

    $(document).ready(function () {

        $("#fancy-forgot-pass").fancybox({
            'width': '50%',
            'height': '50%',
            'autoScale': false,
            'transitionIn': 'none',
            'transitionOut': 'none',
            'titleShow': false,
            'type': 'iframe'
        });

        $(".fancy-contact").fancybox({
            'width': '60%',
            'height': '80%',
            'autoScale': false,
            'transitionIn': 'none',
            'transitionOut': 'none',
            'type': 'iframe'
        });

        $("#fancy-tell-a-friend").fancybox({
            'width': '60%',
            'height': '98%',
            'autoScale': false,
            'transitionIn': 'none',
            'transitionOut': 'none',
            'type': 'iframe'
        });

        $("#fancy-twitter").fancybox({
            'width': '63%',
            'height': '99%',
            'autoScale': true,
            'transitionIn': 'none',
            'transitionOut': 'none',
            'type': 'iframe'
        });

        $("#fancy-invite-friend").fancybox({
            'width': '75%',
            'height': '98%',
            'autoScale': false,
            'transitionIn': 'none',
            'transitionOut': 'none',
            'type': 'iframe'
        });

        $("#fancy-invite-friend-successstory").fancybox({
            'width': '75%',
            'height': '98%',
            'autoScale': false,
            'transitionIn': 'none',
            'transitionOut': 'none',
            'type': 'iframe'
        });
    });	
</script>
</html>


