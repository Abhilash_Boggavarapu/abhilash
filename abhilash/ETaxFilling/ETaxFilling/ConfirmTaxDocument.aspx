﻿<%@ Page Language="C#"   MasterPageFile="~/Client.Master" AutoEventWireup="true" CodeBehind="ConfirmTaxDocument.aspx.cs" Inherits="ETaxFilling.ConfirmTaxDocument" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script src="obj/Scripts/Watermark/WaterMark.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $("[id*=txtRemarks]").WaterMark();
        });
</script>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="center">
            <h5> <strong>Please upload Form 8879 to before confirming your document as this is mandatory for E-filing your Tax Return</strong></h5>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
    <tr>
    <td align="center">
   <h5> Click link for Downloading the<strong> Form 8879 </strong>Document</h5>
    </td>
    </tr>
    <tr>
    <td align="center">
        <asp:Label Text="" ID="lblStus" CssClass="error" runat="server" />
    </td>
    </tr>
    <tr>
    <td align="center">
   <asp:GridView ID="GdVDownloadConfirmDocuments" runat="server" AutoGenerateColumns="False"
     Height="75px" Width="100%" margin="5%" 
            onrowcommand="GdVDownloadConfirmDocuments_RowCommand" >
        <Columns>
<asp:TemplateField HeaderText="S.No.">
<ItemTemplate><%#Container.DataItemIndex+1 %><br />
</ItemTemplate>
</asp:TemplateField>
        
         <asp:BoundField DataField ="TaxSheetID"   Visible="false"/> 
      <asp:BoundField DataField="SheetName" HeaderText="Document Name" />
      <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
      <asp:BoundField DataField="CreatedOn" HeaderText="Uploaded Date & Time" />
         <asp:TemplateField>
         <ItemTemplate>
         <asp:LinkButton ID="BtnDownload" runat ="server" CommandName="Download" CommandArgument='<%# DataBinder.Eval(Container,"DataItem.TaxSheetID") %>' Text ="Download" ></asp:LinkButton>
         </ItemTemplate>
         </asp:TemplateField>
        </Columns>
    </asp:GridView>
            </td>
            </tr>
    </table>
    <br />
    <asp:Panel ID="pnlConfirm" runat="server">
    <table>
        <tr>
           
            <td>
                <asp:CheckBox Text="Click here if you are not eligible for e-filing    " 
                    ID="cb" runat="server" TextAlign="Left" TabIndex="1" />
               
            </td>
        </tr>
    </table>
    <br />
   <table style="width: 565px">
        <tr>
            <td>
                <asp:FileUpload ID="FileUpload" runat="server" TabIndex="2" />
            </td>
            <td>
                <asp:TextBox ID="txtRemarks" runat="server" ToolTip="Remarks" TabIndex="3" 
                    TextMode="MultiLine" Width="202px" />
            </td>
            <td>
                <asp:Button Text="Upload 8879 Form" CssClass="button" runat="server" ID="btnUpload" 
                    onclick="btnUpload_Click" TabIndex="4" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
<asp:Label ID="lblMsg" runat="server" CssClass="error" ></asp:Label> 
    <table>
        <tr>
            <td>
             Click here to confirm your Tax Documents
            </td>
            <td>
            </td>
            <td>
                <asp:Button Text="Confirm Tax Document" CssClass="button" 
                    ID="btnConfirmTaxDocument" runat="server" TabIndex="6" 
                    onclick="btnConfirmTaxDocument_Click" />
            </td>
            <td>                <asp:Button Text="View" ID="btnClnDocView" runat="server" 
                    onclick="btnClnDocView_Click" CssClass="button" 
    TabIndex="5" />
            </td>
        </tr>
        <tr>
            <asp:Label Text="" ID="lblConfirm" runat="server" />
        </tr>
    </table>
    </asp:Panel>
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td align="center">

                <asp:Label Text="" ID="lblGrid" runat="server" CssClass="error" />
     <asp:GridView ID="grdClintDocDisplay" runat="server" AutoGenerateColumns="False"
     Height="75px" Width="100%" margin="5%"  OnRowCommand="grdClintDocDisplay_RowCommand"  >
        <Columns>
<asp:TemplateField HeaderText="SNo">
<ItemTemplate><%#Container.DataItemIndex+1 %><br />
</ItemTemplate>
</asp:TemplateField>
        
         <asp:BoundField DataField ="TaxSheetID"   Visible="false"/> 
      <asp:BoundField DataField="SheetName" HeaderText="Document Name" />
      <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
      <asp:BoundField DataField="CreatedOn" HeaderText="Created On Date& Time" />
         <asp:TemplateField>
         <ItemTemplate>
         <asp:LinkButton ID="BtnDownload" runat ="server" CommandName="Download" CommandArgument='<%# DataBinder.Eval(Container,"DataItem.TaxSheetID") %>' Text ="Download" ></asp:LinkButton>
         </ItemTemplate>
         </asp:TemplateField>
        </Columns>
    </asp:GridView>
            </td>
        </tr>
    </table>

</asp:Content>