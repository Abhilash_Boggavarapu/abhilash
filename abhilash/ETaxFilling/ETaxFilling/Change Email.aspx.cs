﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ETaxBO;
using ETaxDal;
using System.Text.RegularExpressions;
namespace ETaxFilling
{
    public partial class Change_Email : System.Web.UI.Page
    {

        ChangeSettingsDAL objChangeSettingDal = new ChangeSettingsDAL();
        MasterDAL objMasterDal = new MasterDAL();
        long userID;
        protected void Page_Load(object sender, EventArgs e)
        
        {
            if (!Utility.Utility.checkLoginSession())
            {
                Response.Redirect("userlogin.aspx");
            }
            userID = Utility.Utility.getLoginSession().UserID;
            if (!IsPostBack)
            {
                lblmsgNEmail.Text = string.Empty;
                lblMail.Text = "";
                lblmsgNEmail.Text = "";
                txtEmailId.Text = objChangeSettingDal.GetUseremailid(userID);
            }
        }

        protected void BtnSave_Click(object sender, EventArgs e)
        {
            lblmsgNEmail.Text = string.Empty;
                      
            if (string.IsNullOrEmpty(txtNEmailId.Text))
            {
                divCEmailerr.InnerText = Utility.Utility.ReqNEmailId;
            }
            else if (!txtNEmailId.Text.IsValidEmailID())
            {
                divCEmailerr.InnerText = Utility.Utility.ValNEmailId;
                txtNEmailId.Text = string.Empty;
                txtNEmailId.Focus();
            }
            else if (string.IsNullOrEmpty(txtCEmailId.Text))
            {
                divCEmailerr.InnerText = Utility.Utility.ReqCEmailId;
            }
            else if (!txtNEmailId.Text.Equals(txtCEmailId.Text))
            {
                divCEmailerr.InnerText = Utility.Utility.MatchEmailId;
                txtCEmailId.Text = string.Empty;
                txtCEmailId.Focus();
            }
            else
            {
               divCEmailerr.InnerText = string.Empty;
                BO_tblUser objUser = new BO_tblUser();
                objUser.EmailID = txtNEmailId.Text;
                int x = objChangeSettingDal.EditEmailId(userID, objUser);
                if (x == 1)
                {
                    lblMail.Visible = true;
                    lblMail.Text = "Your EmailID updated successfully";
                    lblMail.ForeColor = System.Drawing.Color.Green;
                    txtCEmailId.Text = txtEmailId.Text = txtNEmailId.Text = string.Empty;
                }
                else
                {
                    lblMail.Text = " Your EmailID Is Not Updated";
                    lblMail.ForeColor = System.Drawing.Color.Red;
                }

            }
        }
        protected void TxtNEmailId_TextChanged(object sender, EventArgs e)
        {
            lblMail.Text = "";
            string Emailid = objMasterDal.checkEmailId(txtNEmailId.Text);
            if ( Emailid != null)
            {
                lblmsgNEmail.Text = "Emailid is Already Exist";
                lblmsgNEmail.ForeColor = System.Drawing.Color.Red;
                txtNEmailId.Text = string.Empty;
            }
            }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("ChangeSettings.aspx");
        }
        }
  
    }

