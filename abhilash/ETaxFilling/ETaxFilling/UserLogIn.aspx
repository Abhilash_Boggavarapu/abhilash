﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserLogIn.aspx.cs" Inherits="ETaxFilling.UserLogIn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

 <%--<a href="resetpass.aspx" tabindex="4">Forgot Password</a>
                        <a href="ClientRegistration.aspx" tabindex="5">New Registration</a>--%>
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>DeltaTax : Login</title>
        <meta name="description" content="">
        <meta name="author" content="Simon Stamm & Markus Siemens">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="stylesheet" href='http://envato.stammtec.de/themeforest/peach/css/1105b53.css'>
        <script src="http://envato.stammtec.de/themeforest/peach/js/libs/modernizr-2.0.6.min.js"></script>
    </head>
    <body class="special_page">
        <div class="top">
            <div class="gradient">
            </div>
            <div class="white">
            </div>
            <div class="shadow">
            </div>
        </div>
        <div class="content">
            <h1> <img src="assets/images/logo-w.png" /> </h1>
            <div class="background">
            </div>
            <div class="wrapper">
                <div class="box">
                    <div class="header grey">
                        <img src="http://envato.stammtec.de/themeforest/peach/img/icons/packs/fugue/16x16/lock.png"
                            width="16" height="16">
                        <h3>
                            Login</h3>
                    </div>
                    <form method="get" action="#" runat ="server" >
                    <div class="content no-padding">
                        <div class="section _100">
                            <label>
                                Username
                            </label>
                            <div class="required">
                             <asp:TextBox ID="txtUserName" CssClass="ValidationRequired" runat="server" 
                            Width="250px" Height="24px" MaxLength="50" 
                            TabIndex="1"></asp:TextBox>
                                
                            </div>
                        </div>
                        <div class="section _100">
                            <label>
                                Password
                            </label>
                            <div class="required">
                            <asp:TextBox ID="txtPswd" runat="server" CssClass="ValidationRequired"
                            TextMode="Password" Width="250px" Height="23px" MaxLength="15" 
                             TabIndex="2" ></asp:TextBox>
                               
                            </div>
                            
                        </div>
                    </div>
              
                     <div class="actions">
                        <div class="actions-left" style="margin-top: 8px; font-size:12px;">
                        
                            <asp:HyperLink ID="hypFpass" runat="server" NavigateUrl="~/ResetPassword.aspx"> Forgot Password?</asp:HyperLink>&nbsp;&nbsp;&nbsp;<a href="ClientRegistration.aspx" class="cmssubtitle">Signup</a>
                         
                        </div>
                        <div class="actions-right">
                        <asp:Button ID="btnSave" runat="server" Text="Login" onclick="btnSave_Click" 
                            Width="76px" TabIndex="3" />
                           
                        </div>
                  
                    </div>
                  
                    </form>
					
                </div>
                <div class="shadow">
                </div>
				<div align="center"><h3><a href="index.html">Go to Website</a></h3>
				</div>
            </div>	
			
        </div>
        <script src="../../../ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
        <script>            window.jQuery || document.write('<script src="http://envato.stammtec.de/themeforest/peach/js/libs/jquery-1.7.1.min.js"><\/script>');</script>
        <script src="../../../ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>
        <script>            window.jQuery.ui || document.write('<script src="http://envato.stammtec.de/themeforest/peach/js/libs/jquery-ui-1.8.16.min.js"><\/script>');</script>
        <script defer src='http://envato.stammtec.de/themeforest/peach/js/23acda8.js'></script>
        <script>            $(window).load(function () { var a = $("form").validate({ invalidHandler: function (d, b) { var e = b.numberOfInvalids(); if (e) { var c = e == 1 ? "You missed 1 field. It has been highlighted." : "You missed " + e + " fields. They have been highlighted."; $(".box .content").removeAlertBoxes(); $(".box .content").alertBox(c, { type: "warning", icon: true, noMargin: false }); $(".box .content .alert").css({ width: "", margin: "0", borderLeft: "none", borderRight: "none", borderRadius: 0 }) } else { $(".box .content").removeAlertBoxes() } }, showErrors: function (c, d) { this.defaultShowErrors(); var b = this; $.each(d, function () { var f = $(this.element); var e = f.parent().find("label.error").hide(); e.addClass("red"); e.css("width", ""); f.trigger("labeled"); e.fadeIn() }) }, submitHandler: function (b) { window.location.replace("#") } }) });</script>
        <!--[if lt IE 7 ]><script defer src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script> <script defer>window.attachEvent("onload",function(){CFInstall.check({mode:"overlay"})});</script><![endif]-->
    </body>

    <meta http-equiv="content-type" content="text/html;charset=utf-8">
</html>
