﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ETaxDal;
using ETaxBO;

namespace ETaxFilling
{
    public partial class ViewTaxSummary : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AdminMasterDAL objAdminMasterDal = new AdminMasterDAL();
            List<BO_TaxSummary> objTaxSummary = (objAdminMasterDal.getTaxSummary(Utility.Utility.getLoginSession().TaxPayerID));
            //ViewState["objTaxSummary"] = objTaxSummary;
            if (objTaxSummary.Count == 0)
            {
                btnPayment.Enabled = false;
            }
            if (!Utility.Utility.checkLoginSession())
            {
                Response.Redirect("userlogin.aspx");
            }
            else
            {
                if (!IsPostBack)
                {
                    if (objTaxSummary != null)
                    {
                        repeter.DataSource = objTaxSummary;
                        repeter.DataBind();
                    }
                    else
                    {
                       
                    }
                }
            }
        }

        protected void btnPayment_Click(object sender, EventArgs e)
        {   
                Response.Redirect("MakePayment.aspx");
        }
    }
}