﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intmail_DAL;
using ETaxDal;
using ETaxBO;
using System.Data;

namespace ETaxFilling
{
    public partial class Inbox : System.Web.UI.Page
    {
        protected void page_preinit(object sender, EventArgs e)
        {

            if (Utility.Utility.CheckAdminLoginSession())
                MasterPageFile = IM_Utility.AdminMasterpagepath;
            else
                MasterPageFile = IM_Utility.UserMasterpagepath;

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack == false)
            {
                Bindinbox();
            }

        }
        private void Bindinbox()
        {
            IntMail Im_mail = new IntMail();
            string Memberid;
            if (Utility.Utility.CheckAdminLoginSession())

                Memberid = Utility.Utility.GetAdminSession().UserID.ToString();
            else
                Memberid = Utility.Utility.getLoginSession().UserID.ToString();

            gvInbox.DataSource = Im_mail.Mail_Inbox(Memberid);
            gvInbox.DataBind();
        }

        protected void gvInbox_RowCommand(object sender,
GridViewCommandEventArgs e)
        {
            if (e.CommandName == "view")
            {
                string ID = e.CommandArgument.ToString();

                string type = "0";

                string status = new IntMail().Mail_Inbox_MessageRead(ID).ToString();
                if (status == "1")
                {
                    Response.Redirect("Description.aspx?id=" + ID +"&type=" + type + "");
                }
                else
                {
                    lblMessage.Visible = true;
                    lblMessage.Text = status;
                }

            }
        }

        protected void gvInbox_PageIndexChanging(object sender,GridViewPageEventArgs e)
        {
            gvInbox.PageIndex = e.NewPageIndex;
            Bindinbox();
        }

}
}