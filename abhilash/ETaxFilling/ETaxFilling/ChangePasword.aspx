﻿<%@ Page Language="C#" MasterPageFile="~/Client.master" AutoEventWireup="true" CodeBehind="ChangePasword.aspx.cs"
    Inherits="ETaxFilling.ChangePasword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <table align="center" cellpadding="0" cellspacing="8" border="0" width="100%" class="box">
            <tr class="box">
                <td colspan="2">
                    <h2 class="divider top20 bottom10">
                        Change Password</h2>
                    <div id="divCpasserr" style="color: #FF0000" runat="server">
                        &nbsp;</div>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <span style="color:red;">*</span> Password :
                </td>
                <td>
                    <asp:TextBox ID="TxtPwd" CssClass="ValidationRequired password" runat="server" Width="150px"
                        TextMode="Password" TabIndex="1"></asp:TextBox>
                    &nbsp;<asp:Label ID="lblmsg" runat="server" CssClass="error"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <span style="color:red;">*</span> New Password :
                </td>
                <td>
                    <asp:TextBox ID="TxtNPwd" CssClass="ValidationRequired password" runat="server" Width="150px"
                        TextMode="Password" TabIndex="2"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <span style="color:red;">*</span> Confirm Password :
                </td>
                <td>
                    <asp:TextBox ID="TxtCPwd" CssClass="ValidationRequired password" runat="server" Width="150px"
                        TextMode="Password" TabIndex="3"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td style="font-size: 12px;">
                    Note: Password length must be 6 Charecters (should contain one alphabets )
                </td>
            </tr>
            <tr>
                <td>
                    <asp:HyperLink ID="HyperLink1" runat="server" CssClass="button" NavigateUrl="~/ChangeSettings.aspx"
                        TabIndex="5">Back</asp:HyperLink>
                </td>
                <td>
                    <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="Save" CssClass="button"
                        Height="25px" Width="81px" TabIndex="4" />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Label Text="" CssClass="error" ID="lblStatus" runat="server" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
