﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intmail_DAL;
using ETaxDal;
using ETaxBO;
using System.Data;
namespace ETaxFilling
{
    public partial class Description : System.Web.UI.Page
    {
        protected void page_preinit(object sender, EventArgs e)
        {

            if (Utility.Utility.CheckAdminLoginSession())
                MasterPageFile = IM_Utility.AdminMasterpagepath;
            else
                MasterPageFile = IM_Utility.UserMasterpagepath;

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string id = Request.QueryString["id"];
            string type = Request.QueryString["type"].ToString();

            string Memberid;
            if (Utility.Utility.CheckAdminLoginSession())

                Memberid = Utility.Utility.GetAdminSession().UserID.ToString();
            else
                Memberid = Utility.Utility.getLoginSession().UserID.ToString();

            DataTable dt = new IntMail().Mail_Description(Memberid, id, type);

            if (dt.Rows.Count != 0)
            { 
                if (type =="0")
                {
                lblFrom.Text = dt.Rows[0]["ToEmailid"].ToString();
                lblForTtitle.Text = "From Emailid";
            }
            else {
                lblFrom.Text = dt.Rows[0]["FromEmailid"].ToString();
                lblForTtitle.Text = " To Emailid";
            }
                //' lblTo.Text = Mail.Rows(0)("To_id").ToString()
            lblSubject.Text =dt.Rows[0]["Subject"].ToString();
            lblDate.Text =dt.Rows[0]["Date"].ToString();
            txtbMessage.Text = dt.Rows[0]["Message"].ToString();
            }

        }
    }
}