﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ETaxBO;
using ETaxDal;
namespace ETaxFilling
{
    public partial class EditContactDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Utility.Utility.checkLoginSession())
            {
                Response.Redirect("userlogin.aspx");
            }
        }
        protected void BtnSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtMobNo.Text))
            {
                divCDerror.InnerText = Utility.Utility.ReqMobileNo;
            }
            else if (!txtMobNo.Text.isNumber())
            {
                divCDerror.InnerText = Utility.Utility.ValMobileNo;
                txtMobNo.Text = string.Empty;
                txtMobNo.Focus();
            }
            else if (string.IsNullOrEmpty(txtHNo.Text))
             {
                 divCDerror.InnerText = Utility.Utility.ReqHomeNo;
             }
            else if (!txtHNo.Text.isNumber())
             {
                 divCDerror.InnerText = Utility.Utility.ValHomeNo;
                 txtHNo.Text = string.Empty;
                 txtHNo.Focus();
             }
            else
             {
                 ChangeSettingsDAL objmobile = new ChangeSettingsDAL();
                    divCDerror.InnerText = string.Empty;
                    long UserId = Utility.Utility.getLoginSession().UserID;
                    BO_tblUser obj = new BO_tblUser();
                    obj.MobileNumber = txtMobNo.Text;
                    obj.HomeNumber = txtHNo.Text;
                    objmobile.UpdateMobNo(UserId, obj);
                    lblConform.Text = "Your Contact Details are updated successfully";
                    lblConform.ForeColor = System.Drawing.Color.Green;
                    txtHNo.Text = txtMobNo.Text = string.Empty;
                }
            }
        }
   }
 

 
