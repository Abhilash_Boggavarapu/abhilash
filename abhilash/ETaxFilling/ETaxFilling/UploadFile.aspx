﻿<%@ Page Language="C#" MasterPageFile="~/Client.master" AutoEventWireup="true" CodeBehind="UploadFile.aspx.cs" Inherits="ETaxFilling.UploadFile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

  <table cellpadding="0" cellspacing="0" width="100%" class="box" >
                <tr>
                <td style="text-align: right">
                           Document Name
                           :</td>
                <td style="width: 144px">
                    <asp:DropDownList ID="ddlForms" runat="server" Height="25px" Width="162px">
                      
                    </asp:DropDownList>
                </td>
                </tr>
                    <tr>
                       
                        <td align="center">
                           <strong>Browse</strong> 
                        </td>
                        <td align="center">
                            <strong>Remarks</strong> 
                        </td>
                        <td align="center">
                            <strong>Upload</strong> 
                        </td>
                    </tr>

<tr>
<td>
  <asp:FileUpload ID="FileUpload1" runat="server" AllowMultiple="true" TabIndex="1" 
        Width="226px" />
   </td>
 <td style="width: 144px">     
     <asp:TextBox runat="server" ID="txtForm" TextMode="MultiLine" MaxLength="1000"
                    TabIndex="2" Width="190px" />
      </td>
   <td align="center" style="width: 35px;">
        <asp:Button Text="Upload" ID="btnUpload" runat="server" 
            OnClick="btnUpload_Click" CssClass="button" Width="85px"  />
                        </td>
                      </tr>
                      <tr>
                      <td>
                      &nbsp;</td>
                        <td align="center" style="width: 21px;">
                            <asp:Button Text="View Uploaded Documents" ID="btnView" runat="server" 
                                OnClick="btnView_Click" Height="32px" CssClass="button" Width="184px"  />
                        </td>
                        &nbsp;<td>
                        </td>
                      </tr>
                        

    </table>
     <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
         <td>
            <h4><asp:Label ID="Label1" runat="server"></asp:Label></h4> 
              </td>
            
              </tr>
            <tr>
            <td>
                <h4>
                    <asp:Label ID="lblMsg" runat="server"></asp:Label></h4>
            </td>
            </tr>
      
    </table>
    <asp:GridView ID="GridViewUpload" runat="server" AutoGenerateColumns="False" align="center"
        Height="75px" Width="100%" OnRowCommand="GridViewUpload_RowCommand" AllowPaging="True" >
                   
        <Columns>
            <asp:TemplateField HeaderText="S.No.">
                <ItemTemplate>
                    <%#Container.DataItemIndex+1 %><br />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
            <asp:BoundField DataField="FileName" HeaderText="File Name" />
            <asp:TemplateField HeaderText="Download">
                <ItemTemplate>
                    <asp:LinkButton ID="BtnDownload" runat="server" CommandName="Download" CommandArgument='<%# Eval("TaxPayerDocumentID") %>'
                        Text="Download"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
       <%-- <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />--%>
    </asp:GridView>
    <tr><td>  <asp:Button Text="Next" ID="btnNext" runat="server" 
            OnClick="btnNext_Click" CssClass="button" Width="85px" /></td></tr>
   
</asp:Content>
