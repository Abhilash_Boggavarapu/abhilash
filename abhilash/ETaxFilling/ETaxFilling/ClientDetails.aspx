﻿<%@ Page Language="C#" MasterPageFile="~/Client.Master" AutoEventWireup="true" CodeBehind="ClientDetails.aspx.cs"
    Inherits="ETaxFilling.ClientDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        $(function () {
            if ($($("input[type='radio'][value='rbYes']")).attr("checked") == "checked") {
                $("#txtProjectStartDate,#txtProjectEndDate").datepicker({
                    showOn: "button",
                    buttonImage: "B_Images/calendar_icon.png",
                     changeMonth: true,
                     changeYear: true,
                    buttonImageOnly: true
                });
            }
            else {
                $("#txtProjectStartDate,#txtProjectEndDate").datepicker({
                    showOn: "button",
                    buttonImage: "B_Images/calendar_of_events.gif",
                    changeMonth: true,
                    changeYear: true,
                    buttonImageOnly: true
                });
                DisableDatePicker();
            }

        });

        function DisableDatePicker() {
            $("#txtProjectStartDate,#txtProjectEndDate").datepicker("disable");

        }
    </script>
    <asp:ScriptManager runat="server" />
    <div align="center">
        <table>
            <tr>
                <td>
                    <div>
                        <ul class="submenu">
                            <li><a href="EmployerDetails.aspx">Employer Details</a></li>
                            <li><a style="color: ButtonHighlight" href="ClientDetails.aspx">Client Details</a></li>
                            <li><a href="ResidencyDetails.aspx">Residency Details</a></li>
                        </ul>
                    </div>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td>
                    <asp:LinkButton runat="server" ID="lnkTaxPayer" Text="Tax Payer" CssClass="button"
                        OnClick="lnkTaxPayer_Click" Height="25px"></asp:LinkButton>
                </td>
                <td>
                    <asp:LinkButton runat="server" ID="lnkSpouse" Text="Spouse" CssClass="button" OnClick="lnkSpouse_Click" Height="30px"></asp:LinkButton>
                </td>
            </tr>
        </table>
        <table width="100%" class="box" cellpadding="0" cellspacing="0" border="0">
            <tr class="box">
                <td>
                    <h2 class="divider top20 bottom10">
                        <asp:Label ID="lblClientDetails" runat="server" Text=" TaxPayer Client Details" />
                    </h2>
                </td>
            </tr>
        </table>
        <asp:Panel ID="panelRb" runat="server">
            <h4 align="center">
                Have You Worked At Client Location? <asp:RadioButton ID="rbYes" runat="server" AutoPostBack="True"
                    GroupName="Client" OnCheckedChanged="rbYes_CheckedChanged" Text="Yes" TabIndex="1" />
                &nbsp;<asp:RadioButton ID="rbNo" runat="server" AutoPostBack="True" GroupName="Client"
                    Text="No" Checked="True" OnCheckedChanged="rbYes_CheckedChanged" TabIndex="2" /></h4>
            <div runat="server" id="divCDErr" style="color: #FF0000">
            </div>
        </asp:Panel>
        <div>
            <h2>
                <asp:Label Text="" ID="lblMsg" runat="server" CssClass="error" /></h2>
        </div>
        <asp:Panel ID="PanelClient" runat="server">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="box">
                <tr>
                    <td style="text-align: right">
                         <span style="color: #FF0000">*</span>Select Employer :
                    </td>
                    <td >
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <asp:DropDownList runat="server" ID="ddlEmployeer" Height="20px" Width="150px" TabIndex="3">
                                    <asp:ListItem></asp:ListItem>
                                </asp:DropDownList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td>
                        <asp:Label Text="" ID="lblSEmp" CssClass="error" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right">
                        <span style="color: #FF0000">* </span>Name :
                    </td>
                    <td style="text-align: left;" colspan="2">
                        <asp:TextBox ID="txtClientName" runat="server" CssClass="ValidationRequired" MaxLength="50"
                            TabIndex="4" Height="31px" Width="150px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right">
                        <span style="color: #FF0000">* </span>Project Start Date :
                    </td>
                    <td style="text-align: left;" colspan="2">
                        <asp:TextBox ID="txtProjectStartDate" ClientIDMode="Static" runat="server" CssClass="ValidationRequired date"
                            TabIndex="5" Height="30px" Width="150px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right">
                        <span style="color: #FF0000">* </span>Project End Date :
                    </td>
                    <td style="text-align: left;" colspan="2">
                        <asp:TextBox ID="txtProjectEndDate" ClientIDMode="Static" runat="server" CssClass="ValidationRequired date "
                            TabIndex="6" Height="30px" Width="150px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right">
                        <span style="color: #FF0000">* </span>Country :
                    </td>
                    <td >
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="ddlClientCountry" runat="server" CssClass="ValidationRequired" Height="20px" Width="150px"
                                     OnSelectedIndexChanged="ddlClientCountry_SelectedIndexChanged"
                                    AutoPostBack="True" TabIndex="7">
                                </asp:DropDownList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right;">
                        <span style="color: #FF0000">* </span>State :
                    </td>
                    <td >
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="ddlClientState" runat="server" Height="20px" Width="150px"
                                    CssClass="ValidationRequired" OnSelectedIndexChanged="ddlClientState_SelectedIndexChanged"
                                    AutoPostBack="True" TabIndex="8">
                                </asp:DropDownList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right">
                        <span style="color: #FF0000">* </span>City :
                    </td>
                    <td >
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="ddlClientCity" runat="server" Height="20px" Width="150px" CssClass="formButton"
                                    TabIndex="9" AutoPostBack="True" 
                                    onselectedindexchanged="ddlClientCity_SelectedIndexChanged" >
                                </asp:DropDownList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                 <tr id="trcity" runat="server">
                <td></td>
                <td align="left">
                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>
              <asp:TextBox ID="txtCity" Height="20px"   Width="150px" runat="server" />
               </ContentTemplate></asp:UpdatePanel>
                   
                </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td colspan="2">
                        <asp:Button ID="btnSave" runat="server" CssClass="button" OnClick="btnSave_Click"
                            Text="Save" Height="30px" Width="90px" TabIndex="10" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td align="center">
                <asp:HyperLink ID="HyperLink2" runat="server" CssClass="button" Height="25px" Width="50px"
                          TabIndex="9" NavigateUrl="~/EmployerDetails.aspx">Back</asp:HyperLink>
                </td>
<td align="right">
                    <asp:HyperLink ID="HyperLink1" runat="server" CssClass="button" Height="25px" Width="50px"
                         NavigateUrl="~/ResidencyDetails.aspx" TabIndex="8">Next</asp:HyperLink>
                </td>
                <td>
                </td>
               
            </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="box">
            <tr id="gridDisplay" runat="server">
                <td>
                    <asp:GridView ID="GrdClientlist" runat="server" AlternatingRowStyle-CssClass="alt"
                        margin="5%" AutoGenerateColumns="False" PagerStyle-CssClass="pgr" Width="100%"
                        OnRowCommand="GrdClientlist_RowCommand" HorizontalAlign="Center" AllowPaging="True"
                        OnPageIndexChanging="GrdClientlist_PageIndexChanging" CssClass="mGrid">
                        <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
                        <Columns>
                            <asp:TemplateField HeaderText="SNo">
                                <ItemTemplate>
                                    <%#Container.DataItemIndex+1 %><br />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="TaxPayerClientID" Visible="false" />
                            <asp:BoundField DataField="ClientName" HeaderText="Client Name" SortExpression="EID" />
                            <asp:BoundField DataField="ProjectStartDate" HeaderText="Project Start Date" DataFormatString="{0:d}"
                                HtmlEncode="False" />
                            <asp:BoundField DataField="ProjectEndDate" HeaderText="Project End Date" DataFormatString="{0:d}"
                                HtmlEncode="False" />
                            <asp:BoundField DataField="CountryName" HeaderText="Country" />
                            <asp:BoundField DataField="StateName" HeaderText="State" />
                            <asp:BoundField DataField="CityName" HeaderText="City" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkbtnEdit" runat="server" Text="Edit" CommandArgument='<%# DataBinder.Eval(Container,"DataItem.TaxPayerClientID") %>'
                                        CommandName="ClientEdit"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--  <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="lnkbtnRemove" runat="server" Text="Remove" CommandArgument='<%# DataBinder.Eval(Container,"DataItem.TaxPayerClientID") %>'
                        CommandName="Remove" ></asp:LinkButton>                       
                </ItemTemplate>
            </asp:TemplateField>--%>
                        </Columns>
                        <PagerSettings FirstPageText="First" LastPageText="Last" />
                        <PagerStyle CssClass="pgr"></PagerStyle>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
