﻿<%@ Page Language="C#"  MasterPageFile="~/Client.Master" AutoEventWireup="true" CodeBehind="ViewTaxSummary.aspx.cs" Inherits="ETaxFilling.ViewTaxSummary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <link href="Scripts/Accordian/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="Scripts/Accordian/jquery-1.4.4.js" type="text/javascript"></script>
    <script src="Scripts/Accordian/jquery-ui.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        $(function () {
            $("#products").accordion();
        });
    </script>
   <table width="100%" border="0" cellpadding="0" cellspacing="0">
   <tr><td><form action="" method="post">
    <div>
    <h2 class="divider top20 bottom10"
>Tax Summary    </h2>
    </div>
    <div id="products">
        <asp:Repeater ID="repeter" runat="server">


            <ItemTemplate>
                <h4><a href="#">Tax Summary  Created On Date: <%# DataBinder.Eval(Container.DataItem, "CreatedOn")%></a></h4>
                <div>
                    <p>
                    
                     <br />
                    <%# DataBinder.Eval(Container.DataItem, "TaxSummary")%>
                    </p>
                </div>
                </ItemTemplate>
        </asp:Repeater>
       
    </div>
    </form></td></tr>
        <tr>
            <td align="center">
            <asp:Button ID="btnPayment" Text="Proceed To Payment" CssClass="button" runat="server" 
            onclick="btnPayment_Click" />
            </td>
        </tr>
    </table>
</asp:Content>

