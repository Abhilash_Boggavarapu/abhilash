﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ETaxFilling.Utility
{
    public static class Clear
    {
        public static  void ClearDependents(Control Parent)
        {
            foreach (Control c in Parent.Controls)
            {
                if (c.Controls.Count > 0)
                {
                    ClearDependents(c);
                }
                else
                {
                    switch (c.GetType().ToString())
                    {
                        case "System.Web.UI.WebControls.TextBox":
                            ((TextBox)c).Text = string.Empty;
                            break;
                        case "System.Web.UI.WebControls.CheckBox":
                            ((CheckBox)c).Checked = false;
                            break;
                        case "System.Web.UI.WebControls.RadioButton":
                            ((RadioButton)c).Checked = false;
                            break;
                        case "System.Web.UI.WebControls.DropDownList":
                            {
                                DropDownList srcctrl = (DropDownList)c;
                                if (srcctrl.Items.Count>0)
                                ((DropDownList)c).SelectedIndex = 0;
                            }
                            break;

                    }
                }
            }
        }
    }
}