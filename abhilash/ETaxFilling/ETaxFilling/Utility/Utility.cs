﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ETaxDal;
using ETaxBO;

namespace ETaxFilling.Utility
{
    public static class Utility
    {
        public static string setSelectCountry ="Select";
        public static string setSelectState = "Select";
        public static string setSelectCity = "Select";
        public static string setOtherCity = "other";
        public static string setSelectGender = "--Select--";
        public static string setSelectVisastatus = "--Select--";
        public static string setSelectMaritalstatus = "--Select--";
        public static string setSelectRelationship = "--Select--";
        public static string setSelectEmployer = "Select";
        public static string ReqBDetails = "Please enter your account number";
        public static string ValBDetails = "Account Number must be in numerics";
        public static string ReqRouting = "Please enter routing number";
        public static string ValRouting = "Routing number must be in number";
        public static string ReqBName = "Please enter Bank Name";
        public static string ValBName = "Bank Name must be in characters";
        public static string ReqAName = "Please enter your Account Name";
        public static string ValAName = "Account Name must be in characters";
        public static string ReqAType = "Please select Account type";
        public static string ReqNEmailId = "Please enter your e-mail address";
        public static string ValNEmailId = "Please enter valid e-mail address";
        public static string ReqCEmailId = "Please enter confirm e-mail address";
        public static string MatchEmailId = "Your EmailIds does not match";
        public static string ReqPwd = "Please enter Password";
        public static string ValPwd = "Password must contain atleast 6 characters long and contain atleast one character and one number";
        public static string ReqNPwd = "Please enter new password";
        public static string ValNPwd = "New Password must contain atleast 6 characters long and contain atleast one character and one number";
        public static string ReqCPwd = " Passwords should not be empty";
        public static string MatchPwd = "Your Passwords does not Match";
        public static string ReqEmp = "Please Select Employer";                
        public static string ReqClientName = "Please enter Client Name";
        public static string ValClientName = "Client Name must be in characters";
        public static string ReqSDate = "Please enter Start Date";
        public static string ValSDate = "Please enter Start date in MM/DD/YYYY format";
        public static string ReqEDate = "Please enter End Date";
        public static string ValEDate = "Please enter End date in MM/DD/YYYY format";
        public static string LessSEDate = "Start Date Should be less than End Date";
        public static string ReqCountry ="Please Select Country";
        public static string ReqState = "Please Select State";
        public static string ReqCity = "Please Select City";
        public static string ReqFName = "Please enter your first name";
        public static string ValFName = "Firstname must be in characters";
        public static string ReqMName = "Please enter your middle name";
        public static string ValMName = "Middlename must be in characters";
        public static string ReqLName = "Please enter your last name";
        public static string ValLName="Lastname must be in characters";
        public static string ReqGender = "Please Select Gender";
        public static string ReqZip = "Please enter Zipcode";
        public static string ValZip = "Please Enter Valid ZipCode";
        public static string ReqMobileNo = "Please enter your Mobile Number";
        public static string ValMobileNo = "Please Enter Valid Mobile Number";
        public static string ReqWorkNo = "Please enter your Work Number";
        public static string ValWorkNo = "Please enter Valid Work Number";
        public static string ReqHomeNo = "Please enter your Home Number";
        public static string ValHomeNo = "Please enter Valid Home Number";
        public static string ReqLandNo = "Please enter LandNumber";
        public static string ValLandNo = "Please enter Valid Land Number";
        public static string ReqEmpName = "Please enter employer name";
        public static string ValEmpName = "Employee name must be in Characters";
        public static string ReqRefbyName = "Please enter Referrer name";
        public static string ValRefbyName = "Referrer name must be in Characters";
        public static string ReqReferralName = "Please enter your Referalname";
        public static string ValReferralName = "Referral name must be in Characters";
        public static string ReqDOB = "Please enter DOB";
        public static string ValDOB = "Please enter DOB in MM/DD/YYYY format";
        public static string ReqRelationship = "Please Select Relationship";
        public static string ReqSSN="Please enter SSN number";
        public static string ValSSN = "Please Enter Valid SSN Number";
        public static string ReqVisaStatus = "Please Select VisaStatus";
        public static string ReqDOE = "Please enter date of entry";
        public static string ValDOE = "Please enter DOE in MM/DD/YYYY format";
        public static string ReqOccupation = "Please enter Occupation details";
        public static string ValOccupation = "Occupation must be in characters";
        public static string ReqStreetNo = "Please enter Street Number";
        public static string ValStreetNo = "Please enter a valid StreetNumber";
        public static string ReqAptNo = "Please enter Appartment Number";
        public static string ValAptNo = "Please enter a valid Appartment Number";
        public static string ReqMaritalStatus = "Plese select Marital status";
        public static string ReqDoc = "Please enter Form Name";
        public static string ReqExpence = "Please enter your ExpenceType";
        public static string ValExpence = "ExpenceType must be in Characters";
        public static string ReqMaritalType = "Please enter your MaritalType";
        public static string ValMaritalType = "MaritalType must be in  Characters";
        public static string ReqReferralTypes = "Please enter your ReferralType";
        public static string ValReferralTypes = "ReferralType must be in Characters";
        public static string ReqRelationshipType = "Please enter your RelationshipType";
        public static string ValRelationshipType = "RelationshipType must be in Characters";
        public static string ValVisaStatus = "Enter Valid Visa Status";
        public static string ReqStateName = "Please enter State Name";
        public static string ValStateName = "State Name must be in Characters";
        public static string ReqCityName="Please enter your City Name";
        public static string ValCityName = "City Name must be in Characters";
        public static string ReqDurationOC = "Please enter your Durationofconsent";
        public static string ValDurationOC = "Please enter valid Numbers";
        public static string ReqSign = "Please enter TaxPayer signature";
        public static string ValSign = "Signature must be in Characters";
        public static string ValFilenumber = "Please enter valid Numbers";
        public static string Requsername = "Please enter recipient email address";
        public static string Valusername = "Please enter valid email address";
        public static string ReqSubject = "Please enter subject";
        public static string ReqMessage = "Please enter Message";
        public static string ReqAnyField = "Please enter atleast one field";
        public static string reqEnterCity = "Please Enter City";
        public static bool checkLoginSession()
        {
            if (HttpContext.Current.Session["UserObj"] == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static BO_tblTaxPayerDetail getLoginSession()
        {
            if (HttpContext.Current.Session["UserObj"] == null)
            {
               
                throw new Exception("Logged in user does not exist or session expires.Please login again.");
            }
            else
            {
                return ((BO_tblTaxPayerDetail)HttpContext.Current.Session["UserObj"]);
            }

        }

        public static void setLoginSession(BO_tblTaxPayerDetail objTaxPayerDetail)
        {
            if (objTaxPayerDetail == null)
            {
                throw new Exception("User does not exist or session expires.Please login again.");
            }
            else
            {
                HttpContext.Current.Session["UserObj"] = objTaxPayerDetail;
            }
        }
        public static void setCurrentTaxPayer(MasterItems.TaxPayerType taxPayer)
        {
            if (HttpContext.Current.Session["UserObj"] == null)
            {
                throw new Exception("User does not exist or session expires.Please login again.");
            }
            else
            {
                BO_tblTaxPayerDetail objTaxPayer = (BO_tblTaxPayerDetail)HttpContext.Current.Session["UserObj"];
                objTaxPayer.CurrentTaxPayer = taxPayer;
                HttpContext.Current.Session["UserObj"] = objTaxPayer;
            }
        }
        public static void setMaritalStatus(MasterItems.MaritalStatuses maritalStatus)
        {
            if (HttpContext.Current.Session["UserObj"] == null)
            {
                throw new Exception("User does not exist or session expires.Please login again.");
            }
            else
            {
                BO_tblTaxPayerDetail objTaxPayer = (BO_tblTaxPayerDetail)HttpContext.Current.Session["UserObj"];
                objTaxPayer.MaritalStatus  = maritalStatus.ToString ();
                HttpContext.Current.Session["UserObj"] = objTaxPayer;
            }
        }

        //public static void setViewFrom7216(string value)
        //{
        //    if (HttpContext.Current.Session["UserObj"] == null)
        //    {
        //        throw new Exception("User does not exist or session expires.Please login again.");
        //    }
        //    else
        //    {
        //        BO_tblTaxPayerDetail objTaxPayer = (BO_tblTaxPayerDetail)HttpContext.Current.Session["UserObj"];
        //        objTaxPayer.Veiw7216 = value;
        //        HttpContext.Current.Session["UserObj"] = objTaxPayer;
        //    }
        //}       
        public static bool CheckAdminLoginSession()
        {
            if (HttpContext.Current.Session["AdminObj"] == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public static BO_tblUser GetAdminSession()
        {
            if (HttpContext.Current.Session["AdminObj"] == null)
            {
                throw new Exception("Logged in user does not exist or session expires.Please login again.");
            }
            else
            {
                return ((BO_tblUser)HttpContext.Current.Session["AdminObj"]);
            }
        }
        public static void SetAdminSession(BO_tblUser objtblUser)
        {
            if (objtblUser == null)
            {
                throw new Exception("User does not exist or session expires.Please login again.");
            }
            else
            {
                HttpContext.Current.Session["AdminObj"] = objtblUser;
            }
        }

        public static void setLoginSessionToEmpty(BO_tblTaxPayerDetail objTaxPayerDetail)
        {
            if (objTaxPayerDetail == null)
            {
                HttpContext.Current.Session["UserObj"] = objTaxPayerDetail;
            }
            
        }
    }
}