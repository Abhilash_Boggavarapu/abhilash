﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Configuration;
using System.IO;

namespace ETaxFilling.Utility
{
    
         public class MailMessage
        {
            private string _cc = null;
            private string _bcc = null;
            private string _mailServer = null;
            private string _from = null;
            private string _to = null;
            private string _body = null;
            private string _isbodyhtml = null;
            private string _subject = null;
            private string _mailFormat = null;
            private string _attachments = null;
            private Exception _mailException = null;
            #region mail class accessors


            public string CC
            {
                get { return _cc; }
                set { _cc = value; }
            }

            public string BCC
            {
                get { return _bcc; }
                set { _bcc = value; }
            }

            public string Body
            {
                get { return _body; }
                set { _body = value; }
            }
            public bool isBodyHtml { get; set; }




            public string Subject
            {
                get { return _subject; }
                set { _subject = value; }
            }
            public string MailServer
            {
                get
                {
                    if (null == _mailServer) _mailServer = "dtptax.com";
                    return _mailServer;
                }
                set { _mailServer = value; }
            }

            public string From
            {
                get { return _from; }
                set { _from = value; }
            }

            public string To
            {
                get { return _to; }
                set { _to = value; }
            }

            public string MailFormat
            {
                get { return _mailFormat; }
                set { _mailFormat = value; }
            }

            public string Attachments
            {
                get { return _attachments; }
                set { _attachments = value; }
            }

            public Exception MailException
            {
                get { return _mailException; }
                set { _mailException = value; }
            }

            #endregion


            public bool SendMail()
            {
                bool retval = true;


                System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();

                if (this.Body != null) msg.Body = this.Body;

                if (this.CC != null) msg.CC.Add(this.CC);
                if (this.To != null) msg.To.Add(this.To);
                if (this.From != null) msg.From = (new MailAddress(this.From));

                if (this.BCC != null) msg.Bcc.Add(this.BCC);
                if (this.Subject != null) msg.Subject = this.Subject;

                if (this.isBodyHtml)
                {
                    msg.IsBodyHtml = true;

                }
                else
                {
                    msg.IsBodyHtml = false;
                }




                //set attachments here

                try
                {
                    if (this.Attachments != null)
                    {
                        char[] delim = new char[] { ',' };

                        foreach (string sattach in this.Attachments.Split(delim))
                        {
                            Attachment data = new Attachment(GetNewFileNamePath() + @"\" + sattach);

                            // Add the file attachment to this e-mail message.
                            msg.Attachments.Add(data);
                        }
                    }
                    //var smtp = new System.Net.Mail.SmtpClient();
                    SmtpClient client = new SmtpClient(this.MailServer);
                    //client.Host = smtp.Host;
                    //client.Port = smtp.Port;           // from web.config files
                    //client.Credentials = smtp.Credentials;

                     client.UseDefaultCredentials = true;

                     client.Credentials = new System.Net.NetworkCredential("info@dtptax.com", "DTP@12");
                    client.EnableSsl =false; //Gmail works on Server Secured Layer

                    client.Send(msg);

                    retval = true;

                }
                catch (Exception ex)
                {
                    throw ex;
                }

                finally
                {
                    msg = null;
                }

                return retval;
            }

            private string GetNewFileNamePath()
            {
                string filePath = ConfigurationSettings.AppSettings["MailFilePath"].ToString();
                return filePath;
            }

            public bool DeleteSentFile()
            {
                if (this.Attachments != null)
                {
                    char[] delim = new char[] { ',' };
                    foreach (string sattach in this.Attachments.Split(delim))
                    {
                        if (File.Exists(GetNewFileNamePath() + @"\" + sattach))
                        {
                            FileInfo fi = new FileInfo(GetNewFileNamePath() + @"\" + sattach);
                            fi.Delete();
                        }
                    }

                }
                return true;
            }
        }
    }