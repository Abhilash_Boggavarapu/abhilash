﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intmail_DAL;
using ETaxDal;
using ETaxBO;
using ETaxFilling.Utility;
namespace ETaxFilling.MailBox
{
    public partial class Compose : System.Web.UI.Page
    {
   
        protected void page_preinit(object sender, EventArgs e)
        {

            if (Utility.Utility.CheckAdminLoginSession())
                MasterPageFile = IM_Utility.AdminMasterpagepath;
            else
                MasterPageFile = IM_Utility.UserMasterpagepath;

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Utility.Utility.CheckAdminLoginSession())
            {
                if (Page.IsPostBack == false)
                { 
                trTomember.Visible = true;
                txtbtoUserName.Text = "";
                }
            }
            else
                trTomember.Visible = false;
            
        }
        protected void btnSubmit_Click1(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtbtoUserName.Text))
            {
                lblmessage.Text = Utility.Utility.Requsername;
                lblmessage.Visible = true;
            }
            
            else if (string.IsNullOrEmpty(txtbSub.Text))
            {
                lblmessage.Text = Utility.Utility.ReqSubject;
            }
            else if (string.IsNullOrEmpty(txtbMessage.Text))
            {
                lblmessage.Text = Utility.Utility.ReqMessage;
            }
            else
            {
                lblmessage.Text = string.Empty;
                MasterDAL objMaster = new MasterDAL();             
                string sub = txtbSub.Text;
                    

                    if (objMaster.GetUseridbyMailid(txtbtoUserName.Text.Trim()) != 0)
                    {
                        IntMail Im_mail = new IntMail();

                        string fromid;

                        if (Utility.Utility.CheckAdminLoginSession())

                            fromid = Utility.Utility.GetAdminSession().UserID.ToString();
                        else
                            fromid = Utility.Utility.getLoginSession().UserID.ToString();

                        if (Convert.ToInt32(Im_mail.send_Internal_mail(fromid, objMaster.GetUseridbyMailid(txtbtoUserName.Text.Trim()).ToString(), sub, txtbMessage.Text)) == 1)
                        {
                            Utility.Clear.ClearDependents(this.Page);
                            lblmessage.Visible = true;
                            lblmessage.ForeColor = System.Drawing.Color.Green;
                            lblmessage.Text = "Message Sent Successfully.";
                            

                        }
                    }
                    else
                    {
                        lblmessage.Visible = true;
                        lblmessage.Text = "Enter Valid User Name";
                        txtbtoUserName.Focus();
                    }
                }
            }
        }
    }
