﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ETaxDal;
using ETaxBO;


namespace ETaxFilling
{
    public partial class ClientRegistration : System.Web.UI.Page
    {
        MasterDAL objMasterDal = new MasterDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            lblRc.Text = string.Empty;
            divCREGerror.InnerText = string.Empty;
            if (!IsPostBack)
            {
                txtCity.Visible = false;
                ddlCountry.DataTextField = "Country";
                ddlCountry.DataValueField = "CountryID";
                ddlState.DataTextField = "StateName";
                ddlState.DataValueField = "StateID";
                ddlCity.DataTextField = "City";
                ddlCity.DataValueField = "CityID";
                ddlCountry.DataSource = objMasterDal.getCountries();
                ddlCountry.DataBind();
                ddlCountry.Items.Insert(0, new ListItem { Text = Utility.Utility.setSelectCountry, Value = "0", Selected = true });
                ddlState.Items.Insert(0, new ListItem { Text = Utility.Utility.setSelectState, Value = "0", Selected = true });
                ddlCity.Items.Insert(0, new ListItem { Text = Utility.Utility.setSelectCity, Value = "0", Selected = true });
            }
        }
        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlState.Items.Clear();
            ddlCity.Items.Clear();
            int CountryId = int.Parse(ddlCountry.SelectedValue);
            ddlState.DataSource = objMasterDal.getStates(CountryId);
            ddlState.DataBind();
            ddlState.Items.Insert(0, new ListItem { Text = Utility.Utility.setSelectState, Value = "0", Selected = true });
            ddlCity.Items.Insert(0, new ListItem { Text = Utility.Utility.setSelectCity, Value = "0", Selected = true });
        }

        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlCity.Items.Clear();
            int StateId = int.Parse(ddlState.SelectedValue);
            ddlCity.DataSource = objMasterDal.getCities(StateId);
            ddlCity.DataBind();
            ddlCity.Items.Insert(0, new ListItem { Text = Utility.Utility.setSelectCity, Value = "0", Selected = true });
            ddlCity.Items.Insert(1, new ListItem { Text = Utility.Utility.setOtherCity, Value = "" });
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtUserName.Text))
            {
                divCREGerror.InnerText = Utility.Utility.ReqNEmailId;
            }
            else if (!txtUserName.Text.IsValidEmailID())
            {
                divCREGerror.InnerText = Utility.Utility.ValNEmailId;
                txtUserName.Text = string.Empty;
                txtUserName.Focus();
            }
            else if (string.IsNullOrEmpty(txtPassword.Text))
            {
                divCREGerror.InnerText = Utility.Utility.ReqPwd;
            }
            else if (!txtPassword.Text.IsStrongPassword())
            {
                divCREGerror.InnerText = Utility.Utility.ValPwd;
                txtPassword.Text = string.Empty;
                txtPassword.Focus();
            }
            else if (string.IsNullOrEmpty(txtConformPassword.Text))
            {
                divCREGerror.InnerText = Utility.Utility.ReqCPwd;
            }

            else if (!txtPassword.Text.Equals(txtConformPassword.Text))
            {
                divCREGerror.InnerText = Utility.Utility.MatchPwd;
                txtConformPassword.Text = string.Empty;
                txtConformPassword.Focus();
            }
            else if (string.IsNullOrEmpty(txtFirstname.Text))
            {
                divCREGerror.InnerText = Utility.Utility.ReqFName;
            }
            else if (Validations.IsAlpha(txtFirstname.Text))
            {
                divCREGerror.InnerText = Utility.Utility.ValFName;
                txtFirstname.Text = string.Empty;
                txtFirstname.Focus();
            }
            else if (string.IsNullOrEmpty(txtLastname.Text))
            {
                divCREGerror.InnerText = Utility.Utility.ReqLName;
            }
            else if (txtLastname.Text.IsAlpha())
            {
                divCREGerror.InnerText = Utility.Utility.ValLName;
                txtLastname.Text = string.Empty;
                txtLastname.Focus();
            }
            else if (ddlGender.Items.Count == 0 || ddlGender.SelectedItem.Text == Utility.Utility.setSelectGender)
            {
                divCREGerror.InnerText = Utility.Utility.ReqGender;
            }
            else if (ddlCountry.Items.Count == 0 || ddlCountry.SelectedItem.Text == Utility.Utility.setSelectCountry)
            {
                divCREGerror.InnerText = Utility.Utility.ReqCountry;
            }
            else if (ddlState.Items.Count == 0 || ddlState.SelectedItem.Text == Utility.Utility.setSelectState)
            {
                divCREGerror.InnerText = Utility.Utility.ReqState;
            }
            else if (ddlCity.Items.Count == 0 || ddlCity.SelectedItem.Text == Utility.Utility.setSelectCity)
            {
                divCREGerror.InnerText = Utility.Utility.ReqCity;
            }
            
            else if (!Validations.isNumber(txtZip.Text) && txtZip.Text != string.Empty)
            {
                divCREGerror.InnerText = Utility.Utility.ValZip;
                txtZip.Text = string.Empty;
                txtZip.Focus();
            }
            else if (string.IsNullOrEmpty(txtMobileNo.Text))
            {
                divCREGerror.InnerText = Utility.Utility.ReqMobileNo;
            }
            else if (!txtMobileNo.Text.isNumber())
            {
                divCREGerror.InnerText = Utility.Utility.ValMobileNo;
                txtMobileNo.Text = string.Empty;
                txtMobileNo.Focus();
            }
            else if (!txtWorkNo.Text.isNumber() && txtWorkNo.Text != string.Empty)
            {
                divCREGerror.InnerText = Utility.Utility.ValWorkNo;
                txtWorkNo.Text = string.Empty;
                txtWorkNo.Focus();
            }
            else if (!txtHomeNo.Text.isNumber() && txtHomeNo.Text != string.Empty)
            {
                divCREGerror.InnerText = Utility.Utility.ValHomeNo;
                txtHomeNo.Text = string.Empty;
                txtHomeNo.Focus();
            }
            else if (txtEmployName.Text.IsAlpha() && txtEmployName.Text != string.Empty)
            {
                divCREGerror.InnerText = Utility.Utility.ValEmpName;
                txtEmployName.Text = string.Empty;
                txtEmployName.Focus();
            }
            else if (txtRefferdBy.Text.IsAlpha() && txtRefferdBy.Text != string.Empty)
            {
                divCREGerror.InnerText = Utility.Utility.ValRefbyName;
                txtRefferdBy.Text = string.Empty;
                txtRefferdBy.Focus();
            }
            else
            {
                divCREGerror.InnerText = string.Empty;
                BO_tblUser objBoUser = new BO_tblUser();

                if (ckbTMCN.Checked == true && Page.IsValid)
                {
                    objBoUser.RoleID = Convert.ToInt32(MasterItems.Roles.User);
                    objBoUser.EmailID = txtUserName.Text;
                    objBoUser.Password = txtPassword.Text;
                    objBoUser.FirstName = txtFirstname.Text;
                    objBoUser.LastName = txtLastname.Text;
                    objBoUser.ZipCode = txtZip.Text;
                    objBoUser.Gender = ddlGender.SelectedValue;
                    objBoUser.CountryID = Convert.ToInt32(ddlCountry.SelectedValue);
                    objBoUser.StateID = Convert.ToInt32(ddlState.SelectedValue);
                    if (Convert.ToInt32(ddlCity.SelectedIndex) == 1)
                    {
                        if (txtCity.Text == string.Empty)
                        {
                            divCREGerror.InnerText = Utility.Utility.reqEnterCity;
                        }
                        else
                        {
                            BO_tblCity objTblCity = new BO_tblCity();
                            objTblCity.CountryID = Convert.ToInt32(ddlCountry.SelectedValue);
                            objTblCity.StateID = Convert.ToInt32(ddlState.SelectedValue);
                            objTblCity.City = txtCity.Text;
                            objBoUser.CityID = objMasterDal.insertCity(objTblCity);
                        }
                    }
                    else
                    {
                        objBoUser.CityID = Convert.ToInt32(ddlCity.SelectedValue);
                    }
                    if (divCREGerror.InnerText == string.Empty)
                    {
                        objBoUser.MobileNumber = txtMobileNo.Text;
                        objBoUser.HomeNumber = txtHomeNo.Text;
                        objBoUser.WorkNubmer = txtWorkNo.Text;
                        objBoUser.EmployerName = txtEmployName.Text;
                        objMasterDal.saveUserDetails(objBoUser);
                        //newely added

                        Utility.MailMessage ms = new Utility.MailMessage();
                        ms.To = objBoUser.EmailID;
                        ms.Subject = "Deltatax Planners Registration Conformation";
                        string domain;
                        Uri url = HttpContext.Current.Request.Url;
                        domain = url.AbsoluteUri.Replace(url.PathAndQuery, string.Empty);
                        ms.From = "info@dtptax.com";
                        ms.Body = @"Thank You Registring in  Deltatax Planers.com" + "<br>" + "<a href=" + domain + "http://www.dtptax.com/UserLogIn.aspx> Click here to login</a> <br>" + "Your EmailID is : " + objBoUser.EmailID + "<br>" + "Your  password is : " + objBoUser.Password + "<br>";
                        ms.isBodyHtml = true;

                        ms.SendMail();
                        Response.Redirect("UserLogIn.aspx");
                    }
                    
                }
                else
                {
                    if (!Page.IsValid) 
                    {
                        lblRc.Text = "Wrong text you have entered";
                        lblRc.ForeColor = System.Drawing.Color.Red;
                    }
                    if (ckbTMCN.Checked == false)
                    {
                        divCREGerror.InnerText = "Please check the Terms and conditions ";
                    }
                }
            }
        }

        protected void txtUserName_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtUserName.Text))
            {
                divCREGerror.InnerText = "Please enter e-mail address";
            }
            else if (!txtUserName.Text.IsValidEmailID())
            {
                divCREGerror.InnerText = "Please Enter Valid Email Address";
                txtUserName.Text = string.Empty;
                txtUserName.Focus();
            }
            else
            {
                divCREGerror.InnerText = "";
                string Emailid = objMasterDal.checkEmailId(txtUserName.Text);
                if (Emailid != null)
                {
                    lblmsg.Text = "Email Id is already present";
                    lblmsg.ForeColor = System.Drawing.Color.Red;
                    txtUserName.Text = string.Empty;
                }
                else
                {
                    lblmsg.Text = "Email Id is available";

                    lblmsg.ForeColor = System.Drawing.Color.Green;
                }
            }

        }

        protected void ddlCity_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (Convert.ToInt32(ddlCity.SelectedIndex) == 1)
            {
                txtCity.Visible = true;
            }
            else
            {
                txtCity.Visible = false;
            }
        }
    }
}