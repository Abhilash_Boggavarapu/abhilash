﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Client.master" CodeBehind="EditContactDetails.aspx.cs" Inherits="ETaxFilling.EditContactDetails" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="divCDerror" style="color:#FF0000"  runat="server"></div>


<div>
             <table align="center" cellpadding="0" cellspacing="8" border="0" width="100%" class="box">
        <tr class="box">
        <td colspan="2">
        <h2 class="divider top20 bottom10">Edit Contact Details</h2>
        </td>
        </tr>
                <tr>
                    <td align="right">
                      
                        <asp:Label ID="LblMobNo" runat="server" Text="Mobile Number : "></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtMobNo" runat="server" CssClass="ValidationRequired IsNumeric" 
                            TabIndex="1"></asp:TextBox>
                       
                    </td>
                </tr>
                <tr>
                    <td align="right">
                       
                        <asp:Label ID="LblHNum" runat="server" Text="Home Number : "></asp:Label>
                    </td>
                    <td>
                       
                        <asp:TextBox ID="txtHNo" runat="server" CssClass="ValidationRequired IsNumeric" 
                             MaxLength="20" TabIndex="2" ></asp:TextBox>
                        
                    </td>
                </tr>
                <tr>
                <td>
                    <asp:HyperLink ID="HyperLink1" runat="server" CssClass="button" NavigateUrl="~/ChangeSettings.aspx">Back</asp:HyperLink>
                </td>
                    <td align="left" style=" padding-left:10px;">
                       <asp:Button 
                            ID="BtnSave" runat="server" 
                            
                        Text="Update" CssClass="button" 
                            onclick="BtnSave_Click" Width="111px" Height="26px" TabIndex="3" />
                     
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                     
                        <asp:Label ID="lblConform" runat="server" CssClass="error"></asp:Label>
                    </td>
                </tr>
            </table>
    </div>
</asp:Content>
