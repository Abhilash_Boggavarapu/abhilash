﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Client.master" AutoEventWireup="true" CodeBehind="Description.aspx.cs" Inherits="ETaxFilling.Description" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding-right:10px;" id="TABLE1" class="box">
  <tr>
    <td align="left"><asp:Label ID="lblTitle" runat="server" Text="Mail Description" Width="100%" CssClass="divider top20 bottom10"></asp:Label></td>
  </tr>
 
  <tr>
  <td><asp:Label ID="lblError" runat="server" CssClass="lblerror"  Visible="False"></asp:Label></td>
  </tr>
  <tr>
    <td class="content" align="center"><table width="80%" border="0" cellpadding="0" cellspacing="8">
	  <tr>
        <td style="width:45%; text-align:right;" class="l_text"><asp:Label ID="lblForTtitle" runat="server" Text="From/To :"></asp:Label></td>
        <td class="formtext"><asp:Label ID="lblFrom" runat="server" CssClass="usertext" Text="From"></asp:Label></td>
      </tr>
	  <tr>
        <td style="width:45%; text-align:right;" class="l_text">Subject :</td>
        <td class="formtext"><asp:Label ID="lblSubject" runat="server" Text="Subject"></asp:Label></td>
      </tr>
	  <tr>
        <td style="width:45%; text-align:right;" class="l_text">Date :</td>
        <td class="formtext"><asp:Label ID="lblDate" runat="server" Text="Date"></asp:Label></td>
      </tr>
	  <tr>
        <td style="width:45%; text-align:right;" class="l_text">Message :</td>
        <td class="formtext"><asp:TextBox ID="txtbMessage" runat="server" Height="184px" TextMode="MultiLine" Width="384px" ReadOnly="true"></asp:TextBox></td>
      </tr>
	  </table></td>
  </tr>
</table>

</asp:Content>
