﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ETaxDal;
using ETaxBO;

namespace ETaxFilling
{
    public partial class RefferFromHome : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            divRHerr.InnerText = string.Empty;
        }

        protected void btnAddRFHome_Click(object sender, EventArgs e)
        {

            if (Validations.IsAlpha(txtRName.Text) && txtRName.Text !=string.Empty)
            {
                divRHerr.InnerText = Utility.Utility.ValRefbyName;
                txtRName.Text = string.Empty;
                txtRName.Focus();
            }
          
            else if (!txtEmailId.Text.IsValidEmailID() && txtEmailId.Text !=string.Empty)
            {
                divRHerr.InnerText = Utility.Utility.ValNEmailId;
                txtEmailId.Text = string.Empty;
                txtEmailId.Focus();
            }
           
            else if (!txtPhone1.Text.IsNumeric() && txtPhone1.Text !=string.Empty)
            {
                divRHerr.InnerText = Utility.Utility.ValMobileNo;
                txtPhone1.Text = string.Empty;
                txtPhone1.Focus();
            }
            else if (!txtPhone2.Text.IsNumeric() && txtPhone2.Text !=string.Empty)
            {
                    divRHerr.InnerText = Utility.Utility.ValWorkNo;
                    txtPhone2.Text = string.Empty;
                    txtPhone2.Focus();
            }
            else if (Validations.IsAlpha(txtRBy.Text) && txtRBy.Text !=string.Empty)
            {
                divRHerr.InnerText = Utility.Utility.ValRefbyName;
                txtRBy.Text = string.Empty;
                txtRBy.Focus();
            }         
            else if (!txtREid.Text.IsValidEmailID() && txtREid.Text !=string.Empty)
            {
                divRHerr.InnerText = Utility.Utility.ValNEmailId;
                txtREid.Text = string.Empty;
                txtREid.Focus();
            }
            else if (!txtPhone.Text.IsNumeric() && txtPhone.Text !=string.Empty)
            {
                divRHerr.InnerText = Utility.Utility.ValMobileNo;
                txtPhone.Text = string.Empty;
                txtPhone.Focus();
            }
            else if (txtEmailId.Text == string.Empty && txtPhone.Text == string.Empty && txtPhone1.Text == string.Empty && txtPhone2.Text == string.Empty && txtRBy.Text == string.Empty && txtREid.Text == string.Empty && txtRName.Text == string.Empty)
            {
                divRHerr.InnerText = Utility.Utility.ReqAnyField;
                txtRName.Text = string.Empty;
                txtRName.Focus();
            }
            else
            {
                ReferralDal objDReferal = new ReferralDal();
                divRHerr.InnerText = string.Empty;
                BO_tblUserReferral objReferal = new BO_tblUserReferral();
                int ReferralTypeID = Convert.ToInt32(MasterItems.ReferralTypes.ReferredfromHomePage);
                objReferal.ReferralTypeID = ReferralTypeID;
                objReferal.ReferralName = txtRName.Text;
                objReferal.EmailID = txtEmailId.Text;
                objReferal.Phone1 = txtPhone1.Text;
                objReferal.Phone2 = txtPhone2.Text;
                objReferal.ReferredName = txtRBy.Text;
                objReferal.ReferredEmailID = txtREid.Text;
                objReferal.ReferredPhone = txtPhone.Text;
                objDReferal.SaveReferralDetails(objReferal);
                lblMsg.ForeColor = System.Drawing.Color.Green;
                lblMsg.Text = " Saved Sucessfully";
                Utility.Clear.ClearDependents(this.Page);
            }
        }
    }
}