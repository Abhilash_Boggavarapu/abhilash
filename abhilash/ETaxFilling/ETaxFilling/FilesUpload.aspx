﻿<%@ Page Language="C#" MasterPageFile="~/Client.master" AutoEventWireup="true" CodeBehind="FilesUpload.aspx.cs"
    Inherits="ETaxFilling.FilesUpload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <h2 class="divider top20 bottom10">
                    <asp:Label ID="lblMsg" runat="server"></asp:Label></h2>
            </td>
        </tr>
    </table>
    <asp:GridView ID="GridViewUpload" runat="server" AutoGenerateColumns="False"
        Height="75px" Width="100%" OnRowCommand="GridViewUpload_RowCommand" AllowPaging="True"
        OnPageIndexChanging="GridViewUpload_PageIndexChanging">
        <Columns>
            <asp:TemplateField HeaderText="S.No.">
                <ItemTemplate>
                    <%#Container.DataItemIndex+1 %><br />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
            <asp:BoundField DataField="FileName" HeaderText="File Name" />
            <asp:TemplateField HeaderText="Download">
                <ItemTemplate>
                    <asp:LinkButton ID="BtnDownload" runat="server" CommandName="Download" CommandArgument='<%# Eval("TaxPayerDocumentID") %>'
                        Text="Download"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
    </asp:GridView>
    <table align="center" cellpadding="0" cellspacing="5" border="0" width="100%" class="box">
        <tr class="box">
            <td colspan="2">
                <h2 class="divider top20 bottom10">
                    <asp:Label ID="lblHeader" runat="server" Text="Upload Tax Documents" Style="font-weight: 700;
                        font-size: large"></asp:Label></h2>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" CssClass="error"></asp:Label>
                <asp:Label ID="Label2" runat="server" CssClass="error"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" border="1px" width="100%" style="border: 0px #333333 solid;">
                    <tr>
                        <td align="center">
                            S.No.
                        </td>
                        <td align="center">
                            Form Name
                        </td>
                        <td align="center" style=" width:150px;">
                            Browse
                        </td>
                        <td align="center">
                            Remarks
                        </td>
                        <td align="center" style="width: 35px;">
                            Upload
                        </td>
                        <td align="center" style="width: 21px;">
                            View
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            1
                        </td>
                        <td align="center">
                            W2 Form
                            <br />
                           ( Wage Tax Statement) 
                        </td>
                        <td>
                            <asp:FileUpload ID="FileUpload1" runat="server"  
                                TabIndex="1" Width="227px" />
                            <br />
                            <asp:Label ID="lblCount" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtW2Form" TextMode="MultiLine" MaxLength="1000"
                                TabIndex="2" Width="220px" />
                        </td>
                        <td align="center" style="width: 35px;">
                            <asp:Button Text="Upload" ID="btnW2Form" runat="server" CssClass="button small" CommandArgument="1"
                                OnClientClick="return ValidateFile" OnClick="btnW2Form_Click" TabIndex="3" />
                        </td>
                        <td align="center" style="width: 21px;">
                            <asp:Button Text="View Uploaded Documents" ID="btnView" runat="server" 
                                CssClass="button small" CommandArgument="1"
                                OnClick="Button1_Click" TabIndex="4" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            2
                        </td>
                        <td align="center">
                            Form 1099
                            <br />
                            <span style="font-size: 10">(Miscellaneous Income Statement) </span>
                        </td>
                        <td>
                            <asp:FileUpload ID="FileUpload2" runat="server" class="multi" TabIndex="5" 
                                Width="228px" />
                            <br />
                            &nbsp;
                          <h4> <asp:Label ID="lblCount0" runat="server"></asp:Label></h4> 
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtForm1099" TextMode="MultiLine" MaxLength="1000"
                                TabIndex="6" Width="220px"></asp:TextBox>
                        </td>
                        <td align="center" style="width: 35px;">
                            <asp:Button Text="Upload" ID="btnForm1099" runat="server" CssClass="button small"
                                CommandArgument="2" OnClientClick="return ValidateFile" OnClick="btnForm1099_Click"
                                TabIndex="7" />
                        </td>
                        <td align="center" style="width: 21px;">
                            <asp:Button Text="View Uploaded Documents" ID="btnView1" runat="server" CssClass="button small" CommandArgument="2"
                                OnClick="Button1_Click" TabIndex="8" />
                        </td>
                    </tr>
                    <tr style="border-bottom: 1px #333333 solid; padding-top: 10px;">
                        <td align="center" style="width: 44px">
                            3
                        </td>
                        <td align="center">
                            1099 Int
                            <br />
                            <span style="font-size: 10">(Interest Income Statement) </span>
                        </td>
                        <td>
                            <asp:FileUpload ID="FileUpload3" runat="server" class="multi" TabIndex="9" 
                                Width="229px" />
                            <br />
                           <h4><asp:Label ID="lblCount1" runat="server"></asp:Label></h4>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtInt1099" TextMode="MultiLine" MaxLength="1000"
                                TabIndex="10" Width="220px"></asp:TextBox>
                        </td>
                        <td align="center" style="width: 35px;">
                            <asp:Button Text="Upload" ID="btnInt1099" runat="server" CssClass="button small"
                                CommandArgument="3" OnClientClick="return ValidateFile" OnClick="btnInt1099_Click"
                                TabIndex="11" />
                        </td>
                        <td align="center" style="width: 21px;">
                            <asp:Button Text="View Uploaded Documents" ID="btnView2" runat="server" CssClass="button small" CommandArgument="3"
                                OnClick="Button1_Click" TabIndex="12" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            4
                        </td>
                        <td align="center">
                            1099G
                            <br />
                            <span style="font-size: 10">( State Tax Refund) </span>
                        </td>
                        <td style="width: 23%">
                            <asp:FileUpload ID="FileUpload4" runat="server" class="multi" TabIndex="13" 
                                Width="231px" />
                            <br />
                           <h4><asp:Label ID="lblCount2" runat="server"></asp:Label></h4>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtStateTaxRefund" TextMode="MultiLine" MaxLength="1000"
                                TabIndex="14" Width="220px"></asp:TextBox>
                        </td>
                        <td align="center" style="width: 35px;">
                            <asp:Button Text="Upload" ID="btnStateTaxRefund" CssClass="button small" runat="server"
                                CommandArgument="4" OnClientClick="return ValidateFile" OnClick="btnStateTaxRefundClick"
                                TabIndex="15" />
                        </td>
                        <td align="center" style="width: 21px;">
                            <asp:Button Text="View Uploaded Documents" ID="btnView3" runat="server" CssClass="button small" CommandArgument="4"
                                OnClick="Button1_Click" TabIndex="16" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            5
                        </td>
                        <td align="center">
                            1099- R
                            <br />
                            <span style="font-size: 10">( individual Retirement Arrangement) </span>
                        </td>
                        <td>
                            <asp:FileUpload ID="FileUpload5" runat="server" class="multi" TabIndex="17" 
                                Width="227px" />
                            <br />
                            <h4><asp:Label ID="lblCount3" runat="server"></asp:Label></h4>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtR1099" TextMode="MultiLine" MaxLength="1000" TabIndex="18"
                                Width="220px"></asp:TextBox>
                        </td>
                        <td align="center" style="width: 35px;">
                            <asp:Button Text="Upload" ID="btnR1099" runat="server" CssClass="button small" CommandArgument="5"
                                OnClientClick="return ValidateFile" OnClick="btnR1099_Click" TabIndex="19" />
                        </td>
                        <td align="center" style="width: 21px;">
                            <asp:Button Text="View Uploaded Documents" ID="btnView4" runat="server" CssClass="button small" CommandArgument="5"
                                OnClick="Button1_Click" TabIndex="20" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            6
                        </td>
                        <td align="center">
                            1098
                            <br />
                            <span style="font-size: 10">( Home Mortgage Statement) </span>
                        </td>
                        <td>
                            <asp:FileUpload ID="FileUpload6" runat="server" class="multi" TabIndex="21" 
                                Width="231px" />
                            <br />
                         <h4><asp:Label ID="lblCount4" runat="server"></asp:Label></h4>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtHomeMortageStatement" TextMode="MultiLine" MaxLength="1000"
                                TabIndex="22" Width="220px"></asp:TextBox>
                        </td>
                        <td align="center" style="width: 35px;">
                            <asp:Button Text="Upload" ID="btnHomeMortage" CssClass="button small" runat="server"
                                CommandArgument="6" OnClientClick="return ValidateFile" OnClick="btnHomeMortage_Click"
                                TabIndex="23" />
                        </td>
                        <td align="center" style="width: 21px;">
                            <asp:Button Text="View Uploaded Documents" ID="btnView5" runat="server" CssClass="button small" CommandArgument="6"
                                OnClick="Button1_Click" TabIndex="24" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            7
                        </td>
                        <td align="center">
                            1098-T<br />
                            <span style="font-size: 10">( Tuition fee Statement) </span>
                        </td>
                        <td>
                            <asp:FileUpload ID="FileUpload7" runat="server" class="multi" TabIndex="25" 
                                Width="226px" />
                            <br />
                            <h4><asp:Label ID="lblCount5" runat="server"></asp:Label></h4>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtT1O98" TextMode="MultiLine" MaxLength="1000" TabIndex="26"
                                Width="220px"></asp:TextBox>
                        </td>
                        <td align="center" style="width: 35px;">
                            <asp:Button Text="Upload" ID="btnT1098" runat="server" CssClass="button small" CommandArgument="7"
                                OnClientClick="return ValidateFile" OnClick="btnT1098_Click" TabIndex="27" />
                        </td>
                        <td align="center" style="width: 21px;">
                            <asp:Button Text="View Uploaded Documents" ID="btnView6" runat="server" CssClass="button small" CommandArgument="7"
                                OnClick="Button1_Click" TabIndex="28" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            8
                        </td>
                        <td align="center">
                            1098- E
                            <br />
                            <span style="font-size: 10">(Education Interest Loan)</span>
                        </td>
                        <td>
                            <asp:FileUpload ID="FileUpload8" runat="server" class="multi" TabIndex="29" 
                                Width="229px" />
                            <br />
                            <h4><asp:Label ID="lblCount6" runat="server"></asp:Label></h4>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtE1098" TextMode="MultiLine" MaxLength="1000" TabIndex="30"
                                Width="220px"></asp:TextBox>
                        </td>
                        <td align="center" style="width: 35px;">
                            <asp:Button Text="Upload" ID="btnE1098" runat="server" CssClass="button small" CommandArgument="8"
                                OnClientClick="return ValidateFile" OnClick="btnE1098_Click" TabIndex="31" />
                        </td>
                        <td align="center" style="width: 21px;">
                            <asp:Button Text="View Uploaded Documents" ID="btnView7" runat="server" CssClass="button small" CommandArgument="8"
                                OnClick="Button1_Click" TabIndex="32" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            9
                        </td>
                        <td align="center">
                            1065 K
                            <br />
                            <span style="font-size: 10">(Partnership)</span>
                        </td>
                        <td>
                            <asp:FileUpload ID="FileUpload9" runat="server" class="multi" TabIndex="33" 
                                Width="227px" />
                            <br />
                            <h4><asp:Label ID="lblCount7" runat="server"></asp:Label></h4>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtK1065" TextMode="MultiLine" MaxLength="1000" TabIndex="34"
                                Width="220px"></asp:TextBox>
                        </td>
                        <td align="center" style="width: 35px;">
                            <asp:Button Text="Upload" ID="btnK1065" runat="server" CssClass="button small" CommandArgument="9"
                                OnClientClick="return ValidateFile" OnClick="btnK1065_Click" TabIndex="35" />
                        </td>
                        <td align="center" style="width: 21px;">
                            <asp:Button Text="View Uploaded Documents" ID="btnView8" runat="server" CssClass="button small" CommandArgument="9"
                                OnClick="Button1_Click" TabIndex="36" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            10
                        </td>
                        <td align="center">
                            Upload other document
                        </td>
                        <td>
                            <asp:FileUpload ID="FileUpload10" runat="server" class="multi" TabIndex="37" 
                                Width="233px" />
                            <br />
                          <h4><asp:Label ID="lblCount8" runat="server"></asp:Label></h4>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtUploadOtherDocument" TextMode="MultiLine" MaxLength="1000"
                                TabIndex="38" Width="220px"></asp:TextBox>
                        </td>
                        <td align="center" style="width: 35px;">
                            <asp:Button Text="Upload" ID="btnUploadOtherDocument" CssClass="button small" CommandArgument="10"
                                runat="server" OnClientClick="return ValidateFile" OnClick="btnUploadOtherDocument_Click"
                                TabIndex="39" />
                        </td>
                        <td align="center" style="width: 21px;">
                            <asp:Button Text="View Uploaded Documents" ID="btnView9" runat="server" CssClass="button small" CommandArgument="10"
                                OnClick="Button1_Click" TabIndex="40" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
            </td>
            <td align="center">
                <asp:HyperLink ID="HyperLink1" runat="server" CssClass="button" Height="25px" Width="50px"
                    NavigateUrl="~/View Documents.aspx" TabIndex="8">Next</asp:HyperLink>
            </td>
        </tr>
    </table>
</asp:Content>
