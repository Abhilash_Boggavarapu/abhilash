﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ETaxBO;
using ETaxDal;

namespace ETaxFilling
{
    public partial class UserLogIn : System.Web.UI.Page
    {
        MasterDAL objMasterDal = new MasterDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            BO_tblTaxPayerDetail objTaxPayerDetail = objMasterDal.Checklogin(txtUserName.Text, txtPswd.Text, ETaxBO.MasterItems.Roles.User);
            if (objTaxPayerDetail == null)
            {
                //lblStatus.ForeColor = System.Drawing.Color.Red
                //lblStatus.Text = "The username or password you entered is incorrect..";
                ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Invalid Username and Password')</script>");
            }
            else
            {
                Utility.Utility.setLoginSession(objTaxPayerDetail);               
                    
                    if (!Utility . Utility.getLoginSession().ConsentFormStatus)
                    {
                        Response.Redirect("entry7216form.aspx");
                        }
                    else
                    {
                        Response.Redirect("MyFileStatus.aspx");
                    }
             }
         }

       
    }
}