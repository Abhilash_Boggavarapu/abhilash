﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ETaxFilling
{
    public partial class ChangeSettings : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Utility.Utility.checkLoginSession())
            {
                Response.Redirect("userlogin.aspx");
            }
        }
    }
}