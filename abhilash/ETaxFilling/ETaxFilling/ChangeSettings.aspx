﻿<%@ Page Language="C#" MasterPageFile="~/Client.Master" AutoEventWireup="true" CodeBehind="ChangeSettings.aspx.cs"
    Inherits="ETaxFilling.ChangeSettings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table align="center" cellpadding="0" cellspacing="8" border="0" width="100%" class="box">
        <tr class="box">
            <td colspan="3">
                <h2 class="divider top20 bottom10">
                    Change Settings</h2>
            </td>
        </tr>
        <tr>
            <td>
                <asp:HyperLink ID="HypPwd" runat="server" NavigateUrl="~/ChangePasword.aspx" CssClass="button"
                    Height="35px">Change Password</asp:HyperLink>
            </td>
            <td>
                <asp:HyperLink ID="HypEmail" runat="server" NavigateUrl="~/Change Email.aspx" CssClass="button"
                    TabIndex="1" Height="35px">Change Email ID</asp:HyperLink>
            </td>
            <td>
                <asp:HyperLink ID="HypContac" runat="server" NavigateUrl="~/EditContactDetails.aspx"
                    CssClass="button" TabIndex="2" Height="35px">Change Contact Details</asp:HyperLink>
            </td>
        </tr>
    </table>
</asp:Content>
