﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ETaxDal;
using ETaxBO;

namespace ETaxFilling
{
    public partial class FillTaxConfirm : System.Web.UI.Page
    { 
        EmployeDAL objDEmploye = new EmployeDAL();
        ClientDetailsDAL objClent = new ClientDetailsDAL();
        ResidentDetailsDAL objResidentdal = new ResidentDetailsDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            btnNext.Visible = false;
            if (!Utility.Utility.CheckAdminLoginSession())
            {
                Response.Redirect("~/Administrator/AdminLogin.aspx");
            }
            else
            {
                lblmsg.Text = string.Empty;
                lblEmpmsg.Text = string.Empty;
                lblClientmsg.Text = string.Empty;
                lblResmsg.Text = string.Empty;
                lblSpouseEmpmsg.Text = string.Empty;
                lblSpouseClientmsg.Text = string.Empty;
                lblSouseResmsg.Text = string.Empty;
                if (Utility.Utility.getLoginSession().MaritalStatus == MasterItems.MaritalStatuses.Single.ToString())
                {
                    PanelSpouseClientDetails.Visible = false;
                    PanelSpouseDetails.Visible = false;
                   PanelTaxInfoSpouseDetails.Visible = false;
                }
                BO_TaxPayerTaxFilingStatusDetails isUpdateable = new StatusDAL().getFilingStatus((long)Utility.Utility.getLoginSession().TaxPayerID, (int)MasterItems.TaxFillingStatusTypes.FillTaxInformation);
                if (isUpdateable != null)
                {
                    btnConfirmDown.Visible = false;
                    btnConfirm.Visible = false;
                    btnEditClientDetails.Visible = false;
                    btnEditEmpDetails.Visible = false;
                    btnEditResDetails.Visible = false;
                    btnSpouseClientDetails.Visible = false;
                    btnEditSpouseResidencyDetails.Visible = false;
                    btnEditSpouseEmpDetails.Visible = false;

                    btnNext.Visible = false;
                }
                List<BO_tblTaxPayerEmployerDetail> objtaxPayeremployee = objDEmploye.GetTaxPayerEmployersDetails(Utility.Utility.getLoginSession().TaxPayerID);
                if (objtaxPayeremployee.Count != 0)
                {
                    GrdEmployeeDetails.DataSource = objtaxPayeremployee;
                    GrdEmployeeDetails.DataBind();
                }
                else
                {
                    lblEmpmsg.Text = "Not Provided";
                    lblEmpmsg.ForeColor = System.Drawing.Color.Red;
                }
                List<BO_tblTaxPayerClientDetail> objtaxpayerClient = objClent.GetTaxpayrClientsDetails(Utility.Utility.getLoginSession().TaxPayerID);
                if (objtaxpayerClient.Count != 0)
                {
                    GrdClientlDetails.DataSource = objtaxpayerClient;
                    GrdClientlDetails.DataBind();
                }
                else
                {
                    lblClientmsg.Text = "Not Provided";
                    lblClientmsg.ForeColor = System.Drawing.Color.Red;
                }
                List<BO_tblTaxPayerResidencyDetail> objtaxpayrResidency = objResidentdal.getResidencyDetails(Utility.Utility.getLoginSession().TaxPayerID);
                if (objtaxpayrResidency.Count != 0)
                {
                    GrdResedentDetails.DataSource = objtaxpayrResidency;
                    GrdResedentDetails.DataBind();
                }
                else
                {
                    lblResmsg.Text = "Not Provided";
                    lblResmsg.ForeColor = System.Drawing.Color.Red;
                }
                List<BO_tblTaxPayerEmployerDetail> objtaxspouseEmp = objDEmploye.GetTaxPayerEmployersDetails((long)Utility.Utility.getLoginSession().TaxPayerSpouseID);
                if (objtaxspouseEmp.Count != 0)
                {
                    GrdSpouseEmpDetails.DataSource = objtaxspouseEmp;
                    GrdSpouseEmpDetails.DataBind();
                }
                else
                {
                    lblSpouseEmpmsg.Text = "Not Provided";
                    lblSpouseEmpmsg.ForeColor = System.Drawing.Color.Red;
                }
                List<BO_tblTaxPayerClientDetail> objtaxSpouseClient = objClent.GetTaxpayrClientsDetails((long)Utility.Utility.getLoginSession().TaxPayerSpouseID);
                if (objtaxSpouseClient.Count != 0)
                {
                    GrdSpouseClientlDetails.DataSource = objtaxSpouseClient;
                    GrdSpouseClientlDetails.DataBind();
                }
                else
                {
                    lblSpouseClientmsg.Text = "Not Provided";
                    lblSpouseClientmsg.ForeColor = System.Drawing.Color.Red;

                }
                List<BO_tblTaxPayerResidencyDetail> objtaxSpouseRes = objResidentdal.getResidencyDetails((long)Utility.Utility.getLoginSession().TaxPayerSpouseID);
                if (objtaxSpouseRes.Count != 0)
                {
                    GrdSpouseResidencyDetails.DataSource = objtaxSpouseRes;
                    GrdSpouseResidencyDetails.DataBind();
                }
                else
                {
                    lblSouseResmsg.Text = "Not Provided";
                    lblSouseResmsg.ForeColor = System.Drawing.Color.Red;
                }
            }
        }

        protected void btnEditEmpDetails_Click(object sender, EventArgs e)
        {
            Utility.Utility.setCurrentTaxPayer(MasterItems.TaxPayerType.self);
            Response.Redirect("EmployerDetails.aspx");
        }

        protected void btnEditClientDetails_Click(object sender, EventArgs e)
        {
            Utility.Utility.setCurrentTaxPayer(MasterItems.TaxPayerType.self);
            Response.Redirect("ClientDetails.aspx");
        }

        protected void btnEditResDetails_Click(object sender, EventArgs e)
        {
            Utility.Utility.setCurrentTaxPayer(MasterItems.TaxPayerType.self);
            Response.Redirect("ResidencyDetails.aspx");
        }

        protected void btnEditSpouseEmpDetails_Click(object sender, EventArgs e)
        {
            Utility.Utility.setCurrentTaxPayer(MasterItems.TaxPayerType.Spouse);
            Response.Redirect("EmployerDetails.aspx");
        }

        protected void btnSpouseClientDetails_Click(object sender, EventArgs e)
        {
            Utility.Utility.setCurrentTaxPayer(MasterItems.TaxPayerType.Spouse);
            Response.Redirect("ClientDetails.aspx");
        }

        protected void btnEditSpouseDetails_Click(object sender, EventArgs e)
        {
            Utility.Utility.setCurrentTaxPayer(MasterItems.TaxPayerType.Spouse);
            Response.Redirect("ResidencyDetails.aspx");
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            Response.Redirect("UploadFile.aspx");
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            btnNext.Visible = true;
            btnConfirmDown.Visible = false;
            btnConfirm.Visible = false;
            btnNext.Visible = true;
            BO_TaxPayerTaxFilingStatusDetails objTaxPayerTaxFilingStatusDetails = new BO_TaxPayerTaxFilingStatusDetails();
            objTaxPayerTaxFilingStatusDetails.TaxPayerID = Utility.Utility.getLoginSession().TaxPayerID;
            objTaxPayerTaxFilingStatusDetails.TaxFilingStatusTypeID = (int)MasterItems.TaxFillingStatusTypes.FillTaxInformation;
            bool isSucess = new StatusDAL().ConfirmFileStaus(objTaxPayerTaxFilingStatusDetails);
            if (isSucess)
            {
                //lblmsg.Text = "Confirmed Sucessfully";
                Response.Redirect("UploadFile.aspx");

            }
        }
    }
}