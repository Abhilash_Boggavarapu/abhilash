﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ETaxDal;
using ETaxBO;

namespace ETaxFilling
{
    public partial class ResidencyDetails : System.Web.UI.Page
    {
        ResidentDetailsDAL objResidentDal = new ResidentDetailsDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            txtCity.Enabled = txtEndDate.Enabled = txtStartDate.Enabled = txtStateName.Enabled = true;
            lblMsg.Text = "";
            lblSpouse.Text = "";
            if (!Utility.Utility.checkLoginSession())
            {
                Response.Redirect("UserLogIn.aspx");
            }
            else
            {
                if (!IsPostBack)
                {
                    StatusDAL objStatus = new StatusDAL();
                    Utility.Utility.getLoginSession().CurrentTaxPayer = MasterItems.TaxPayerType.self;
                    BO_TaxPayerTaxFilingStatusDetails isUpdateable = objStatus.getFilingStatus((long)Utility.Utility.getLoginSession().TaxPayerID, (int)MasterItems.TaxFillingStatusTypes.FillTaxInformation);
                    if (isUpdateable != null)
                    {
                        Response.Redirect("FillTaxConfirm.aspx");
                        //btnSave.Visible = false;
                    }
                    else 
                    {
                        if (Utility.Utility.getLoginSession().CurrentTaxPayer == MasterItems.TaxPayerType.Spouse)
                            Utility.Utility.setCurrentTaxPayer(MasterItems.TaxPayerType.Spouse);
                        else
                            Utility.Utility.setCurrentTaxPayer(MasterItems.TaxPayerType.self);

                         if(Utility.Utility.getLoginSession().MaritalStatus == MasterItems.MaritalStatuses.Single.ToString())
                        {
                            lnkSpouse.Visible = false;
                        }
                       
                        bindData();
                    }
                }
            }
        }
        private void bindData()
        {
            long TaxPayerID = 0;
            if (Utility.Utility.getLoginSession().CurrentTaxPayer == MasterItems.TaxPayerType.self)
            {
                TaxPayerID = Utility.Utility.getLoginSession().TaxPayerID;
            }

            else if (Utility.Utility.getLoginSession().CurrentTaxPayer == MasterItems.TaxPayerType.Spouse)
            {
                if (Utility.Utility.getLoginSession().TaxPayerSpouseID != null && Utility.Utility.getLoginSession().TaxPayerSpouseID != 0)
                {
                    TaxPayerID = (long)Utility.Utility.getLoginSession().TaxPayerSpouseID;
                }
                else
                {
                    lblSpouse.Text = " Do not have spouse to save client details";
                    lblSpouse.ForeColor = System.Drawing.Color.Red;
                }
            }
            if (TaxPayerID != 0)
            {
              GrdResedentDetails.DataSource = objResidentDal.getResidencyDetails(TaxPayerID);
              GrdResedentDetails.DataBind();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtStateName.Text))
            {
                divRSDerror.InnerText = Utility.Utility.ReqStateName;              
                
            }
            else if (Validations.IsAlpha(txtStateName.Text))
            {
                divRSDerror.InnerText = Utility.Utility.ValStateName;              
                txtStateName.Text = string.Empty;
                txtStateName.Focus();
            }
            else if (string.IsNullOrEmpty(txtCity.Text))
            {
                divRSDerror.InnerText = Utility.Utility.ReqCityName;              
            }
            else if (Validations.IsAlpha(txtCity.Text))
            {
                divRSDerror.InnerText = Utility.Utility.ValCityName;
                txtCity.Text = string.Empty;
                txtCity.Focus();
            }
            else if (string.IsNullOrEmpty(txtStartDate.Text))
            {
                divRSDerror.InnerText = Utility.Utility.ReqSDate;
            }
            else if (!txtStartDate.Text.DataFormat())
            {
                divRSDerror.InnerText = Utility.Utility.ValSDate;
                txtStartDate.Text = string.Empty;
                txtStartDate.Focus();
            }
            else if (string.IsNullOrEmpty(txtEndDate.Text))
            {
                divRSDerror.InnerText = Utility.Utility.ReqEDate;
            }
            else if (!txtEndDate.Text.DataFormat())
            {
                divRSDerror.InnerText = Utility.Utility.ValEDate;
                txtEndDate.Text = string.Empty;
                txtEndDate.Focus();
            }
            else if (Convert.ToDateTime(txtStartDate.Text) > Convert.ToDateTime(txtEndDate.Text))
            {
                divRSDerror.InnerText = Utility.Utility.LessSEDate;
                txtEndDate.Text = string.Empty;
                txtEndDate.Focus();
            }
            else
            {
                divRSDerror.InnerText = string.Empty;
                BO_tblTaxPayerResidencyDetail objtblTaxPayerResidencyDetail = new BO_tblTaxPayerResidencyDetail();
                if (ViewState["ResidencyDetailsID"] != null)
                    objtblTaxPayerResidencyDetail.TaxPayerResidencyDetailsID = Convert.ToInt64(ViewState["ResidencyDetailsID"]);
                if (Utility.Utility.getLoginSession().CurrentTaxPayer == MasterItems.TaxPayerType.self)
                {
                    objtblTaxPayerResidencyDetail.TaxPayerID = Utility.Utility.getLoginSession().TaxPayerID;
                }

                else if (Utility.Utility.getLoginSession().CurrentTaxPayer == MasterItems.TaxPayerType.Spouse)
                {
                    if (Utility.Utility.getLoginSession().TaxPayerSpouseID != null && Utility.Utility.getLoginSession().TaxPayerSpouseID != 0)
                    {
                        objtblTaxPayerResidencyDetail.TaxPayerID = (long)Utility.Utility.getLoginSession().TaxPayerSpouseID;
                    }
                    else
                    {
                        lblSpouse.Text = "Do not have spouse to save client details";
                    }
                }
                objtblTaxPayerResidencyDetail.StateName = txtStateName.Text;
                objtblTaxPayerResidencyDetail.CityName = txtCity.Text;
                objtblTaxPayerResidencyDetail.StartDate = DateTime.Parse(txtStartDate.Text);
                objtblTaxPayerResidencyDetail.EndDate = DateTime.Parse(txtEndDate.Text);
                objResidentDal.insertOrUpdateResidencyDetails(objtblTaxPayerResidencyDetail);
                bindData();
                lblMsg.ForeColor = System.Drawing.Color.Green;
                if (btnSave.Text == "Save")
                    lblMsg.Text = "Your Residency Details are submited.";
                else
                    lblMsg.Text = "Your Residency Details are Updated successfully";
                Utility.Clear.ClearDependents(this.Page);
                btnSave.Text = "Save";
            }
        }
        protected void GrdResedentDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "EditROW":
                    btnSave.Text = "Save";
                    lblMsg.Text = "";
                    ViewState["ResidencyDetailsID"] = e.CommandArgument;
                    SetResidencyDetails();
                    btnSave.Text = "Update";
                    break;
                case "Remove":
                    break;
            }
        }
        private void SetResidencyDetails()
        {
            if (ViewState["ResidencyDetailsID"] != null)
            {
                BO_tblTaxPayerResidencyDetail objTaxPayerResidencyDetail = objResidentDal.getResidencySingleRecord(Convert.ToInt64(ViewState["ResidencyDetailsID"]));
                if (!objTaxPayerResidencyDetail.Equals(null))
                {
                    txtStateName.Text = objTaxPayerResidencyDetail.StateName;
                    txtCity.Text = objTaxPayerResidencyDetail.CityName;
                    txtStartDate.Text = objTaxPayerResidencyDetail.StartDate.ToShortDateString();
                    txtEndDate.Text = objTaxPayerResidencyDetail.EndDate.ToShortDateString();
                }
            }
        }

        protected void lnkTaxPayer_Click(object sender, EventArgs e)
        {
            lblMsg.Text = "";
            lblSpouse.Text = "";
            lblClientDetails.Text = "Taxpayer Residency Details";
            ViewState["ResidencyDetailsID"] = null;
            Utility.Clear.ClearDependents(this.Page);
            Utility.Utility.setCurrentTaxPayer(MasterItems.TaxPayerType.self);
            bindData();
        }
        protected void lnkSpouse_Click(object sender, EventArgs e)
        {
            lblClientDetails.Text = "Spouse Residency details";
            if (Utility.Utility.getLoginSession().TaxPayerSpouseID == null || Utility.Utility.getLoginSession().TaxPayerSpouseID == 0)
            {
                lblMsg.Text = "Please enter Spouse Personal details";
                lblMsg.ForeColor = System.Drawing.Color.Red;
                GrdResedentDetails.DataSource = null;
                GrdResedentDetails.DataBind();
                Utility.Utility.setCurrentTaxPayer(MasterItems.TaxPayerType.Spouse);
                txtCity.Enabled = txtEndDate.Enabled = txtStartDate.Enabled = txtStateName.Enabled = false;
            }
            else
            {   
                ViewState["ResidencyDetailsID"] = null;
                Utility.Clear.ClearDependents(this.Page);
                Utility.Utility.setCurrentTaxPayer(MasterItems.TaxPayerType.Spouse);
                bindData();
            }
        }
        protected void GrdResedentDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GrdResedentDetails.PageIndex = e.NewPageIndex;
            bindData();
        }
       }
    }