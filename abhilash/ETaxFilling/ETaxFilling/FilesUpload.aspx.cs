﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ETaxBO;
using ETaxDal;
using System.IO;
namespace ETaxFilling
{
    public partial class FilesUpload : System.Web.UI.Page
    {
        int DocumentID;
        long taxPayerID;
        UploadDocumentsDAL objDocumentDal = new UploadDocumentsDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            Label1.Text = string.Empty;
            Label2.Text = string.Empty;
                List<BO_tblTaxPayerDocument> objDocumnents = objDocumentDal.getDocumentsCount(Utility.Utility.getLoginSession().TaxPayerID);
                foreach (var item in objDocumnents)
                {
                    switch ((MasterItems.Documents)item.DocumentID)
                    {
                        case MasterItems.Documents.WageTaxStatement:
                            lblCount.Text ="No of Files Uploaded:" + "   " + item.Count.ToString();
                            break;
                        case MasterItems.Documents.MiscellaneousIncomeStatement:
                            lblCount0.Text = "No of Files Uploaded:" + "  " + item.Count.ToString();
                            break;
                        case MasterItems.Documents.InterestIncomeStatement:
                            lblCount1.Text = "No of Files Uploaded:" + "  " + item.Count.ToString();
                            break;
                        case MasterItems.Documents.StateTaxRefund:
                            lblCount2.Text = "No of Files Uploaded:" + "  " + item.Count.ToString();
                            break;
                        case MasterItems.Documents.individualRetirementArrangement:
                            lblCount3.Text = "No of Files Uploaded:" + "  " + item.Count.ToString();
                            break;
                        case MasterItems.Documents.HomeMortgageStatement:
                            lblCount4.Text = "No of Files Uploaded:" + "  " + item.Count.ToString();
                            break;
                        case MasterItems.Documents.TuitionfeeStatement:
                            lblCount5.Text = "No of Files Uploaded:" + "  " + item.Count.ToString();
                            break;
                        case MasterItems.Documents.EducationInterestLoan:
                            lblCount6.Text = "No of Files Uploaded:" + "  " + item.Count.ToString();
                            break;
                        case MasterItems.Documents.Partnership:
                            lblCount7.Text = "No of Files Uploaded:" + "  " + item.Count.ToString();
                            break;
                        case MasterItems.Documents.Uploadotherdocument:
                            lblCount8.Text = "No of Files Uploaded:" + "  " + item.Count.ToString();
                            break;
                        default:
                            break;
                    }
                }
                if (!Utility.Utility.checkLoginSession())
                {
                    Response.Redirect("userlogin.aspx");
                }
                BO_TaxPayerTaxFilingStatusDetails isUpdateable = new StatusDAL().getFilingStatus((long)Utility.Utility.getLoginSession().TaxPayerID, (int)MasterItems.TaxFillingStatusTypes.UploadTaxDocuments);
                if (isUpdateable != null)
                {
                    Response.Redirect("View Documents.aspx");
                }
                taxPayerID = Utility.Utility.getLoginSession().TaxPayerID;

                lblMsg.Text = "";
                if (!IsPostBack)
                {
                    Utility.Utility.setCurrentTaxPayer(MasterItems.TaxPayerType.self);
                }
            }
        
        private void FileExtension(int DocumnetID, string Remarks, FileUpload fileUpload)
        {
            if (fileUpload.PostedFile.FileName == "")
            {
                Label1.ForeColor = System.Drawing.Color.Red;
                Label1.Text = "No File selected To Upload";
            }
            else
            {
                string[] validFileTypes = { "zip","rar","doc","docx", "pdf","jpeg","jpg" };
                string ext = System.IO.Path.GetExtension(fileUpload.PostedFile.FileName);
                bool isValidFile = false;
                for (int i = 0; i < validFileTypes.Length; i++)
                {
                    if (ext == "." + validFileTypes[i])
                    {
                        isValidFile = true;
                        break;
                    }
                }
                if (!isValidFile)
                {
                    Label1.ForeColor = System.Drawing.Color.Red;
                    Label1.Text = "Invalid File. Please upload a File with extension " +
                    string.Join(",", validFileTypes);
                }
                else
                {
                    SaveDocuments(DocumnetID, Remarks);
                    Label1.ForeColor = System.Drawing.Color.Green;
                    Label1.Text = "File uploaded successfully.";
                }
            }
        }
        private void SaveDocuments(int DocumnetID, string Remarks)
        {
            try
            {
                MasterDAL objMasterDal = new MasterDAL();

                BO_tblTaxPayerDocument objTaxDocument = new BO_tblTaxPayerDocument();
                // Get the HttpFileCollection

                HttpFileCollection hfc = Request.Files;
                for (int i = 0; i < hfc.Count; i++)
                {
                    HttpPostedFile hpf = hfc[i];
                    if (hpf.ContentLength == 0)
                    {
                        Label1.ForeColor = System.Drawing.Color.Red;
                        Label1.Text = "Can't  Upload Empty Files";
                    }
                    if (hpf.ContentLength > 0)
                    {
                        string fileName = hpf.FileName;
                        int fileSize = hpf.ContentLength;
                        byte[] documentBinary = new byte[fileSize];
                        hpf.InputStream.Read(documentBinary, 0, fileSize);
                        objTaxDocument.DocumentID = DocumnetID;
                        objTaxDocument.FileName = fileName;
                        objTaxDocument.FileContent = documentBinary;
                        objTaxDocument.Remarks = Remarks;
                        objTaxDocument.TaxPayerID = taxPayerID;
                        objDocumentDal.SaveTaxPayerDocuments(objTaxDocument);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            Label1.Text = string.Empty;
            int DocumentID = Convert.ToInt32(((Button)sender).CommandArgument);
            MasterItems.Documents docs = (MasterItems.Documents)DocumentID;
            switch (docs)
            {
                case MasterItems.Documents.WageTaxStatement:
                    display(DocumentID);     
                    lblMsg.Text = lblMsg.Text + "Wage Tax Statement";
                    break;
                case MasterItems.Documents.MiscellaneousIncomeStatement:
                    display(DocumentID);
                    lblMsg.Text = lblMsg.Text + "Miscellaneous Income Statement ";               
                    break;
                case MasterItems.Documents.InterestIncomeStatement:
                    display(DocumentID);
                    lblMsg.Text = lblMsg.Text + "Interest Income Statement";
                 
                    break;
                case MasterItems.Documents.StateTaxRefund:
                    display(DocumentID);
                    lblMsg.Text = lblMsg.Text + "State Tax Refund";
                    break;
                case MasterItems.Documents.individualRetirementArrangement:
                    display(DocumentID);
                    lblMsg.Text = lblMsg.Text + "Individual Retirement Arrangement";
                    break;
                case MasterItems.Documents.HomeMortgageStatement:
                    display(DocumentID);
                    lblMsg.Text = lblMsg.Text + "Home Mortgage Statement";
                    break;
                case MasterItems.Documents.TuitionfeeStatement:
                    display(DocumentID);
                    lblMsg.Text = lblMsg.Text + "Tuition Fee Statement";
                    break;
                case MasterItems.Documents.EducationInterestLoan:
                    display(DocumentID);
                    lblMsg.Text = lblMsg.Text + "Education Interest Loan";
                    break;
                case MasterItems.Documents.Partnership:
                    display(DocumentID);
                    lblMsg.Text = lblMsg.Text + "Partnership";
                    break;
                case MasterItems.Documents.Uploadotherdocument:
                    display(DocumentID);
                    lblMsg.Text = lblMsg.Text + "Upload Other Document";
                    break;
                default:
                    break;
            }
        }
        private void display(int documentID)
        {
            GridViewUpload.DataSource = null;
            GridViewUpload.DataBind();
            List<BO_tblTaxPayerDocument> objDoc = objDocumentDal.GetTaxpayerDocuments(documentID, taxPayerID);
            if (objDoc.Count != 0)
            {
                GridViewUpload.DataSource = objDoc;
                GridViewUpload.DataBind(); 
            }
            else
            {
                lblMsg.Text = "No Files Found for ";
            }
        }

       protected void GridViewUpload_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Download")
            {
                BO_tblTaxPayerDocument objDocument = objDocumentDal.GetTaxpayerFile(Convert.ToInt32(e.CommandArgument));
                byte[] fileData = objDocument.FileContent;
                Response.ClearContent();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + objDocument.FileName);
                BinaryWriter bw = new BinaryWriter(Response.OutputStream);
                bw.Write(fileData);
                bw.Close();
                Response.ContentType = "application/pdf";
                Response.End();
            }
        }
          string Remarks; 
        protected void btnW2Form_Click(object sender, EventArgs e)
         {
            DocumentID = Convert.ToInt32(MasterItems.Documents.WageTaxStatement);
            Remarks = txtW2Form.Text;
            FileExtension(DocumentID, Remarks, FileUpload1);
            txtW2Form.Text = string.Empty;       
        }
        protected void btnForm1099_Click(object sender, EventArgs e)
        {
            DocumentID = Convert.ToInt32(MasterItems.Documents.MiscellaneousIncomeStatement);
            Remarks = txtForm1099.Text;
            FileExtension(DocumentID, Remarks, FileUpload2);
            txtForm1099.Text = string.Empty;    
        }
        protected void btnInt1099_Click(object sender, EventArgs e)
        {
            DocumentID = Convert.ToInt32(MasterItems.Documents.InterestIncomeStatement);
            Remarks = txtInt1099.Text;
            FileExtension(DocumentID, Remarks, FileUpload3);
            txtInt1099.Text = string.Empty;
         
        }
        protected void btnStateTaxRefundClick(object sender, EventArgs e)
        {
            DocumentID = Convert.ToInt32(MasterItems.Documents.StateTaxRefund);
            Remarks = txtStateTaxRefund.Text;
            FileExtension(DocumentID, Remarks, FileUpload4);
            txtStateTaxRefund.Text = string.Empty;
            
        }
        protected void btnR1099_Click(object sender, EventArgs e)
        {
            DocumentID = Convert.ToInt32(MasterItems.Documents.individualRetirementArrangement);
            Remarks = txtR1099.Text;
            FileExtension(DocumentID, Remarks, FileUpload5);
            txtR1099.Text = string.Empty;
        }
        protected void btnHomeMortage_Click(object sender, EventArgs e)
        {
            DocumentID = Convert.ToInt32(MasterItems.Documents.HomeMortgageStatement);
            Remarks = txtHomeMortageStatement.Text;
            FileExtension(DocumentID, Remarks, FileUpload6);
            txtHomeMortageStatement.Text = string.Empty;
            
        }
        protected void btnT1098_Click(object sender, EventArgs e)
        {
            DocumentID = Convert.ToInt32(MasterItems.Documents.TuitionfeeStatement);
            Remarks = txtT1O98.Text;
            FileExtension(DocumentID, Remarks, FileUpload7);
            txtT1O98.Text = string.Empty;  
        }
        protected void btnE1098_Click(object sender, EventArgs e)
        {
            DocumentID = Convert.ToInt32(MasterItems.Documents.EducationInterestLoan);
            Remarks = txtE1098.Text;
            FileExtension(DocumentID, Remarks, FileUpload8);
            txtE1098.Text = string.Empty;  
        }
        protected void btnK1065_Click(object sender, EventArgs e)
        {
            DocumentID = Convert.ToInt32(MasterItems.Documents.Partnership);
            Remarks = txtK1065.Text;
            FileExtension(DocumentID, Remarks, FileUpload9);
            txtK1065.Text = string.Empty;       
        }
        protected void btnUploadOtherDocument_Click(object sender, EventArgs e)
        {
            DocumentID = Convert.ToInt32(MasterItems.Documents.Uploadotherdocument);
            Remarks = txtUploadOtherDocument.Text;
            FileExtension(DocumentID, Remarks, FileUpload10);
            txtUploadOtherDocument.Text = string.Empty;
        }
        protected void GridViewUpload_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridViewUpload.PageIndex = e.NewPageIndex;
            display(DocumentID);
        }
      }
   }
