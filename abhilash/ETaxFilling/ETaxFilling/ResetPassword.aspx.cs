﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ETaxDal;
using ETaxBO;

namespace ETaxFilling
{
    public partial class ResetPassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblStatus.Text = string.Empty;
        }
        protected void btnReset_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtEmail.Text))
            {
                divFPerr.InnerText = Utility.Utility.ReqNEmailId;
            }
            else if (!txtEmail.Text.IsValidEmailID())
            {
                divFPerr.InnerText = Utility.Utility.ValNEmailId;
                txtEmail.Text = string.Empty;
                txtEmail.Focus();
            }
            else
            {
                MasterDAL objMasterDal = new MasterDAL();
                divFPerr.InnerText = string.Empty;
                ChangeSettingsDAL objChangeDal = new ChangeSettingsDAL();
                BO_tblUser objUser = new BO_tblUser();
                objUser.Password = GetRandomPasswordUsingGUID(6);
                if (objMasterDal.checkEmailId(txtEmail.Text) != null)
                {
                    lblStatus.Text = "";
                    objChangeDal.ResetNewPassword(txtEmail.Text, objUser);
                    Utility.MailMessage ms = new Utility.MailMessage();
                    ms.Subject = "Etax Filing Forgot Password";
                    string pwd = string.Empty;
                    string toMailId = string.Empty;

                    toMailId = txtEmail.Text;

                    ms.From = "info@dtptax.com";
                    ms.To = toMailId;

                    string domain;
                    Uri url = HttpContext.Current.Request.Url;
                    domain = url.AbsoluteUri.Replace(url.PathAndQuery, string.Empty);

                    ms.Body = @"<a href=" + domain + "/UserLogin.aspx > Click here to login</a> <br>" + "Your New password is : " + objUser.Password + "<br>";
                    ms.isBodyHtml = true;

                    ms.SendMail();

                    lblStatus.Text = "Password has been mailed successfully.";
                    lblStatus.ForeColor = System.Drawing.Color.Green;
                    txtEmail.Text = string.Empty;
                }
                else
                {
                    lblStatus.Text = "Email Does not Exist";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    txtEmail.Text = string.Empty; txtEmail.Focus();
                }
            }
        }
        public string GetRandomPasswordUsingGUID(int length)
        {
            // Get the GUID
            string guidResult = System.Guid.NewGuid().ToString();

            // Remove the hyphens
            guidResult = guidResult.Replace("-", string.Empty);

            // Make sure length is valid
            if (length <= 0 || length > guidResult.Length)
                throw new ArgumentException("Length must be between 1 and " + guidResult.Length);

            // Return the first length bytes
            return guidResult.Substring(0, length);
        }

    }
}