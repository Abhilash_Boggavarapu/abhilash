﻿
namespace ETaxFilling.Administrator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Data;
    using ETaxDal;
    using ETaxBO;

    public partial class EmployerDetails : System.Web.UI.Page
    {
        EmployeDAL objDEmploye = new EmployeDAL();
        MasterDAL objMasterDal = new MasterDAL();
        long userId;
        protected void Page_Load(object sender, EventArgs e)
        {
            txtCity.Enabled = txtEmpEndDate.Enabled = txtEmployName.Enabled = txtEmpStartDate.Enabled = true;
            if (!Utility.Utility.CheckAdminLoginSession())
            {
                Response.Redirect("~/Administrator/AdminLogin.aspx");
            }
            else
            {
                if (!IsPostBack)
                {
                    txtCity.Visible = false;
                     if (Utility.Utility.getLoginSession().CurrentTaxPayer == MasterItems.TaxPayerType.Spouse)
                        {
                            lblClientDetails.Text = "Spouse Employer Details";
                            Utility.Utility.setCurrentTaxPayer(MasterItems.TaxPayerType.Spouse);
                        }
                        else
                        {
                            lblClientDetails.Text = "Taxpayer Employer Details";
                            Utility.Utility.setCurrentTaxPayer(MasterItems.TaxPayerType.self);
                        }
                        userId = Utility.Utility.getLoginSession().UserID;
                        if (Utility.Utility.getLoginSession().MaritalStatus ==MasterItems .MaritalStatuses .Single.ToString () )
                        {
                            lnkSpouse.Visible = false;
                        }                      
                        binddata();
                        ddlEmpCountry.DataSource = objMasterDal.getCountries();
                        ddlEmpCountry.DataTextField = "Country";
                        ddlEmpCountry.DataValueField = "CountryID";
                        ddlEmpCountry.DataBind();
                        ddlEmpCountry.Items.Insert(0, new ListItem { Text =Utility.Utility.setSelectCountry, Value = "0", Selected = true });
                        ddlEmpState.Items.Insert(0, new ListItem { Text = Utility.Utility.setSelectState, Value = "0", Selected = true });
                        ddlEmpCity.Items.Insert(0, new ListItem { Text = Utility.Utility.setSelectCity, Value = "0", Selected = true });                     
                    }                            
            }
        }
        protected void ddlEmpState_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtCity.Visible = false;
            fillCities();
        }
        private void fillCities()
        {
            ddlEmpCity.Items.Clear();
            int StateId = int.Parse(ddlEmpState.SelectedValue);
            ddlEmpCity.DataSource = objMasterDal.getCities(StateId);
            ddlEmpCity.DataTextField = "City";
            ddlEmpCity.DataValueField = "CityID";
            ddlEmpCity.DataBind();
            ddlEmpCity.Items.Insert(0, new ListItem { Text = Utility.Utility.setSelectCity, Value = "0", Selected = true });
            ddlEmpCity.Items.Insert(1, new ListItem { Text = Utility.Utility.setOtherCity, Value = "" });
        }
        protected void rbYes_CheckedChanged(object sender, EventArgs e)
        {
            
            lblMsg.Visible = false;
        }

        protected void btnAddEmploye_Click(object sender, EventArgs e)
        {
            lblMsg.Text = string.Empty;
            if (string.IsNullOrEmpty(txtEmployName.Text))
            {
                divEDerr.InnerText = Utility.Utility.ReqEmpName;
            }
            else if (txtEmployName.Text.IsAlpha())
            {
                divEDerr.InnerText = Utility.Utility.ValEmpName;
                txtEmployName.Text = string.Empty;
                txtEmployName.Focus();
            }
            else if (string.IsNullOrEmpty(txtEmpStartDate.Text))
            {
                divEDerr.InnerText = Utility.Utility.ReqSDate;
            }
            else if (!txtEmpStartDate.Text.DataFormat())
            {
                divEDerr.InnerText = Utility.Utility.ValSDate;
                txtEmpStartDate.Text = string.Empty;
                txtEmpStartDate.Focus();
            }
            else if (string.IsNullOrEmpty(txtEmpEndDate.Text))
            {
                divEDerr.InnerText = Utility.Utility.ReqEDate;
            }
            else if (!txtEmpEndDate.Text.DataFormat())
            {
                divEDerr.InnerText = Utility.Utility.ValEDate;
                txtEmpEndDate.Text = string.Empty;
                txtEmpEndDate.Focus();
            }
            else if (Convert.ToDateTime(txtEmpStartDate.Text) > Convert.ToDateTime(txtEmpEndDate.Text))
            {
                divEDerr.InnerText = Utility.Utility.LessSEDate;
            }
            else if (ddlEmpCountry.Items.Count == 0 || ddlEmpCountry.SelectedItem.Text == Utility.Utility.setSelectCountry)
            {
                divEDerr.InnerText = Utility.Utility.ReqCountry;
            }
            else if (ddlEmpState.Items.Count == 0 || ddlEmpState.SelectedItem.Text == Utility.Utility.setSelectState)
            {
                divEDerr.InnerText = Utility.Utility.ReqState;
            }
            else if (ddlEmpCity.Items.Count == 0 || ddlEmpCity.SelectedItem.Text == Utility.Utility.setSelectCity)
            {
                divEDerr.InnerText = Utility.Utility.ReqCity;
            }
            else
            {
                divEDerr.InnerText = string.Empty;
                BO_tblTaxPayerEmployerDetail objTaxEmploye = new BO_tblTaxPayerEmployerDetail();
                if (ViewState["EmployerDetailsID"] != null)
                    objTaxEmploye.TaxPayerEmployerID = Convert.ToInt64(ViewState["EmployerDetailsID"]);
                if (Utility.Utility.getLoginSession().CurrentTaxPayer == MasterItems.TaxPayerType.self)
                {
                    objTaxEmploye.TaxPayerID = Utility.Utility.getLoginSession().TaxPayerID;
                }

                else if (Utility.Utility.getLoginSession().CurrentTaxPayer == MasterItems.TaxPayerType.Spouse)
                {
                    if (Utility.Utility.getLoginSession().TaxPayerSpouseID != null && Utility.Utility.getLoginSession().TaxPayerSpouseID != 0)
                    {
                        objTaxEmploye.TaxPayerID = (long)Utility.Utility.getLoginSession().TaxPayerSpouseID;
                    }
                    else
                    {
                        lblSpouse.Text = "Do not have spouse to save Employer details";
                    }
                }
                objTaxEmploye.EmployerName = txtEmployName.Text;
                objTaxEmploye.EmploymentStartDate = DateTime.Parse(txtEmpStartDate.Text);
                objTaxEmploye.EmploymentEndDate = DateTime.Parse(txtEmpEndDate.Text);
                objTaxEmploye.CountryID = Convert.ToInt32(ddlEmpCountry.SelectedValue);
                if (Convert.ToInt32(ddlEmpCity.SelectedIndex) == 1)
                {
                    if (txtCity.Text == null)
                    {
                        divEDerr.InnerText = Utility.Utility.reqEnterCity;
                    }
                    else
                    {
                        BO_tblCity objTblCity = new BO_tblCity();
                        objTblCity.CountryID = Convert.ToInt32(ddlEmpCity.SelectedValue);
                        objTblCity.StateID = Convert.ToInt32(ddlEmpCity.SelectedValue);
                        objTblCity.City = txtCity.Text;
                        objTaxEmploye.CityID = objMasterDal.insertCity(objTblCity);
                    }
                }
                else
                {
                    objTaxEmploye.CityID = Convert.ToInt32(ddlEmpCity.SelectedValue);
                }
                if (divEDerr.InnerText == string.Empty)
                {
                    objTaxEmploye.StateID = Convert.ToInt32(ddlEmpState.SelectedValue);
                    objDEmploye.InsertUpdateEmploye(objTaxEmploye);
                    binddata();
                    lblMsg.Visible = true;
                    if (btnAddEmploye.Text == "Save")
                    {
                        lblMsg.Text = "Your Employer Details submited.You can add more Employers also";
                    }
                    else
                    {
                        lblMsg.Text = "Your Employer Details are successfully updated";
                        btnAddEmploye.Text = "Save";
                        ViewState["EmployerDetailsID"] = null;
                    }
                    Utility.Clear.ClearDependents(this.Page);
                    lblMsg.ForeColor = System.Drawing.Color.Green;
                }
            }
        }
        protected void GrdEmplist_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "EmpEdit":
                    Utility.Clear.ClearDependents(this.Page);
                    lblMsg.Text = string.Empty;                    
                    ViewState["EmployerDetailsID"] = e.CommandArgument;
                    setEmployer();
                    btnAddEmploye.Text = "Update";
                    break;
                case "Remove":
                    break;
            }
        }

        private void setEmployer()
        {
            if (ViewState["EmployerDetailsID"] != null)
            {
                BO_tblTaxPayerEmployerDetail objEmployerDetails = objDEmploye.GetEmployerDetails(Convert.ToInt64(ViewState["EmployerDetailsID"]));
                if (!objEmployerDetails.Equals(null))
                {
                    txtEmployName.Text = objEmployerDetails.EmployerName;
                    txtEmpStartDate.Text = objEmployerDetails.EmploymentStartDate.ToShortDateString();
                    txtEmpEndDate.Text = objEmployerDetails.EmploymentEndDate.ToShortDateString();
                    ddlEmpCountry.SelectedValue = objEmployerDetails.CountryID.ToString();
                    fillStates();
                    ddlEmpState.SelectedValue = objEmployerDetails.StateID.ToString();
                    fillCities();
                    ddlEmpCity.SelectedValue = objEmployerDetails.CityID.ToString();
                }
            }
        }
        protected void ddlEmpCountry_SelectedIndexChanged1(object sender, EventArgs e)
        {
            fillStates();
        }

        private void fillStates()
        {
            ddlEmpState.Items.Clear();
            ddlEmpCity.Items.Clear();
            int CountryId = int.Parse(ddlEmpCountry.SelectedValue);
            ddlEmpState.DataSource = objMasterDal.getStates(CountryId);
            ddlEmpState.DataTextField = "StateName";
            ddlEmpState.DataValueField = "StateID";
            ddlEmpState.DataBind();
            ddlEmpState.Items.Insert(0, new ListItem { Text = Utility.Utility.setSelectState, Value = "0", Selected = true });
            ddlEmpCity.Items.Insert(0, new ListItem { Text = Utility.Utility.setSelectCity, Value = "0", Selected = true });
        }
        private void binddata()
        {
            long TaxPayerID = 0;
            if (Utility.Utility.getLoginSession().CurrentTaxPayer == MasterItems.TaxPayerType.self)
            {
                TaxPayerID = Utility.Utility.getLoginSession().TaxPayerID;
            }

            else if (Utility.Utility.getLoginSession().CurrentTaxPayer == MasterItems.TaxPayerType.Spouse)
            {
                if (Utility.Utility.getLoginSession().TaxPayerSpouseID != null && Utility.Utility.getLoginSession().TaxPayerSpouseID != 0)
                {
                    TaxPayerID = (long)Utility.Utility.getLoginSession().TaxPayerSpouseID;
                }
                else
                {
                    lblSpouse.Text="Do not have spouse to save Employer Details ";
                }
            }
            if (TaxPayerID != 0)
            {

                GrdEmplist.DataSource = objDEmploye.GetTaxPayerEmployersDetails(TaxPayerID);
                GrdEmplist.DataBind();
            }
        }

        protected void lnkTaxPayer_Click(object sender, EventArgs e)
        {
            lblClientDetails.Text = "Taxpayer Employer Details";
            ViewState["EmployerDetailsID"] = null;
            Utility.Clear.ClearDependents(this.Page);
            btnAddEmploye.Text = "Save";
            Utility.Utility.setCurrentTaxPayer(MasterItems.TaxPayerType.self);
            binddata();
            lblMsg.Text = string.Empty;
            lblSpouse.Text = string.Empty;
        }

        protected void lnkSpouse_Click(object sender, EventArgs e)
        {
            lblMsg.Text = string.Empty;
            lblSpouse.Text = string.Empty;
            if (Utility.Utility.getLoginSession() == null || Utility.Utility.getLoginSession().TaxPayerSpouseID == 0)
            {
                lblMsg.Text = "Please enter Spouse personal Details ";
                lblMsg.ForeColor = System.Drawing.Color.Red;
                GrdEmplist.DataSource = null;
                GrdEmplist.DataBind();
                Utility.Utility.setCurrentTaxPayer(MasterItems.TaxPayerType.Spouse);
                txtCity.Enabled = txtEmpEndDate.Enabled = txtEmployName.Enabled = txtEmpStartDate.Enabled = true;
            }
            else
            {
                lblClientDetails.Text = "Spouse Employer Details";
                ViewState["EmployerDetailsID"] = null;
                Utility.Utility.setCurrentTaxPayer(MasterItems.TaxPayerType.Spouse);
                btnAddEmploye.Text = "Save";
                Utility.Clear.ClearDependents(this.Page);
                binddata();
            }
        }

        protected void GrdEmplist_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GrdEmplist.PageIndex = e.NewPageIndex;
            binddata();
        }

        protected void ddlEmpCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt32(ddlEmpCity.SelectedIndex) == 1)
            {
                txtCity.Visible = true;
            }
            else
            {
                txtCity.Visible = false;
            }
        }
    }
}