﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ETaxDal;
using ETaxBO;
using ETaxFilling.Administrator;

namespace ETaxFilling
{
    public partial class RelatonshipType1 : System.Web.UI.Page
    {
        RelationshipDAL objDRelationship = new RelationshipDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Utility.Utility.CheckAdminLoginSession())
            {
                Response.Redirect("~/Administrator/AdminLogin.aspx");
            }
            lblMsg.Text = "";
            databind();
        }
        private void databind()
        {
            GrdwRelationship.DataSource = objDRelationship.GetRelationDetails();
            GrdwRelationship.DataBind();

        }

        protected void btnSaveRelation_Click1(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtRelation.Text))
            {
                divReTerr.InnerText = Utility.Utility.ReqRelationshipType;
            }
            else if (txtRelation.Text.IsAlpha())
            {
                divReTerr.InnerText = Utility.Utility.ValRelationshipType;
            }
            else
            {
                BO_tblRelationship objRelationship = new BO_tblRelationship();
                if (ViewState["RelationShipID"] != null)
                    objRelationship.RelationShipID = Convert.ToInt32(ViewState["RelationShipID"]);
                objRelationship.RelationShip = txtRelation.Text;
                objDRelationship.SaveUpdateRelationship(objRelationship);
                lblMsg.Text = "The Relationship Details are submited";
                lblMsg.ForeColor = System.Drawing.Color.Green;
                btnSaveRelation.Text = "Save";
                txtRelation.Text = string.Empty;
                databind();
            }
        }
         protected void GrdwRelationship_RowCommand(object sender, GridViewCommandEventArgs e)
         {
             switch (e.CommandName)
             {
                 case "EditRel":

                     BO_tblRelationship objRel = objDRelationship.GetRelationship(Convert.ToInt32(e.CommandArgument));
                     if (!objRel.Equals(null))
                     {
                         GridViewRow row = (GridViewRow)((LinkButton)e.CommandSource).NamingContainer;

                         ViewState["RelationShipID"] = Convert.ToInt32(e.CommandArgument);
                         txtRelation.Text = row.Cells[2].Text;
                         btnSaveRelation.Text = "Update";
                     }

                     break;

                 case "Remove":

                     break;
             }

         }
    }
}