﻿<%@ Page Language="C#" MasterPageFile="~/Administrator/Admin.Master" AutoEventWireup="true" CodeBehind="ClientDetails.aspx.cs" Inherits="ETaxFilling.Administrator.ClientDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">  
     <script type="text/javascript">
         $(function () {
             $("#txtProjectStartDate,#txtProjectEndDate").datepicker({
                 showOn: "button",
                 buttonImage: "../B_Images/calendar_icon.png",
                 changeMonth: true,
                 yearRange: YEAR_RANGE_GLOBAL,
                 changeYear: true,
                 buttonImageOnly: true
             });
         });
    </script>
    <asp:ScriptManager runat="server" />
    <div align="center" >
    <table>
        <tr>
            <td>
                <div>
                    <ul class="submenu">
                        <li><a href="EmployerDetails.aspx">Employer Details</a></li>
                        <li><a style="color: ButtonHighlight" href="ClientDetails.aspx">Client Details</a></li>
                        <li><a href="ResidencyDetails.aspx">Residency Details</a></li>
                    </ul>
                </div>
            </td>
        </tr>
    </table>
    <table>
    <tr>
            <td>
                <asp:LinkButton runat="server" ID="lnkTaxPayer" Text="Tax Payer" CssClass="button" OnClick="lnkTaxPayer_Click"></asp:LinkButton>
            </td>
            <td>
                <asp:LinkButton runat="server" ID="lnkSpouse" Text="Spouse" CssClass="button" OnClick="lnkSpouse_Click"></asp:LinkButton>
            </td>
        </tr>
       
    </table>
    <table width="700px"class="box">
               <tr>
        <td align="center">
       <h3>
           <asp:Label ID="lblClientDetails" runat="server" Text=" TaxPayer Client Details" />
       </h3>
        </td>
        </tr>
           </table>
         
                     <div runat="server" id="divCDErr" style="color:#FF0000"></div> 
    <br />
      <div>
      <h4>
          <asp:Label Text="" ID="lblMsg" runat="server" /></h4>
      </div>
                    <table>
                        <tr>
                            <td style="text-align: right">
                             <span style="color: #FF0000">*</span>Select Employer :
                            </td>
                            <td style="text-align: left">
                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>
                                    
                                <asp:DropDownList runat="server" ID="ddlEmployeer" Height="20px" Width="150px" 
                                            TabIndex="3">
                                    <asp:ListItem></asp:ListItem>
                                </asp:DropDownList>
                                </ContentTemplate>
                                </asp:UpdatePanel>
                               
                            </td>
                            <td>
                                <asp:Label Text="" ID="lblSEmp" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                <span style="color: #FF0000">*</span>Name : 
                            </td>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtClientName" runat="server" CssClass="ValidationRequired"
                                    MaxLength="50" TabIndex="4" Height="20px" Width="150px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                <span style="color: #FF0000">* </span>Project Start Date : 
                            </td>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtProjectStartDate" ClientIDMode="Static" runat="server" 
                                    CssClass="ValidationRequired date" TabIndex="5" Height="20px" 
                                    Width="150px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                <span style="color: #FF0000">* </span>Project End Date : 
                            </td>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtProjectEndDate" ClientIDMode="Static" runat="server" 
                                    CssClass="ValidationRequired date " TabIndex="6" Height="20px" 
                                    Width="150px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                <span style="color: #FF0000">*</span>Country : 
                            </td>
                            <td style="text-align: left " > 
                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>
                                       
                                <asp:DropDownList ID="ddlClientCountry" runat="server" Height="20px" Width="150px" CssClass="ValidationRequired"
                                    OnSelectedIndexChanged="ddlClientCountry_SelectedIndexChanged" AutoPostBack="True" 
                                            TabIndex="7">
                                </asp:DropDownList>
                                 </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                <span style="color: #FF0000">*</span>State : 
                            </td>
                            <td style="text-align: left">
                             <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                     
                                <asp:DropDownList ID="ddlClientState" runat="server"  Height="20px" Width="150px" CssClass="ValidationRequired"
                                    OnSelectedIndexChanged="ddlClientState_SelectedIndexChanged" AutoPostBack="True" 
                                            TabIndex="8">
                                </asp:DropDownList>
                                </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                <span style="color: #FF0000">*</span>City : 
                            </td>
                            <td style="text-align: left">
                             <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                    <ContentTemplate>
                                <asp:DropDownList ID="ddlClientCity" runat="server" Height="20px" Width="150px" 
                                            CssClass="formButton" TabIndex="9" AutoPostBack="True" 
                                            onselectedindexchanged="ddlClientCity_SelectedIndexChanged">
                                </asp:DropDownList>
                                </ContentTemplate>
                                </asp:UpdatePanel>
                            </td> 
                        </tr>
                        <tr id="trcity" runat="server">
                <td></td>
                <td align="left">
                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>
              <asp:TextBox ID="txtCity" Height="20px"   Width="150px" runat="server" />
               </ContentTemplate></asp:UpdatePanel>
                   
                </td>
                </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <asp:Button ID="btnSave" runat="server" CssClass="button" OnClick="btnSave_Click"
                                    Text="Save" Height="30px" Width="90px" TabIndex="10"/>
                               
                            </td>
                        </tr>
                        <tr>
                        <td></td>
                            <td align="right">
                                <asp:HyperLink  Text="Next" NavigateUrl="~/Administrator/ResidencyDetails.aspx" 
                                    CssClass="button" Width="56px" runat="server" />
                            </td>
                        </tr>
                        </table>
                      
                        <table style="width: 100%">
        <tr>
            <td></td><td></td><td></td>
                            <td align ="center" >
                                &nbsp;</td>
        </tr>
    </table>
                   
               
                    
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr id="gridDisplay" runat="server">
                                    <td>
                                        <asp:GridView ID="GrdClientlist" runat="server" 
                                            AlternatingRowStyle-CssClass="alt"  margin="5%"
                                            AutoGenerateColumns="False" PagerStyle-CssClass="pgr" Width="510px"
                                            OnRowCommand="GrdClientlist_RowCommand" HorizontalAlign="Center" AllowPaging="True" 
                                            onpageindexchanging="GrdClientlist_PageIndexChanging" CssClass="mGrid">
                                            <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
                                            <Columns>
                                                <asp:TemplateField HeaderText="SNo">
                                                    <ItemTemplate>
                                                        <%#Container.DataItemIndex+1 %><br />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="TaxPayerClientID" Visible="false" />
                                                <asp:BoundField DataField="ClientName" HeaderText="Client Name" SortExpression="EID" />
                                                <asp:BoundField DataField="ProjectStartDate" HeaderText="Project Start Date" 
                                                    DataFormatString="{0:d}" HtmlEncode="False" />
                                                <asp:BoundField DataField="ProjectEndDate" HeaderText="Project End Date" 
                                                    DataFormatString="{0:d}" HtmlEncode="False" />
                                                <asp:BoundField DataField="CountryName" HeaderText="Country" />
                                                <asp:BoundField DataField="StateName" HeaderText="State" />
                                                <asp:BoundField DataField="CityName" HeaderText="City" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkbtnEdit" runat="server" Text="Edit" CommandArgument='<%# DataBinder.Eval(Container,"DataItem.TaxPayerClientID") %>'
                                                            CommandName="ClientEdit"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%--  <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="lnkbtnRemove" runat="server" Text="Remove" CommandArgument='<%# DataBinder.Eval(Container,"DataItem.TaxPayerClientID") %>'
                        CommandName="Remove" ></asp:LinkButton>                       
                </ItemTemplate>
            </asp:TemplateField>--%>
                                            </Columns>
                                            <PagerSettings FirstPageText="First" LastPageText="Last" />
                                            <PagerStyle CssClass="pgr">
                                            </PagerStyle>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                            </div>
                       
       
</asp:Content>
