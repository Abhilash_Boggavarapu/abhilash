﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ETaxDal;
using ETaxBO;
namespace ETaxFilling.Administrator
{
    public partial class View_Documents : System.Web.UI.Page
    {
        UploadDocumentsDAL objDocument = new UploadDocumentsDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Utility.Utility.CheckAdminLoginSession())
            {
                Response.Redirect("~/Administrator/AdminLogin.aspx");
            }
            else
            {
                BO_TaxPayerTaxFilingStatusDetails isUpdateable = new StatusDAL().getFilingStatus((long)Utility.Utility.getLoginSession().TaxPayerID, (int)MasterItems.TaxFillingStatusTypes.UploadTaxDocuments);
                if (isUpdateable != null)
                {
                    hypEdit.Visible = false;
                    btnConfirm.Visible = false;
                }
            }
            if (!IsPostBack)
            {

                Display();
            } 
        }
        protected void btnDisplayDocuments_Click(object sender, EventArgs e)
        {
            BO_TaxPayerTaxFilingStatusDetails objTaxPayerTaxFilingStatusDetails = new BO_TaxPayerTaxFilingStatusDetails();
            objTaxPayerTaxFilingStatusDetails.TaxPayerID = Utility.Utility.getLoginSession().TaxPayerID;
            objTaxPayerTaxFilingStatusDetails.TaxFilingStatusTypeID = (int)MasterItems.TaxFillingStatusTypes.UploadTaxDocuments ;
            bool isSucess = new StatusDAL().ConfirmFileStaus(objTaxPayerTaxFilingStatusDetails);
            if (isSucess)
            {
                GdwDocuments.Visible = false;
                hypEdit.Visible = false;
                lblmeg.Text = "Your Files are Uploaded Successfully"; 
            }
        }

        private void Display()
        {
             List<BO_tblTaxPayerDocument> objDoc=objDocument.GetTaxpayerAllDocuments(Utility.Utility.getLoginSession().TaxPayerID);
            if (objDoc.Count !=0 )
            {
                GdwDocuments.DataSource = objDocument.GetTaxpayerAllDocuments(Utility.Utility.getLoginSession().TaxPayerID);
                GdwDocuments.DataBind();
            }
            else
            {
                lblmeg.Text = " No Documents Found";
            }
        }
    }
}