﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ETaxDal;
using ETaxDal.Admin;
using ETaxBO;

namespace ETaxFilling.Administrator
{
    public partial class PendingClients : System.Web.UI.Page
    {
        AdminMasterDAL objAdminMasterDal = new AdminMasterDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
           // objAdminMasterDal.SearchCompletedClients();
            if (!IsPostBack)
            {
                if (Utility.Utility.CheckAdminLoginSession() == false)
                {
                    Response.Redirect("AdminLogin.aspx");
                }
                ddlPendingClients.DataSource = objAdminMasterDal.getFileStatusTypes();
                ddlPendingClients.DataTextField = "TaxFilingStatusType";
                ddlPendingClients.DataValueField = "TaxFilingStatusTypeID";
                ddlPendingClients.DataBind();
                dispalyPendingList();
            }
        }

        protected void ddlPendingClients_SelectedIndexChanged(object sender, EventArgs e)
        {
            dispalyPendingList();
        }

        private void dispalyPendingList()
        {
            List<BO_tblTaxPayerDetail> objTaxPayerDetails = objAdminMasterDal.SearchPendingClients(Convert.ToInt32(ddlPendingClients.SelectedValue));
            if (objTaxPayerDetails.Count != 0)
            {
                grdDisplay.DataSource = objTaxPayerDetails;
                grdDisplay.DataBind();
            }
            else
            {
                lblMsg.Text = "No Records Found";
                lblMsg.ForeColor = System.Drawing.Color.Red;
            }
        }
        protected void grdDisplay_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "ViewData")
            {
                long Userid = Convert.ToInt64(e.CommandArgument.ToString());
                SearchDAL objAdminSearch = new SearchDAL();
                BO_tblTaxPayerDetail objTaxPayerDetail = objAdminSearch.GetTaxPayer(Userid);
                Utility.Utility.setLoginSession(objTaxPayerDetail);
                string FirstName = objTaxPayerDetail.UserFirstName;
                long UserId = objTaxPayerDetail.UserID;
                Response.Redirect("AdminHome.aspx");

            }
        }

        protected void grdDisplay_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdDisplay.PageIndex = e.NewPageIndex;
              dispalyPendingList();
        }
    }
}