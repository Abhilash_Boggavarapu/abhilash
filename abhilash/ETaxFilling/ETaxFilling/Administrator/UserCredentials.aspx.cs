﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ETaxDal;
using ETaxBO;

namespace ETaxFilling.Administrator
{
    public partial class UserCredentials : System.Web.UI.Page
    {
        string firstName; string emailID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Utility.Utility.CheckAdminLoginSession())
            {
                Response.Redirect("~/Administrator/AdminLogin.aspx");
            }
         lblerror.Text=string.Empty;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
         
            if (!string.IsNullOrWhiteSpace(txtFirstName.Text) || !string.IsNullOrWhiteSpace(txtEmail.Text))
            {


                if (!string.IsNullOrWhiteSpace(txtFirstName.Text))
                {
                     firstName = txtFirstName.Text;
                }
                if (!string.IsNullOrWhiteSpace(txtEmail.Text))
                {
                    if (txtEmail.Text.IsValidEmailID())
                    {
                        emailID = txtEmail.Text.Trim(); ;
                    }
                    else
                    {
                        lblerror.Text = " Please Enter Valid Email ID";
                        return;
                    }
                }
                display(firstName, emailID);
            }
        }

        private void display(string firstName, string emailID)
        {
            AdminMasterDAL objMasterDal = new AdminMasterDAL();
            List<BO_tblUser> objUser = objMasterDal.getUserUserCredentials(firstName, emailID);
            if (objUser.Count != 0)
            {
                grdList.DataSource = objUser;
                grdList.DataBind();
            }
            else
            {
                lblerror.Text = "No Records Found";
                lblerror.ForeColor = System.Drawing.Color.Red;
            }
        }
    }
}