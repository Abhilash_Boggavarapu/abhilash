﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ETaxDal;
using ETaxBO;

namespace ETaxFilling.Administrator
{
    public partial class DependentDetails : System.Web.UI.Page
    {
        DependentDAL objDTaxDependent = new DependentDAL();
        MasterDAL objMasterDal = new MasterDAL();
       long userId;
       StatusDAL objStausDal = new StatusDAL();
       protected void Page_Load(object sender, EventArgs e)
       {
           if (!Utility.Utility.CheckAdminLoginSession())
           {
               Response.Redirect("~/Administrator/AdminLogin.aspx");
           }
           else
           {
               if (objMasterDal.checkMatrialStatus(Utility.Utility.getLoginSession().TaxPayerID) == MasterItems.MaritalStatuses.Single.ToString())
               {
                   lnkSpouse.Enabled = false;
               }
               if (!IsPostBack)
               {
                   // rbYes.Checked = true;
                   Utility.Utility.setCurrentTaxPayer(MasterItems.TaxPayerType.self);
                   userId = Utility.Utility.getLoginSession().UserID;
                   lblMsg.Visible = false;
                   bindData();
                   fillVisaStatus();
                   fillRelationShip();
               }
           }
       }

        protected void btnSave_Click(object sender, EventArgs e)
        {
           
            if (string.IsNullOrEmpty(txtFname.Text))
            {
                divDDerr.InnerText = Utility.Utility.ReqFName;
            }
            else if (txtFname.Text.IsAlpha())
            {
                divDDerr.InnerText =Utility.Utility.ValFName;
                txtFname.Text = string.Empty;
                txtFname.Focus();
            }
            else if (string.IsNullOrEmpty(txtMname.Text))
            {
                divDDerr.InnerText = Utility.Utility.ReqMName;
            }
            else if (txtMname.Text.IsAlpha())
            {
                divDDerr.InnerText = Utility.Utility.ValMName;
                txtMname.Text = string.Empty;
                txtMname.Focus();
            }
            else if (string.IsNullOrEmpty(txtLname.Text))
            {
                divDDerr.InnerText = Utility.Utility.ReqLName;
            }
            else if (txtLname.Text.IsAlpha())
            {
                divDDerr.InnerText = Utility.Utility.ValLName;
                txtLname.Text = string.Empty;
                txtLname.Focus();
            }
            else if (string.IsNullOrEmpty(txtDob.Text))
            {
                divDDerr.InnerText =Utility.Utility.ReqDOB;
            }
            else if (!txtDob.Text.DataFormat())
            {
                divDDerr.InnerText = Utility.Utility.ValDOB;
                txtDob.Text = string.Empty;
                txtDob.Focus();
            }
            else if (ddlRelation.Items.Count == 0 || ddlRelation.SelectedItem.Text == Utility.Utility.setSelectRelationship)
            {
                divDDerr.InnerText = Utility.Utility.ReqRelationship;
            }
            else  if (txtSsn.Text.IsAlphaNumeric() && txtSsn.Text != string.Empty)
                {
                    divDDerr.InnerText = Utility.Utility.ValSSN;
                    txtSsn.Text = string.Empty;
                    txtSsn.Focus();
                }   
            else if (!txtSsn.Text.IsNumeric() && txtSsn.Text != string.Empty)
            {
                divDDerr.InnerText = Utility.Utility.ValSSN;
                txtSsn.Text = string.Empty;
                txtSsn.Focus();
            }
            else if (!txtDoE.Text.DataFormat() && txtDoE.Text != string.Empty)
            {
                divDDerr.InnerText = Utility.Utility.ValDOE;

                txtDoE.Text = string.Empty;
                txtDoE.Focus();
            }
            else
            {
                divDDerr.InnerText = string.Empty;
                BO_tblTaxPayerDependent objTaxDependent = new BO_tblTaxPayerDependent();
                if (ViewState["DepentDetailsID"] != null)
                    objTaxDependent.TaxPayerDependentID = Convert.ToInt64(ViewState["DepentDetailsID"]);
                objTaxDependent.TaxPayerID = Utility.Utility.getLoginSession().TaxPayerID;
                objTaxDependent.FirstName = txtFname.Text;
                objTaxDependent.MiddleName = txtMname.Text;
                objTaxDependent.LastName = txtLname.Text;
                objTaxDependent.DOB = DateTime.Parse(txtDob.Text);
                objTaxDependent.RelationShipID = Convert.ToInt32(ddlRelation.SelectedValue);
                objTaxDependent.SSN = txtSsn.Text;
                if (ddlVisa.SelectedValue == "")
                {
                    objTaxDependent.VisaStatusID = null;
                }
                else
                {
                    objTaxDependent.VisaStatusID = Convert.ToInt32(ddlVisa.SelectedValue);
                }
                if (txtDoE.Text != string.Empty)
                {
                    objTaxDependent.USEntryDate = DateTime.Parse(txtDoE.Text);
                }
                objDTaxDependent.InsertUpdateDependent(objTaxDependent);
                lblMsg.Visible = true;
                bindData();
                if (btnSave.Text == "Save")
                {
                    lblMsg.Text = "Your Dependent Details are submited.You can add more dependents also";                     
                }
                else
                {
                    lblMsg.Text = " Your Dependent Details are updated successfully";
                    btnSave.Text = "Save";
                    ViewState["DepentDetailsID"] = null;                                
                }
                lblMsg.ForeColor = System.Drawing.Color.Green;
                Utility.Clear.ClearDependents(this.Page);
            }           
        }
        
        protected void rbYes_CheckedChanged(object sender, EventArgs e)
        {         
            lblMsg.Visible = false;
        }
       
        protected void GrdDpntlist_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "DepnEdit":
                    lblMsg.Text = string.Empty;
                    divDDerr.InnerText = string.Empty;                 
                    ViewState["DepentDetailsID"] = e.CommandArgument;
                    dependGrid(e);
                    btnSave.Text = "Update";
                    break;
                case "Remove":
                    break;
            }
        }
       
        protected void lnkTaxPayer_Click(object sender, EventArgs e)
        {
            Utility.Utility.setCurrentTaxPayer(MasterItems.TaxPayerType.self);
            Response.Redirect("TaxPayerDetails.aspx");
            ddlRelation.Items.Clear();
            ddlVisa.Items.Clear();
        }

        protected void lnkSpouse_Click(object sender, EventArgs e)
        {
            Utility.Utility.setCurrentTaxPayer(MasterItems.TaxPayerType.Spouse);
            Response.Redirect("TaxPayerDetails.aspx");
        }
    
        private void dependGrid(GridViewCommandEventArgs e)
        {
            if (ViewState["DepentDetailsID"] != null)
            {
                BO_tblTaxPayerDependent objDpntDetails = objDTaxDependent.GetTaxPayerDependent(Convert.ToInt64(ViewState["DepentDetailsID"]));
                if (!objDpntDetails.Equals(null))
                {
                    HttpContext.Current.Session["DepentDetailsID"] = objDpntDetails;
                    txtFname.Text = objDpntDetails.FirstName;
                    txtMname.Text = objDpntDetails.MiddleName;
                    txtLname.Text = objDpntDetails.LastName;
                    txtDob.Text = objDpntDetails.DOB.ToShortDateString();
                    ddlRelation.SelectedValue = objDpntDetails.RelationShipID.ToString();
                    txtSsn.Text = objDpntDetails.SSN;

                    if (objDpntDetails.VisaStatus != null)
                    {
                        ddlVisa.SelectedValue = objDpntDetails.VisaStatusID.ToString();
          
                    }
                    else
                    {
                        fillVisaStatus();
                     }
                    if (objDpntDetails.USEntryDate != null)
                    {
                        txtDoE.Text = objDpntDetails.USEntryDate.Value.ToShortDateString();
                    }
                }
            }
        }
        private void fillVisaStatus()
        {
            ddlVisa.DataSource = objMasterDal.getVisaStatus();
            ddlVisa.DataTextField = "VisaStatus";
            ddlVisa.DataValueField = "VisaStatusID";
            ddlVisa.DataBind();
            ddlVisa.Items.Insert(0, new ListItem { Text = Utility.Utility.setSelectVisastatus, Value = "", Selected = true });
               }
        private void fillRelationShip()
        {

            ddlRelation.DataSource = objMasterDal.getRelationShips();
            ddlRelation.DataTextField = "RelationShip";
            ddlRelation.DataValueField = "RelationShipID";
            ddlRelation.DataBind();
            ddlRelation.Items.Insert(0, new ListItem { Text = Utility.Utility.setSelectRelationship, Value = "0", Selected = true });
              }
        private void bindData()
        {
            GrdDpntlist.DataSource = objDTaxDependent.GetTaxPayerDependents(Utility.Utility.getLoginSession().TaxPayerID);
            GrdDpntlist.DataBind();
        }

        protected void GrdDpntlist_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GrdDpntlist.PageIndex = e.NewPageIndex;
            bindData();
        }
    }
}
