﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ETaxBO;
using ETaxDal;
using System.IO;

namespace ETaxFilling.Administrator
{
    public partial class UploadSheet : System.Web.UI.Page
    {
        UploadDocumentsDAL objDocumentDal = new UploadDocumentsDAL();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Utility.Utility.CheckAdminLoginSession())
            {
                Response.Redirect("~/Administrator/AdminLogin.aspx");
            }
            lblMsg.Text = string.Empty;
            lblExten.Text = string.Empty;
            if (!IsPostBack)
            {
                btnDownload.Visible = false;
            }
        }
        private void fileExtension()
        {
            if (FileUpload1.PostedFile.FileName == "")
            {
                lblMsg.ForeColor = System.Drawing.Color.Red;
                lblMsg.Text = "No File selected To Upload";
            }
            else
            {
                string[] validFileTypes = { "xlsx","xls"};
                string ext = System.IO.Path.GetExtension(FileUpload1.PostedFile.FileName);
                bool isValidFile = false;
                for (int i = 0; i < validFileTypes.Length; i++)
                {
                    if (ext == "." + validFileTypes[i])
                    {
                        isValidFile = true;
                        break;
                    }
                }
                if (!isValidFile)
                {
                    lblExten.ForeColor = System.Drawing.Color.Red;
                    lblExten.Text = "Invalid File. Please upload a File with extension " +
                    string.Join(",", validFileTypes);
                }
                else
                {
                    saveDocuments();
                    lblExten.ForeColor = System.Drawing.Color.Green;
                    lblExten.Text = "File uploaded successfully.";

                }
            }
        }
        string remarks;
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            remarks = txtRemarks.Text;
            fileExtension();
            txtRemarks.Text = "";
        }

        private void saveDocuments()
        {
            try
            {
                BO_tblTaxSheets objSheets = new BO_tblTaxSheets();

                // Get the HttpFileCollection
                HttpFileCollection hfc = Request.Files;
                for (int i = 0; i < hfc.Count; i++)
                {
                    HttpPostedFile hpf = hfc[i];
                    if (hpf.ContentLength == 0)
                    {
                        lblMsg.ForeColor = System.Drawing.Color.Red;
                        lblMsg.Text = "Can't  Upload Empty Files";
                    }
                    if (hpf.ContentLength > 0)
                    {
                       int taxSheetId= objDocumentDal.TaxSheetID(Convert.ToInt32(MasterItems.TaxSheetSType.TaxInformation), (MasterItems.TaxSheetSource.Admin).ToString());

                        string fileName = hpf.FileName;
                        int fileSize = hpf.ContentLength;
                        byte[] documentBinary = new byte[fileSize];
                        hpf.InputStream.Read(documentBinary, 0, fileSize);
                        objSheets.Remarks = txtRemarks.Text;
                        objSheets.TaxSheetID = taxSheetId;
                        objSheets.SheetName = fileName;
                        objSheets.SheetsType = Convert.ToInt32(MasterItems.TaxSheetSType.TaxInformation);
                        objSheets.TaxSheet = documentBinary;
                        objSheets.TaxSheetSource = MasterItems.TaxSheetSource.Admin.ToString();
                        objDocumentDal.SaveSheet(objSheets);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
   
        protected void View_Click(object sender, EventArgs e)
        {
            btnDownload.Visible = true;

        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            downloadFile();
        }

        private void downloadFile()
        {
            BO_tblTaxSheets objTaxSheets = objDocumentDal.getTaxpayerExelSheet(Convert.ToInt32(MasterItems.TaxSheetSType.TaxInformation), (MasterItems.TaxSheetSource.Admin).ToString());
            byte[] fileData = objTaxSheets.TaxSheet;
            Response.ClearContent();
            Response.AddHeader("Content-Disposition", "attachment; filename=" + objTaxSheets.SheetName);
            BinaryWriter bw = new BinaryWriter(Response.OutputStream);
            bw.Write(fileData);
            bw.Close();
            Response.ContentType = "application/xlsx";
            Response.End();
        }
    }
} 