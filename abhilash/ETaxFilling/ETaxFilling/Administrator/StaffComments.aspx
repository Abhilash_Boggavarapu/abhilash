﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Administrator/Admin.Master" CodeBehind="StaffComments.aspx.cs" Inherits="ETaxFilling.Administrator.StaffComments" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <asp:TextBox  ID="txtComments" TextMode="MultiLine"  runat="server" Height="112px" 
                    Width="408px" />
            </td>
        </tr>
        <tr>
       <td>
            <asp:Button Text="Save" ID="btnSave" CssClass="button"  runat="server" 
                onclick="btnSave_Click" />
        </td>
        </tr>
        <tr>
        <td>
            <asp:Label Text="" ID="lblmsg" CssClass="error" runat="server" />
        </td>
        </tr>
        <tr>
        <td>
        <asp:GridView ID="grdStaffComments" runat="server" AutoGenerateColumns="False" 
        style="width:100%;" >
        <Columns>
<asp:TemplateField HeaderText="S.No.">
<ItemTemplate><%#Container.DataItemIndex+1 %><br />
</ItemTemplate>
</asp:TemplateField>
        
         <asp:BoundField DataField ="StaffCommentsID"   Visible="false"/> 
      <asp:BoundField DataField="StaffComments" HeaderText="Comments" />
      <asp:BoundField DataField="CreatedOn" HeaderText="Date"  DataFormatString="{0:d}" 
        HtmlEncode="False"/>
        </Columns>
    </asp:GridView>
        </td>
        </tr>
    </table>  
</asp:Content>
