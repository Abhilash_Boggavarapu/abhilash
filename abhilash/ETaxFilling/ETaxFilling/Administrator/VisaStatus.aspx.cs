﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ETaxBO;
using ETaxDal;

namespace ETaxFilling
{
    public partial class VisaStatus1 : System.Web.UI.Page
    {

        VisaStatusesDAL objDVisa = new VisaStatusesDAL();
        MasterDAL ObjVisa = new MasterDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Utility.Utility.CheckAdminLoginSession())
            {
                Response.Redirect("~/Administrator/AdminLogin.aspx");
            }
            else
            {
                lblMsg.Text = "";
                Databind();
            }
        }

        private void Databind()
        {
            grdVisaStatus.DataSource = ObjVisa.getVisaStatus();
            grdVisaStatus.DataBind();
        }

        protected void btnSaveVisa_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtVisa.Text))
            {
                divVSerr.InnerText = Utility.Utility.ReqVisaStatus;
            }
            else if (txtVisa.Text.IsAlphaNumeric())
            {
                divVSerr.InnerText = Utility.Utility.ValVisaStatus;
            }
            else
            {
                divVSerr.InnerText = string.Empty;
                BO_tblVisaStatus objVisa = new BO_tblVisaStatus();
                if (ViewState["VisaStatusID"] != null)
                objVisa.VisaStatusID = Convert.ToInt32(ViewState["VisaStatusID"]);
                objVisa.VisaStatus = txtVisa.Text;
                objDVisa.SaveUpdateVisaStatuses(objVisa);
                lblMsg.Text = "The Visa Status Details are submited";
                btnSaveVisa.Text = "Save";
                Databind();
                txtVisa.Text = string.Empty;
            }
        }
        protected void grdVisaStatus_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "EditVisa":

                    BO_tblVisaStatus objVisa = ObjVisa.GetVisa(Convert.ToInt32(e.CommandArgument));
                    if (!objVisa.Equals(null))
                    {
                        GridViewRow row = (GridViewRow)((LinkButton)e.CommandSource).NamingContainer;
                        ViewState["VisaStatusID"] = Convert.ToInt32(e.CommandArgument);
                        txtVisa.Text = row.Cells[2].Text;
                        btnSaveVisa.Text = "Update";
                        
                    }
                    break;
                case "Remove":
                    break;

            }
        }
        protected void grdVisaStatus_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdVisaStatus.PageIndex = e.NewPageIndex;
            Databind();
        }

      }

   }
    
