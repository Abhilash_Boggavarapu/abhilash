﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ETaxDal;
using ETaxBO;
namespace ETaxFilling.Administrator
{
    public partial class MyFileStatus : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bool Pinfo = false;
            bool FillTax = false;
            bool UplodDoc = false;
            bool TaxPrep = false;
            bool TaxPay = false;
            bool Confirm = false;
            bool Efiling = false;
            bool Complete = false;
            StatusDAL objStatus = new StatusDAL();
            if (!Utility.Utility.CheckAdminLoginSession())
            {
                Response.Redirect("~/Administrator/AdminLogin.aspx");
            }
            else if (!IsPostBack)
            {
                Utility.Utility.setCurrentTaxPayer(MasterItems.TaxPayerType.self);

                lblUploadDoc.Text = lblFillTaxInfo.Text = lblPersonalInfo.Text = (MasterItems.Statustypes.Pending).ToString();
                lblUploadDoc.ForeColor = lblFillTaxInfo.ForeColor = lblPersonalInfo.ForeColor = System.Drawing.Color.Red;

                List<BO_TaxPayerTaxFilingStatusDetails> objtaxPayerDetails = objStatus.getFilingStatusList(Utility.Utility.getLoginSession().TaxPayerID);

                foreach (var item in objtaxPayerDetails)
                {
                    switch ((MasterItems.TaxFillingStatusTypes)item.TaxFilingStatusTypeID)
                    {
                        case MasterItems.TaxFillingStatusTypes.PersonalInformation:

                            lblPersonalInfo.Text = (MasterItems.Statustypes.Completed).ToString();
                            lblPersonalInfo.ForeColor = System.Drawing.Color.Green;
                            lblPinfoDate.Text = item.CreatedOn.ToString();
                            Pinfo = true;
                            break;
                        case MasterItems.TaxFillingStatusTypes.FillTaxInformation:
                            lblFillTaxInfo.Text = (MasterItems.Statustypes.Completed).ToString();
                            lblFillTaxInfo.ForeColor = System.Drawing.Color.Green;
                            lblFillTaxDate.Text = item.CreatedOn.ToString();
                            FillTax = true;
                            break;
                        case MasterItems.TaxFillingStatusTypes.UploadTaxDocuments:
                            lblUploadDoc.Text = (MasterItems.Statustypes.Completed).ToString();
                            lblUploadDoc.ForeColor = System.Drawing.Color.Green;
                            lblUploadDate.Text = item.CreatedOn.ToString();
                            UplodDoc = true;
                            break;
                        case MasterItems.TaxFillingStatusTypes.ConformationPending:
                            lblConfirm.Text = "Conformation";
                            lblConformation.Text = (MasterItems.Statustypes.Completed).ToString();
                            lblConformation.ForeColor = System.Drawing.Color.Green;
                            lblCnfmDate.Text = item.CreatedOn.ToString();
                            Confirm = true;
                            break;
                        case MasterItems.TaxFillingStatusTypes.TaxPreparationPending:
                            lblPrep.Text = "Tax Preparation";
                            lblPreparation.Text = (MasterItems.Statustypes.Completed).ToString();
                            lblPreparation.ForeColor = System.Drawing.Color.Green;
                            lblPrpData.Text = item.CreatedOn.ToString();
                            TaxPrep = true;
                            break;
                        case MasterItems.TaxFillingStatusTypes.PaymentPendingClients:
                            lblPay.Text = "Payment";
                            lblPayment.Text = (MasterItems.Statustypes.Completed).ToString();
                            lblPayment.ForeColor = System.Drawing.Color.Green;
                            lblPayDate.Text = item.CreatedOn.ToString();
                            TaxPay = true;
                            break;
                        case MasterItems.TaxFillingStatusTypes.EFilingCompleted:
                            lblEtax.Text = "E-Filing";
                            lblEtaxStus.Text = (MasterItems.Statustypes.Completed).ToString();
                            lblEtaxStus.ForeColor = System.Drawing.Color.Green;
                            lblEtaxDate.Text = item.CreatedOn.ToString();
                            Efiling = true;
                            break;
                        case MasterItems.TaxFillingStatusTypes.CompletedClients:
                            lblComplt.Text = "Completed";
                            lblCompltStaus.Text = (MasterItems.Statustypes.Completed).ToString();
                            lblCompltStaus.ForeColor = System.Drawing.Color.Green;
                            lblCompltDate.Text = item.CreatedOn.ToString();
                            Complete = true;
                            break;
                        default:
                            break;
                    }
                }

                if (Pinfo == true && FillTax == true && UplodDoc == true)
                {
                    if (lblPreparation.Text != (MasterItems.Statustypes.Completed).ToString())
                    {
                        lblPrep.Text = "Tax Preparation";
                        lblPreparation.Text = (MasterItems.Statustypes.Pending).ToString();
                        lblPreparation.ForeColor = System.Drawing.Color.Red;
                    }
                    if (TaxPrep == true)
                    {
                        if (lblPayment.Text != (MasterItems.Statustypes.Completed).ToString())
                        {
                            lblPay.Text = "Payment";
                            lblPayment.Text = (MasterItems.Statustypes.Pending).ToString();
                            lblPayment.ForeColor = System.Drawing.Color.Red;
                        }
                    }
                    if (TaxPay == true)
                    {
                        if (lblConformation.Text != (MasterItems.Statustypes.Completed).ToString())
                        {
                            lblConfirm.Text = "Conformation";
                            lblConformation.Text = (MasterItems.Statustypes.Pending).ToString();
                            lblConformation.ForeColor = System.Drawing.Color.Red;
                        }
                    }
                    if (Confirm == true)
                    {
                        if (lblEtaxStus.Text != (MasterItems.Statustypes.Completed).ToString())
                        {
                            lblEtax.Text = "E-Filing";
                            lblEtaxStus.Text = (MasterItems.Statustypes.Pending).ToString();
                            lblEtaxStus.ForeColor = System.Drawing.Color.Red;
                        }
                    }
                    if (Efiling == true)
                    {
                        if (lblCompltStaus.Text != (MasterItems.Statustypes.Completed).ToString())
                        {
                            lblComplt.Text = "Completed";
                            lblCompltStaus.Text = (MasterItems.Statustypes.Pending).ToString();
                            lblCompltStaus.ForeColor = System.Drawing.Color.Red;
                        }
                    }
                }
            }
        }
    }
}