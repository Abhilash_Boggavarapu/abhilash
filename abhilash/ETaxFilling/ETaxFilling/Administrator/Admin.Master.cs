﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ETaxFilling.Utility;
using ETaxBO;

namespace ETaxFilling.Administrator
{
    public partial class Admin : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (Utility.Utility.checkLoginSession())
            {
                DvTaxPayer.Visible = true;
            }
            else
            {
                DvTaxPayer.Visible = false;
            }
            if (!IsPostBack)
            {

                if (!Utility.Utility.CheckAdminLoginSession())
                {
                    Response.Redirect("~/Administrator/AdminLogin.aspx");
                    adminMenu.Visible = false;
                }
                else
                {
                    adminMenu.Visible = true;
                }
                if (Utility.Utility.checkLoginSession())
                {
                    DvTaxPayer.Visible = true;
                    lblUaserName.Text = Utility.Utility.getLoginSession().UserFirstName;
                    lblEmailId.Text = Utility.Utility.getLoginSession().EmailId;
                    lblFileNo.Text = Utility.Utility.getLoginSession().TaxPayerID.ToString();
                }
                else
                {
                    DvTaxPayer.Visible = false;
                }
            }

        }

        protected void lnkSearchTaxPayer_Click(object sender, EventArgs e)
        {
            Utility.Utility.setLoginSessionToEmpty(null);
            Response.Redirect("~/Administrator/ClientSearch.aspx");
        }

        protected void lnklogout_Click(object sender, EventArgs e)
        {
            HttpContext.Current.Session.Abandon();
            Response.Redirect("~/Administrator/AdminLogin.aspx");
        }

        protected void lnkPendingList_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Administrator/PendingClients.aspx");
        }

        protected void lnkReferral_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Administrator/ReferalDetails.aspx");
        }

        protected void lnkbtnCompose_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Compose.aspx");
        }

        protected void lnkBtnDocuments_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Administrator/Document.aspx");
        }

        protected void lnkbtnMartialTypes_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Administrator/MaritalType.aspx");
        }

        protected void lnkBtnRelatonship_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Administrator/RelatonshipType.aspx");
        }

        protected void lnkBtnVisa_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Administrator/VisaStatus.aspx");
        }

        protected void lnkBtnInbox_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Inbox.aspx");
        }

        protected void lnkSentMessage_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Outbox.aspx");
        }

        protected void lnkUploadSheet_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Administrator/UploadSheet.aspx");
        }

        protected void lnkReferl_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Administrator/ReferalDetails.aspx");
        }

        protected void lnkTotalClientList_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Administrator/TotolClentList.aspx");
        }

        protected void lnkUserCreden_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Administrator/UserCredentials.aspx");
        }
    }
}