﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ETaxBO;
using ETaxDal;

namespace ETaxFilling.Administrator
{
    public partial class AdminLogin : System.Web.UI.Page
    {     
        protected void Page_Load(object sender, EventArgs e)
        {
            lblStatus.Visible = false;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            MasterDAL objMasterDal = new MasterDAL();
            BO_tblUser objUser = objMasterDal.CheckloginAdmin(txtUserName.Text.Trim(), txtPwd.Text, ETaxBO.MasterItems.Roles.Admin);
            if (objUser == null)
            {
                //lblStatus.Visible = true;
                //lblStatus.Text = "The username or password you entered is incorrect.";
                ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Invalid Username and Password')</script>");
            }
            else
            {
                Utility.Utility.SetAdminSession(objUser);
                string FirstName = objUser.FirstName;
                long UserId = objUser.UserID;
                Response.Redirect("AdminHome.aspx");

            }
        }
    }
}