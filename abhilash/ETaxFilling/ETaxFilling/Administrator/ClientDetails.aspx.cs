﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ETaxDal;
using ETaxBO;
using ETaxFilling.Utility;

namespace ETaxFilling.Administrator
{
    public partial class ClientDetails : System.Web.UI.Page
    {
        ClientDetailsDAL objDClient = new ClientDetailsDAL();
        MasterDAL objMasterDal = new MasterDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            txtCity.Enabled = txtClientName.Enabled = txtProjectEndDate.Enabled = txtProjectStartDate.Enabled = true;        
            lblSEmp.Text = string.Empty;
            if (!Utility.Utility.CheckAdminLoginSession())
            {
                Response.Redirect("~/Administrator/AdminLogin.aspx");
            }
             else if (!IsPostBack)
                {
                    txtCity.Visible = false;
                    Utility.Utility.getLoginSession().CurrentTaxPayer = MasterItems.TaxPayerType.self;
                   
                     if (Utility.Utility.getLoginSession().MaritalStatus == MasterItems.MaritalStatuses.Single.ToString())
                    {
                        lnkSpouse.Visible = false;
                    }
                    empDisply();
                    bindData();
                    ddlClientCountry.DataSource = objMasterDal.getCountries();
                    ddlClientCountry.DataTextField = "Country";
                    ddlClientCountry.DataValueField = "CountryID";
                    ddlClientCountry.DataBind();
                    ddlClientCountry.Items.Insert(0, new ListItem { Text = Utility.Utility.setSelectCountry, Value = "0", Selected = true });
                    ddlClientState.Items.Insert(0, new ListItem { Text = Utility.Utility.setSelectState, Value = "0", Selected = true });
                    ddlClientCity.Items.Insert(0, new ListItem { Text = Utility.Utility.setSelectCity, Value = "0", Selected = true });
                    
                  
                }
            }
        
        private void empDisply()
        {
            long taxPayerID=0;
            if (Utility.Utility.getLoginSession().CurrentTaxPayer == MasterItems.TaxPayerType.self)
            {
                taxPayerID = Utility.Utility.getLoginSession().TaxPayerID;
            }
            else if (Utility.Utility.getLoginSession().CurrentTaxPayer == MasterItems.TaxPayerType.Spouse)
            {
                if (Utility.Utility.getLoginSession().TaxPayerSpouseID != 0 || Utility.Utility.getLoginSession().TaxPayerSpouseID != null)
                {
                    taxPayerID = (long)Utility.Utility.getLoginSession().TaxPayerSpouseID;
                }
                else
                {
                    lblSEmp.Text = "No Spouse Employeer Detail found";
                }
            }
            if (taxPayerID != 0)
            {
                ddlEmployeer.Items.Clear();
                List<BO_tblTaxPayerEmployerDetail> list = objMasterDal.getEmployeerDetails(taxPayerID);
                if (list.Count != 0)
                {
                    ddlEmployeer.DataSource = list;
                    ddlEmployeer.DataTextField = "EmployerName";
                    ddlEmployeer.DataValueField = "TaxPayerEmployerID";
                    ddlEmployeer.DataBind();
                    ddlEmployeer.Items.Insert(0, new ListItem { Text = Utility.Utility.setSelectEmployer, Value = "0", Selected = true });
                }
                else
                {
                    lblSEmp.Text = "No Employer Datails Found";
                    lblSEmp.ForeColor = System.Drawing.Color.Red;
                }
            }
   
        }
        private void bindData()
        {
            long TaxPayerID = 0;
            if (Utility.Utility.getLoginSession().CurrentTaxPayer == MasterItems.TaxPayerType.self)
            {
                TaxPayerID = Utility.Utility.getLoginSession().TaxPayerID;
            }

            else if (Utility.Utility.getLoginSession().CurrentTaxPayer == MasterItems.TaxPayerType.Spouse)
            {
                if (Utility.Utility.getLoginSession().TaxPayerSpouseID != null && Utility.Utility.getLoginSession().TaxPayerSpouseID != 0)
                {
                    TaxPayerID = (long)Utility.Utility.getLoginSession().TaxPayerSpouseID;
                }
                else
                {
                    lblSEmp.Text = "Do not have spouse to save client details";
                    lblSEmp.ForeColor = System.Drawing.Color.Green;
                }
            }
            if (TaxPayerID != 0)
            {
                GrdClientlist.DataSource = objDClient.GetTaxpayrClientsDetails(TaxPayerID);
                GrdClientlist.DataBind();
            }
        }
        protected void ddlClientCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillStates();
        }

        public void FillStates()
        {
            ddlClientCity.Items.Clear();
            int CountryId = int.Parse(ddlClientCountry.SelectedValue);
            ddlClientState.DataSource = objMasterDal.getStates(CountryId);
            ddlClientState.DataTextField = "StateName";
            ddlClientState.DataValueField = "StateID";
            ddlClientState.DataBind();
            ddlClientState.Items.Insert(0, new ListItem { Text = Utility.Utility.setSelectState, Value = "0", Selected = true });
            ddlClientCity.Items.Insert(0, new ListItem { Text = Utility.Utility.setSelectCity, Value = "0", Selected = true });

        }
        public void FillCities()
        {
            ddlClientCity.Items.Clear();
            int StateId = int.Parse(ddlClientState.SelectedValue);
            ddlClientCity.DataSource = objMasterDal.getCities(StateId);
            ddlClientCity.DataTextField = "City";
            ddlClientCity.DataValueField = "CityID";
            ddlClientCity.DataBind();
            ddlClientCity.Items.Insert(0, new ListItem { Text = Utility.Utility.setSelectCity, Value = "0", Selected = true });
            ddlClientCity.Items.Insert(1, new ListItem { Text = Utility.Utility.setOtherCity, Value = "1" });
        }
        protected void ddlClientState_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtCity.Visible = false;
            FillCities();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (ddlEmployeer.Items.Count == 0|| ddlEmployeer.SelectedItem.Text==Utility.Utility.setSelectEmployer)
            {
                divCDErr.InnerText = Utility.Utility.ReqEmp;
            }
            else if (string.IsNullOrEmpty(txtClientName.Text))
            {
                divCDErr.InnerText =Utility.Utility.ReqClientName;
            }
            else if (Validations.IsAlpha(txtClientName.Text))
            {
                divCDErr.InnerText = Utility.Utility.ValClientName;
                txtClientName.Text = string.Empty;
                txtClientName.Focus();
            }
            else if (string.IsNullOrEmpty(txtProjectStartDate.Text))
            {
                divCDErr.InnerText = Utility.Utility.ReqSDate;
            }
            else if (!txtProjectStartDate.Text.DataFormat())
            {
                divCDErr.InnerText = Utility.Utility.ValSDate;
                txtProjectStartDate.Text = string.Empty;
                txtProjectStartDate.Focus();
            }
            else if (string.IsNullOrEmpty(txtProjectEndDate.Text))
            {
                divCDErr.InnerText = Utility.Utility.ReqEDate;
            }
            else if (!txtProjectStartDate.Text.DataFormat())
            {
                divCDErr.InnerText = Utility.Utility.ValEDate;
                txtProjectEndDate.Text = string.Empty;
                txtProjectEndDate.Focus();
            }
            else if (Convert.ToDateTime(txtProjectStartDate.Text) > Convert.ToDateTime(txtProjectEndDate.Text))
            {
                divCDErr.InnerText = Utility.Utility.LessSEDate;
            }
            else if (ddlClientCountry.Items.Count == 0 || ddlClientCountry.SelectedItem.Text == Utility.Utility.setSelectCountry)
            {
                divCDErr.InnerText = Utility.Utility.ReqCountry;
            }
            else if (ddlClientState.Items.Count == 0 || ddlClientState.SelectedItem.Text == Utility.Utility.setSelectState)
            {
                divCDErr.InnerText = Utility.Utility.ReqState;
            }
            else if (ddlClientCity.Items.Count == 0 || ddlClientCity.SelectedItem.Text == Utility.Utility.setSelectCity)
            {
                divCDErr.InnerText = Utility.Utility.ReqCity;
            }

            else
            {
                divCDErr.InnerText = string.Empty;
                BO_tblTaxPayerClientDetail objTaxClient = new BO_tblTaxPayerClientDetail();

                if (ViewState["ClientDetailsID"] != null)
                    objTaxClient.TaxPayerClientID = Convert.ToInt64(ViewState["ClientDetailsID"]);

                if (Utility.Utility.getLoginSession().CurrentTaxPayer == MasterItems.TaxPayerType.self)
                {
                    objTaxClient.TaxPayerID = Utility.Utility.getLoginSession().TaxPayerID;
                }

                else if (Utility.Utility.getLoginSession().CurrentTaxPayer == MasterItems.TaxPayerType.Spouse)
                {
                    if (Utility.Utility.getLoginSession().TaxPayerSpouseID != null && Utility.Utility.getLoginSession().TaxPayerSpouseID != 0)
                    {
                        objTaxClient.TaxPayerID = (long)Utility.Utility.getLoginSession().TaxPayerSpouseID;
                    }
                    else
                    {
                          lblSEmp.Text ="Do not have spouse to save Client details";
                          lblSEmp.ForeColor = System.Drawing.Color.Green;
                    }
                }
                objTaxClient.ClientName = txtClientName.Text;
                objTaxClient.EmployerID = Convert.ToInt32(ddlEmployeer.SelectedValue);
                objTaxClient.ProjectStartDate = DateTime.Parse(txtProjectStartDate.Text);
                objTaxClient.ProjectEndDate = DateTime.Parse(txtProjectEndDate.Text);
                objTaxClient.CountryID = Convert.ToInt32(ddlClientCountry.SelectedValue);
                if (Convert.ToInt32(ddlClientCity.SelectedIndex) == 1)
                {
                    if (txtCity.Text == string.Empty)
                    {
                        divCDErr.InnerText = Utility.Utility.reqEnterCity;
                    }
                    else
                    {
                        BO_tblCity objTblCity = new BO_tblCity();
                        objTblCity.CountryID = Convert.ToInt32(ddlClientCity.SelectedValue);
                        objTblCity.StateID = Convert.ToInt32(ddlClientCity.SelectedValue);
                        objTblCity.City = txtCity.Text;
                        objTaxClient.CityID = objMasterDal.insertCity(objTblCity);
                    }
                }
                else
                {
                    objTaxClient.CityID = Convert.ToInt32(ddlClientCity.SelectedValue);
                }
                if(divCDErr.InnerText == string.Empty)
                {
                objTaxClient.StateID = Convert.ToInt32(ddlClientState.SelectedValue);
                objDClient.InsertUpdateClientInformation(objTaxClient);
                bindData();
                lblMsg.Visible = true;
                if (btnSave.Text == "Save")
                {
                    lblMsg.Text = "Your Client Details are submited.You can add more Clients also";
                }
                else
                {
                    lblMsg.Text = " Your Client Details are updated successfully";
                    btnSave.Text = "Save";
                    ViewState["ClientDetailsID"] = null;
                }
                Utility.Clear.ClearDependents(this.Page);
                lblMsg.ForeColor = System.Drawing.Color.Green;             
            }
        }
        }
        
        protected void btnNext_Click(object sender, EventArgs e)
        {
            Response.Redirect("ResidencyDetails.aspx");
        }
        
        protected void GrdClientlist_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "ClientEdit":
                    lblMsg.Text = string.Empty;
                    divCDErr.InnerText = string.Empty;
                   ViewState["ClientDetailsID"]  = e.CommandArgument;
                    SetClientDetails();
                    btnSave.Text = "Update";
                    break;
                case "Remove":
                    break;
            }
        }

        private void SetClientDetails()
        {
            if (ViewState["ClientDetailsID"] != null)
            {
                BO_tblTaxPayerClientDetail objClientDetails = objDClient.GetTaxpayrClientDetails(Convert.ToInt64(ViewState["ClientDetailsID"]));
                if (!objClientDetails.Equals(null))
                {
                    txtClientName.Text = objClientDetails.ClientName;
                    txtProjectStartDate.Text = objClientDetails.ProjectStartDate.ToShortDateString();
                    txtProjectEndDate.Text = objClientDetails.ProjectEndDate.ToShortDateString();                  
                    ddlClientCountry.SelectedValue = objClientDetails.CountryID.ToString();
                    FillStates();
                    ddlClientState.SelectedValue = objClientDetails.StateID.ToString();
                    FillCities();
                    ddlClientCity.SelectedValue = objClientDetails.CityID.ToString();
                   ddlEmployeer.SelectedValue = objClientDetails.EmployerID.ToString();
                    ddlEmployeer.SelectedItem.Text = objClientDetails.EmployerName;                  
                }
            }
        }
   
        protected void lnkTaxPayer_Click(object sender, EventArgs e)
        {
            ddlEmployeer.Items.Clear();
            lblMsg.Text = string.Empty;
            lblSEmp.Text = string.Empty;
            btnSave.Text = "Save";
            ViewState["ClientDetailsID"] = null;
            lblClientDetails.Text = "TaxPayerClient Details";
            Utility.Utility.setCurrentTaxPayer(MasterItems.TaxPayerType.self);
            Utility.Clear.ClearDependents(this.Page);
            bindData();
            empDisply();
        }

        protected void lnkSpouse_Click(object sender, EventArgs e)
        {
            ddlEmployeer.Items.Clear();
            lblMsg.Text = string.Empty;
            lblSEmp.Text = string.Empty;
            btnSave.Text = "Save";
            if (Utility.Utility.getLoginSession().TaxPayerSpouseID == null || Utility.Utility.getLoginSession().TaxPayerSpouseID == 0)
            {


                lblMsg.Text = "Please enter Spouse Personal Details ";
                lblMsg.ForeColor = System.Drawing.Color.Red;
                Utility.Clear.ClearDependents(this.Page);
                Utility.Utility.setCurrentTaxPayer(MasterItems.TaxPayerType.Spouse);
                GrdClientlist.DataSource = null;
                GrdClientlist.DataBind();
                txtCity.Enabled = txtClientName.Enabled = txtProjectEndDate.Enabled = txtProjectStartDate.Enabled = false;
            }
            else
            {
                lblClientDetails.Text = "Spouse Client Details";
                ViewState["ClientDetailsID"] = null;
                Utility.Utility.setCurrentTaxPayer(MasterItems.TaxPayerType.Spouse);
                Utility.Clear.ClearDependents(this.Page);
                bindData();
                empDisply();
            }
        }

        protected void rbYes_CheckedChanged(object sender, EventArgs e)
        {    
            lblMsg.Visible = false;
        }

        protected void GrdClientlist_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GrdClientlist.PageIndex = e.NewPageIndex;
            bindData();
        }

        protected void ddlClientCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt32(ddlClientCity.SelectedIndex) == 1)
            {
                txtCity.Visible = true;
            }
            else
            {
                txtCity.Visible = false;
            }
        }
    }
}