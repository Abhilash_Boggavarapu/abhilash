﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Administrator/Admin.Master" ValidateRequest="false" CodeBehind="TaxSummary.aspx.cs" Inherits="ETaxFilling.Administrator.TaxSummary" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="TB"%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script src="../B_Scripts/Accordian/jquery-1.4.4.js" type="text/javascript"></script>
    <link href="../B_Scripts/Accordian/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="../B_Scripts/Accordian/jquery-ui.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        $(function () {
            $("#products").accordion();
        });
    </script>
    <script type="text/javascript">
    function validate() {
        var doc = document.getElementById('txtSummary');
        if (doc.value.length == 0) {
            alert('Please Enter data in Richtextbox');
            return false;
        }
    }
</script>
    <table>
    <tr>
    <td>
    <h4> 
        <asp:Label Text="" ID="lblMsg" runat="server" /></h4>
    </td>
    </tr>
        <tr>
            <td>
           <TB:FreeTextBox  ID="txtSummary" runat="server" AllowHtmlMode="False" 
                    EnableHtmlMode="False" ></TB:FreeTextBox>
            </td>
           
        </tr>
        <tr>
        <td align="center">
            <asp:Button Text="SaveTaxSummary" ID="btnSummary" 
                OnClientClick="return validate()" runat="server" onclick="btnSummary_Click"  />
            </td>
        </tr>
        <tr>
         <td>
                <asp:Button Text="View TaxSummary" ID="btntaxSummary" runat="server" 
                    Width="139px" onclick="btntaxSummary_Click"  OnClientClick="function()"/>
            </td>
        </tr>
        <tr id="acDiv" runat="server">
        <td>
        <form action="" method="post">
    <div >
    <h2 class="divider top20 bottom10">Tax Summary </h2>

    </div>
    <div id="products">
        <asp:Repeater ID="repeter" runat="server">


            <ItemTemplate>
                <h4><a href="#">Tax Summary  Created On Date: <%# DataBinder.Eval(Container.DataItem, "CreatedOn")%></a></h4>
                <div>
                    <p>
                    
                     <br />
                    <%# DataBinder.Eval(Container.DataItem, "TaxSummary")%>
                    </p>
                </div>
                </ItemTemplate>
        </asp:Repeater>
       
    </div>
    </form>
        </td>
        </tr>
        </table>
</asp:Content>