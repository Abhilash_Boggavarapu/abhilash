﻿<%@ Page Language="C#"  AutoEventWireup="true" MasterPageFile="~/Administrator/Admin.Master" CodeBehind="Document.aspx.cs" Inherits="ETaxFilling.Document" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="../Scripts/jquery.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.maskedinput.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.validate.js" type="text/javascript"></script>
    <script src="../Scripts/mktSignup.js" type="text/javascript"></script>
    <link href="../Styles/stylesheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            color: #FF0000;
        }
    </style>
<div>
<div id="divDerr" style="color:#FF0000" runat="server"></div>
    <table>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            FormName</td>
                        <td>
                            <asp:TextBox ID="txtForm" CssClass="ValidationRequired" runat="server" 
                                MaxLength="100" Height="20px" Width="150px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:Button ID="btnSaveDoc" runat="server" CssClass="formButton" 
                                onclick="btnSaveDoc_Click" Text="Save" Width="80px" />
                        </td>
                    </tr>
                </table>
               
                <br />
                        <asp:Label ID="lblMsg" runat="server" ForeColor="#009933"></asp:Label>
            </td>
            <td>
      </tr>
    </table>
     <asp:GridView ID="GrdwDocumentList" runat="server" AlternatingRowStyle-CssClass="alt"
AutoGenerateColumns="False" 
        PagerStyle-CssClass="pgr" Width="510px" 
            onrowcommand="GrdwDocumentList_RowCommand" HorizontalAlign="Center" 
        AllowPaging="True"   CssClass="mGrid" 
        onpageindexchanging="GrdwDocumentList_PageIndexChanging">
<AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
<Columns>

<asp:TemplateField HeaderText="SNo">
<ItemTemplate><%#Container.DataItemIndex+1 %><br />
</ItemTemplate>
</asp:TemplateField>
    
    <asp:BoundField DataField ="DocumentID"   Visible="false"/>      
<asp:BoundField DataField="FormName" HeaderText="Form Name"   />

<asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="lnkbtnEdit" runat="server" Text="Edit" CommandArgument='<%# DataBinder.Eval(Container,"DataItem.DocumentID") %>'
                        CommandName="EditDoc" ></asp:LinkButton>
                       
                </ItemTemplate>
            </asp:TemplateField>
<%--<asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="lnkbtnRemove" runat="server" Text="Remove" CommandArgument='<%# DataBinder.Eval(Container,"DataItem.DocumentID") %>'
                        CommandName="Remove" ></asp:LinkButton>
                       
                </ItemTemplate>
            </asp:TemplateField>--%>
</Columns>

<PagerStyle CssClass="pgr"></PagerStyle>
            </asp:GridView>
    </div>
   
</asp:Content>