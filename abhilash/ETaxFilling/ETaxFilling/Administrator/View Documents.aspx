﻿<%@ Page Language="C#" MasterPageFile="~/Administrator/Admin.Master" AutoEventWireup="true" CodeBehind="View Documents.aspx.cs" Inherits="ETaxFilling.Administrator.View_Documents" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div align="center" >
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td align="center">
    <asp:Button ID="btnConfirm" runat="server" CssClass="button" Text="Confirm" 
        onclick="btnDisplayDocuments_Click" TabIndex="1" OnClientClick ='return confirm ("Are you sure You want to confirm?") '  />
        </td>
       </tr>  
   <tr>
   <td><asp:GridView ID="GdwDocuments" runat="server" AutoGenerateColumns="False" 
        style="width:100%;" >
        <Columns>
<asp:TemplateField HeaderText="S.No.">
<ItemTemplate><%#Container.DataItemIndex+1 %><br />
</ItemTemplate>
</asp:TemplateField>
        
         <asp:BoundField DataField ="TaxPayerID"   Visible="false"/> 
      <asp:BoundField DataField="FormName" HeaderText="FormName" />
      <asp:BoundField DataField="FileName" HeaderText="FileName" />
      <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
       
        </Columns>
    </asp:GridView>
    </td>
  </tr>
    <tr>
<td align ="center" > <asp:Label id="lblmeg" CssClass="error" runat ="server" ></asp:Label></td>    </tr>
  <tr>
  <td><asp:HyperLink NavigateUrl="~/FilesUpload.aspx" Text="Edit" CssClass="button" runat="server"  ID="hypEdit"/></td>
  
  </tr>
    </table>
    </div>
</asp:Content>