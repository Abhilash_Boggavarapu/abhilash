﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ETaxBO;
using ETaxDal;

namespace ETaxFilling.Administrator
{
    public partial class TaxPayerDetails : System.Web.UI.Page
    {
        MasterDAL objMasterDal = new MasterDAL();
        TaxPayerDAL objTaxPayerDal = new TaxPayerDAL();
        StatusDAL ojStatus = new StatusDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Utility.Utility.CheckAdminLoginSession())
            {
                Response.Redirect("~/Administrator/AdminLogin.aspx");
            }
            else
            {
                txtCity.Visible = false;
                if (Utility.Utility.getLoginSession().MaritalStatus == MasterItems.MaritalStatuses.Single.ToString())
                {
                    lnkSpouse.Enabled = false;
                }
                else
                {
                    lnkSpouse.Enabled = true;
                }

                if (!IsPostBack)
                {

                    if (Utility.Utility.getLoginSession().CurrentTaxPayer == MasterItems.TaxPayerType.Spouse)
                        lblHeader.Text = "Spouse Details";

                    string status1 = objMasterDal.checkMatrialStatus(Utility.Utility.getLoginSession().TaxPayerID);
                    if (status1 == MasterItems.MaritalStatuses.Single.ToString())
                    {
                        lnkSpouse.Enabled = false;

                    }
                    else if (Utility.Utility.getLoginSession().CurrentTaxPayer == MasterItems.TaxPayerType.Spouse)
                        Utility.Utility.setCurrentTaxPayer(MasterItems.TaxPayerType.Spouse);
                    else
                        Utility.Utility.setCurrentTaxPayer(MasterItems.TaxPayerType.self);

                    fillMartialStatus();
                    fillvisaStatus();
                    fillCountries();

                    if (Utility.Utility.getLoginSession().CurrentTaxPayer == MasterItems.TaxPayerType.self)
                    {
                        getTaxpayerDetails(Utility.Utility.getLoginSession().TaxPayerID);
                    }
                    else if (Utility.Utility.getLoginSession().CurrentTaxPayer == MasterItems.TaxPayerType.Spouse)
                    {
                        lblHeader.Text = "Spouse Details";
                        if (Utility.Utility.getLoginSession().TaxPayerSpouseID != null)
                            getTaxpayerDetails((int)Utility.Utility.getLoginSession().TaxPayerSpouseID);
                        else
                        {
                            //Spouse does not Exist
                        }

                    }
                }
            }
        }
        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillStates();
        }
        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtCity.Visible = false;
            fillCities();
        } 
        protected void btnNext_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtFname.Text))
            {
                divTaxDerror.InnerText = Utility.Utility.ReqFName;
            }
            else if (Validations.IsAlpha(txtFname.Text))
            {
                divTaxDerror.InnerText = Utility.Utility.ValFName;
                txtFname.Text = string.Empty;
                txtFname.Focus();
            }
            else if (txtMname.Text.IsAlpha() && txtMname.Text !=string.Empty)
            {
                divTaxDerror.InnerText = Utility.Utility.ValMName;
                txtMname.Text = string.Empty;
                txtMname.Focus();
            }
            else if (string.IsNullOrEmpty(txtLname.Text))
            {
                divTaxDerror.InnerText = Utility.Utility.ReqLName;
            }
            else if (txtLname.Text.IsAlpha())
            {
                divTaxDerror.InnerText = Utility.Utility.ValLName;
                txtLname.Text = string.Empty;
                txtLname.Focus();
            }
            else if (string.IsNullOrEmpty(txtDOB.Text))
            {
                divTaxDerror.InnerText = Utility.Utility.ReqDOB;
            }
            else if (!txtDOB.Text.DataFormat())
            {
                divTaxDerror.InnerText = Utility.Utility.ValDOB;
                txtDOB.Text = string.Empty;
                txtDOB.Focus();
            }
           
            else if (ddlGender.Items.Count == 0 || ddlGender.SelectedItem.Text == Utility.Utility.setSelectGender)
            {
                divTaxDerror.InnerText = Utility.Utility.ReqGender;
            }
            else if (string.IsNullOrEmpty(txtOccupation.Text))
            {
                divTaxDerror.InnerText = Utility.Utility.ReqOccupation;
            }
            else if (txtOccupation.Text.IsAlpha())
            {
                divTaxDerror.InnerText = Utility.Utility.ValOccupation;
                txtOccupation.Text = string.Empty;
                txtOccupation.Focus();
            }
            else if (string.IsNullOrEmpty(txtStreetNo.Text))
            {
                divTaxDerror.InnerText = Utility.Utility.ReqStreetNo;
            }
            else if (Validations.IsAlphaNumeric(txtStreetNo.Text))
            {
                divTaxDerror.InnerText = Utility.Utility.ValStreetNo;
                txtStreetNo.Text = string.Empty;
                txtStreetNo.Focus();
            }
            else if (string.IsNullOrEmpty(txtAptNo.Text))
            {
                divTaxDerror.InnerText = Utility.Utility.ReqAptNo;
            }
            else if (Validations.IsAlphaNumeric(txtAptNo.Text))
            {
                divTaxDerror.InnerText = Utility.Utility.ValAptNo;
                txtAptNo.Text = string.Empty;
                txtAptNo.Focus();
            }
            else if (ddlCountry.Items.Count == 0 || ddlCountry.SelectedItem.Text == Utility.Utility.setSelectCountry)
            {
                divTaxDerror.InnerText = Utility.Utility.ReqCountry;
            }
            else if (ddlState.Items.Count == 0 || ddlState.SelectedItem.Text == Utility.Utility.setSelectState)
            {
                divTaxDerror.InnerText = Utility.Utility.ReqState;
            }
            else if (ddlCity.Items.Count == 0 || ddlCity.SelectedItem.Text == Utility.Utility.setSelectCity)
            {
                divTaxDerror.InnerText = Utility.Utility.ReqCity;
            }
            else if (!Validations.isNumber(txtZipCode.Text) && txtZipCode.Text != string.Empty)
            {
                divTaxDerror.InnerText = Utility.Utility.ValZip;
                txtZipCode.Text = string.Empty;
                txtZipCode.Focus();
            }
            else if (!txtPhome.Text.isNumber() && txtPhome.Text != string.Empty)
            {
                divTaxDerror.InnerText = Utility.Utility.ValHomeNo;
                txtPhome.Text = string.Empty;
                txtPhome.Focus();
            }

            else if (string.IsNullOrEmpty(txtMobile.Text))
            {
                divTaxDerror.InnerText = Utility.Utility.ReqMobileNo;
            }

            else if (!txtMobile.Text.isNumber())
            {
                divTaxDerror.InnerText = Utility.Utility.ValMobileNo;
                txtMobile.Text = string.Empty;
                txtMobile.Focus();
            }
            else if (!txtLandline.Text.isNumber() && txtLandline.Text != string.Empty)
            {
                divTaxDerror.InnerText = Utility.Utility.ValLandNo;
                txtLandline.Text = string.Empty;
                txtLandline.Focus();
            }
            else if (string.IsNullOrEmpty(txtPemail.Text))
            {
                divTaxDerror.InnerText = Utility.Utility.ReqNEmailId;
            }
            else if (!txtPemail.Text.IsValidEmailID())
            {
                divTaxDerror.InnerText = Utility.Utility.ValNEmailId;
                txtPemail.Text = string.Empty;
                txtPemail.Focus();
            }
            else if(!txtSEmail.Text.IsValidEmailID() && txtSEmail.Text != string.Empty)
            {
                divTaxDerror.InnerText = Utility.Utility.ValNEmailId;
                txtSEmail.Text = string.Empty;
                txtSEmail.Focus();
            }
            else if (Utility.Utility.getLoginSession().CurrentTaxPayer == MasterItems.TaxPayerType.self && (ddlMaritalStatus.Items.Count == 0 || ddlMaritalStatus.SelectedItem.Text == Utility.Utility.setSelectMaritalstatus))
                {
                divTaxDerror.InnerText = Utility.Utility.ReqMaritalStatus; 
            }
            else
            {
                buildObject();
                Ms.Visible = false;
            }        
        }
      
        protected void lnkTaxPayer_Click(object sender, EventArgs e)
        {
            Ms.Visible = true;
            Utility.Utility.setCurrentTaxPayer(MasterItems.TaxPayerType.self);
            getTaxpayerDetails((int)Utility.Utility.getLoginSession().TaxPayerID);
            lblHeader.Text = "Tax Payer Details";
            lnkTaxPayer.ForeColor = System.Drawing.Color.White;
            lnkSpouse.ForeColor = System.Drawing.Color.Orange;
        }

        protected void lnkSpouse_Click(object sender, EventArgs e)
        {
            Ms.Visible = false;
            NavigatetoSpouse();
        }
        private void buildObject()
        {
            divTaxDerror.InnerText = string.Empty;
            BO_tblTaxPayerDetail objtaxpayerDetails = new BO_tblTaxPayerDetail();
            objtaxpayerDetails.UserID = Utility.Utility.getLoginSession().UserID;
            MasterItems.TaxPayerType taxPayer = Utility.Utility.getLoginSession().CurrentTaxPayer;
            objtaxpayerDetails.PayerType = Convert.ToInt32(taxPayer);
            objtaxpayerDetails.FirstName = txtFname.Text;
            objtaxpayerDetails.LastName = txtLname.Text;
            objtaxpayerDetails.MiddleName = txtMname.Text;
            objtaxpayerDetails.DOB = DateTime.Parse(txtDOB.Text);
            objtaxpayerDetails.ApartmentNo = txtAptNo.Text;
            objtaxpayerDetails.StreetNo = txtStreetNo.Text;
            objtaxpayerDetails.Gender = ddlGender.SelectedValue;
            objtaxpayerDetails.HomeNumber = txtPhome.Text;
            objtaxpayerDetails.LandlineNumber = txtLandline.Text;
            objtaxpayerDetails.MobileNumber = txtMobile.Text;
            objtaxpayerDetails.Occupation = txtOccupation.Text;
            objtaxpayerDetails.PrimaryEMailID = txtPemail.Text;
            objtaxpayerDetails.SecondaryEMailID = txtSEmail.Text;
            objtaxpayerDetails.SSN = txtSsn.Text;
            objtaxpayerDetails.ZipCode = txtZipCode.Text;
            objtaxpayerDetails.MaritalStatusID = Convert.ToInt32(ddlMaritalStatus.SelectedValue);
            objtaxpayerDetails.StateID = Convert.ToInt32(ddlState.SelectedValue);
            objtaxpayerDetails.CountryID = Convert.ToInt32(ddlCountry.SelectedValue);
            if (Convert.ToInt32(ddlCity.SelectedIndex) == 1)
            {
                if (txtCity.Text == string.Empty)
                {
                    divTaxDerror.InnerText = Utility.Utility.reqEnterCity;
                }
                else
                {
                    BO_tblCity objTblCity = new BO_tblCity();
                    objTblCity.CountryID = Convert.ToInt32(ddlCountry.SelectedValue);
                    objTblCity.StateID = Convert.ToInt32(ddlState.SelectedValue);
                    objTblCity.City = txtCity.Text;
                    objtaxpayerDetails.CityID = objMasterDal.insertCity(objTblCity);
                }
            }
            else
            {
                objtaxpayerDetails.CityID = Convert.ToInt32(ddlCity.SelectedValue);
            }
            if (ddlVstatus.SelectedValue == "")
            {
                objtaxpayerDetails.VisaStatusID = null;
            }
            else
            {
                objtaxpayerDetails.VisaStatusID = Convert.ToInt32(ddlVstatus.SelectedValue);
            }
            if (txtDOE.Text != string.Empty)
            {
                objtaxpayerDetails.USEntryDate = DateTime.Parse(txtDOE.Text);
            }
            long? TaxpayerSpouseID = null;
            objTaxPayerDal.InsertUpdateTaxPayer(objtaxpayerDetails, Utility.Utility.getLoginSession(), ref TaxpayerSpouseID);
            Ms.Visible = false;
            if (TaxpayerSpouseID != null)
            {
                //setting TaxPayer Spouse ID to current login session
                BO_tblTaxPayerDetail objTaxPayr = Utility.Utility.getLoginSession();
                objTaxPayr.TaxPayerSpouseID = TaxpayerSpouseID;
                Utility.Utility.setLoginSession(objTaxPayr);
                //Resetting current taxpayer as Taxpayer becouse current taxper might be spouse
            }
            if (divTaxDerror.InnerText == string.Empty)
            {
                if (Utility.Utility.getLoginSession().CurrentTaxPayer == MasterItems.TaxPayerType.self)
                {
                    if (ddlMaritalStatus.SelectedItem.Text == MasterItems.MaritalStatuses.Single.ToString())
                        Utility.Utility.setMaritalStatus(MasterItems.MaritalStatuses.Single);
                    else
                        Utility.Utility.setMaritalStatus(MasterItems.MaritalStatuses.Married);

                    if (Utility.Utility.getLoginSession().MaritalStatus == MasterItems.MaritalStatuses.Single.ToString())
                    {
                        Response.Redirect("DependentDetails.aspx");
                    }
                    else
                    {
                        NavigatetoSpouse();
                    }
                }
                else
                {
                    Utility.Utility.setCurrentTaxPayer(MasterItems.TaxPayerType.self);
                    Response.Redirect("DependentDetails.aspx");
                }
            }
        }
        private void NavigatetoSpouse()
        {
            lblHeader.Text = "Spouse Details";
            lnkSpouse.ForeColor = System.Drawing.Color.White;
            lnkTaxPayer.ForeColor = System.Drawing.Color.Orange;
            Utility.Utility.setCurrentTaxPayer(MasterItems.TaxPayerType.Spouse);
            getTaxpayerDetails((int)Utility.Utility.getLoginSession().TaxPayerSpouseID);
           
        }
        private void getTaxpayerDetails(long id)
        {
            if (id != 0)
            {
                BO_tblTaxPayerDetail objTaxPayer = objTaxPayerDal.GetTaxpayerDeails(id);
                txtFname.Text = objTaxPayer.FirstName;
                txtLname.Text = objTaxPayer.LastName;
                txtMname.Text = objTaxPayer.MiddleName;
                txtOccupation.Text = objTaxPayer.Occupation;
                txtSsn.Text = objTaxPayer.SSN;
                txtMobile.Text = objTaxPayer.MobileNumber;
                txtPemail.Text = objTaxPayer.PrimaryEMailID;
                txtDOB.Text = objTaxPayer.DOB.ToShortDateString();
                txtSEmail.Text = objTaxPayer.SecondaryEMailID;
                txtSsn.Text = objTaxPayer.SSN;
                txtStreetNo.Text = objTaxPayer.StreetNo;
                txtAptNo.Text = objTaxPayer.ApartmentNo;
                if (objTaxPayer.USEntryDate != null)
                {
                    txtDOE.Text = objTaxPayer.USEntryDate.Value.ToShortDateString();
                }
                txtPhome.Text = objTaxPayer.HomeNumber;
                txtZipCode.Text = objTaxPayer.ZipCode;
                txtLandline.Text = objTaxPayer.LandlineNumber;
                ddlGender.SelectedIndex = (objTaxPayer.Gender == "Female" ? 2 : (objTaxPayer.Gender == "Male" ? 1 : 0));
                ddlCountry.SelectedItem.Text = objTaxPayer.CountryName;
                ddlCountry.SelectedValue = objTaxPayer.CountryID.ToString();
                fillStates();
                ddlState.SelectedValue = objTaxPayer.StateID.ToString();
                fillCities();
                ddlCity.SelectedValue = objTaxPayer.CityID.ToString();
                ddlMaritalStatus.SelectedValue = objTaxPayer.MaritalStatusID.ToString();
                ddlMaritalStatus.SelectedItem.Text = objTaxPayer.MaritalStatus;
                if (objTaxPayer.VisaStatus != null)
                {
                    ddlVstatus.SelectedValue = objTaxPayer.VisaStatusID.ToString();
                    ddlVstatus.SelectedItem.Text = objTaxPayer.VisaStatus;
                }
                else
                {
                    fillvisaStatus();
                }
            }
            else
            {
                Utility.Clear.ClearDependents(this.Page);
            }
        }
        private void fillCountries()
        {
            ddlCountry.DataSource = objMasterDal.getCountries();
            ddlCountry.DataTextField = "Country";
            ddlCountry.DataValueField = "CountryID";
            ddlCountry.DataBind();
        }
        private void fillStates()
        {
            ddlCity.Items.Clear();
            int CountryId = int.Parse(ddlCountry.SelectedValue);
            ddlState.DataSource = objMasterDal.getStates(CountryId);
            ddlState.DataTextField = "StateName";
            ddlState.DataValueField = "StateID";
            ddlState.DataBind();
            ddlState.Items.Insert(0, new ListItem { Text = Utility.Utility.setSelectState, Value = "0", Selected = true });
            ddlCity.Items.Insert(0, new ListItem { Text = Utility.Utility.setSelectCity, Value = "0", Selected = true });
        }
        private void fillCities()
        {
            ddlCity.Items.Clear();
            int StateId = int.Parse(ddlState.SelectedValue);
            ddlCity.DataSource = objMasterDal.getCities(StateId);
            ddlCity.DataTextField = "City";
            ddlCity.DataValueField = "CityID";
            ddlCity.DataBind();
            ddlCity.Items.Insert(0, new ListItem { Text = Utility.Utility.setSelectCity, Value = "0", Selected = true });
            ddlCity.Items.Insert(1, new ListItem { Text = Utility.Utility.setOtherCity, Value = "" });
        }
        private void fillvisaStatus()
        {
            ddlVstatus.DataSource = objMasterDal.getVisaStatus();
            ddlVstatus.DataTextField = "VisaStatus";
            ddlVstatus.DataValueField = "VisaStatusID";
            ddlVstatus.DataBind(); 
            ddlVstatus.Items.Insert(0, new ListItem { Text = Utility.Utility.setSelectVisastatus, Value = "", Selected = true });
        }
        private void fillMartialStatus()
        {
            ddlMaritalStatus.DataSource = objMasterDal.getMaritalStatus();
            ddlMaritalStatus.DataTextField = "MaritalStatus";
            ddlMaritalStatus.DataValueField = "MaritalStatusID";
            ddlMaritalStatus.DataBind();
        }

        protected void ddlCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt32(ddlCity.SelectedIndex) == 1)
            {
                txtCity.Visible = true;
            }
            else
            {
                txtCity.Visible = false;
            }
        }
    } 
}