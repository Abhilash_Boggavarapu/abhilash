﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ETaxDal;
using ETaxBO;

namespace ETaxFilling.Administrator
{
    public partial class PersonalInfoConfirmDetails : System.Web.UI.Page
    {
        TaxPayerDAL objTaxPayerDal = new TaxPayerDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Utility.Utility.CheckAdminLoginSession())
            {
                Response.Redirect("~/Administrator/AdminLogin.aspx");
            }
            else
            {
                BO_TaxPayerTaxFilingStatusDetails isUpdateable = new StatusDAL().getFilingStatus((long)Utility.Utility.getLoginSession().TaxPayerID, (int)MasterItems.TaxFillingStatusTypes.PersonalInformation);
                if (isUpdateable != null)
                {
                    //btnConFirmDown.Visible = false;
                    //btnConfirm.Visible = false;
                    btnEditDependentDetails.Visible = false;
                    btnEditSpouseDetails.Visible = false;
                    btnEdittaxPayerDetails.Visible = false;
                }
                else
                {
                    if (Utility.Utility.getLoginSession().MaritalStatus == MasterItems.MaritalStatuses.Single.ToString())
                    {
                        PanelSpouseConfirmData.Visible = false;
                        btnEditSpouseDetails.Visible = false;
                    }
                    else if (Utility.Utility.getLoginSession().TaxPayerSpouseID != 0 && Utility.Utility.getLoginSession().TaxPayerSpouseID != null)
                    {
                        PanelSpouseConfirmData.Enabled = true;
                        btnEditSpouseDetails.Visible = true;
                    }
                }

                BO_tblTaxPayerDetail objTaxPayer = objTaxPayerDal.GetTaxpayerDeails(Utility.Utility.getLoginSession().TaxPayerID);
                if (objTaxPayer != null)
                {
                    lbltaxFname.Text = objTaxPayer.FirstName;
                    lbltaxMiddleName.Text = objTaxPayer.MiddleName;
                    lbltaxLastName.Text = objTaxPayer.LastName;
                    lbltaxDOB.Text = objTaxPayer.DOB.ToShortDateString();
                    lbltaxSSN.Text = objTaxPayer.SSN;
                    lbltaxGender.Text = objTaxPayer.Gender;
                    lbltaxOccupation.Text = objTaxPayer.Occupation;
                    lbltaxStreetNo.Text = objTaxPayer.StreetNo;
                    lbltaxAppartmentNo.Text = objTaxPayer.ApartmentNo;
                    lbltaxMaritalStatus.Text = objTaxPayer.MaritalStatus;
                    lbltaxCountry.Text = objTaxPayer.CountryName;
                    lbltaxState.Text = objTaxPayer.StateName;
                    lbltaxCity.Text = objTaxPayer.CityName;
                    lbltaxZipCode.Text = objTaxPayer.ZipCode;
                    lbltaxHome.Text = objTaxPayer.HomeNumber;
                    lbltaxMobile.Text = objTaxPayer.MobileNumber;
                    lbltaxLandline.Text = objTaxPayer.LandlineNumber;
                    lbltaxprimaryEmail.Text = objTaxPayer.PrimaryEMailID;
                    lbltaxsecondaryEmail.Text = objTaxPayer.SecondaryEMailID;
                    lbltaxVisaStatus.Text = objTaxPayer.VisaStatus;
                    lbltaxDateOfEntryToUS.Text = objTaxPayer.USEntryDate.ToString();
                }
                else
                {
                    lblPrscon.Text = "Not Provided";
                }
                long taxPayerSpouseID = (long)Utility.Utility.getLoginSession().TaxPayerSpouseID;
                if (taxPayerSpouseID == 0)
                {
                    PanelSpouseConfirmData.Visible = false;
                    lblSpouse.Text = "Not Provided";
                    lblSpouse.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    BO_tblTaxPayerDetail objTaxPayerSpouse = objTaxPayerDal.GetTaxpayerDeails(taxPayerSpouseID);
                    if (objTaxPayerSpouse != null)
                    {
                        lblSpouseFName.Text = objTaxPayerSpouse.FirstName;
                        lblSpouseMiddlName.Text = objTaxPayerSpouse.MiddleName;
                        lblSpouseLstName.Text = objTaxPayerSpouse.LastName;
                        lblSpouseDateOfBirth.Text = objTaxPayerSpouse.DOB.ToShortDateString();
                        lblSpouseSSN.Text = objTaxPayerSpouse.SSN;
                        lblSpouseGendr.Text = objTaxPayerSpouse.Gender;
                        lblSpouseOccptn.Text = objTaxPayerSpouse.Occupation;
                        lblSpouseStreetNo.Text = objTaxPayerSpouse.StreetNo;
                        lblSpouseappartmntNo.Text = objTaxPayerSpouse.ApartmentNo;
                        lblSpouseMaritaStatus.Text = objTaxPayerSpouse.MaritalStatus;
                        lblSpouseCuntry.Text = objTaxPayerSpouse.CountryName;
                        lblSpousestate.Text = objTaxPayerSpouse.StateName;
                        lblSpousecity.Text = objTaxPayerSpouse.CityName;
                        lblSpouseHmeNo.Text = objTaxPayerSpouse.HomeNumber;
                        lblSpouseMobileNo.Text = objTaxPayerSpouse.MobileNumber;
                        lblSpouseLandlineNo.Text = objTaxPayerSpouse.LandlineNumber;
                        lblSpousePrimaryEmail.Text = objTaxPayerSpouse.PrimaryEMailID;
                        lblSpouseSecondryEmail.Text = objTaxPayerSpouse.SecondaryEMailID;
                        lblSpouseVisStatus.Text = objTaxPayerSpouse.VisaStatus;
                        lblSDOE.Text = objTaxPayerSpouse.USEntryDate.ToString();
                    }
                    else
                    {
                        lblSpouse.Text = "Not Provided";
                        lblSpouse.ForeColor = System.Drawing.Color.Red;
                    }
                }
                DependentDAL objDTaxDependent = new DependentDAL();
                List<BO_tblTaxPayerDependent> objDependent = objDTaxDependent.GetTaxPayerDependents(Utility.Utility.getLoginSession().TaxPayerID);
                if (objDependent.Count != 0)
                {
                    GVWDependentConfirmDetails.DataSource = objDependent;
                    GVWDependentConfirmDetails.DataBind();
                }
                else
                {
                    lblDependent.Text = " Not Provided";
                    lblDependent.ForeColor = System.Drawing.Color.Red;
                }

            }
        }
        protected void btnEdittaxPayerDetails_Click(object sender, EventArgs e)
        {
            Response.Redirect("TaxPayerDetails.aspx");
        }

        protected void btnEditSpouseDetails_Click(object sender, EventArgs e)
        {
            Utility.Utility.setCurrentTaxPayer(MasterItems.TaxPayerType.Spouse);

            Response.Redirect("TaxPayerDetails.aspx");
        }

        protected void btnEditDependentDetails_Click(object sender, EventArgs e)
        {
            Response.Redirect("DependentDetails.aspx");
        }

        ////protected void btnConfirm_Click(object sender, EventArgs e)
        ////{
        ////     btnConfirm.Visible = false;
        ////    BO_TaxPayerTaxFilingStatusDetails objTaxPayerTaxFilingStatusDetails = new BO_TaxPayerTaxFilingStatusDetails();
        ////    objTaxPayerTaxFilingStatusDetails.TaxPayerID = Utility.Utility.getLoginSession().TaxPayerID;
        ////    objTaxPayerTaxFilingStatusDetails.TaxFilingStatusTypeID = (int)MasterItems.TaxFillingStatusTypes.PersonalInformation;
        ////    bool isSucess = new StatusDAL().ConfirmFileStaus(objTaxPayerTaxFilingStatusDetails);
        ////    if (isSucess)
        ////    {
        ////        Response.Redirect("EmployerDetails.aspx");
        ////    }
        ////}
    }
}