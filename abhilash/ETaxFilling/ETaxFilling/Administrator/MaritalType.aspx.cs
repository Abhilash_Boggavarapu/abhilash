﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ETaxBO;
using ETaxDal;

namespace ETaxFilling
{
    public partial class MaritalType1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Utility.Utility.CheckAdminLoginSession())
            {
                Response.Redirect("~/Administrator/AdminLogin.aspx");
            }
            else
            {
                lblMsg.Text = string.Empty;
                databind();
            }
        }
          MasterDAL objmarrital = new MasterDAL(); 
        private void databind()
        {
            GrdwMarriedList.DataSource = objmarrital.getMaritalStatus();
            GrdwMarriedList.DataBind();
          
        }


        protected void btnSaveMarital_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtMarital.Text))
            {
                divMTerr.InnerText = Utility.Utility.ReqMaritalType;
            }
            else if (txtMarital.Text.IsAlpha())
            {
                divMTerr.InnerText = Utility.Utility.ValMaritalType;
            }
            MaritalStatusesDAL objDMaritalDal = new MaritalStatusesDAL();
            BO_tblMaritalStatus objMarital = new BO_tblMaritalStatus();

            if (ViewState["MaritalStatusID"] != null)
                objMarital.MaritalStatusID = Convert.ToInt32(ViewState["MaritalStatusID"]);
            objMarital.MaritalStatus = txtMarital.Text;
            objDMaritalDal.SaveUpdateMaritalStatuses(objMarital);
            lblMsg.Text = "The MaritaType Details Are Submited";
            lblMsg.ForeColor = System.Drawing.Color.Green; 
            btnSaveMarital.Text = "Save";
            
            txtMarital.Text = string.Empty;
            databind();
        }

        protected void GrdwMarriedList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "EditMar":

                    BO_tblMaritalStatus objmar = objmarrital.GetMarstatus(Convert.ToInt32(e.CommandArgument));
                    if (!objmar.Equals(null))
                    {
                        GridViewRow row = (GridViewRow)((LinkButton)e.CommandSource).NamingContainer;

                        ViewState["MaritalStatusID"] = Convert.ToInt32(e.CommandArgument);
                        txtMarital.Text = row.Cells[2].Text;
                        btnSaveMarital.Text = "Update";
                    }
                  
                    break;

                case "Remove":

                    break;
            }     

        }
    }
}