﻿<%@ Page Language="C#" AutoEventWireup="true"   MasterPageFile="~/Administrator/Admin.Master" CodeBehind="ClientSearch.aspx.cs" Inherits="ETaxFilling.Administrator.ClientSearch" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <script type="text/javascript">
      $(function () {
          $("#txtDOB").datepicker({
              showOn: "button",
              buttonImage: "../B_Images/calendar_icon.png",
              changeMonth: true,
              yearRange: YEAR_RANGE_GLOBAL,
              changeYear: true,
              buttonImageOnly: true
          });
      });
    </script>
    <div align="center" >
        &nbsp;<div id="txtError" runat="server"></div>
        <table style="width: 100%">
            <tr>

                 <td>
           <table>
           
           <tr>
           <td>
           Search Clients by Status
           </td>
           <td>
           <asp:DropDownList runat="server" ID="ddlPendingClients" Height="20px" 
        Width="185px"></asp:DropDownList>
           </td>
           </tr>
           <tr>
           <td colspan="2">
           <asp:Button ID="btnSearchByStatus" runat="server" Text="Search" Width="78px" 
                          CssClass="button " onclick="btnSearchByStatus_Click"/>
           </td>
           </tr>
           <tr>
           <td>
               <asp:Label Text="" ID="lblStatus" CssClass="error" runat="server" />
           </td>
           </tr>
           </table>
           
           </td>
           <td>
           <table>
              <tr>
           <td >
            <asp:Label ID="lblMsg" runat="server" CssClass="error"></asp:Label>
           </td>
           <td></td>
           </tr>
           <tr>
                <td align="right" >
                    File No :</td>
                <td  align="left" >
                    <asp:TextBox ID="txtID" runat="server"  MaxLength="50"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="right" >
                    Name :</td>
                <td  align="left" >
                    <asp:TextBox ID="txtName" runat="server" MaxLength="50"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="right">
                    EmailId :</td>
                <td  align="left" >
                    <asp:TextBox ID="txtEmailid" runat="server" MaxLength="50" ></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="right">
                    DateOfBirth :</td>
                <td  align="left" >
                    <asp:TextBox ID="txtDOB" ClientIDMode ="Static" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td></td>
                <td align="left" ><asp:Button ID="btnSearch" runat="server" Text="Search" Width="78px" 
                        onclick="btnSearch_Click"  CssClass="button "/></td>
            </tr>
        
           </table>
           </td>
            </tr>
        </table>
        <table >
        <tr>
         <td colspan="2" align="center">
            <asp:GridView ID="grdDisplay" runat="server" AutoGenerateColumns ="False" 
            onrowcommand="grdDisplay_RowCommand" AllowPaging="True" 
            onpageindexchanging="grdDisplay_PageIndexChanging" Width="100%" CssClass="mGrid">
        <Columns>
        <asp:BoundField DataField ="UserID" HeaderText ="UserID" Visible="false" />
        <asp:BoundField DataField ="TaxPayerID" HeaderText ="File No" />
        <asp:BoundField DataField ="FirstName" HeaderText ="First Name" />
        <asp:BoundField DataField ="LastName" HeaderText ="Last Name" />
         <asp:BoundField DataField ="DOB" HeaderText ="DOB" DataFormatString="{0:d}" 
                HtmlEncode="False" />
         <asp:BoundField DataField ="EmailId" HeaderText =" EMail ID" />
         <asp:BoundField DataField ="MobileNumber" HeaderText ="Mobile Number" />
      
        <asp:TemplateField>
        <ItemTemplate>
        <asp:LinkButton ID="LinkButton1" runat="server"  Text ="View" CommandName="ViewData" CommandArgument='<%#Eval("TaxPayerID") %>'></asp:LinkButton>
        </ItemTemplate>
        </asp:TemplateField>

           <asp:TemplateField HeaderText="Tax Preparation Status">
        <ItemTemplate>   
      <asp:LinkButton ID="lnkPreparationStatus" runat="server"  OnClientClick ='return confirm ("Are you sure You want to continue?")' Visible ='<%#Eval("TaxPreparationCompletedEnabled") %>' Text ="Proceed if completed?" CommandName="PreparationCompleted" CommandArgument='<%#Eval("TaxPayerID") %>'></asp:LinkButton>
        </ItemTemplate>
        </asp:TemplateField>

           <asp:TemplateField HeaderText ="EFiling Status">
        <ItemTemplate>
         <asp:LinkButton ID="lnkEFilingCompleted" runat="server"  OnClientClick ='return confirm ("Are you sure You want to continue?")'  Visible ='<%#Eval("EFilingCompletedEnabled") %>' Text ="Proceed if completed" CommandName="EFilingCompleted" CommandArgument='<%#Eval("TaxPayerID") %>'></asp:LinkButton>
        </ItemTemplate>
        </asp:TemplateField>

         <asp:TemplateField HeaderText ="Completed Status">
        <ItemTemplate>
         <asp:LinkButton ID="lnkCompleted" runat="server"  OnClientClick ='return confirm ("Are you sure You want to continue?")'  Visible ='<%#Eval("CompletedEnabled") %>' Text ="Proceed if completed" CommandName="CompletedClients" CommandArgument='<%#Eval("TaxPayerID") %>'></asp:LinkButton>
        </ItemTemplate>
        </asp:TemplateField>
        </Columns>
        </asp:GridView>
     </td></tr>
     </table>
     </div>
     </asp:Content>