﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/Administrator/Admin.Master" CodeBehind="UploadSheet.aspx.cs" Inherits="ETaxFilling.Administrator.UploadSheet" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <table width="700px" border="0px" cellpadding="0" cellspacing="6" class="box">
                            <tr>
                                <td colspan="2">
                                    <h2 class="divider top20 bottom10">
                                        Upload Tax Sheet</h2>
                                </td>
                            </tr>
                            </table>
    <%--<div align="left"><h4><strong> Upload Tax Sheet</strong></h4></div>--%>
    <%-- <asp:Panel ID="Panel1" runat="server" ">--%>
    <table>
    <tr>
    <td align="center"><strong>Browse</strong> </td>
    <td align="center"> <strong>Remarks</strong></td>
     <td align="center"> <strong>Upload</strong></td>
      <td align="center"><strong>View</strong> </td>
    </tr>
        <tr>
            <td>
           <asp:FileUpload ID="FileUpload1" runat="server" Width="232px" />
            </td>
            <td>
                <asp:TextBox ID="txtRemarks" runat="server" />
            </td>
            <td>
                <asp:Button Text="Upload" ID="btnUpload" runat="server" 
                    onclick="btnUpload_Click" Width="68px" />
            </td>
            <td>
                <asp:Button Text="View" ID="View" runat="server" onclick="View_Click" 
                    Width="63px" />
            </td>
        </tr>
       <tr>
       <td>
           <asp:Label Text="" ID="lblExten" runat="server" />
           <br />
           <asp:Label ID="lblMsg" runat="server" Text="" />
           <br />
           <asp:LinkButton ID="btnDownload" runat="server" onclick="btnDownload_Click" 
               Text="Download" Visible="False" />
       </td>
       </tr>
    </table>
</asp:Content>