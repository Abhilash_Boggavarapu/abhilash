﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/Administrator/Admin.Master" CodeBehind="ReferalDetails.aspx.cs" Inherits="ETaxFilling.Administrator.ReferalDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="../B_Scripts/jquery.bgiframe-2.1.2.js" type="text/javascript"></script>
    <script src="../B_Scripts/jquery-ui.js" type="text/javascript"></script>
   <link href="../B_Styles/style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function fnmodalpopup() {
            $("#grdClientDisplay").dialog({
                height: 450,
                width:650,
                modal: true,
                close: false
            });
        }
    </script>
    <script type="text/javascript">
        $(function () {
            $("#txtStartDate,#txtEndDate").datepicker({
                showOn: "button",
                buttonImage: "../B_Images/calendar_icon.png",
                yearRange: YEAR_RANGE_GLOBAL,
                changeMonth: true,
                changeYear: true,
                buttonImageOnly: true
            });
        });
    </script>
     <div id="ErrRDetails" style="color:#FF0000" runat="server"></div>
    <table>
    <tr align="center">
    <td>
        <asp:RadioButton Text="HomeReferral" AutoPostBack="true" GroupName="x"  ID="rbHome"
            runat="server" oncheckedchanged="rbHome_CheckedChanged"  />
            </td>
            <td>
        <asp:RadioButton Text="ClientReferral" AutoPostBack="true" GroupName="x" 
            ID="rbClient" runat="server" oncheckedchanged="rbClient_CheckedChanged"  />
    </td>
    </tr>
    <tr>
    <td>
    StartDate:
    </td>
    <td>
        <asp:TextBox ID="txtStartDate" ClientIDMode="Static" 
            CssClass="ValidationRequired date" runat="server" />
        </td>
        </tr>
        <tr>
        <td>
    EndDate:
    </td>
    <td>
        <asp:TextBox  ID="txtEndDate"  ClientIDMode="Static" CssClass="ValidationRequired date" runat="server" />
          </td>
        </tr>
      
        <tr>
        <td>
        <asp:Button Text="Search" ID="btnSearch" CssClass="button" runat="server" 
            onclick="btnSearch_Click" />
    </td>
    </tr>
    <tr>
    <td>
    
        <asp:Label Text="" ID="lblError" runat="server" />
    </td>
    </tr>
    </table>
    <table>
        <tr>
            <td>
            <asp:GridView ID="grdHomeRferal" runat="server" AutoGenerateColumns="False" 
                        Width="700px" AllowPaging="True" 
                        onpageindexchanging="grdReHomeRferal_PageIndexChanging" 
                    CssClass="mGrid" >
                        <Columns>
                            <asp:TemplateField HeaderText="SNo">
                                <ItemTemplate>
                                    <%#Container.DataItemIndex+1 %><br />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="ReferredID" Visible="false" />
                            <asp:BoundField DataField="ReferralName" HeaderText="Referral Name" SortExpression="EID" />
                            <asp:BoundField DataField="EmailID" HeaderText="EmailID " />
                            <asp:BoundField DataField="Phone1" HeaderText="Mobile No" />
                            <asp:BoundField DataField="ReferredName" HeaderText="Referred Name" />
                            <asp:BoundField DataField="ReferredEmailID" HeaderText="EmailID" />
                            <asp:BoundField DataField="ReferredPhone" HeaderText="Mobile No" />
                            <asp:BoundField DataField="CreatedOn" HeaderText="Date"  DataFormatString="{0:d}" 
        HtmlEncode="False" />
                        </Columns>
                    </asp:GridView>
            </td>
        </tr>
        <tr>
        <td>
           <asp:GridView ID="grdClientReferal" runat="server" AutoGenerateColumns="False" 
                        Width="700px" AllowPaging="True" 
                        onpageindexchanging="grdClientReferal_PageIndexChanging"  
                CssClass="mGrid" onrowcommand="grdClientReferal_RowCommand">
                        <Columns>
                            <asp:TemplateField HeaderText="SNo">
                                <ItemTemplate>
                                    <%#Container.DataItemIndex+1 %><br />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="UserID" Visible="false" />
                            <asp:BoundField DataField="UserName" HeaderText="User Name" SortExpression="EID" />
                            <asp:BoundField DataField="UserEmailID" HeaderText="EmailID " />
                            <asp:BoundField DataField="UserPhoneNo" HeaderText="Mobile No" />
                            <asp:BoundField DataField="Count" HeaderText="No Of Referred" />
                           <%-- <asp:TemplateField HeaderText="View" >
                            <ItemTemplate>
                                <a href="javascript:window.open('ClientReferalDetails.aspx?UserID=<%# Eval("UserID") %>','NewPage','width=400,height=500,toolbar=no, 
                         directories=no,status=no,location=center,menubar=no,scrollbars=no,copyhistory=no,resizable=yes')">View Details  </a> 
                                                                                            
                            </ItemTemplate>
                            </asp:TemplateField>--%>
                            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="lnkbtnEdit" runat="server" Text="View" CommandArgument='<%# DataBinder.Eval(Container,"DataItem.UserID") %>'
                        CommandName="ViewRow" ></asp:LinkButton>
                       
                </ItemTemplate>
            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
        </td>
        </tr>
    </table>
      <div id="grdClientDisplay">
     <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                        Width="700px" AllowPaging="True"  CssClass="mGrid"
                       
        onpageindexchanging="GridView1_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField HeaderText="SNo">
                                <ItemTemplate>
                                    <%#Container.DataItemIndex+1 %><br />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="ReferralName" HeaderText="Name" />
                            <asp:BoundField DataField="EmailID" HeaderText="EmailID " />
                            <asp:BoundField DataField="Phone1" HeaderText="Mobile No " />
                            <asp:BoundField DataField="CreatedOn" HeaderText="Date&Time" DataFormatString="{0:d}" 
        HtmlEncode="False" />
                            <asp:TemplateField>
              
            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
    </div>
</asp:Content>
