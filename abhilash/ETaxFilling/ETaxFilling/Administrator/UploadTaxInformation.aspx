﻿<%@ Page Language="C#"  AutoEventWireup="true"  MasterPageFile="~/Administrator/Admin.Master" CodeBehind="UploadTaxInformation.aspx.cs" Inherits="ETaxFilling.Administrator.UploadTaxInformation" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="../Scripts/Upload/jquery-1.3.2.js" type="text/javascript"></script>
    <script src="../Scripts/Upload/jquery.MultiFile.js" type="text/javascript"></script>

     <table width="700px" border="0px" cellpadding="0" cellspacing="3" >
                            <tr>
                                <td align="center">
                                    <h2 class="divider top20 bottom10">
                                        Upload TaxReturn Documents</h2>
                                </td>
                            </tr>
       </table>  
       <div>
           <asp:Label Text="" ID="Label1" runat="server" />
       </div>  
        <asp:Panel ID="PanelUpldTaxRetrnDoc" runat="server" Height="108px">
      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="box">
           <tr>
                   <td align="center"><strong>Browse</strong> </td>
                   <td align="center"><strong>Remarks</strong> </td>  
                    <td align="center"><strong>Upload</strong></td>     
                     <td align="center"><strong>View</strong></td>  
                     </tr>  
                     <tr>                                                       
                           <td>
                               <asp:FileUpload ID="FileUpload2" runat="server" class="multi" Height="40px" 
                                   Width="237px" /> </td>
                             <td> 
                                 <asp:TextBox runat="server"  ID="txtRDRemarks" TextMode="MultiLine" 
                                     Width="155px" Height="22px" ></asp:TextBox></td>
                              <td>
                                   <asp:Button Text="Upload" ID="btnRDUpload" runat="server"  
                                       OnClientClick="return ValidateFile" CommandArgument="1"  
                                       onclick="btnRDUpload_Click" Width="86px" /></td>
              <td>  
         <asp:Button ID="btnRDView" runat="server" onclick="BtnView_Click" 
             onclientclick="return ValidateFile" CommandArgument="1"  Text="View" Height="25px" 
                      Width="52px" />
              </td>
             </tr>
             </table>
             </asp:Panel>

              <table width="700px" border="0px" cellpadding="0" cellspacing="6">
                            <tr>
                                <td colspan="2">
                                    <h2 class="divider top20 bottom10">
                                       Confirm Tax Documents</h2>
                                </td>
                            </tr>
             </table>  
             <asp:Panel ID="Panel1" runat="server" Height="173px">
          <table width="100%" border="0" cellpadding="0" cellspacing="0" class="box">
           <tr>
                   <td align="center"><strong>Browse</strong> </td>
                   <td align="center"><strong>Remarks</strong> </td>  
                    <td align="center"><strong>Upload</strong></td>     
                     <td align="center"><strong>View</strong></td>  
                     </tr> 
                         <tr>
                           <td>
                               <asp:FileUpload ID="FileUpload3" runat="server" class="multi"  Height="40px" 
                                   Width="227px" />
                                
                                   </td>
                               <td>
                                   <asp:TextBox ID="txtConfirm" runat="server" TextMode="MultiLine" Width="166px" 
                                       Height="20px"></asp:TextBox>
                               </td>
                               <td>
                                   <asp:Button ID="btnConfirm" runat="server" CommandArgument="3" 
                                       onclick="btnConfirm_Click" OnClientClick="return ValidateFile" 
                                       Text="Upload" Width="89px" />
                               </td>
                               <td>
                                   <asp:Button ID="btnView3" runat="server" CommandArgument="3" 
                                       onclick="BtnView_Click" Text="View" Width="56px" />
                               </td>
                               </tr>
                               <tr><td></td></tr>
                               <tr>
                               <td align="center">
                               <asp:Button ID="ViewtaxpayerDoc" runat="server" onclick="ViewtaxpayerDoc_Click" Text="View TaxPayer Confirm Tax Documents" />
                               </td>
                               
                               </tr>
         
                  </table>
                  </asp:Panel>
                 <br />
                 <div>
                  <asp:Label ID="lblMsg" runat="server" Text="" />
                 </div>                       
                 <table>
                 <tr>
                 <td>
 <asp:GridView ID="GdVwUploadInformation" runat="server" AutoGenerateColumns="False" 
        Height="75px" Width="174px" margin="5%" 
        onrowcommand="GdVwUploadInformation_RowCommand" AllowPaging="True" 
        onpageindexchanging="GdVwUploadInformation_PageIndexChanging" CssClass="mGrid" >
        <Columns>
<asp:TemplateField HeaderText="SNo">
<ItemTemplate><%#Container.DataItemIndex+1 %><br />
</ItemTemplate>
</asp:TemplateField>
         <asp:BoundField DataField ="TaxSheetID"   Visible="false"/> 
           <asp:BoundField DataField="SheetName" HeaderText="File Name" />
              <asp:BoundField DataField="CreatedOn" HeaderText="Uploaded On Date&Time" 
                DataFormatString="{0:d}" HtmlEncode="False" />
      <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
         <asp:TemplateField>
         <ItemTemplate>
        <asp:LinkButton ID="BtnDownload" runat ="server" CommandName="Download" CommandArgument='<%# DataBinder.Eval(Container,"DataItem.TaxSheetID") %>' Text ="Download" > </asp:LinkButton>
         </ItemTemplate>
         </asp:TemplateField>
        </Columns>
        <PagerSettings FirstPageText="First" LastPageText="Last" 
            Mode="NumericFirstLast" />
    </asp:GridView>
    </td>
    </tr>
    </table>
    <br />
    <table border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
            <asp:GridView ID="grdClintDisplay" runat="server" AutoGenerateColumns="False"
     Height="75px" Width="174px" margin="5%"  OnRowCommand="grdClintDocDisplay_RowCommand" >
        <Columns>
<asp:TemplateField HeaderText="SNo">
<ItemTemplate><%#Container.DataItemIndex+1 %><br />
</ItemTemplate>
</asp:TemplateField>
         <asp:BoundField DataField ="TaxSheetID"   Visible="false"/> 
      <asp:BoundField DataField="SheetName" HeaderText="Document Name" />
      <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
      <asp:BoundField DataField="CreatedOn" HeaderText="Created On Date& Time" />
         <asp:TemplateField>
         <ItemTemplate>
         <asp:LinkButton ID="BtnDownload" runat ="server" CommandName="Download" CommandArgument='<%# DataBinder.Eval(Container,"DataItem.TaxSheetID") %>' Text ="Download" ></asp:LinkButton>
         </ItemTemplate>
         </asp:TemplateField>
        </Columns>
    </asp:GridView>
            </td>
        </tr>
    </table>
    
    </asp:Content>