﻿<%@ Page Language="C#" MasterPageFile="~/Administrator/Admin.Master" AutoEventWireup="true" CodeBehind="ResidencyDetails.aspx.cs" Inherits="ETaxFilling.Administrator.ResidencyDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">   
    <script type="text/javascript">

        $(function () {
            $("#txtStartDate,#txtEndDate").datepicker({
                showOn: "button",
                buttonImage: "../B_Images/calendar_icon.png",
                yearRange: YEAR_RANGE_GLOBAL,
                changeMonth: true,
                changeYear: true,
                buttonImageOnly: true
            });
        });

    </script>
     <style type="text/css">
        .style1
        {
            color: #FF0000;
        }
    </style>
    <div align ="center" > 
    <table  >
       <tr> 
        <td>
        <div><ul class ="submenu" >
                        <li>  <a  href="EmployerDetails.aspx"   >Employer Details</a></li>
	       <li> <a href="ClientDetails.aspx" >Client Details</a> </li>
      <li> <a   style="color: ButtonHighlight "  href="#" >Residency Details</a></li>
      </ul>
     </div>
     </td>
     </tr>
     </table> 
     <table>
    <tr>
    <td>
    <asp:LinkButton runat="server" ID="lnkTaxPayer" Text="Tax Payer" CssClass="button" OnClick="lnkTaxPayer_Click"></asp:LinkButton>
            </td>
            <td>
                <asp:LinkButton runat="server" ID="lnkSpouse" Text="Spouse" CssClass="button" OnClick="lnkSpouse_Click"></asp:LinkButton>
            </td>
        </tr>
       
    </table>
    <table width="700px"class="box">
               <tr>
        <td align="center">
       <h3>
           <asp:Label ID="lblClientDetails" runat="server" Text=" Taxpayer Residency Details" />
       </h3>
        </td>
        </tr>
           </table>

     <div id="divRSDerror" style="color:#FF0000" runat="server"></div>
        <table>
            <tr>
            <td>
                <asp:Label Text="" ID="lblSpouse" runat="server" />
            </td>
                <td>
                <h4>
                    <asp:Label Text="" ID="lblMsg" runat="server" />
                </h4>
                </td>
            </tr>
        </table>
    <table style="width: 73%">
        <tr>
            <td align="right" > <span style="color:red;">*</span>
                State Name:</td>
            <td align="left">
            <asp:TextBox ID="txtStateName" runat="server"  CssClass="ValidationRequired" 
                    MaxLength="50" Height="20px" Width="150px"></asp:TextBox>
               </td>
        </tr>
        <tr>
            <td align="right" > <span style="color:red;">*</span>
                City:</td>
            <td align="left">
            <asp:TextBox ID="txtCity" runat="server"  CssClass="ValidationRequired" 
                    MaxLength="50" TabIndex="1" Height="20px" Width="150px"></asp:TextBox>
               </td>
        </tr>
        <tr>
            <td align="right"> <span style="color:red;">*</span>
                Start Date:</td>
            <td align="left">
            <asp:TextBox ID="txtStartDate" ClientIDMode="Static" 
                    CssClass="ValidationRequired date" runat="server" 
                    TabIndex="2" Height="20px" Width="150px"></asp:TextBox>
                </td>
        </tr>
        <tr>
            <td  align="right" > <span style="color:red;">*</span>
                End Date:</td>
            <td align="left" >
            <asp:TextBox ID="txtEndDate" ClientIDMode="Static" 
                    CssClass="ValidationRequired date" runat="server" 
                    TabIndex="3" Height="20px" Width="150px"></asp:TextBox>

                </td>
        </tr>
        <tr>
            <td >
               </td>
            <td>
            <asp:Button ID="btnSave" runat="server" Text="Save" Width="90px" 
                    onclick="btnSave_Click" TabIndex="4" Height="30px" CssClass ="button" />
               
                </td>
        </tr>
       
    </table>
      <table style="width: 100%">
       <tr>
            <td >
                &nbsp;</td>
            
        </tr>
        <tr>
            
                <td></td>
            <td align="right">
                <asp:HyperLink Text="Next" Width="68px" 
                    NavigateUrl="~/Administrator/FillTaxConfirm.aspx" CssClass="button" 
                    runat="server" />
              </td>
        </tr>
    </table>
    <div align="center">
    <asp:GridView ID="GrdResedentDetails" runat="server" AlternatingRowStyle-CssClass="alt"
        AutoGenerateColumns="False"
        PagerStyle-CssClass="pgr" Width="510px" 
            onrowcommand="GrdResedentDetails_RowCommand" HorizontalAlign="Center"  
            AllowPaging="True" onpageindexchanging="GrdResedentDetails_PageIndexChanging" 
            CssClass="mGrid">
<AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
<Columns>

<asp:TemplateField HeaderText="SNo">
<ItemTemplate><%#Container.DataItemIndex+1 %><br />
</ItemTemplate>
</asp:TemplateField>
    
    <asp:BoundField DataField ="TaxPayerResidencyDetailsID"   Visible="false"/>      
<asp:BoundField DataField="CityName" HeaderText="City" SortExpression="EID"  />
<asp:BoundField DataField="StateName" HeaderText="State" />
<asp:BoundField DataField="StartDate" HeaderText="Start Date" 
        DataFormatString="{0:d}" HtmlEncode="False" />
<asp:BoundField DataField="EndDate" HeaderText="End Date" DataFormatString="{0:d}" 
        HtmlEncode="False"/>

<asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="lnkbtnEdit" runat="server" Text="Edit" CommandArgument='<%# DataBinder.Eval(Container,"DataItem.TaxPayerResidencyDetailsID") %>'
                        CommandName="EditROW" ></asp:LinkButton>
                       
                </ItemTemplate>
            </asp:TemplateField>
            
</Columns>

<PagerStyle CssClass="pgr"></PagerStyle>
</asp:GridView>
    </div>
    </div>
    </asp:Content>

