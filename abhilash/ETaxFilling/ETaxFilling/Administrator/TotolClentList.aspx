﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Administrator/Admin.Master" CodeBehind="TotolClentList.aspx.cs" Inherits="ETaxFilling.Administrator.TotolClentList" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="../B_Scripts/jquery-ui.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript"> 
      $(function () {
          $("#txtStartDate,#txtEndDate").datepicker({
              showOn: "button",
              buttonImage: "../B_Images/calendar_icon.png",
              yearRange: YEAR_RANGE_GLOBAL,
              changeMonth: true,
              changeYear: true,
              buttonImageOnly: true
          });
      });
   </script>
 <div id="ErrRDetails" style="color:#FF0000" runat="server"></div>
    <table border="0" cellpadding="0" cellspacing="0" align="center">
        <tr align="center">
            <td>
            Start Date:
            </td>
            <td>
                <asp:TextBox id="txtStartDate"  CssClass="ValidationRequired date" runat="server" />  
            </td>
        </tr>
        <tr>
        <td> End Date:
        </td>
        <td>
            <asp:TextBox id="txtEndDate"  CssClass="ValidationRequired date" runat="server" />  
        </td>
        </tr>
        <tr>
        <td></td>
        <td align="center">
            <asp:button text="Search" id="btnSearch" runat="server" onclick="btnSearch_Click" />
        </td>
        </tr>
        <tr>
        <td>
            <asp:label text="" id="lblError" runat="server" />
        </td>
        </tr>
        </table>
        <div align="left">
        <asp:GridView ID="grdList" runat="server" AutoGenerateColumns="False" 
                        Width="700px" AllowPaging="True"  CssClass="mGrid" onpageindexchanging="grdList_PageIndexChanging" 
              >
                        <Columns>
                            <asp:TemplateField HeaderText="SNo">
                                <ItemTemplate>
                                    <%#Container.DataItemIndex+1 %><br />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="FirstName" HeaderText="First Name" />
                            <asp:BoundField DataField="LastName" HeaderText="Last Name " />
                            <asp:BoundField DataField="EmailID" HeaderText="Email ID " />
                              <asp:BoundField DataField="MobileNumber" HeaderText="Mobile No " />
                            <asp:BoundField DataField="CreatedOn" HeaderText="Date" DataFormatString="{0:d}" 
        HtmlEncode="False" />
                            <asp:TemplateField>
              
            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
        
       </div>
</asp:Content>