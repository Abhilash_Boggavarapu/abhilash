﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/Administrator/Admin.Master" CodeBehind="VisaStatus.aspx.cs" Inherits="ETaxFilling.VisaStatus1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div>
    <div id="divVSerr" style="color:#FF0000" runat="server"></div>
        <table width="100%" class="box" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td style="width: 301px">
                    <table>
                        <tr>
                            <td>
                                Visa Status</td>
                            <td>
                                <asp:TextBox ID="txtVisa" runat="server" CssClass="ValidationRequired" 
                                    Height="20px" Width="124px" MaxLength="20"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <asp:Button ID="btnSaveVisa" runat="server" onclick="btnSaveVisa_Click" 
                                    Text="Save" Width="80px" />
                                <br />
                                    <asp:Label ID="lblMsg" runat="server" ForeColor="#006600"></asp:Label>
                                <br />
                            </td>
                        </tr>
                    </table>
 <asp:GridView ID="grdVisaStatus" runat="server" AlternatingRowStyle-CssClass="alt"
AutoGenerateColumns="False"  GridLines="None" 
        PagerStyle-CssClass="pgr" Width="295px" 
            onrowcommand="grdVisaStatus_RowCommand" HorizontalAlign="Center" 
            CellPadding="4" ForeColor="#333333" AllowPaging="True" CssClass="mGrid" 
                        onpageindexchanging="grdVisaStatus_PageIndexChanging">
           
<AlternatingRowStyle CssClass="alt" BackColor="White"></AlternatingRowStyle>
<Columns>

<asp:TemplateField HeaderText="SNo">
<ItemTemplate><%#Container.DataItemIndex+1 %><br />
</ItemTemplate>
</asp:TemplateField>
    
    <asp:BoundField DataField ="VisaStatusID"   Visible="false"/>      
<asp:BoundField DataField="VisaStatus" HeaderText="VisaStatus"   />

<asp:TemplateField>
                <ItemTemplate>
    <asp:LinkButton ID="lnkbtnEdit" runat="server" Text="Edit" CommandArgument='<%# DataBinder.Eval(Container,"DataItem.VisaStatusID") %>'
                        CommandName="EditVisa" ></asp:LinkButton>
                       
                </ItemTemplate>
            </asp:TemplateField>        
               
</Columns>
            </asp:GridView>
                    <br />
                    
                </td>
               
                
               
           
        
                
            
       
   
    </div>
    </div>
    </div>
</div>
    </div>
    </div>
    </div>
    </div>
</asp:Content>

