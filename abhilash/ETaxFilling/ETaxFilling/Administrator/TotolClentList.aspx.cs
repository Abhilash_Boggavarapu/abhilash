﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ETaxBO;
using ETaxDal;
namespace ETaxFilling.Administrator
{
    public partial class TotolClentList : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            lblError.Text = string.Empty;
            if (!IsPostBack)
            {
                if (!Utility.Utility.CheckAdminLoginSession())
                {
                    Response.Redirect("~/Administrator/AdminLogin.aspx");
                }
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtStartDate.Text))
            {
                ErrRDetails.InnerText = Utility.Utility.ReqSDate;
            }
            else if (!txtStartDate.Text.DataFormat())
            {
                ErrRDetails.InnerText = Utility.Utility.ValSDate;
                txtStartDate.Text = string.Empty;
                txtStartDate.Focus();
            }
            else if (string.IsNullOrEmpty(txtEndDate.Text))
            {
                ErrRDetails.InnerText = Utility.Utility.ReqEDate;
            }
            else if (!txtEndDate.Text.DataFormat())
            {
                ErrRDetails.InnerText = Utility.Utility.ValEDate;
                txtEndDate.Text = string.Empty;
                txtEndDate.Focus();
            }
            else if (Convert.ToDateTime(txtStartDate.Text) > Convert.ToDateTime(txtEndDate.Text))
            {
                ErrRDetails.InnerText = Utility.Utility.LessSEDate;
            }
            else
            {
                disply();
            }
        }

        private void disply()
        {
            DateTime startDate = DateTime.Parse(txtStartDate.Text);
            DateTime endDate = DateTime.Parse(txtEndDate.Text);
            AdminMasterDAL objAdminMasterDal = new AdminMasterDAL();
            List<BO_tblUser> objUser = objAdminMasterDal.getUserList(startDate, endDate);
            if (objUser.Count != 0)
            {
                grdList.DataSource = objUser;
                grdList.DataBind();
            }
            else
            {
                lblError.Text = "No Records Found";
                lblError.ForeColor = System.Drawing.Color.Red;
            }
        }

        protected void grdList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdList.PageIndex = e.NewPageIndex;
            disply();
        }
    }
}