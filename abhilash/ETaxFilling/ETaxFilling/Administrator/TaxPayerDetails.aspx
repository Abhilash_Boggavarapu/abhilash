﻿<%@ Page Language="C#" MasterPageFile="~/Administrator/Admin.Master" AutoEventWireup="true" CodeBehind="TaxPayerDetails.aspx.cs" Inherits="ETaxFilling.Administrator.TaxPayerDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager runat="server"></asp:ScriptManager>
    <script type="text/javascript">

        $(function () {
            $("#txtDOB,#txtDOE").datepicker({
                showOn: "button",
                buttonImage: "../B_Images/calendar_icon.png",
                yearRange: YEAR_RANGE_GLOBAL,
                changeMonth: true,
                changeYear: true,
                buttonImageOnly: true
            });
        });

    </script>

     <%--    Rama Krishna--%>

    <div align ="center">
    <table width="700px" border="0px" cellpadding="0" cellspacing="6">
        <tr> <td align="center"><div><ul class ="submenu" >
                        <li><asp:LinkButton runat="server"  ID="lnkTaxPayer" Text="Tax Payer Details" onclick="lnkTaxPayer_Click" 
                               ></asp:LinkButton>
                               </li>
	       <li><asp:LinkButton runat="server" ID="lnkSpouse" Text="Spouse Details" onclick="lnkSpouse_Click"  
                               ></asp:LinkButton>   </li>
      <li> <a href="DependentDetails.aspx">Dependent Details</a></li>
      </ul>
     </div>
     </td>
     </tr>
     </table> 
	<table width="700px" border="0px" cellpadding="0" cellspacing="6" class="box">
               <tr>
        <td colspan="3">
      
         <h2><asp:Label Text="Tax Payer Details" ID="lblHeader" StringFormat="{}{0:c2}" class="divider top20 bottom10" runat="server" /> </h2>
          <div id="divTaxDerror" style="color:#FF0000" runat="server"></div>
        </td>
        </tr>
            <tr>
                <td align="right">
                       <span style="color: #FF0000">*</span> First Name : </td>
                <td align="left">
                    <asp:TextBox ID="txtFname" runat="server"  CssClass="ValidationRequired" 
                        Width="150px" Height="15px" MaxLength="50" TabIndex="1" ></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="right">
                   Middle Name : </td>
                <td align="left">
                    <asp:TextBox ID="txtMname" runat="server" 
                        Width="150px" MaxLength="50" Height="15px" TabIndex="2"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <span style="color: #FF0000">*</span> Last Name : </td>
                <td align="left">
                    <asp:TextBox ID="txtLname" runat="server"  CssClass="ValidationRequired" 
                        Width="150px" MaxLength="50" TabIndex="3" Height="15px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <span style="color: #FF0000">*</span> Date Of Birth : </td>
                <td align="left">
                    <asp:TextBox ID="txtDOB" ClientIDMode="Static" runat="server"  CssClass="ValidationRequired date" 
                        Width="150px" Height="15px" TabIndex="4" ></asp:TextBox>
                    
                     </td>          
            </tr>
            <tr>
                <td align="right">
                    SSN : </td>
                <td align="left">
                    <asp:TextBox ID="txtSsn" runat="server" 
                        Width="150px" MaxLength="15" Height="15px" TabIndex="5"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <span style="color: #FF0000">*</span> Gender :</td>
                <td align="left">
        
                    <asp:DropDownList ID="ddlGender" runat="server" Height="22px" Width="160px"
                        TabIndex="6">    
                        <asp:ListItem>Select</asp:ListItem>
                        <asp:ListItem>Male</asp:ListItem>
                        <asp:ListItem>Female</asp:ListItem>
                    </asp:DropDownList>
                  
                </td>
                 <td > 
                     &nbsp;</td>
            </tr>
            <tr>
                <td align="right">
                   <span style="color: #FF0000">*</span> Occupation : </td>
                <td align="left">
                    <asp:TextBox ID="txtOccupation" runat="server"  CssClass="ValidationRequired" 
                        Width="150px" MaxLength="50" Height="15px" TabIndex="7"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <span style="color: #FF0000">*</span> Street No : </td>
                <td align="left">
                    <asp:TextBox ID="txtStreetNo" runat="server"  CssClass="ValidationRequired" 
                        Width="150px" MaxLength="20" Height="15px" TabIndex="8"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <span style="color: #FF0000">*</span> Appartment No :</td>
                <td align="left">
                    <asp:TextBox ID="txtAptNo" runat="server"  CssClass="ValidationRequired char" 
                        Width="150px" MaxLength="10" Height="15px" TabIndex="9"></asp:TextBox>
                </td>
            </tr>
            
            <tr id="Ms" runat="server">
                <td align="right" >
                    <span style="color: #FF0000">*</span> Marital Status : </td>
                <td align="left">
                  
                    <asp:DropDownList ID="ddlMaritalStatus" runat="server" Height="22px" Width="160px"
                      TabIndex="10">
                    </asp:DropDownList>
                   
                </td>
            </tr>
            <tr>
                <td align="right">
                   <span style="color: #FF0000">*</span> Country : </td>
                <td align="left">
                    <asp:UpdatePanel   ID="UpdatePanel" runat="server">
                        <ContentTemplate>
                    <asp:DropDownList ID="ddlCountry" runat="server" Height="22px" Width="160px"
                        AutoPostBack="True" 
                        onselectedindexchanged="ddlCountry_SelectedIndexChanged" 
                                TabIndex="11">
                    </asp:DropDownList>
                     </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td align="right">
                   <span style="color: #FF0000">*</span> State : </td>
                <td align="left">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="ddlState" runat="server" Height="22px" Width="160px"
                        AutoPostBack="True" 
                                onselectedindexchanged="ddlState_SelectedIndexChanged" TabIndex="12">
                    </asp:DropDownList>
                    </ContentTemplate>
                    </asp:UpdatePanel>

                    
                </td>
            </tr>
            <tr>
                <td align="right">
                   <span style="color: #FF0000">*</span>
                    City : </td>
                <td align="left">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                     
                    <asp:DropDownList ID="ddlCity" runat="server" Height="22px" Width="160px" TabIndex="13" 
                                AutoPostBack="True" onselectedindexchanged="ddlCity_SelectedIndexChanged">
                    </asp:DropDownList>
                   </ContentTemplate></asp:UpdatePanel>
                </td>
            </tr>
             <tr id="trcity" runat="server">
                <td></td>
                <td align="left">
                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>
              <asp:TextBox ID="txtCity"   Height="15px"   Width="150px" runat="server" />
               </ContentTemplate></asp:UpdatePanel>
                   
                </td>
                </tr>
            <tr>
                <td align="right">
&nbsp;ZipCode:</td>
                <td align="left">
                    <asp:TextBox ID="txtZipCode" runat="server" 
                        Width="150px" MaxLength="10" Height="15px" TabIndex="9"></asp:TextBox>
              
                  </td>
            </tr>
            </table>
        <asp:Panel runat="server" ForeColor="Black" GroupingText="Contact Details:">
        <table width="700px" class="box">
            <tr>
               <td align="right" style="width: 239px" >
                    
                    <asp:Label ID="lblHome" runat="server" Text="Home : "></asp:Label>
                </td>
                <td style="text-align: left">
                    <asp:TextBox ID="txtPhome" runat="server" Height="15px" Width="150px"  MaxLength="15" 
                        TabIndex="14" ></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="right" style="width: 239px" >
                   <span style="color: #FF0000">*</span>
                    <asp:Label ID="lblMobile" runat="server" Text="Mobile : "></asp:Label>
                </td>
                <td style="text-align: left">
                    <asp:TextBox ID="txtMobile" runat="server"  Width="150px"
                        MaxLength="15" Height="15px" TabIndex="15"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="right" style="width: 239px" >
                    <asp:Label ID="lblLandline" runat="server" Text="Landline : "></asp:Label>
                </td>
                <td style="text-align: left">
                    <asp:TextBox ID="txtLandline" runat="server"  Width="150px"
                        MaxLength="15" Height="15px" TabIndex="16"></asp:TextBox>
                </td>
            </tr>
            </table>
        </asp:Panel>
        <asp:Panel runat="server">
       <table width="700px" border="0px" cellpadding="0" cellspacing="6" class="box">
            <tr>
                <td align="right" style="width: 234px">
                   <span style="color: #FF0000">*</span> Primary Email ID:</td>
                <td align="left">
                    <asp:TextBox ID="txtPemail" runat="server" Width="150px"
                        MaxLength="100" Height="15px" TabIndex="17"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="right" style="width: 234px">
                    Secondary Email ID:</td>
                <td align="left">
                    <asp:TextBox ID="txtSEmail" 
                        runat="server" Width="150px" 
                         MaxLength="100" Height="15px" TabIndex="18"></asp:TextBox>
                </td>
            </tr>
            </table>
             </asp:Panel>
             <table width="700px" border="0px" cellpadding="0" cellspacing="6" class="box">
            <tr>
                <td align="right" style="width: 234px" >
                     Visa Status : </td>
                <td align="left">
                    <asp:DropDownList ID="ddlVstatus" runat="server" Width="150px" 
                        style="height: 20px" 
                        TabIndex="19">
                       
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="right" style="width: 234px">
                     Date Of Entry To Us: </td>
                <td align="left">
                    <asp:TextBox ID="txtDOE" ClientIDMode="Static" Height="15px" runat="server" Width="140px" TabIndex="20"  
                        ></asp:TextBox>
                </td>
           </table>
    <table align="center">
        <tr align="center">
            <td align="center">
                <asp:Label ID="lblStatus" runat="server"></asp:Label>
            </td>
                <td>
                    <asp:Button ID="btnNext" align="center"  runat="server" CssClass="button" Text="Save&amp;Next" 
                        onclick="btnNext_Click" Height="29px" Width="138px" TabIndex="21" />
                   </td>
            </tr>       
    </table>
    </div>            
     
</asp:Content>