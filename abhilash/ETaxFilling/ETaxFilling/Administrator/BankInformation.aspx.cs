﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ETaxBO;
using ETaxDal;
using ETaxFilling.Utility;

namespace ETaxFilling.Administrator
{
    public partial class BankInformation1 : System.Web.UI.Page
    {
        BankInformDAL objDBank = new BankInformDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            lblDisplay.Text = string.Empty;
            if (!Utility.Utility.CheckAdminLoginSession())
            {
                Response.Redirect("~/Administrator/AdminLogin.aspx");
            }          
            if (!IsPostBack)
            {
                bindData();
            }
        }

        protected void btnCDetails_Click(object sender, EventArgs e)
        {
           if (string.IsNullOrEmpty(txtAcNo.Text))
            {
                divBIerr.InnerText = Utility.Utility.ReqBDetails;
            }
            else if (!txtAcNo.Text.IsNumeric())
            {
                divBIerr.InnerText = Utility.Utility.ValBDetails;
                txtAcNo.Text = string.Empty;
                txtAcNo.Focus();
            }
            else if (string.IsNullOrEmpty(txtRoutingNo.Text))
            {
                divBIerr.InnerText = Utility.Utility.ReqRouting;
            }
            else if (!txtRoutingNo.Text.IsNumeric())
            {
                divBIerr.InnerText = Utility.Utility.ValRouting;
                txtRoutingNo.Text = string.Empty;
                txtRoutingNo.Focus();
            }
            else if (string.IsNullOrEmpty(txtBName.Text))
            {
                divBIerr.InnerText = Utility.Utility.ReqBName;
            }
            else if (txtBName.Text.IsAlpha())
            {
                divBIerr.InnerText = Utility.Utility.ValBName;
                txtBName.Text = string.Empty;
                txtBName.Focus();
            }
            else if (string.IsNullOrEmpty(txtAName.Text))
            {
                divBIerr.InnerText = Utility.Utility.ReqAName;
            }
            else if (txtAName.Text.IsAlpha())
            {
                divBIerr.InnerText = Utility.Utility.ValAName;
                txtAName.Text = string.Empty;
                txtAName.Focus();
            }
            else if (string.IsNullOrEmpty(ddlAType.Text))
            {
                divBIerr.InnerText = Utility.Utility.ReqAType;
            }
            else
            {
                divBIerr.InnerText = string.Empty;
                BO_tblTaxPayerBankDetail objTaxPayer = new BO_tblTaxPayerBankDetail();
                if (ViewState["BankDetailsID"] != null)
                    objTaxPayer.BankDetailsID = Convert.ToInt64(ViewState["BankDetailsID"]);
                objTaxPayer.TaxPayerID = Utility.Utility.getLoginSession().TaxPayerID;
                objTaxPayer.BankName = txtBName.Text;
                objTaxPayer.AccountHolderName = txtAName.Text;
                objTaxPayer.AccountNo = Convert.ToInt64(txtAcNo.Text);
                objTaxPayer.RoutingNo = txtRoutingNo.Text;
                objTaxPayer.AccountHolderName = txtAName.Text;
                objTaxPayer.AccounTtype = (ddlAType.SelectedValue).ToString();
                objDBank.InsertUpdateBankInformation(objTaxPayer);
                lblStatus.Visible = true;
                bindData();
                if (btnCDetails.Text == "Save")
                {
                    lblStatus.Text = "Your Bank Details are submited successfully";
                }
                else
                {
                    lblStatus.Text = " Your Bank Details are updated successfully";
                    btnCDetails.Text = "Save";
                    ViewState["BankDetailsID"] = null;
                }
                lblStatus.ForeColor = System.Drawing.Color.Green;
                Utility.Clear.ClearDependents(this.Page);
            }           
        }
        private void bindData()
        {
            GrdBank.DataSource = objDBank.GetBankDetails(Utility.Utility.getLoginSession().TaxPayerID);
            GrdBank.DataBind();
        }
      
        protected void GrdBank_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GrdBank.PageIndex = e.NewPageIndex;
            bindData();
        }

        protected void GrdBank_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "EditROW":
                    BO_tblTaxPayerBankDetail objBank =objDBank.GetBankInform(Convert.ToInt64( e.CommandArgument));
                    if (!objBank.Equals(null))
                    {
                        ViewState["BankDetailsID"] = Convert.ToInt32(e.CommandArgument);
                        txtAcNo.Text = objBank.AccountNo.ToString() ;
                        txtRoutingNo.Text = objBank.RoutingNo;
                        txtBName.Text = objBank.BankName;
                        txtAName.Text = objBank.AccountHolderName;
                        ddlAType.Text = objBank.AccounTtype;
                        btnCDetails.Text = "Update";
                    }
                    break;
                case "Remove":

                    break;
            }
        }
    }
}