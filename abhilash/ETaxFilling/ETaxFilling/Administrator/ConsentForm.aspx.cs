﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ETaxDal;
using ETaxBO;

namespace ETaxFilling
{
    public partial class ConsentForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblDesc.Text = string.Empty;
            if (!IsPostBack)
            {
                if (Utility.Utility.CheckAdminLoginSession() == false)
                {
                    Response.Redirect("AdminLogin.aspx");
                }
                else
                {
                    ConsentFormDAL objDConsent = new ConsentFormDAL();
                    tblConsentForm objConsent = objDConsent.GetConsentForm();
                    if (objConsent != null)
                    {
                        txtDesc1.Text = objConsent.Description1;
                        txtDesc2.Text = objConsent.Description2;
                        txtDesc3.Text = objConsent.Description3;
                        txtDesc4.Text = objConsent.Description4;
                    }
                }
            }
        }

        BO_tblConsentForm objConsent = new BO_tblConsentForm();
        protected void btnSave1_Click(object sender, EventArgs e)
        {
            ConsentFormDAL objDConsent = new ConsentFormDAL();
            objConsent.ConsentFormID = (int)MasterItems.ConsentForm.ConsentFormID;
            objConsent.Description1 = txtDesc1.Text;
            objConsent.Description2 = txtDesc2.Text;
            objConsent.Description3 = txtDesc3.Text;
            objConsent.Description4 = txtDesc4.Text;
            objDConsent.SaveConsentForm(objConsent);
            lblDesc.Text = "Save Successfully";

        }
    }
}