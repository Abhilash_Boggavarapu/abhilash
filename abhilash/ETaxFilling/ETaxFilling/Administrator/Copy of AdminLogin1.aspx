﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminLogin.aspx.cs" Inherits="ETaxFilling.Administrator.AdminLogin" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
   
   <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script src="B_Scripts/Watermark/WaterMark.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $("[id*=txtUserName],[id*=txtPwd]").WaterMark();
        });
</script>
</head>
<body>
    <form id="form1" runat="server">
   <div style="width: 393px; height: 249px; margin-left: 289px">
        <div style="height: 183px; width: 296px; margin-left: 43px">
            <table>
                <tr>
                    <td style="font-size: xx-large; text-align: center">
                        Login Here</td>
                </tr>
            </table>
            <table class="style2">
                <tr>
                    <td class="style3" style="text-align: left">
                      User Name<span style="color:red;">*</span>
                        </td>
                    <td>
                        <asp:TextBox ID="txtUserName" runat="server" CssClass="ValidationRequired" 
                            MaxLength="50"  Width="150px" TabIndex="1" ToolTip = "Enter UserName"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style3">
                        Password<span 
                            style="color:red;">*</span>
                        </td>
                    <td>
                        <asp:TextBox ID="txtPwd" runat="server" CssClass="ValidationRequired" 
                            TextMode="Password" MaxLength="15"  Width="150px" 
                            TabIndex="2" ToolTip = "Enter Password"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style3">
                    </td>
                    <td>
                        <asp:Button ID="btnLogin" runat="server" Text="Login" onclick="btnSave_Click" 
                            Width="80px" TabIndex="3" />
                    </td>
                </tr>
            </table>
            <asp:Label ID="lblStatus" runat="server" ForeColor="#CC0000"></asp:Label>
        </div>
    </div>
    </form>
</body>
</html>

