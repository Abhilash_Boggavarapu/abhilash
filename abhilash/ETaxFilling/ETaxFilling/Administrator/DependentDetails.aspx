﻿<%@ Page Language="C#"  MasterPageFile="~/Administrator/Admin.Master"  AutoEventWireup="true" CodeBehind="DependentDetails.aspx.cs" Inherits="ETaxFilling.Administrator.DependentDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

      <script type="text/javascript">
          $(function () {
              $("#txtDob,#txtDoE").datepicker({
                  showOn: "button",
                  buttonImage: "../B_Images/calendar_icon.png",
                  yearRange: YEAR_RANGE_GLOBAL,
                  changeMonth: true,
                  changeYear: true,
                  buttonImageOnly: true
              });
          });
    </script>
<div align ="center" >

 <table>
        <tr> 
        <td>
        <div><ul class ="submenu" >
                       <li><asp:LinkButton runat="server" ID="lnkTaxPayer" Text="Tax Payer Details" onclick="lnkTaxPayer_Click" 
                               ></asp:LinkButton>
                               </li>
	       <li><asp:LinkButton runat="server" ID="lnkSpouse" Text="Spouse Details" onclick="lnkSpouse_Click"  
                               ></asp:LinkButton>   </li>
      <li> <a style="color: ButtonHighlight " href="#" >Dependent Details</a></li>
      </ul>
     </div>
     </td>
     </tr>
     </table> 
   

             <table width="700px"class="box">
      <tr>
    <td>
       <h3 align="center">
           <asp:Label ID="lblClientDetails" runat="server" Text=" Dependant Details" />
       </h3>
        </td>
        </tr>
     </table>

                    <br />

<div id="divDDerr" style="color:#FF0000" runat="server"></div>
<div> 
    <h4><asp:Label Text="" ID="lblMsg" runat="server" /> </h4>     </div>
      
        <table cellpadding="0" cellspacing="0" border="0" class="box">

            <tr>
                <td align="right">
                    <span style="color: #FF0000">*</span> First Name : </td>
                <td align="left">
                   <asp:TextBox ID="txtFname" runat="server" CssClass="ValidationRequired" 
                        MaxLength="50" Height="20px" Width="150px" TabIndex="3"></asp:TextBox>
                  </td>
            </tr>
            <tr>
                <td align="right">
                    <span style="color: #FF0000">*</span> Middle Name : </td>
                <td  align="left">
                    <asp:TextBox ID="txtMname" runat="server" CssClass="ValidationRequired" 
                        MaxLength="50" TabIndex="4" Height="20px" Width="150px"></asp:TextBox>
                 </td>
            </tr>
            <tr>
                <td align="right">
                    <span style="color: #FF0000">*</span> Last Name : </td>
                <td  align="left">
                    <asp:TextBox ID="txtLname" runat="server" CssClass="ValidationRequired" 
                        MaxLength="20" TabIndex="5" Height="20px" Width="150px"></asp:TextBox>
                  </td>
            </tr>
            <tr>
                <td align="right">
                   <span style="color: #FF0000">*</span> Date of Birth : </td>
                <td  align="left">
                    <asp:TextBox ID="txtDob" ClientIDMode="Static" runat="server" CssClass="ValidationRequired date" 
                        TabIndex="6" Height="20px" Width="150px"></asp:TextBox>
                    <br />
                   </td>
            </tr>
            <tr>
                <td align="right">
                   <span style="color: #FF0000">*</span> Relationship : </td>
                <td  align="left" >
 
                    <asp:DropDownList ID="ddlRelation" CssClass="ValidationRequired" runat="server" 
                        TabIndex="7" Height="20px" Width="150px" >
                       
                        <asp:ListItem>Select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="right">
                    &nbsp;SSN/ITIN : </td>
                <td  align="left">
                   <asp:TextBox ID="txtSsn" runat="server" 
                        MaxLength="15" TabIndex="8" Height="20px" Width="150px"></asp:TextBox>
                   </td>
            </tr>
            <tr>
                <td align="right">
                    &nbsp;Visa Status : </td>
                <td  align="left">
             
                    <asp:DropDownList ID="ddlVisa" runat="server" 
                        TabIndex="9" Height="20px" Width="150px">
                        <asp:ListItem>Select</asp:ListItem>
                    </asp:DropDownList>
                   </td>
            </tr>
            <tr>
                <td align="right">
                    Date of Entry into US : </td>
                <td  align="left">
                    <asp:TextBox ID="txtDoE" ClientIDMode="Static" runat="server" TabIndex="10" Height="20px" Width="150px" ></asp:TextBox>
                   </td>
            </tr>
            <tr>
                <td>
                   </td>
                <td align="left" style=" padding-left:20px;">
                    <asp:Button ID="btnSave" runat="server" CssClass="button" 
                        onclick="btnSave_Click" Text="Save" Width="91px" TabIndex="11" /> 
                </td>
            </tr>
        </table>
   
    <table style="width: 100%">
        <tr>
            <td style="width: 274px">
                &nbsp;</td>
            <td align="right" >
                <asp:HyperLink NavigateUrl="~/Administrator/PersonalInfoConfirmDetails.aspx" CssClass="button" Text="Next" Width="100px" runat="server" />
                 </td>
        </tr>
    </table>
  
    <table >
    
    <tr id ="gridDisplay" runat ="server"><td>
        <asp:GridView ID="GrdDpntlist" runat="server" 
            AlternatingRowStyle-CssClass="alt" margin="10%"
AutoGenerateColumns="False"  GridLines="Vertical"
        PagerStyle-CssClass="pgr" Width="510px" onrowcommand="GrdDpntlist_RowCommand" 
            AllowPaging="True" CssClass="mGrid" 
            onpageindexchanging="GrdDpntlist_PageIndexChanging">  
           
<AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
<Columns><asp:TemplateField HeaderText="SNo">
<ItemTemplate><%#Container.DataItemIndex+1 %><br />
</ItemTemplate>
</asp:TemplateField>
    
    <asp:BoundField DataField ="TaxPayerDependentID"  Visible="false"/>      
<asp:BoundField DataField="FirstName" HeaderText="First Name" />
<asp:BoundField DataField="MiddleName" HeaderText="Middle Name" />
<asp:BoundField DataField="LastName" HeaderText="Last Name" />
<asp:BoundField DataField="DOB" HeaderText="Date Of Birth" DataFormatString="{0:d}" 
        HtmlEncode="False"/>
<asp:BoundField  DataField="Relationship"  HeaderText="Relationship" />
<asp:BoundField  DataField="SSN"  HeaderText="SSN/ITIN" />
<asp:BoundField  DataField="USEntryDate"  HeaderText="USEntryDate " 
        DataFormatString="{0:d}" HtmlEncode="False" />
<asp:BoundField  DataField="VisaStatus"  HeaderText="Visa Status " />


<asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="lnkbtnEdit" runat="server" Text="Edit" CommandArgument='<%# DataBinder.Eval(Container,"DataItem.TaxPayerDependentID") %>'
                        CommandName="DepnEdit" ></asp:LinkButton>
                       
                </ItemTemplate>
            </asp:TemplateField>
            <%--<asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="lnkbtnRemove" runat="server" Text="Remove" CommandArgument='<%# DataBinder.Eval(Container,"DataItem.TaxPayerDependentID") %>'
                        CommandName="Remove" ></asp:LinkButton>                       
                </ItemTemplate>
            </asp:TemplateField>--%>

</Columns>
            <PagerSettings FirstPageText="First" LastPageText="Last" 
                Mode="NumericFirstLast" />

<PagerStyle CssClass="pgr"></PagerStyle>
</asp:GridView>

</td></tr>
</table>
  </div>
 

</asp:Content>