﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ETaxDal;
using ETaxBO;

namespace ETaxFilling.Administrator
{
    public partial class ReferalDetails : System.Web.UI.Page
    {
        long userID;
        DateTime startDate;
        DateTime endDate;
        ReferralDal objReferalDal = new ReferralDal();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Utility.Utility.CheckAdminLoginSession())
            {
                Response.Redirect("~/Administrator/AdminLogin.aspx");
            }
            lblError.Text = string.Empty;
            if (!IsPostBack)
            {
                rbHome.Checked = true; 
            }
        }
        protected void grdReHomeRferal_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdHomeRferal.PageIndex = e.NewPageIndex;
            diplayreferralhomeDetails();
        }

        private void displayClientReferalDetails()
        {
            DateTime startDate = DateTime.Parse(txtStartDate.Text);
            DateTime endDate = DateTime.Parse(txtEndDate.Text);
            List<BO_tblUserReferral> objUserReferral = objReferalDal.getReferalClientDetails(Convert.ToInt32(MasterItems.ReferralTypes.ReferredfromClientLogin), startDate, endDate);
            if (objUserReferral.Count != 0)
            {
                grdClientReferal.DataSource = objUserReferral;
                grdClientReferal.DataBind();
            }
            else
            {
                lblError.Text = "No Records Found";
                lblError.ForeColor = System.Drawing.Color.Red;
            }
        }

        protected void grdClientReferal_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdClientReferal.PageIndex = e.NewPageIndex;
            displayClientReferalDetails();
            GridView1.DataSource = null;
            GridView1.DataBind();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtStartDate.Text))
            {
                ErrRDetails.InnerText = Utility.Utility.ReqSDate;
            }
            else if (!txtStartDate.Text.DataFormat())
            {
                ErrRDetails.InnerText = Utility.Utility.ValSDate;
                txtStartDate.Text = string.Empty;
                txtStartDate.Focus();
            }
            else if (string.IsNullOrEmpty(txtEndDate.Text))
            {
                ErrRDetails.InnerText = Utility.Utility.ReqEDate;
            }
            else if (!txtEndDate.Text.DataFormat())
            {
                ErrRDetails.InnerText = Utility.Utility.ValEDate;
                txtEndDate.Text = string.Empty;
                txtEndDate.Focus();
            }
            else if (Convert.ToDateTime(txtStartDate.Text) > Convert.ToDateTime(txtEndDate.Text))
            {
                ErrRDetails.InnerText = Utility.Utility.LessSEDate;
            }
            else
            {
                if (rbHome.Checked == true)
                {
                    ErrRDetails.InnerText = string.Empty;
                    grdClientReferal.DataSource = null;
                    grdClientReferal.DataBind();
                    diplayreferralhomeDetails();
                }
                else if (rbClient.Checked == true)
                {
                    ErrRDetails.InnerText = string.Empty;
                    grdHomeRferal.DataSource = null;
                    grdHomeRferal.DataBind();
                    displayClientReferalDetails();
                }
            }
        }
        private void diplayreferralhomeDetails()
        {
            startDate = DateTime.Parse(txtStartDate.Text);
            endDate = DateTime.Parse(txtEndDate.Text);
            List<BO_tblUserReferral> objUserReferral = objReferalDal.getReferalHomeDetails(Convert.ToInt32(MasterItems.ReferralTypes.ReferredfromHomePage), startDate, endDate);
            if (objUserReferral.Count != 0)
            {
                grdHomeRferal.DataSource = objUserReferral;
                grdHomeRferal.DataBind();
            }
            else
            {
                lblError.Text = "No Records Found";
                lblError.ForeColor = System.Drawing.Color.Red;
            }
        }

        protected void grdClientReferal_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            ViewState["userid"] = null;
            if (e.CommandName == "ViewRow")
            {
                 userID = Convert.ToInt64((e.CommandArgument));
                 ViewState["userid"] = userID;
                bindDetails(userID);
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "popup", "javascript:fnmodalpopup();", true);    
            }
        }

        private void bindDetails(long userID)
        {
            GridView1.DataSource = objReferalDal.getReferalClientDetails(userID, Convert.ToInt32(Convert.ToInt32(MasterItems.ReferralTypes.ReferredfromClientLogin)));
            GridView1.DataBind();
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.DataSource = null;
            GridView1.DataBind();
            GridView1.PageIndex = e.NewPageIndex;
            userID = Convert.ToInt64(ViewState["userid"]);
            bindDetails(userID);
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "popup", "javascript:fnmodalpopup();", true);
        }

        protected void rbClient_CheckedChanged(object sender, EventArgs e)
        {
            grdHomeRferal.DataSource = null;
            grdHomeRferal.DataBind();
            txtEndDate.Text = txtStartDate.Text = string.Empty;
        }

        protected void rbHome_CheckedChanged(object sender, EventArgs e)
        {
            grdClientReferal.DataSource = null;
            grdClientReferal.DataBind();
            GridView1.DataSource = null;
            GridView1.DataBind();
            txtEndDate.Text = txtStartDate.Text = string.Empty;
        }
    }
}