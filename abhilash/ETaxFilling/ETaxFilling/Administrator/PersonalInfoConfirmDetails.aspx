﻿<%@ Page Language="C#" MasterPageFile="~/Administrator/Admin.Master" AutoEventWireup="true" CodeBehind="PersonalInfoConfirmDetails.aspx.cs" Inherits="ETaxFilling.Administrator.PersonalInfoConfirmDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <script src="../B_Scripts/Calender/jquery-1.7.2.min.js" type="text/javascript"></script>
   
    
                       <table width="100%" border="0px" cellpadding="0" cellspacing="4" class="box">
                            <tr>
                                <td colspan="2">
                                    <h2 class="divider top20 bottom10">
                                        Tax Payer Details</h2>
                                </td>
                            </tr>
                            <tr>
                               <%-- <td align="center">
                                    <asp:Button runat="server" ID="btnConfirm" Text="Confirm"  OnClick="btnConfirm_Click"
                          CssClass="button" OnClientClick ='return confirm ("Are you sure You want to confirm?") ' 
                                        Width="85px" />

                                       

                                </td>--%>
                                <td align="center">
                                    <asp:Button ID="btnEdittaxPayerDetails" runat="server" Text="Edit" Width="80px" OnClick="btnEdittaxPayerDetails_Click"
                                        CssClass="button" />
                                </td>
                            </tr>
                        </table>
                        <table width="700px" border="0px" cellpadding="10" cellspacing="10" class="box">
                            <tr>
                                <td align="right">
                                    <asp:Label ID="lblPrscon" runat="server"></asp:Label>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    First Name :
                                </td>
                                <td style="width: 50%;">
                                    <asp:Label ID="lbltaxFname" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    Middle Name :
                                </td>
                                <td style="width: 50%;">
                                    <asp:Label ID="lbltaxMiddleName" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    Last Name :
                                </td>
                                <td style="width: 50%;">
                                    <asp:Label ID="lbltaxLastName" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    Date Of Birth :
                                </td>
                                <td style="width: 50%;">
                                    <asp:Label ID="lbltaxDOB" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    SSN :
                                </td>
                                <td style="width: 50%;">
                                    <asp:Label ID="lbltaxSSN" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    Gender :
                                </td>
                                <td style="width: 50%;">
                                    <asp:Label ID="lbltaxGender" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    Occupation :
                                </td>
                                <td style="width: 50%;">
                                    <asp:Label ID="lbltaxOccupation" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    Street No :
                                </td>
                                <td style="width: 50%;">
                                    <asp:Label ID="lbltaxStreetNo" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    AppartmentNo :
                                </td>
                                <td style="width: 50%;">
                                    <asp:Label ID="lbltaxAppartmentNo" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    MaritalStatus :
                                </td>
                                <td style="width: 50%;">
                                    <asp:Label ID="lbltaxMaritalStatus" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    Country :
                                </td>
                                <td style="width: 50%;">
                                    <asp:Label ID="lbltaxCountry" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    State :
                                </td>
                                <td style="width: 50%;">
                                    <asp:Label ID="lbltaxState" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    City :
                                </td>
                                <td style="width: 50%;">
                                    <asp:Label ID="lbltaxCity" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    ZipCode</td>
                                <td style="width: 50%;">
                                    <asp:Label ID="lbltaxZipCode" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    Home No :
                                </td>
                                <td style="width: 50%;">
                                    <asp:Label ID="lbltaxHome" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    Mobile No :
                                </td>
                                <td style="width: 50%;">
                                    <asp:Label ID="lbltaxMobile" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    Lanline No :
                                </td>
                                <td style="width: 50%;">
                                    <asp:Label ID="lbltaxLandline" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    Primary Email ID :
                                </td>
                                <td style="width: 50%;">
                                    <asp:Label ID="lbltaxprimaryEmail" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    Secondary Email ID :
                                </td>
                                <td style="width: 50%;">
                                    <asp:Label ID="lbltaxsecondaryEmail" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    Visa Status :
                                </td>
                                <td style="width: 50%;">
                                    <asp:Label ID="lbltaxVisaStatus" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    Date Of Entry Into US :
                                </td>
                                <td style="width: 50%;">
                                    <asp:Label ID="lbltaxDateOfEntryToUS" runat="server"></asp:Label>
                                </td>
                                </tr>
                        </table>

                         <asp:Panel ID="PanelSpouseConfirmData" runat="server">     
                        <table width="100%" border="0px" cellpadding="0" cellspacing="10" class="box">
                            <tr>
                                <td width="500px">
                                    <h2 class="divider top20 bottom10">Spouse Details</h2>
                                </td>
                                 <td>
                                    <asp:Button ID="btnEditSpouseDetails" runat="server" Text="Edit" 
                                         CssClass="button" OnClick="btnEditSpouseDetails_Click" TabIndex="1" 
                                         Width="146px" />
                                </td>
                            </tr>                           
                            <tr>
                                <td colspan="2" align="center">
                                   <asp:Label Text="" CssClass="error" ID="lblSpouse" runat="server" />
                                </td>
                                </tr>
                                </table>
                                
                      
                        <table width="700px" border="0px" cellpadding="0" cellspacing="10" class="box"> 
                          
                           
                            <tr>
                                <td align="right" >
                                    First Name : 
                                </td>
                                <td style="width: 50%;">
                                    <asp:Label ID="lblSpouseFName" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" >
                                    Middle Name : 
                                </td>
                                <td style="width: 50%;">
                                    <asp:Label ID="lblSpouseMiddlName" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" >
                                    Last Name : 
                                </td>
                                <td style="width: 50%;">
                                    <asp:Label ID="lblSpouseLstName" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" >
                                    Date Of Birth : 
                                </td>
                                <td style="width: 50%;">
                                    <asp:Label ID="lblSpouseDateOfBirth" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" >
                                    SSN : 
                                </td>
                                <td style="width: 50%;">
                                    <asp:Label ID="lblSpouseSSN" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" >
                                    Gender : 
                                </td>
                                <td style="width: 50%;">
                                    <asp:Label ID="lblSpouseGendr" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" >
                                    Occupation : 
                                </td>
                                <td style="width: 50%;">
                                    <asp:Label ID="lblSpouseOccptn" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" >
                                    Street No : 
                                </td>
                                <td style="width: 50%;">
                                    <asp:Label ID="lblSpouseStreetNo" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" >
                                    Appartment No : 
                                </td>
                                <td style="width: 50%;">
                                    <asp:Label ID="lblSpouseappartmntNo" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" >
                                    Marital Status : 
                                </td>
                                <td style="width: 50%;">
                                    <asp:Label ID="lblSpouseMaritaStatus" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" >
                                    Country : 
                                </td>
                                <td style="width: 50%;">
                                    <asp:Label ID="lblSpouseCuntry" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" >
                                    State : 
                                </td>
                                <td style="width: 50%;">
                                    <asp:Label ID="lblSpousestate" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" >
                                    City : 
                                </td>
                                <td style="width: 50%;">
                                    <asp:Label ID="lblSpousecity" runat="server"></asp:Label>
                                   
                                </td>
                            </tr>
                            <tr>
                                <td align="right" >
                                    Home No : 
                                </td>
                                <td style="width: 50%;">
                                    <asp:Label ID="lblSpouseHmeNo" runat="server"></asp:Label>
                                    
                                </td>
                            </tr>
                            <tr>
                                <td align="right" >
                                    Mobile No : 
                                </td>
                                <td style="width: 50%;">
                                    <asp:Label ID="lblSpouseMobileNo" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" >
                                    Lanline No : 
                                </td>
                                <td style="width: 50%;">
                                    <asp:Label ID="lblSpouseLandlineNo" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" >
                                    Primary Email ID : 
                                </td>
                                <td style="width: 50%;">
                                    <asp:Label ID="lblSpousePrimaryEmail" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" >
                                    Secondary Email ID : 
                                </td>
                                <td style="width: 50%;">
                                    <asp:Label ID="lblSpouseSecondryEmail" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" >
                                    Visa Status : 
                                </td>
                                <td style="width: 50%;">
                                    <asp:Label ID="lblSpouseVisStatus" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" >
                                    Date Of Entry Into US : 
                                </td>
                                <td style="width: 50%;">
                                    <asp:Label ID="lblSDOE" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
            
              
                      <table width="100%" border="0px" cellpadding="0" cellspacing="3" class="box">                     
    <tr>
        <td>
        <h2 class="divider top20 bottom10">Dependent Details</h2>
        </td>
       </tr>
       <tr><td>
            <asp:Button ID="btnEditDependentDetails" runat="server" Text="Edit"  
                   CssClass="button" OnClick="btnEditDependentDetails_Click" TabIndex="2" 
                Width="87px" />
              
          </td></tr>
       <tr>
       <td align="center">  <asp:Label Text="" CssClass="error" ID="lblDependent" runat="server" />
       </td>
       </tr>
                          
      <tr id ="gridDisplay" runat ="server"><td>
 <asp:GridView ID="GVWDependentConfirmDetails" runat="server" AlternatingRowStyle-CssClass="alt"
AutoGenerateColumns="False"  GridLines="Vertical"
 PagerStyle-CssClass="pgr" Width="100%" CssClass="mGrid" >           
<AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
<Columns>                                                           
<asp:TemplateField HeaderText="S.No.">
<ItemTemplate><%#Container.DataItemIndex+1 %><br />
</ItemTemplate>
</asp:TemplateField>
    
    <asp:BoundField DataField ="TaxPayerDependentID"  Visible="false"/>      
<asp:BoundField DataField="FirstName" HeaderText="First Name" />
<asp:BoundField DataField="MiddleName" HeaderText="Middle Name" />
<asp:BoundField DataField="LastName" HeaderText="Last Name" />
<asp:BoundField DataField="DOB" HeaderText="Date Of Birth" DataFormatString="{0:d}" 
        HtmlEncode="False"/>
<asp:BoundField  DataField="Relationship"  HeaderText="Relationship" />
<asp:BoundField  DataField="SSN"  HeaderText="SSN/ITIN" />
<asp:BoundField  DataField="USEntryDate"  HeaderText="USEntryDate " 
        DataFormatString="{0:d}" HtmlEncode="False" />
<asp:BoundField  DataField="VisaStatus"  HeaderText="Visa Status " />

</Columns>

<PagerStyle CssClass="pgr"></PagerStyle>
</asp:GridView>

</td></tr>
<%--<tr><td align="center"><asp:Button runat="server" ID="btnConFirmDown" Text="Confirm"  OnClick="btnConfirm_Click"
                          CssClass="button" 
        OnClientClick ='return confirm ("Are you sure You want to confirm?") ' 
        Width="86px" /></td></tr>--%>
   </table>
      
    </div>       
   

</asp:Content>