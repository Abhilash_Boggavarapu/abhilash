﻿
namespace ETaxFilling.Administrator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using ETaxBO;
    using System.IO;
    using ETaxDal;
        public partial class UploadTaxInformation : System.Web.UI.Page
        {
       UploadDocumentsDAL objDocumentDal = new UploadDocumentsDAL();
            int sheetType;
            long taxPayerID;
            string remarks;
            protected void Page_Load(object sender, EventArgs e)
            {
                if (!Utility.Utility.CheckAdminLoginSession())
                {
                    Response.Redirect("~/Administrator/AdminLogin.aspx");
                }
                else
                {
                    lblMsg.Text = string.Empty;
                    grdClintDisplay.Visible = false;
                    GdVwUploadInformation.Visible = false;
                    taxPayerID = Utility.Utility.getLoginSession().TaxPayerID;
                }
            }

            private void fileExtension(string remarks, int sheetType, FileUpload fileupload)
            {

                if (fileupload.PostedFile.FileName == "")
                {
                    Label1.ForeColor = System.Drawing.Color.Red;
                    Label1.Text = "No File selected To Upload";
                }
                else
                {
                    string[] validFileTypes = { "zip", "rar", "doc", "docx", "pdf", "jpeg","jpg","xlsx","xls" };
                    string ext = System.IO.Path.GetExtension(fileupload.PostedFile.FileName);
                    bool isValidFile = false;
                    for (int i = 0; i < validFileTypes.Length; i++)
                    {
                        if (ext == "." + validFileTypes[i])
                        {
                            isValidFile = true;
                            break;
                        }
                    }
                    if (!isValidFile)
                    {
                        Label1.ForeColor = System.Drawing.Color.Red;
                        Label1.Text = "Invalid File. Please upload a File with extension " +
                        string.Join(",", validFileTypes);
                    }
                    else
                    {
                        SaveDocuments(remarks, sheetType);
                        Label1.ForeColor = System.Drawing.Color.Green;
                        Label1.Text = "File uploaded successfully.";

                    }
                }
            }
            private void SaveDocuments(string remarks,int sheetType)
            {
                try
                {
                    BO_tblTaxSheets objSheets = new BO_tblTaxSheets();

                    // Get the HttpFileCollection
                    HttpFileCollection hfc = Request.Files;
                    for (int i = 0; i < hfc.Count; i++)
                    {
                        HttpPostedFile hpf = hfc[i];
                        if (hpf.ContentLength == 0)
                        {
                            Label1.ForeColor = System.Drawing.Color.Red;
                            Label1.Text = "Can't upload empty files";
                        }
                        if (hpf.ContentLength > 0)
                        {
                            string fileName = hpf.FileName;
                            int fileSize = hpf.ContentLength;
                            byte[] documentBinary = new byte[fileSize];
                            hpf.InputStream.Read(documentBinary, 0, fileSize);
                            objSheets.Remarks = remarks;
                            objSheets.SheetName = fileName;
                            objSheets.SheetsType = sheetType;
                            objSheets.TaxSheet = documentBinary;
                            objSheets.TaxPayerID = taxPayerID;
                            objSheets.TaxSheetSource = MasterItems.TaxSheetSource.Admin.ToString();
                            objDocumentDal.SaveTaxSheet(objSheets);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            protected void GdVwUploadInformation_RowCommand(object sender, GridViewCommandEventArgs e)
            {

                if (e.CommandName == "Download")
                {
                   BO_tblTaxSheets objTaxSheets  = objDocumentDal.GetTaxPayerSheets(Convert.ToInt32(e.CommandArgument.ToString()));
                   byte[] fileData = objTaxSheets.TaxSheet;
                    Response.ClearContent();
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + objTaxSheets.SheetName);
                    BinaryWriter bw = new BinaryWriter(Response.OutputStream);
                    bw.Write(fileData);
                    bw.Close();
                    Response.ContentType = "application";
                    Response.End();
                }
            }
         protected void BtnView_Click(object sender, EventArgs e)
            {
                Label1.Text = "";
                GdVwUploadInformation.Visible = true;
                sheetType = Convert.ToInt32(((Button)sender).CommandArgument);
                MasterItems.TaxSheetSType SheetTypes = (MasterItems.TaxSheetSType)sheetType;
                
                switch (SheetTypes)
                {
                    case MasterItems.TaxSheetSType.TaxInformation:
                        dispaly(sheetType);
                        lblMsg.Text = lblMsg.Text + "TaxSheets";
                        break;
                    case MasterItems.TaxSheetSType.ReturnDocuments:
                       dispaly(sheetType);
                        lblMsg.Text = lblMsg.Text + "ReturnDocuMents";
                        break;
                   case MasterItems.TaxSheetSType.ConfirmTaxDocuments:
                        dispaly(sheetType);
                        lblMsg.Text = lblMsg.Text + "ConfirmTaxDocuments";
                        break;
                    
                    default:
                break;
                }           
            }
            private void dispaly(int sheetType)
            {
                //GdVwUploadInformation.DataSource = objDocumentDal.GetTaxpayerInformation(taxPayerID, sheetType,(MasterItems.TaxSheetSource.Admin).ToString());
                //GdVwUploadInformation.DataBind();
            GdVwUploadInformation.DataSource = null;
            GdVwUploadInformation.DataBind();
            List<BO_tblTaxSheets> objDoc = objDocumentDal.GetTaxpayerInformation(taxPayerID, sheetType,(MasterItems.TaxSheetSource.Admin).ToString());
            if (objDoc.Count != 0)
            {
                GdVwUploadInformation.DataSource = objDoc;
                GdVwUploadInformation.DataBind();              
            }
            else
            {
                lblMsg.Text = "No Files Found for ";
            }
        }
           protected void btnRDUpload_Click(object sender, EventArgs e)
            {
                remarks = txtRDRemarks.Text;
                fileExtension(remarks, Convert.ToInt32(MasterItems.TaxSheetSType.ReturnDocuments), FileUpload2);
                txtRDRemarks.Text = string.Empty;
            }

            protected void btnConfirm_Click(object sender, EventArgs e)
            {
                remarks = txtConfirm.Text;
                fileExtension(remarks, Convert.ToInt32(MasterItems.TaxSheetSType.ConfirmTaxDocuments), FileUpload3);
                txtConfirm.Text = string.Empty;
            }

            protected void GdVwUploadInformation_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                GdVwUploadInformation.PageIndex = e.NewPageIndex;
                dispaly(sheetType);
            }
            protected void ViewtaxpayerDoc_Click(object sender, EventArgs e)
            {
                grdClintDisplay.Visible = true;
                UploadDocumentsDAL objDocuMentDal = new UploadDocumentsDAL();
                List<BO_tblTaxSheets> objTaxSheets = objDocuMentDal.getReturnTaxpayerInformation(Utility.Utility.getLoginSession().TaxPayerID, Convert.ToInt32(MasterItems.TaxSheetSType.ConfirmTaxDocuments), (MasterItems.TaxSheetSource.TaxPayer).ToString());
                if (objTaxSheets.Count!=0)
                {
                    grdClintDisplay.DataSource = objTaxSheets;
                    grdClintDisplay.DataBind();
                }
                else
                {
                    lblMsg.Text = "No Records Found";
                    lblMsg.ForeColor = System.Drawing.Color.Red;
                }
            }
            protected void grdClintDocDisplay_RowCommand(object sender, GridViewCommandEventArgs e)
            {
                if (e.CommandName == "Download")
                {
                    BO_tblTaxSheets objTaxSheets = objDocumentDal.GetTaxPayerSheets(Convert.ToInt32(e.CommandArgument.ToString()));
                    byte[] fileData = objTaxSheets.TaxSheet;
                    Response.ClearContent();
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + objTaxSheets.SheetName);
                    BinaryWriter bw = new BinaryWriter(Response.OutputStream);
                    bw.Write(fileData);
                    bw.Close();
                    Response.ContentType = "application";
                    Response.End();
                }
            }
        }
    }
        
    
