﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ETaxFilling.Administrator
{
    public partial class AdminHome : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Utility.Utility.CheckAdminLoginSession() == false)
                {
                    Response.Redirect("AdminLogin.aspx");
                }
            }
        }
    }
}