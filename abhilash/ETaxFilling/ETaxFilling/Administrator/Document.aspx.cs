﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ETaxDal;
using ETaxBO;

namespace ETaxFilling
{
    public partial class Document : System.Web.UI.Page
    {
        int DocumentID;

        UploadDocumentsDAL ObjDocument = new UploadDocumentsDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Utility.Utility.CheckAdminLoginSession())
            {
                Response.Redirect("~/Administrator/AdminLogin.aspx");
            }
            lblMsg.Text = "";
            dataBind();
        }

        private void dataBind()
        {
            GrdwDocumentList.DataSource = ObjDocument.GetFormsName();
            GrdwDocumentList.DataBind();
        }

        protected void btnSaveDoc_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtForm.Text))
            {
                divDerr.InnerText = Utility.Utility.ReqDoc;
            }
            else
            {
                divDerr.InnerText = string.Empty;
                BO_tblDocument objDoc = new BO_tblDocument();
                if (ViewState["DocumentID"] != null)
                objDoc.DocumentID = Convert.ToInt32(ViewState["DocumentID"]);
                objDoc.FormName = txtForm.Text;
                ObjDocument.SaveUpdateDocument(objDoc);
                dataBind();
                if (btnSaveDoc.Text == "Save")
                    lblMsg.Text = "Submitted successfully";
                else
                    lblMsg.Text = "Updated successfully";       
                txtForm.Text = string.Empty;
            }
        }
         protected void GrdwDocumentList_RowCommand(object sender, GridViewCommandEventArgs e)
         {
             switch (e.CommandName)
             {
                 case "EditDoc":

                     BO_tblDocument objDocument = ObjDocument.GetFormName(Convert.ToInt32(e.CommandArgument));
                     if (!objDocument.Equals(null))
                     {
                         GridViewRow row = (GridViewRow)((LinkButton)e.CommandSource).NamingContainer;

                         ViewState["DocumentID"] = Convert.ToInt32(e.CommandArgument);
                         txtForm.Text = row.Cells[2].Text;
                         btnSaveDoc.Text = "Update";
                     }

                     break;

                 case "Remove":

                     break;
                     
             }
         }

         protected void GrdwDocumentList_PageIndexChanging(object sender, GridViewPageEventArgs e)
         {
             GrdwDocumentList.PageIndex = e.NewPageIndex;
             dataBind();
         }

    }
}