﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ETaxDal;
using ETaxDal.Admin;
using ETaxBO;

namespace ETaxFilling.Administrator
{
    public partial class ClientSearch : System.Web.UI.Page
    {     
        AdminMasterDAL objAdminMasterDal = new AdminMasterDAL();
        string name;
        long taxPayerID;
        string emailid;
        DateTime? dob = null;
        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (Utility.Utility.CheckAdminLoginSession() == false)
            {
                Response.Redirect("AdminLogin.aspx");
                Utility.Utility.setLoginSessionToEmpty(null);
            }
            else if (!IsPostBack)
            {
                ddlPendingClients.DataSource = objAdminMasterDal.getFileStatusTypes();
                ddlPendingClients.DataTextField = "TaxFilingStatusType";
                ddlPendingClients.DataValueField = "TaxFilingStatusTypeID";
                ddlPendingClients.DataBind();
                lblMsg.Text = string.Empty;
                lblStatus.Text = string.Empty;
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            grdDisplay.DataSource = null;
            grdDisplay.DataBind();
            ViewState["Search"] = null;
            ViewState["Search"] = 2;
            if (!string.IsNullOrWhiteSpace(txtID.Text) || !string.IsNullOrWhiteSpace(txtName.Text) || !string.IsNullOrWhiteSpace(txtEmailid.Text) || !string.IsNullOrWhiteSpace(txtDOB.Text))
            {

                if (!string.IsNullOrWhiteSpace(txtID.Text))
                {
                    if (txtID.Text.IsNumeric())
                    {
                        taxPayerID = Convert.ToInt64(txtID.Text);
                    }
                    else
                    {
                        txtError.InnerText = Utility.Utility.ValFilenumber;
                    }
                }
                if (!string.IsNullOrWhiteSpace(txtName.Text))
                {
                    name = txtName.Text;
                }
                if (!string.IsNullOrWhiteSpace(txtEmailid.Text))
                {
                    if (txtEmailid.Text.IsValidEmailID())
                    {
                        emailid = txtEmailid.Text.Trim();
                    }
                    else
                    {
                        lblMsg.Text = " Please Enter Valid Email ID";
                        lblMsg.ForeColor = System.Drawing.Color.Red;
                        return;
                    }
                }
                if (!string.IsNullOrWhiteSpace(txtDOB.Text))
                {
                    if (txtDOB.Text.DataFormat())
                    {
                        dob = Convert.ToDateTime(txtDOB.Text);
                    }
                    else
                    {
                        lblMsg.Text = " Please Enter MM/DD/YYYY Format";
                        lblMsg.ForeColor = System.Drawing.Color.Red;
                        return;
                    }
                }
                dispaly(name, emailid, dob);
            }
            else
            {
                grdDisplay.DataSource = null;
                grdDisplay.DataBind();
                lblMsg.Text = " Please Enter Atleast One Field";
                lblMsg.ForeColor = System.Drawing.Color.Red;
            }
        }

        private void dispaly(string name, string emailid, DateTime? dob)
        {
            TaxPayerDAL objDtaxPay = new TaxPayerDAL();
            List<BO_tblTaxPayerDetail> objSerch = objDtaxPay.SearchTaxPayer(taxPayerID,name, emailid, dob, Convert.ToInt32(MasterItems.TaxPayerType.self));
            if (objSerch.Count != 0)
            {
                grdDisplay.DataSource = objSerch;
                grdDisplay.DataBind();
            }
            else
            {
                lblMsg.Text = "No records Found";
                txtDOB.Text = txtEmailid.Text = txtName.Text = string.Empty;
                lblMsg.ForeColor = System.Drawing.Color.Red;
                grdDisplay.DataSource = null;
                grdDisplay.DataBind();
            }
        }
      
        protected void grdDisplay_RowCommand(object sender, GridViewCommandEventArgs e)
        {
           
            if (e.CommandName == "ViewData")
            {
                long taxPayerID = Convert.ToInt64(e.CommandArgument.ToString());
                SearchDAL objAdminSearch = new SearchDAL();
                BO_tblTaxPayerDetail objTaxPayerDetail = objAdminSearch.GetTaxPayerDetails(taxPayerID);
                Utility.Utility.setLoginSession(objTaxPayerDetail);
                Response.Redirect("MyFileStatus.aspx");
                // Response.Redirect("AdminHome.aspx");

            }
            else if (e.CommandName == "PreparationCompleted")
            {
                long taxPayerID = Convert.ToInt64(e.CommandArgument.ToString());
                SearchDAL objAdminSearch = new SearchDAL();
                BO_tblTaxPayerDetail objTaxPayerDetail = objAdminSearch.GetTaxPayerDetails(taxPayerID);
                Utility.Utility.setLoginSession(objTaxPayerDetail);
                    try
                    {
                         List<BO_TaxSummary> objTaxSummary = objAdminMasterDal.getTaxSummary(Utility.Utility.getLoginSession().TaxPayerID);
                         if (objTaxSummary.Count != 0)
                         {
                             BO_TaxPayerTaxFilingStatusDetails objTaxPayerTaxFilingStatusDetails = new BO_TaxPayerTaxFilingStatusDetails();
                             objTaxPayerTaxFilingStatusDetails.TaxPayerID = Convert.ToInt64(e.CommandArgument.ToString());
                             objTaxPayerTaxFilingStatusDetails.TaxFilingStatusTypeID = (int)MasterItems.TaxFillingStatusTypes.TaxPreparationPending;
                             bool isSucess = new StatusDAL().ConfirmFileStaus(objTaxPayerTaxFilingStatusDetails);
                             if (isSucess)
                             {
                                 ((LinkButton)e.CommandSource).Visible = false;

                             }
                         }
                         else
                         {
                             lblStatus.Text = "Please Generate Taxpayer Taxsummary..";
                             lblStatus.ForeColor = System.Drawing.Color.Red;
                             lblStatus.Focus();
                         }
                    }

                    catch (Exception ex)
                    {
                        lblMsg.Text = ex.Message;
                    }
            }
            else if (e.CommandName == "EFilingCompleted")
            {
                long taxPayerID = Convert.ToInt64(e.CommandArgument.ToString());
                SearchDAL objAdminSearch = new SearchDAL();
                BO_tblTaxPayerDetail objTaxPayerDetail = objAdminSearch.GetTaxPayerDetails(taxPayerID);
                Utility.Utility.setLoginSession(objTaxPayerDetail);
                try
                {
                    BO_TaxPayerTaxFilingStatusDetails objTaxPayerTaxFilingStatusDetails = new BO_TaxPayerTaxFilingStatusDetails();
                    objTaxPayerTaxFilingStatusDetails.TaxPayerID = Convert.ToInt64(e.CommandArgument.ToString());
                    objTaxPayerTaxFilingStatusDetails.TaxFilingStatusTypeID = (int)MasterItems.TaxFillingStatusTypes.EFilingCompleted;
                    bool isSucess = new StatusDAL().ConfirmFileStaus(objTaxPayerTaxFilingStatusDetails);
                    if (isSucess)
                    {
                        ((LinkButton)e.CommandSource).Visible = false;
                        ((LinkButton)e.CommandSource).Text = "Completed";
                    }
                }
                catch (Exception ex)
                {
                    lblMsg.Text = ex.Message;
                }
            }
            else if (e.CommandName == "CompletedClients")
            {
                long taxPayerID = Convert.ToInt64(e.CommandArgument.ToString());
                SearchDAL objAdminSearch = new SearchDAL();
                BO_tblTaxPayerDetail objTaxPayerDetail = objAdminSearch.GetTaxPayerDetails(taxPayerID);
                Utility.Utility.setLoginSession(objTaxPayerDetail);
                try
                {
                    BO_TaxPayerTaxFilingStatusDetails objTaxPayerTaxFilingStatusDetails = new BO_TaxPayerTaxFilingStatusDetails();
                    objTaxPayerTaxFilingStatusDetails.TaxPayerID = Convert.ToInt64(e.CommandArgument.ToString());
                    objTaxPayerTaxFilingStatusDetails.TaxFilingStatusTypeID = (int)MasterItems.TaxFillingStatusTypes.CompletedClients;
                    bool isSucess = new StatusDAL().ConfirmFileStaus(objTaxPayerTaxFilingStatusDetails);
                    if (isSucess)
                    {
                        ((LinkButton)e.CommandSource).Visible = false;
                        ((LinkButton)e.CommandSource).Text = "Completed";
                    }
                }
                catch (Exception ex)
                {
                    lblMsg.Text = ex.Message;
                }
            }
        }

        protected void grdDisplay_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdDisplay.PageIndex = e.NewPageIndex;

            if (Convert.ToInt32(ViewState["Search"]) == 1)
            {
                dispalyPendingList();
            }
            else if (Convert.ToInt32(ViewState["Search"]) == 2)
            {
                dispaly(name, emailid, dob);
            }
        }
               
        private void dispalyPendingList()
        {
            List<BO_tblTaxPayerDetail> objTaxPayerDetails = objAdminMasterDal.SearchPendingClients(Convert.ToInt32(ddlPendingClients.SelectedValue));
            if (objTaxPayerDetails.Count != 0)
            {
                grdDisplay.DataSource = objTaxPayerDetails;
                grdDisplay.DataBind();
            }
            else
            {
                lblMsg.Text = "No Records Found";
                lblMsg.ForeColor = System.Drawing.Color.Red;
                grdDisplay.DataSource = null;
                grdDisplay.DataBind();
            }
        }

        protected void btnSearchByStatus_Click(object sender, EventArgs e)
        {
            ViewState["Search"] = null;
            grdDisplay.DataSource = null;
            grdDisplay.DataBind();
            dispalyPendingList();
            ViewState["Search"] = 1;
        }
    }
}