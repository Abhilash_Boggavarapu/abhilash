﻿<%@ Page Language="C#"   MasterPageFile="~/Administrator/Admin.Master" AutoEventWireup="true" CodeBehind="EmployerDetails.aspx.cs" Inherits="ETaxFilling.Administrator.EmployerDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <script type="text/javascript">
         $(function () {
             $("#txtEmpStartDate,#txtEmpEndDate").datepicker({
                 showOn: "button",
                 buttonImage: "B_Images/calendar_icon.png",
                 yearRange: YEAR_RANGE_GLOBAL,
                 changeMonth: true,
                 changeYear: true,
                 buttonImageOnly: true
             });
         });
     </script>
  

    <div align ="center" >
   <asp:ScriptManager runat="server" />
    <table  >
        <tr> 
        <td>                                                           
        <div><ul class ="submenu" >
                        <li>  <a  style="color: ButtonHighlight "  href="#"   >Employer Details</a></li>
	       <li> <a  href="ClientDetails.aspx" >Client Details</a> </li>
      <li> <a href="ResidencyDetails.aspx" >Residency Details</a></li>
      </ul>
     </div>
     </td>
     </tr>
     </table> 
     
    <table>
    <tr>
    <td>
        <asp:Label Text="" ID="lblSpouse" runat="server" />
    </td>
            <td>
                <asp:LinkButton runat="server" ID="lnkTaxPayer" Text="Tax Payer" 
                    CssClass="button" OnClick="lnkTaxPayer_Click" TabIndex="1"></asp:LinkButton>
            </td>
            <td>
                <asp:LinkButton runat="server" ID="lnkSpouse" Text="Spouse" CssClass="button" 
                    OnClick="lnkSpouse_Click" TabIndex="2"></asp:LinkButton>
            </td>
            
        </tr>
       
    </table>
    <table width="700px"class="box">
      <tr>
    <td align="center">
       <h3>
           <asp:Label ID="lblClientDetails" runat="server" Text=" Taxpayer Employer Details" />
       </h3>
        </td>
        </tr>
      
     </table>
    
              <div id="divEDerr" style="color:#FF0000" runat="server"></div>
    
    <br />
    <div>
        <h4><asp:Label Text="" ID="lblMsg" runat="server" /></h4>
    </div>
    
        <table align="center">
            <tr>
                <td align ="right" >
                    <span lang="EN-IN"><span style="color: #FF0000">*</span> Name :</span></td>
                <td>
                    <asp:TextBox ID="txtEmployName" runat="server" 
                CssClass="ValidationRequired" MaxLength="50" Height="20px" Width="150px" 
                        TabIndex="5"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align ="right" >
                    <span lang="EN-IN"><span style="color: #FF0000">* </span>Start Date :</span></td>
                <td>
                    <asp:TextBox ID="txtEmpStartDate" ClientIDMode="Static" runat="server" 
                CssClass="ValidationRequired date" Height="20px" Width="150px" TabIndex="6"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align ="right" >
                    <span lang="EN-IN"><span style="color: #FF0000">*</span>End Date :</span></td>
                <td>
                    <asp:TextBox ID="txtEmpEndDate" ClientIDMode="Static" runat="server" 
                CssClass="ValidationRequired date" Height="20px" Width="150px" TabIndex="7"></asp:TextBox>
                </td>
            </tr>
          
            <tr>
                <td align ="right" >
                    <span lang="EN-IN"><span style="color: #FF0000">*</span> Country :</span></td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate> 
                       <asp:DropDownList ID="ddlEmpCountry" runat="server" 
                CssClass="ValidationRequired"  Height="20px" Width="150px" AutoPostBack="True" 
                        onselectedindexchanged="ddlEmpCountry_SelectedIndexChanged1" TabIndex="8">
                    </asp:DropDownList>
                     </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td align ="right" >
                    <span lang="EN-IN"><span style="color: #FF0000">*</span> State :</span></td>
                <td>
                 <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                    <asp:DropDownList ID="ddlEmpState" runat="server" 
                CssClass="ValidationRequired" Height="20px" Width="150px" AutoPostBack="True" 
                                OnSelectedIndexChanged ="ddlEmpState_SelectedIndexChanged" TabIndex="9">
                    </asp:DropDownList>
                     </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td align ="right" >
                   <span lang="EN-IN"><span style="color: #FF0000">* </span></span>City :</span></td>
                <td>
                 <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                    <asp:DropDownList ID="ddlEmpCity" runat="server" 
                CssClass="ValidationRequired" Height="20px" Width="150px" TabIndex="10" 
                                AutoPostBack="True" onselectedindexchanged="ddlEmpCity_SelectedIndexChanged">
                    </asp:DropDownList>
                    </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                 
            </tr>
            <tr id="trcity" runat="server">
                <td></td>
                <td align="left">
                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>
              <asp:TextBox ID="txtCity" Height="20px"   Width="150px" runat="server" />
               </ContentTemplate></asp:UpdatePanel>
                   
                </td>
                </tr>
            <tr>
            
                <td style="width: 203px">
                   </td>
                <td>
                    <asp:Button ID="btnAddEmploye" runat="server" 
                CssClass="button" onclick="btnAddEmploye_Click" Text="Save" 
                Width="90px" Height="30px" TabIndex="11" />
                </td>
            </tr>
            
        </table>
  
    
    <table style="width: 100%">
    <tr>
                
                <td>
                    &nbsp;</td>
            </tr>
        <tr>
            
                <td></td>
            <td align="right" >
                <asp:HyperLink NavigateUrl="~/Administrator/ClientDetails.aspx" 
                    CssClass="button" Width="62px" Text="Next" runat="server" />
                </td>
        </tr>
    </table>
    </div>
    <table  align="center" >
    
    <tr id ="gridDisplay" runat ="server" align="center">
    <td>
        <asp:GridView ID="GrdEmplist" runat="server" AlternatingRowStyle-CssClass="alt"
AutoGenerateColumns="False"
        PagerStyle-CssClass="pgr" Width="510px" onrowcommand="GrdEmplist_RowCommand" 
           margin="15%" AllowPaging="True" CssClass="mGrid" 
            onpageindexchanging="GrdEmplist_PageIndexChanging" >
<AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
<Columns>

<asp:TemplateField HeaderText="SNo">
<ItemTemplate><%#Container.DataItemIndex+1 %><br />
</ItemTemplate>
</asp:TemplateField>
    
    <asp:BoundField DataField ="TaxPayerEmployerID"  Visible="false"/>      
<asp:BoundField DataField="EmployerName" HeaderText="Name" SortExpression="EID"  />
<asp:BoundField DataField="EmploymentStartDate" HeaderText="Start Date" 
        DataFormatString="{0:d}" HtmlEncode="False"/>
<asp:BoundField DataField="EmploymentEndDate" HeaderText="End Date" 
        DataFormatString="{0:d}" HtmlEncode="False" />
<asp:BoundField DataField="CountryName" HeaderText="Country"/>
<asp:BoundField  DataField="StateName"  HeaderText="State" />
<asp:BoundField  DataField="City"  HeaderText="City" />


<asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="lnkbtnEdit" runat="server" Text="Edit" CommandArgument='<%# DataBinder.Eval(Container,"DataItem.TaxPayerEmployerID") %>'
                        CommandName="EmpEdit" ></asp:LinkButton>
                       
                </ItemTemplate>
            </asp:TemplateField>
            <%--<asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="lnkbtnRemove" runat="server" Text="Remove" CommandArgument='<%# DataBinder.Eval(Container,"DataItem.TaxPayerEmployerID") %>'
                        CommandName="Remove" ></asp:LinkButton>                       
                </ItemTemplate>
            </asp:TemplateField>--%>

</Columns>

            <PagerSettings FirstPageText="First" LastPageText="Last" />

<PagerStyle CssClass="pgr"></PagerStyle>
</asp:GridView>

</td></tr>
</table>
 
  
</asp:Content>