﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Administrator/Admin.Master"  CodeBehind="MaritalType.aspx.cs" Inherits="ETaxFilling.MaritalType1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div>
<div id="divMTerr" runat="server"></div>
    <br />
        <table style="width: 100%">
            <tr>
                <td style="width: 371px">
                    <table>
                        <tr>
                            <td>
                                MaritalStatus</td>
                            <td>
                                <asp:TextBox ID="txtMarital" runat="server" CssClass="ValidationRequired" 
                                    MaxLength="20" Height="20px" Width="124px" 
                                   ></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 23px">
                            </td>
                            <td style="height: 23px">
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <asp:Button ID="btnSaveMarital" runat="server" CssClass="formButton" 
                                    onclick="btnSaveMarital_Click" Text="Save" Width="80px" />
                                    <asp:Label ID="lblMsg" runat="server" ForeColor="#006600"></asp:Label>
                            </td>
                        </tr>
                    </table>
                     <asp:GridView ID="GrdwMarriedList" runat="server" AlternatingRowStyle-CssClass="alt"
AutoGenerateColumns="False"  GridLines="None" 
        PagerStyle-CssClass="pgr" Width="295px" 
            onrowcommand="GrdwMarriedList_RowCommand" HorizontalAlign="Center" 
            CellPadding="4" ForeColor="#333333" AllowPaging="True" CssClass="mGrid">
           
<AlternatingRowStyle CssClass="alt" BackColor="White"></AlternatingRowStyle>
<Columns>

<asp:TemplateField HeaderText="SNo">
<ItemTemplate><%#Container.DataItemIndex+1 %><br />
</ItemTemplate>
</asp:TemplateField>
    
    <asp:BoundField DataField ="MaritalStatusID"   Visible="false"/>      
<asp:BoundField DataField="MaritalStatus" HeaderText="MarritalStatus"   />

<asp:TemplateField>
                <ItemTemplate>
    <asp:LinkButton ID="lnkbtnEdit" runat="server" Text="Edit" CommandArgument='<%# DataBinder.Eval(Container,"DataItem.MaritalStatusID") %>'
                        CommandName="EditMar" ></asp:LinkButton>
                       
                </ItemTemplate>
            </asp:TemplateField>        
               
</Columns>
            </asp:GridView>
                    <br />
                    
                </td>
               
                </td>
                </td>
            </tr>
        </table>
   <br />
    </div>
</asp:Content>
