﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ETaxDal;
using ETaxBO;

namespace ETaxFilling.Administrator
{
    public partial class StaffComments : System.Web.UI.Page
    {
        AdminMasterDAL objMasterDal = new AdminMasterDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Utility.Utility.CheckAdminLoginSession())
            {
                Response.Redirect("~/Administrator/AdminLogin.aspx");
            }
            else
            {
                displyComments();
            }
        }

        private void displyComments()
        {
            grdStaffComments.DataSource = objMasterDal.getStaffComments(Utility.Utility.getLoginSession().TaxPayerID);
            grdStaffComments.DataBind();
            lblmsg.Text = string.Empty;
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
           
            Bo_StaffComments objStaffComments = new Bo_StaffComments();
            objStaffComments.StaffComments = txtComments.Text;
            objStaffComments.TaxPayerID = Utility.Utility.getLoginSession().TaxPayerID;
            if (objMasterDal.saveStaffComments(objStaffComments) == 1)
            {
                lblmsg.Text = "Save Sucessfully";
            }
            txtComments.Text = string.Empty;
            displyComments();
        }
    }
}