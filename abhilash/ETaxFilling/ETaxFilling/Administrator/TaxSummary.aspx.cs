﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ETaxDal;
using ETaxBO;


namespace ETaxFilling.Administrator
{
    public partial class TaxSummary : System.Web.UI.Page
    {
        AdminMasterDAL objAdminMasterDal = new AdminMasterDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Utility.Utility.CheckAdminLoginSession())
            {
                Response.Redirect("~/Administrator/AdminLogin.aspx");
            }
            acDiv.Visible = false;
            lblMsg.Text = string.Empty;
        }

        protected void btnSummary_Click(object sender, EventArgs e)
        {
            if (txtSummary.Text == string.Empty)
            {
                lblMsg.Text = "Please Enter data in TextBox";
                lblMsg.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
               
                BO_TaxSummary objTaxSummary = new BO_TaxSummary();
                //int taxSummaryId = objAdminMasterDal.getTaxSummaryId(Utility.Utility.getLoginSession().TaxPayerID);
                //if (taxSummaryId != 0)
                //{
                //    objTaxSummary.TaxSummaryId = taxSummaryId;
                //}
                objTaxSummary.TaxSummary = txtSummary.Text;

                objTaxSummary.TaxPayerID = Utility.Utility.getLoginSession().TaxPayerID;
               if (objAdminMasterDal.SaveOrInsertTaxSummary(objTaxSummary) == 1)
                {
                    lblMsg.Text = "Sucessfully Saved";
                    lblMsg.ForeColor = System.Drawing.Color.Green;
                    txtSummary.Text = string.Empty;
                }
            }
        }

        protected void btntaxSummary_Click(object sender, EventArgs e)
        {
            acDiv.Visible = true;
            List<BO_TaxSummary> objTaxSummary = (objAdminMasterDal.getTaxSummary(Utility.Utility.getLoginSession().TaxPayerID));
            if (objTaxSummary != null)
            {
                repeter.DataSource = objTaxSummary;
                repeter.DataBind();
            }
        }
    }
}