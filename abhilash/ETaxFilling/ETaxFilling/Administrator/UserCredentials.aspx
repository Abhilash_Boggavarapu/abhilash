﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Administrator/Admin.Master" CodeBehind="UserCredentials.aspx.cs" Inherits="ETaxFilling.Administrator.UserCredentials" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="ErrRDetails" style="color:#FF0000" runat="server"></div>
    <table border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
            <td>
            Client First Name:
            </td>
            <td>
                <asp:textbox  id="txtFirstName" runat="server" />  
            </td>
            <tr>
            <td>
            Email ID:
            </td>
            <td>
                <asp:textbox id="txtEmail" runat="server" />  
            </td>
            </tr>
            <tr>
            <td></td> <td>
                <asp:button text="Search" id="btnSearch" runat="server" 
                    onclick="btnSearch_Click" /></td>
            </tr>
            <tr>
            <td>
                <asp:label text="" id="lblerror" runat="server" />
            </td>
            </tr>
            </table>
           <div align="center">
                        <asp:GridView ID="grdList" runat="server" AutoGenerateColumns="False" 
                        Width="700px" AllowPaging="True"  CssClass="mGrid" >
              
                        <Columns>
                            <asp:TemplateField HeaderText="SNo">
                                <ItemTemplate>
                                    <%#Container.DataItemIndex+1 %><br />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="FirstName" HeaderText="First Name" />
                            <asp:BoundField DataField="LastName" HeaderText="Last Name " />
                            <asp:BoundField DataField="EmailID" HeaderText="Email ID " />
                             <asp:BoundField DataField="Password" HeaderText="Password " />   
                            <asp:BoundField DataField="CreatedOn" HeaderText="Date" DataFormatString="{0:d}" 
        HtmlEncode="False" />
                            <asp:TemplateField>
              
            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                       </div>
</asp:Content>