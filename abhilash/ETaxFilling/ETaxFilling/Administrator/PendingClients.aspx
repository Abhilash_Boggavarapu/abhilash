﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Administrator/Admin.Master" CodeBehind="PendingClients.aspx.cs" Inherits="ETaxFilling.Administrator.PendingClients" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div align="center">
    <asp:DropDownList runat="server" ID="ddlPendingClients" Height="20px" 
        Width="185px" AutoPostBack="True" 
        onselectedindexchanged="ddlPendingClients_SelectedIndexChanged"
         >
    </asp:DropDownList>
    <table>
    <tr>
    <td>
        <asp:Label Text="" ID="lblMsg" runat="server" />
    </td>
    </tr>
    <tr>
    <td>
    <asp:GridView ID="grdDisplay" runat="server" AutoGenerateColumns ="False" 
           margin="5%"  onrowcommand="grdDisplay_RowCommand" AllowPaging="True" 
            onpageindexchanging="grdDisplay_PageIndexChanging" CssClass="mGrid">
        <Columns>
        <asp:BoundField DataField ="TaxPayerID" HeaderText ="TaxPayer ID" />
        <asp:BoundField DataField ="FirstName" HeaderText ="First Name" />
        <asp:BoundField DataField ="LastName" HeaderText ="Last Name" />
         <asp:BoundField DataField ="DOB" HeaderText ="DOB" />
         <asp:BoundField DataField ="PrimaryEMailID" HeaderText =" EMail ID" />
         <asp:BoundField DataField ="MobileNumber" HeaderText ="Mobile Number" />
        <%-- <asp:BoundField DataField ="Occupation" HeaderText ="Occupation" />
         <asp:BoundField DataField ="VisaStatus" HeaderText ="VisaStatus" />--%>
        <asp:TemplateField>
        <ItemTemplate>
        <asp:LinkButton ID="LinkButton1" runat="server" Text ="View" CommandName="ViewData" CommandArgument='<%#Eval("UserID") %>'></asp:LinkButton>
        </ItemTemplate>
        </asp:TemplateField>
        </Columns>
        </asp:GridView>
         </td>
        </tr>
        </table>
</div>
</asp:Content>
