﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ETaxBO;
using ETaxDal;
using System.IO;
namespace ETaxFilling.Administrator
{
    public partial class UploadFile : System.Web.UI.Page
    {
        long taxPayerID;
        UploadDocumentsDAL objDocumentDal = new UploadDocumentsDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMsg.Text = string.Empty;
            Label1.Text = string.Empty;
            if (!Utility.Utility.CheckAdminLoginSession())
            {
                Response.Redirect("~/Administrator/AdminLogin.aspx");
            }
            taxPayerID = Utility.Utility.getLoginSession().TaxPayerID;
            lblMsg.Text = "";
            if (!IsPostBack)
            {
                bindAllDocuments();

            }
        }
        private void bindAllDocuments()
        {
            Utility.Utility.setCurrentTaxPayer(MasterItems.TaxPayerType.self);
            ddlForms.DataSource = objDocumentDal.GetAllForms();
            ddlForms.DataTextField = "FormName";
            ddlForms.DataValueField = "DocumentID";
            ddlForms.DataBind();
        }

        private void SaveDocuments(int DocumentID, string Remarks)
        {
            try
            {
                MasterDAL objMasterDal = new MasterDAL();

                BO_tblTaxPayerDocument objTaxDocument = new BO_tblTaxPayerDocument();
                // Get the HttpFileCollection

                HttpFileCollection hfc = Request.Files;
                for (int i = 0; i < hfc.Count; i++)
                {
                    HttpPostedFile hpf = hfc[i];
                    if (hpf.ContentLength == 0)
                    {
                        Label1.ForeColor = System.Drawing.Color.Red;
                        Label1.Text = "Can't Upload Empty Files";
                    }
                    if (hpf.ContentLength > 0)
                    {
                        string fileName = hpf.FileName;
                        int fileSize = hpf.ContentLength;
                        byte[] documentBinary = new byte[fileSize];
                        hpf.InputStream.Read(documentBinary, 0, fileSize);
                        objTaxDocument.DocumentID = DocumentID; 
                        objTaxDocument.FileName = fileName;
                        objTaxDocument.FileContent = documentBinary;
                        objTaxDocument.Remarks = Remarks;
                        objTaxDocument.TaxPayerID = taxPayerID;
                        objDocumentDal.SaveTaxPayerDocuments(objTaxDocument);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void FileExtension(int DocumentID, string Remarks)
        {
            if (FileUpload1.PostedFile.FileName == "")
            {
                Label1.ForeColor = System.Drawing.Color.Red;
                Label1.Text = "No File selected To Upload";
            }
            else
            {
                string[] validFileTypes = { "zip", "rar", "doc", "docx", "pdf", "jpeg", "jpg","xlsx","xls" };
                string ext = System.IO.Path.GetExtension(FileUpload1.PostedFile.FileName);
                bool isValidFile = false;
                for (int i = 0; i < validFileTypes.Length; i++)
                {
                    if (ext == "." + validFileTypes[i])
                    {
                        isValidFile = true;
                        break;
                    }
                }
                if (!isValidFile)
                {
                    Label1.ForeColor = System.Drawing.Color.Red;
                    Label1.Text = "Invalid File. Please upload a File with extension " +
                    string.Join(",", validFileTypes);
                }
                else
                {
                    SaveDocuments(DocumentID, Remarks);
                    Label1.ForeColor = System.Drawing.Color.Green;
                    Label1.Text = "File uploaded successfully.";
                    txtForm.Text = string.Empty;
                }
            }
        }
        private void display(int documentID)
        {
            GridViewUpload.DataSource = null;
            GridViewUpload.DataBind();
            List<BO_tblTaxPayerDocument> objDoc = objDocumentDal.GetTaxpayerDocuments(documentID, taxPayerID);
            if (objDoc.Count != 0)
            {
                GridViewUpload.DataSource = objDoc;
                GridViewUpload.DataBind();
            }
            else
            {
                lblMsg.ForeColor = System.Drawing.Color.Red;
                lblMsg.Text = "No Files Found ";
            }
        }

        protected void GridViewUpload_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Download")
            {
                BO_tblTaxPayerDocument objDocument = objDocumentDal.GetTaxpayerFile(Convert.ToInt32(e.CommandArgument));
                byte[] fileData = objDocument.FileContent;
                Response.ClearContent();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + objDocument.FileName);
                BinaryWriter bw = new BinaryWriter(Response.OutputStream);
                bw.Write(fileData);
                bw.Close();
                Response.ContentType = "application";
                Response.End();
            }
        }
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            string remarks=txtForm.Text;
            FileExtension(Convert.ToInt32(ddlForms.SelectedValue), remarks);
        }

        protected void btnView_Click(object sender, EventArgs e)
        {
            //Label1.Text = string.Empty;
            Label1.Text = ddlForms.SelectedItem.ToString();
            display(Convert.ToInt32(ddlForms.SelectedValue));
        }   
    }
}
