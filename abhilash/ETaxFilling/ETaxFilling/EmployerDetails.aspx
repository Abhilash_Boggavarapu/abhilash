﻿<%@ Page Language="C#"  MasterPageFile="~/Client.Master" AutoEventWireup="true" CodeBehind="EmployerDetails.aspx.cs" Inherits="ETaxFilling.EmployerDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   
    <script type="text/javascript">
        $(function () {
            if ($($("input[type='radio'][value='rbYes']")).attr("checked") == "checked") {
                $("#txtEmpStartDate,#txtEmpEndDate").datepicker({
                    showOn: "button",
                    buttonImage: "B_Images/calendar_icon.png",
                    changeMonth: true,
                    changeYear: true,
                    buttonImageOnly: true
                });
            }
            else {
                $("#txtEmpStartDate,#txtEmpEndDate").datepicker({
                    showOn: "button",
                    buttonImage: "B_Images/calendar_icon.png",
                    changeMonth: true,
                    changeYear: true,
                    buttonImageOnly: true
                });
                DisableDatePicker();
            }

        });

        function DisableDatePicker()
        {
            $("#txtEmpStartDate,#txtEmpEndDate").datepicker("disable");
            
        }
   </script>
    <div align ="center" >
   <asp:ScriptManager runat="server" />
    <table  >
        <tr> 
        <td>                                                           
        <div><ul class ="submenu" >
                        <li>  <a  style="color: ButtonHighlight "  href="#"   >Employer Details</a></li>
	       <li> <a  href="ClientDetails.aspx" >Client Details</a> </li>
      <li> <a href="ResidencyDetails.aspx" >Residency Details</a></li>
      </ul>
     </div>
     </td>
     </tr>
     </table> 
     
    <table>
    <tr>
    <td>
        <asp:Label Text="" ID="lblSpouse" runat="server" />
    </td>
            <td>
                <asp:LinkButton runat="server" ID="lnkTaxPayer" Text="Tax Payer" 
                    CssClass="button" OnClick="lnkTaxPayer_Click" TabIndex="1"></asp:LinkButton>
            </td>
            <td>
                <asp:LinkButton runat="server" ID="lnkSpouse" Text="Spouse" CssClass="button" 
                    OnClick="lnkSpouse_Click" TabIndex="2"></asp:LinkButton>
            </td>
            
        </tr>
       
    </table>
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="box">
      <tr>
    <td>
       <h2 class="divider top20 bottom10">
           <asp:Label ID="lblClientDetails" runat="server" Text=" Taxpayer Employer Details" />
       </h2>
        </td>
        </tr>
      
     </table>
     <asp:Panel ID="panelRb" runat="server">
   <h4> Do You Have Employer ? <asp:RadioButton ID="rbYes" runat="server" 
        AutoPostBack="True" GroupName="Employe" oncheckedchanged="rbYes_CheckedChanged" 
        Text="Yes" TabIndex="3" />
       &nbsp;<asp:RadioButton ID="rbNo" runat="server" AutoPostBack="True" 
        GroupName="Employe" Text="No"  oncheckedchanged="rbYes_CheckedChanged" 
           TabIndex="4" Checked="True"  /></h4>
              <div id="divEDerr" style="color:#FF0000" runat="server"></div>
           </asp:Panel>
    <div align="center">
        <asp:Label Text="" ID="lblMsg" CssClass="error" runat="server" />
    </div>
    <asp:Panel ID="PanelEmp" runat="server">
      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="box">
            <tr>
                <td align ="right" >
                    <span lang="EN-IN"><span style="color: #FF0000">*</span> Name :</span></td>
                <td>
                    <asp:TextBox ID="txtEmployName" runat="server" 
                CssClass="ValidationRequired" MaxLength="50"  Width="150px" 
                        TabIndex="5"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align ="right" >
                    <span lang="EN-IN"><span style="color: #FF0000">* </span>Start Date :</span></td>
                <td>
                    <asp:TextBox ID="txtEmpStartDate" ClientIDMode="Static" runat="server" 
                CssClass="ValidationRequired date"  Width="150px" TabIndex="6"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align ="right" >
                    <span lang="EN-IN"><span style="color: #FF0000">*</span>End Date :</span></td>
                <td>
                    <asp:TextBox ID="txtEmpEndDate" ClientIDMode="Static" runat="server" 
                CssClass="ValidationRequired date"  Width="150px" TabIndex="7"></asp:TextBox>
                </td>
            </tr>
          
            <tr>
                <td align ="right" >
                    <span lang="EN-IN"><span style="color: #FF0000">*</span> Country :</span></td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate> 
                       <asp:DropDownList ID="ddlEmpCountry" runat="server" 
                CssClass="ValidationRequired"   Width="150px" AutoPostBack="True" 
                        onselectedindexchanged="ddlEmpCountry_SelectedIndexChanged1" TabIndex="8">
                    </asp:DropDownList>
                     </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td align ="right" >
                    <span lang="EN-IN"><span style="color: #FF0000">*</span> State :</span></td>
                <td>
                 <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                    <asp:DropDownList ID="ddlEmpState" runat="server" 
                CssClass="ValidationRequired"  Width="150px" AutoPostBack="True" 
                                OnSelectedIndexChanged ="ddlEmpState_SelectedIndexChanged" TabIndex="9">
                    </asp:DropDownList>
                     </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td align ="right" >
                   <span lang="EN-IN"><span style="color: #FF0000">* </span></span>City :</span></td>
                <td>
                 <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                    <asp:DropDownList ID="ddlEmpCity" runat="server" 
                 Width="150px" TabIndex="10" 
                                AutoPostBack="True" onselectedindexchanged="ddlEmpCity_SelectedIndexChanged">
                    </asp:DropDownList>
                    </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                 </tr>
                  <tr id="trcity" runat="server">
                <td></td>
                <td align="left">
                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>
              <asp:TextBox ID="txtCity"    Width="150px" runat="server" />
               </ContentTemplate></asp:UpdatePanel>
                   
                </td>
                </tr>
            <tr>
            
                <td style="width: 203px">
                   </td>
                <td>
                    <asp:Button ID="btnAddEmploye" runat="server" 
                CssClass="button" onclick="btnAddEmploye_Click" Text="Save" 
                Width="90px" Height="30px" TabIndex="11" />
                </td>
            </tr>
            
        </table>
    </asp:Panel>
    
   <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="right" >
                <asp:HyperLink runat="server" CssClass="button"  Height="25px" Width="50px"
                    NavigateUrl="~/ClientDetails.aspx" TabIndex="9">Next</asp:HyperLink>
            </td>
        </tr>
    </table>
    </div>
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="box">
    
    <tr id ="gridDisplay" runat ="server" align="center">
    <td>
        <asp:GridView ID="GrdEmplist" runat="server" AlternatingRowStyle-CssClass="alt"
AutoGenerateColumns="False"
        PagerStyle-CssClass="pgr" Width="100%" onrowcommand="GrdEmplist_RowCommand" 
           margin="15%" AllowPaging="True" CssClass="mGrid" 
            onpageindexchanging="GrdEmplist_PageIndexChanging" >
<AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
<Columns>

<asp:TemplateField HeaderText="SNo">
<ItemTemplate><%#Container.DataItemIndex+1 %><br />
</ItemTemplate>
</asp:TemplateField>
    
    <asp:BoundField DataField ="TaxPayerEmployerID"  Visible="false"/>      
<asp:BoundField DataField="EmployerName" HeaderText="Name" SortExpression="EID"  />
<asp:BoundField DataField="EmploymentStartDate" HeaderText="Start Date" 
        DataFormatString="{0:d}" HtmlEncode="False"/>
<asp:BoundField DataField="EmploymentEndDate" HeaderText="End Date" 
        DataFormatString="{0:d}" HtmlEncode="False" />
<asp:BoundField DataField="CountryName" HeaderText="Country"/>
<asp:BoundField  DataField="StateName"  HeaderText="State" />
<asp:BoundField  DataField="City"  HeaderText="City" />


<asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="lnkbtnEdit" runat="server" Text="Edit" CommandArgument='<%# DataBinder.Eval(Container,"DataItem.TaxPayerEmployerID") %>'
                        CommandName="EmpEdit" ></asp:LinkButton>
                       
                </ItemTemplate>
            </asp:TemplateField>
            <%--<asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="lnkbtnRemove" runat="server" Text="Remove" CommandArgument='<%# DataBinder.Eval(Container,"DataItem.TaxPayerEmployerID") %>'
                        CommandName="Remove" ></asp:LinkButton>                       
                </ItemTemplate>
            </asp:TemplateField>--%>

</Columns>

            <PagerSettings FirstPageText="First" LastPageText="Last" />

<PagerStyle CssClass="pgr"></PagerStyle>
</asp:GridView>

</td></tr>
</table>
</asp:Content>