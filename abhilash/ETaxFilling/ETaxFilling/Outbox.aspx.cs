﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intmail_DAL;
using ETaxDal;
using ETaxBO;
using System.Data;

namespace ETaxFilling
{
    public partial class Outbox : System.Web.UI.Page
    {
        protected void page_preinit(object sender, EventArgs e)
        {

            if (Utility.Utility.CheckAdminLoginSession())
                MasterPageFile = IM_Utility.AdminMasterpagepath;
            else
                MasterPageFile = IM_Utility.UserMasterpagepath;

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack == false)
            {
                BindOutbox();
            }

        }
        private void BindOutbox()
        {
            IntMail Im_mail = new IntMail();
            string Memberid;
            if (Utility.Utility.CheckAdminLoginSession())

                Memberid = Utility.Utility.GetAdminSession().UserID.ToString();
            else
                Memberid = Utility.Utility.getLoginSession().UserID.ToString();


            gvOutbox.DataSource = Im_mail.Mail_Outbox(Memberid);
            gvOutbox.DataBind();
        }

        protected void gvOutbox_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "view")
            {
                string ID = e.CommandArgument.ToString();

                string type = "1";

                string status = new IntMail().Mail_Outbox_MessageRead(ID).ToString();
                if (status == "1")
                {
                    Response.Redirect("Description.aspx?id=" + ID +"&type=" + type + "");
                }
                else
                {
                    lblMessage.Visible = true;
                    lblMessage.Text = status;
                }

            }
        }

        protected void gvOutbox_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvOutbox.PageIndex = e.NewPageIndex;
            BindOutbox();
        }
    }
}
