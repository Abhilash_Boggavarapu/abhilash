﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ETaxDal;
using ETaxFilling.Utility;
using ETaxBO;

namespace ETaxFilling
{
    public partial class From7216 : System.Web.UI.Page 
    { 
        
       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Utility.Utility.checkLoginSession())
            {
                Response.Redirect("userlogin.aspx");

            }
            else
            {
                if (!IsPostBack)
                {
                   
                    BO_tblUser objUser = new Form7216().getConsentDurationDetails(Utility.Utility.getLoginSession().UserID);
                    if (objUser.SignedOnConsentForm != null)
                    {
                        lbldate.Text = objUser.SignedOnConsentForm.Value .ToShortDateString();
                        lblcensent.Text = objUser.ConsentDuration.ToString();
                        lblFullNameSign .Text = objUser.SignatureOnConsentForm.ToString();
                        lblFullName.Text = objUser.SignatureOnConsentForm.ToString();
                    }
                   
                }

            }
        }
        
       
    }
}