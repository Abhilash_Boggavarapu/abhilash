﻿<%@ Page Language="C#"  MasterPageFile="~/Client.Master" AutoEventWireup="true" CodeBehind="DependentDetails.aspx.cs" Inherits="ETaxFilling.DependentDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <link href="B_Styles/Validations%20css/stylesheet.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript">
        $(function () {

            if ($($("input[type='radio'][value='rbYes']")).attr("checked") == "checked") {
                $("#txtDob,#txtDoE").datepicker({
                    showOn: "button",
                    buttonImage: "B_Images/calendar_icon.png",
                    changeMonth: true,
                    changeYear: true,
                    buttonImageOnly: true
                });
            }
            else {
                $("#txtDob,#txtDoE").datepicker({
                    showOn: "button",
                    buttonImage: "B_Images/calendar_icon.png",
                    changeMonth: true,
                    changeYear: true,
                    buttonImageOnly: true
                });
                DisableDatePicker();
            }

        });
        function DisableDatePicker()
        {
            $("#txtDob,#txtDoE").datepicker("disable");
            
        } 
      </script>

<div align ="center" >

 <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr> 
        <td align="center">
        <div><ul class ="submenu" >
                       <li><asp:LinkButton runat="server" ID="lnkTaxPayer" Text="Tax Payer Details" onclick="lnkTaxPayer_Click" 
                               ></asp:LinkButton>
                               </li>
	       <li><asp:LinkButton runat="server" ID="lnkSpouse" Text="Spouse Details" onclick="lnkSpouse_Click"  
                               ></asp:LinkButton>   </li>
      <li> <a style="color: ButtonHighlight " href="#" >Dependent Details</a></li>
      </ul>
     </div>
     </td>
     </tr>
     </table> 
   

             <table width="100%" class="box" cellpadding="0" cellspacing="0">
      <tr>
    <td>
       <h2 class="divider top20 bottom10">
           <asp:Label ID="lblClientDetails" runat="server" Text=" Dependant Details" />
       </h2>
        </td>
        </tr>
     </table>
            <asp:Panel ID="panelRb" runat="server">
              <h4>Do You HaveDependants? <asp:RadioButton ID="rbYes" runat="server" Text="Yes" 
                    oncheckedchanged="rbYes_CheckedChanged" AutoPostBack="True" CssClass="HaveDependantsRadioYes"
                    GroupName="Dependents" TabIndex="1" />

                  &nbsp;<asp:RadioButton ID="rbNo" runat="server" Text="No"  oncheckedchanged="rbYes_CheckedChanged" AutoPostBack="True" 
                    GroupName="Dependents" /></h4> 
                    </asp:Panel>

<div id="divDDerr" style="color:#FF0000" runat="server"></div>
<div> 
    <asp:Label Text="" ID="lblMsg" runat="server" CssClass="error" /></div>
          
    <asp:Panel ID="PanelDpt" runat="server">

        <table cellpadding="0" cellspacing="0" border="0" class="box">

            <tr>
                <td align="right">
                    <span style="color: #FF0000">*</span> First Name : </td>
                <td>
                   <asp:TextBox ID="txtFname" runat="server" CssClass="ValidationRequired" 
                        MaxLength="50" TabIndex="3"></asp:TextBox>
                  </td>
            </tr>
            <tr>
                <td align="right">
                  Middle Name : </td>
                <td>
                    <asp:TextBox ID="txtMname" runat="server" MaxLength="50" TabIndex="4" ></asp:TextBox>
                 </td>
            </tr>
            <tr>
                <td align="right">
                    <span style="color: #FF0000">*</span> Last Name : </td>
                <td>
                    <asp:TextBox ID="txtLname" runat="server" CssClass="ValidationRequired" 
                        MaxLength="20" TabIndex="5"></asp:TextBox>
                  </td>
            </tr>
            <tr>
                <td align="right">
                   <span style="color: #FF0000">*</span> Date of Birth : </td>
                <td>
                    <asp:TextBox ID="txtDob" ClientIDMode="Static" runat="server" CssClass="ValidationRequired date" 
                        TabIndex="6"></asp:TextBox>
                    <br />
                   </td>
            </tr>
            <tr>
                <td align="right">
                   <span style="color: #FF0000">*</span> Relationship : </td>
                <td  >
 
                    <asp:DropDownList ID="ddlRelation" CssClass="ValidationRequired" runat="server" 
                        TabIndex="7" >
                      
                        <asp:ListItem></asp:ListItem>
                      
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="right">
                    &nbsp;SSN/ITIN : </td>
                <td>
                   <asp:TextBox ID="txtSsn" runat="server"  MaxLength="15" TabIndex="8"></asp:TextBox>
                   </td>
            </tr>
            <tr>
                <td align="right">
                    &nbsp;Visa Status : </td>
                <td  >
             
                    <asp:DropDownList ID="ddlVisa" runat="server" CssClass="ValidationRequired" 
                        TabIndex="9" >
                        <asp:ListItem></asp:ListItem>
                    </asp:DropDownList>
                   </td>
            </tr>
            <tr>
                <td align="right">
                    Date of Entry into US : </td>
                <td>
                    <asp:TextBox ID="txtDoE" ClientIDMode="Static" runat="server"
                        TabIndex="10"></asp:TextBox>
                   </td>
            </tr>
            <tr>
                <td>
                   </td>
                <td align="left" style=" padding-left:20px;">
                    <asp:Button ID="btnSave" runat="server" CssClass="button" 
                        onclick="btnSave_Click" Text="Save" Width="91px" TabIndex="11" /> 
                </td>
            </tr>
        </table>
    </asp:Panel>

      
 
    
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
        <td>
            &nbsp;</td>
            <td align="right" colspan="2" >
                    <asp:HyperLink ID="HyperLink1" NavigateUrl="~/PersonalInfoConfirmDetails.aspx" Text="Next" CssClass="button" runat="server" />
      </td>
        </tr>
    </table>
  
    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="box" >
    
    <tr id ="gridDisplay" runat ="server"><td>
        <asp:GridView ID="GrdDpntlist" runat="server" 
            AlternatingRowStyle-CssClass="alt" margin="10%"
AutoGenerateColumns="False"  GridLines="Vertical"
        PagerStyle-CssClass="pgr" Width="100%" onrowcommand="GrdDpntlist_RowCommand" 
            AllowPaging="True" CssClass="mGrid" 
            onpageindexchanging="GrdDpntlist_PageIndexChanging1">  
           
<AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
<Columns><asp:TemplateField HeaderText="S.No.">
<ItemTemplate><%#Container.DataItemIndex+1 %><br />
</ItemTemplate>
</asp:TemplateField>
    
    <asp:BoundField DataField ="TaxPayerDependentID"  Visible="false"/>      
<asp:BoundField DataField="FirstName" HeaderText="First Name" />
<asp:BoundField DataField="MiddleName" HeaderText="Middle Name" />
<asp:BoundField DataField="LastName" HeaderText="Last Name" />
<asp:BoundField DataField="DOB" HeaderText="Date Of Birth" DataFormatString="{0:d}" 
        HtmlEncode="False"/>
<asp:BoundField  DataField="Relationship"  HeaderText="Relationship" />
<asp:BoundField  DataField="SSN"  HeaderText="SSN/ITIN" />
<asp:BoundField  DataField="USEntryDate"  HeaderText="USEntryDate " 
        DataFormatString="{0:d}" HtmlEncode="False" />
<asp:BoundField  DataField="VisaStatus"  HeaderText="Visa Status " />


<asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="lnkbtnEdit" runat="server" Text="Edit" CommandArgument='<%# DataBinder.Eval(Container,"DataItem.TaxPayerDependentID") %>'
                        CommandName="DepnEdit" ></asp:LinkButton>
                       
                </ItemTemplate>
            </asp:TemplateField>
            <%--<asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="lnkbtnRemove" runat="server" Text="Remove" CommandArgument='<%# DataBinder.Eval(Container,"DataItem.TaxPayerDependentID") %>'
                        CommandName="Remove" ></asp:LinkButton>                       
                </ItemTemplate>
            </asp:TemplateField>--%>

</Columns>
            <PagerSettings FirstPageText="First" LastPageText="Last" 
                Mode="NumericFirstLast" />

<PagerStyle CssClass="pgr"></PagerStyle>
</asp:GridView>

</td></tr>
</table>
  </div>
 

</asp:Content>