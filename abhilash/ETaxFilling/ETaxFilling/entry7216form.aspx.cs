﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ETaxDal;
using ETaxFilling.Utility;
using ETaxBO;

namespace ETaxFilling
{
    public partial class entry7216from : System.Web.UI.Page
    {


                    
        MasterDAL objMasterDAL = new MasterDAL();
        private DateTime DateTimeof()
        {
            TimeZone zone = TimeZone.CurrentTimeZone;
            // Demonstrate ToLocalTime and ToUniversalTime.
            DateTime local = zone.ToLocalTime(DateTime.Now);
            DateTime universal = zone.ToUniversalTime(DateTime.Now);

            return universal;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Utility.Utility.checkLoginSession())
            {
                Response.Redirect("userlogin.aspx");

            }
            else
            {
                lbldate.Text = DateTimeof().ToShortDateString();
               
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtDOC.Text))
            {
                divConerr.InnerText = Utility.Utility.ReqDurationOC;
            }
            else if (!txtDOC.Text.isNumber())
            {
                divConerr.InnerText = Utility.Utility.ValFilenumber;
                txtDOC.Text = string.Empty;
                txtDOC.Focus();
            }
            else if (string.IsNullOrEmpty(txtSign.Text))
            {
                divConerr.InnerText = Utility.Utility.ReqSign;
            }
            else if (txtSign.Text.IsAlpha())
            {
                divConerr.InnerText =Utility.Utility.ValSign;
                txtSign.Text = string.Empty;
                txtSign.Focus();
            }
            
            else
            {
                divConerr.InnerText = string.Empty;
                Form7216 from7216 = new Form7216();
                BO_tblUser objUser = new BO_tblUser();
                objUser.ConsentDuration = Convert.ToByte(txtDOC.Text);
                objUser.ConsentFormStatus = true;
                objUser.SignatureOnConsentForm = txtSign.Text;
                objUser.SignedOnConsentForm = Convert.ToDateTime(lbldate .Text);
                bool isSucess = from7216.SetConsentDuration(Utility.Utility.getLoginSession().UserID, objUser);
                if (isSucess)
                {
                    BO_tblTaxPayerDetail objTaxPayer = (BO_tblTaxPayerDetail)HttpContext.Current.Session["UserObj"];
                    objTaxPayer.ConsentFormStatus = true;
                    HttpContext.Current.Session["UserObj"] = objTaxPayer;
                    Response.Redirect("MyFileStatus.aspx");
                }
            }
        }

        protected void txtSign_TextChanged(object sender, EventArgs e)
        {
            lblFullName.Text = txtSign.Text;
            btnSubmit.Enabled = true;
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("UserLogin.aspx");
        }

        
    }
}