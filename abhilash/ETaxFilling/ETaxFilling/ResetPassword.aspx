﻿<%@ Page Language="C#"  AutoEventWireup="true" CodeBehind="ResetPassword.aspx.cs" Inherits="ETaxFilling.ResetPassword" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Delta Tax Planners : Refer A Friend</title>
    <meta name="title" content="" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="alexaVerifyID" content="Ga2U3YWa-o9hB-cZ4ob6KoXwqZA" />
    <link rel="icon" type="image/png" href="images/favicon.png" />
    <link href="styles/front_style_home.css" media="screen" rel="stylesheet" type="text/css" />
      <script type="text/javascript">
          var fooElement = document.getElementById("#divFPerr");
          fooElement.style.color = "red";
    </script>
    <style type="text/css">
        .style1
        {
            text-align: right;
        }
    </style>
</head>
<body class='innerbody'>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <div>
        <!--wrapper start here -->
        <div class="wrapper">
            <!--header start here -->
            <!--header start here -->
            <div class="header clearfix">
                <div class="top_navigation">
                    <!--container start here -->
                    <div class="container">
                        <h1 class="logo alignleft">
                            <a href="index.html" title="Delta Tax Planners">Delta Tax Planners</a>
                        </h1>
                        <div class="top_link alignright">
                            <div class="top_linkrcor alignright">
                                <div class="alignleft">
                                    <a href="index.html">
                                        <img src="images/home-icon.gif" /></a> | <a href="Faq.html">FAQ</a> | <a href="Links.html">
                                            Links</a></div>
                                <div class="alignleft">
                                    | <a href="../../UserLogIn.aspx">Login</a></div>
                                | <a href="#">Sign Up</a>
                                <div class="alignnone">
                                </div>
                            </div>
                            <div class="alignright toplinklcor">
                            </div>
                            <div class="alignnone">
                            </div>
                        </div>
                    </div>
                    <!--container end here -->
                </div>
                <div class="alignnone">
                </div>
                <div class="mid-nav">
                    <!--container start here -->
                    <div class="container">
                        <div class="navigation_home">
                            <ul>
                                <li class="home"><a href="index.html" title="Home" class="home">Home</a></li>
                                <li class="aboutus"><a href="aboutus.html" title="About us" class="aboutus active">About
                                    us</a></li>
                                <li class="view"><a href="Services.html" title="Services" class="view ">Services</a></li>
                                <li class="myaccount"><a href="whydtp.html" title="Why DTP ?" class="myaccount ">Why
                                    DTP ?</a></li>
                                <li class="share"><a href="contactus.html" title="Contact Us" class="share ">Contact
                                    Us </a></li>
                                <li class="rewards last"><a href="faq.html" title="FAQ" class="rewards">FAQ </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!--container end here -->
                </div>
            </div>
            <!--header end here -->
            <!--header end here -->
            <!--middle start here -->
            <div class="inner_middle">
                <div class="container">
                    <div class="white_roundbdr_box">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <h2 class="text_header">
                                        <img src="images/success-story.png" />
                                       Forgot Password :</h2>
                                </td>
                                <td width="10">
                                </td>
                                <td width="200" valign="middle" align="right">
                                    <a href="#">
                                        <img src="images/its-free-signupnow.jpg" class="header_banner" /></a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td valign="top">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="white_roundbdr_box">
                                    <tr>
                                        <td class="sp_re_top">
                                            <span></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="sp_re_middle">
                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td align="left">
                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                                <td>
                                                                    <table  width="100%" class="cms" cellpadding="0" cellspacing="0" border="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                
                                                                                <div style="margin-left: 0px">
    <div id="divFPerr" style="color:#FF0000" runat="server"></div>
        <table width="100%" cellpadding="0" cellspacing="0" border="0">
          
            <tr>
                <td style="font-size: x-large; text-align: center">
                    &nbsp;</td>
            </tr>
        </table>
        <table width="100%" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td class="style1">
                    <asp:Label ID="lblemail" runat="server" Text="Email ID" ></asp:Label>
                    <span style="color:red;">*</span>
                    &nbsp;:</td>
                <td>
                    <asp:TextBox ID="txtEmail" runat="server" MaxLength="50" 
                        CssClass="ValidationRequired email" AutoPostBack="True" 
                        Width="124px"></asp:TextBox>
                </td>
                </tr>
                <tr>
                <td></td>
                <td></td>
                </tr>
            </tr>
            <tr>
                <td class="style3">
                <asp:HyperLink ID="HyperLink1" runat="server" CssClass="button" 
                        NavigateUrl="~/UserLogIn.aspx">Back</asp:HyperLink>
                </td>
                <td>
                    <asp:Button ID="btnReset" runat="server" CssClass="button" Text="Submit" 
                        Width="108px" onclick="btnReset_Click" TabIndex="1" />
                </td>
                <td>
                    <br />
                </td> 
            </tr>
            <tr>
            <td></td>
            <td><asp:Label ID="lblStatus" runat="server" ForeColor="#FF3300"></asp:Label></td>
            </tr>
        </table>
    </div>
    

                                                                                
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="sp_re_bottom">
                                            <span></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="15px;">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <!--middle end here -->
            <!--footer start here -->
            <div>
                <!--footer start here -->
                <script language="javascript">

                    var winPop = null;

                    function Open_win_bottom(id, page_rank, total_record, is_login) {
                        if (parseInt(is_login)) {
                            url = 'http://www.viewbestads.com/public/default/account/viewadvdetail/id/' + id + '/page_rank/' + page_rank + '/total_record/' + total_record + '/adv_flag/' + 'N';
                        }
                        else {
                            url = 'http://www.viewbestads.com/public/default/news/viewadvnewsdetail/id/' + id + '/page_rank/' + page_rank + '/total_record/' + total_record + '/news_flag/' + 'N';

                        }


                        if (winPop && !winPop.closed) { return alert("Page in Use!"); }

                        else { winPop = window.open(url, 'add_win', 'menubar=no,fullscreen=yes,resizable=yes,scrollbars=yes,location=0'); }



                    }
                </script>
                <div class="footer clearfix">
                    <div class="container">
                        <div class="footerbox alignleft">
                            <div class="footerbox-title latestnews">
                                Latest Positive News</div>
                            <div class="fzbox">
                                <ul>
                                    <li>
                                        <p>
                                            <a href="http://www.irs.gov/uac/2013-Standard-Mileage-Rates-Up-1-Cent-per-Mile-for-Business,-Medical-and-Moving"
                                                target="_blank">Standard Mileage Rates for 2013</a></p>
                                    </li>
                                    <li>
                                        <p>
                                            <a href="http://www.irs.gov/uac/Newsroom/Tax-Relief-for-Victims-of-Hurricane-Sandy-in-Maryland"
                                                target="_blank">Tax Relief for Victims of Hurricane Sandy in Maryland</a></p>
                                    </li>
                                </ul>
                            </div>
                            <div class="viewall">
                                <a href="news/viewadvnews/news_flag/T-2.html" title="View All" class="alignright">View
                                    All </a>
                            </div>
                        </div>
                        <div class="footerbox alignleft">
                            <div class="footerbox-title ourblog">
                                Our Blog</div>
                            <div class="fzbox">
                                <ul class="nopad">
                                    <li>
                                        <p>
                                            <a href="http://www.irs.gov/Tax-Professionals/PTIN-Requirements-for-Tax-Return-Preparers"
                                                target="_blank">Renew or Register Your PTIN for 2013</a> <span class="clear">&nbsp;</span>
                                            <span class="alignleft dateicon"><a href="blog/index6213.html?p=86" target="_blank">
                                                2012-12-18</a></span> <span class="alignright commenticon"><a href="blog/index6213.html?p=86"
                                                    target="_blank">0</a></span> <span class="clear">&nbsp;</span>
                                        </p>
                                    </li>
                                    <li>
                                        <p>
                                            <a href="http://www.irs.gov/uac/Free-File:-Do-Your-Federal-Taxes-for-Free" target="_blank">
                                                Free File: It's Fast, Easy & Secure</a> <span class="clear">&nbsp;</span> <span class="alignleft dateicon">
                                                    <a href="#" target="_blank">2012-12-15</a></span> <span class="alignright commenticon">
                                                        <a href="#" target="_blank">0</a></span> <span class="clear">&nbsp;</span>
                                        </p>
                                    </li>
                                </ul>
                            </div>
                            <div class="clear">
                            </div>
                            <div class="viewall">
                                <a href="#" title="View All" class="alignright" target="_blank">View All</a></div>
                        </div>
                        <div class="footerbox alignleft nomargin">
                            <div class="footerbox-title followontwitter">
                                Follow us on Twitter!</div>
                            <div class="social-bottom">
                                <a href="#" target="_blank" class="facebook-bot">Facebook</a><a href="#" target="_blank"
                                    class="twitter-bot">Twitter</a>
                            </div>
                            <br>
                            <font style="color: #FFFFFF; font-size: 11px; line-height: 42px;">Get Social (trust
                                us, it's healthy!) </font>
                        </div>
                    </div>
                    <!--<div class="viewall"><a href="http://twitter.com/ViewBestAds" title="View All" class="alignright">View All</a></div>-->
                </div>
                <div class="clear">
                </div>
            </div>
        </div>
        <!--footer end here -->
        <div class="copyright">
            <div class="container">
                <div class="alignleft">
                    Copyright &copy; 2012 Delta Tax Planners. All Rights Reserved.</div>
                <div class="alignright developbox">
                    <span class="alignleft">Designed and Developed by : &nbsp;</span><a href="http://www.venhan.com/"
                        target="_blank" title="Venhan Technologies Pvt Ltd." style="font-weight: bold;
                        color: #FFFFFF;"> Venhan Pvt. Ltd.</a></div>
                <div class="clear">
                </div>
            </div>
        </div>
        <script type="text/javascript">
            var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
            document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
        </script>
        <script type="text/javascript">
            try {
                var pageTracker = _gat._getTracker("UA-10012798-1");
                pageTracker._trackPageview();
            } catch (err) { }</script>
        <!-- vzSense -->
        <!--
<script type="text/javascript" src="http://vizisense.net/pixel.js"></script>
<script type="text/javascript">_vz_accountID="7Q2XBJRRTuk+IeIprwmt2w=="; if(typeof(_vz_sense) == "function") _vz_sense();</script>
<noscript>
  <img src="http://vizisense.net/cgi-bin/trail.fcgi?accountID=7Q2XBJRRTuk+IeIprwmt2w==&amp;isScript=false" style="display: none;" border="0" height="1" width="1" alt="vzSense"/>
</noscript>-->
    </div>
    </form>
</body>
<script src="../ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js" type="text/javascript"></script>
<script src="js/fancybox/jquery.fancybox-1.3.1.js" type="text/javascript"></script>
<script type="text/javascript">

    $(document).ready(function () {

        $("#fancy-forgot-pass").fancybox({
            'width': '50%',
            'height': '50%',
            'autoScale': false,
            'transitionIn': 'none',
            'transitionOut': 'none',
            'titleShow': false,
            'type': 'iframe'
        });

        $(".fancy-contact").fancybox({
            'width': '60%',
            'height': '80%',
            'autoScale': false,
            'transitionIn': 'none',
            'transitionOut': 'none',
            'type': 'iframe'
        });

        $("#fancy-tell-a-friend").fancybox({
            'width': '60%',
            'height': '98%',
            'autoScale': false,
            'transitionIn': 'none',
            'transitionOut': 'none',
            'type': 'iframe'
        });

        $("#fancy-twitter").fancybox({
            'width': '63%',
            'height': '99%',
            'autoScale': true,
            'transitionIn': 'none',
            'transitionOut': 'none',
            'type': 'iframe'
        });

        $("#fancy-invite-friend").fancybox({
            'width': '75%',
            'height': '98%',
            'autoScale': false,
            'transitionIn': 'none',
            'transitionOut': 'none',
            'type': 'iframe'
        });

        $("#fancy-invite-friend-successstory").fancybox({
            'width': '75%',
            'height': '98%',
            'autoScale': false,
            'transitionIn': 'none',
            'transitionOut': 'none',
            'type': 'iframe'
        });
    });	
</script>
</html>