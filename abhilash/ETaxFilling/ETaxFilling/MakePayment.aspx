﻿<%@ Page Language="C#"  MasterPageFile="~/Client.Master" AutoEventWireup="true" CodeBehind="MakePayment.aspx.cs" Inherits="ETaxFilling.MakePayment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="box">
<tr><td>
    <div style="" id="credit" class="pcpPanel ui-tabs-panel ui-widget-content ui-corner-bottom">
                            <div id="payment-box" class="payment-box ">

                                 
                                <fieldset class="credit-card-form prepend-1 prepend-top">
                                    <p id="card-radios" class="card-radio card-options">
                                        <label class="span-4">Card Type:</label>
                                                                                <label>
                                        <input class="required show-cc-form" name="ccard-type" value="1" 
                                            checked="checked" id="credit-card" type="radio" tabindex="0"/>
                                                      <img src="Images/Payment/visa-master_card.gif" width="99" height="28"></label>
                                                                                <label>
                                        <input class="required hide-cc-form" name="ccard-type" value="5" 
                                            id="amex-cards" type="radio" tabindex="1">
                                                      <img src="Images/Payment/pay-american.jpg" width="49" height="28"></label>
                                                                                <label>
                                        <input class="required hide-cc-form" name="ccard-type" value="4" id="diners" 
                                            type="radio" tabindex="2">
                                                      <img src="Images/Payment/pay-diners.jpg" width="50" height="28"></label>
                                                                            </p>
                                                                        <fieldset class="cc-select-form">
                                    <div id="user-card-radios" class="row user-card-options hidden"><label class="span-4" style="padding-top:6px" for="cname_on_card">Select your card :</label>
                                            <div class="payuCardDropDown select">
                                                <div class="drop" style="display: block;" id="drop_list_1">
                                                    <div class="drop_inner price_a_trip">
                                                        <ul>
                                                                                                                    <li>
                                                              <a class="payment_title" rel="1" href="javascript:void(0);">
                                                                <img src="Images/Payment/visa-master_card.gif" alt="" height="25"> 
                                                                <small>Visa | Master</small> 
                                                              </a>
                                                            </li>
                                                                        
                                                                        
                            <li>
                                                              <a class="payment_title" rel="2" href="javascript:void(0);">
                                                                <img src="Images/Payment/pay-american.jpg" alt="" height="25"> 
                                                                <small>American Express</small>
                                                              </a>
                                                            </li>
                                                                        
                                                                        
                              <li><a class="payment_title" rel="4" href="javascript:void(0);">
                                                               <img src="Images/Payment/pay-diners.jpg" alt="" height="25"> 
                                                               <small>Diners Club</small> 
                                                             </a>
                                                            </li>
                                                                                                                    </ul>
                                                    </div>
                                                    
                                                </div>
                                                <a href="javascript:void(0);" class="drop_image" id="drop_image_1"></a> 
                                                <span class="slected" id="drop_box_1">Select one</span>
                                            </div>
                                        </div>
                                     </fieldset>
                                    <fieldset style="" class="cc-form">
                                        <div class="row"><label class="span-4" for="cname_on_card">Name on Card:</label>
                                           <input name="cname_on_card" class="required text" id="cname_on_card" 
                                                maxlength="50" type="text" tabindex="3"></div>

                                        <div class="row"><label class="span-4" for="ccard_number">Card Number:</label>
                                            <input name="ccard_number" class="required creditcard text" maxlength="19" 
                                                id="ccard_number" type="text" tabindex="4">
                                                                                     </div>

                                        <div class="row"><label class="span-4" for="ccvv_number">CVV Number:</label>
                                            <input name="ccvv_number" class="required validccvv digits password" 
                                                maxlength="3" id="ccvv_number" pattern="[0-9]*" type="password" tabindex="5">
                                                                                        <span class="cvv_help">
                                                <a class="help-link" href="#ccvv_help">What is CVV number?</a>
                                                <img id="ccvv_help" class="help-img" src="Images/Payment/cvv_help.gif">
                                            </span>
                                                                                    </div>
                                        <p class="dropdowns"><label class="span-4">Expiry Date:</label>
                                        <span class="hide-ccExpiry hidden" style="font-size: 13px; width:160px; float: left; font-weight: bold; line-height: 28px; padding: 5px 0px 0px 5px;"><img src="Images/Payment/disabled_select.jpg"><img src="Images/Payment/disabled_select1.jpg" style="clear:none; margin-left:10px"></span>
                                            <select id="cexpiry_date_month" name="cexpiry_date_month" tabindex="6">
                                                <option selected="selected" value="01">Jan</option>
                                                <option value="02">Feb</option>
                                                <option value="03">Mar</option>
                                                <option value="04">Apr</option>
                                                <option value="05">May</option>
                                                <option value="06">Jun</option>
                                                <option value="07">Jul</option>
                                                <option value="08">Aug</option>
                                                <option value="09">Sep</option>
                                                <option value="10">Oct</option>
                                                <option value="11">Nov</option>
                                                <option value="12">Dec</option>
                                            </select>
                                            <select id="cexpiry_date_year" name="cexpiry_date_year" tabindex="7">
                                                                                                <option selected="selected" value="2012">
                                                    2012                                                </option>
                                                                                                <option value="2013">
                                                    2013                                                </option>
                                                                                                <option value="2014">
                                                    2014                                                </option>
                                                                                                <option value="2015">
                                                    2015                                                </option>
                                                                                                <option value="2016">
                                                    2016                                                </option>
                                                                                                <option value="2017">
                                                    2017                                                </option>
                                                                                                <option value="2018">
                                                    2018                                                </option>
                                                                                                <option value="2019">
                                                    2019                                                </option>
                                                                                                <option value="2020">
                                                    2020                                                </option>
                                                                                                <option value="2021">
                                                    2021                                                </option>
                                                                                                <option value="2022">
                                                    2022                                                </option>
                                                                                                <option value="2023">
                                                    2023                                                </option>
                                                                                                <option value="2024">
                                                    2024                                                </option>
                                                                                                <option value="2025">
                                                    2025                                                </option>
                                                                                                <option value="2026">
                                                    2026                                                </option>
                                                                                                <option value="2027">
                                                    2027                                                </option>
                                                                                                <option value="2028">
                                                    2028                                                </option>
                                                                                                <option value="2029">
                                                    2029                                                </option>
                                                                                                <option value="2030">
                                                    2030                                                </option>
                                                                                                <option value="2031">
                                                    2031                                                </option>
                                                                                                <option value="2032">
                                                    2032                                                </option>
                                                                                                <option value="2033">
                                                    2033                                                </option>
                                                                                                <option value="2034">
                                                    2034                                                </option>
                                                                                                <option value="2035">
                                                    2035                                                </option>
                                                                                                <option value="2036">
                                                    2036                                                </option>
                                                                                                <option value="2037">
                                                    2037                                                </option>
                                                                                            </select>
                                        </p>
                                    </fieldset>                                 </fieldset>
                                                                <p class="p-endnote prepend-top prepend-1" style="margin-top:20px"><span>Note:</span> In the next step you will be redirected to your bank's website to verify yourself. </p>
                            </div>
                          <span style="clear:both"></span>
                        </div>
</td></tr>
</table>
</asp:Content>