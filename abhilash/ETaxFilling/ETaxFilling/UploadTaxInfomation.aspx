﻿<%@ Page Language="C#" MasterPageFile="~/Client.master" CodeBehind="UploadTaxInfomation.aspx.cs" Inherits="ETaxFilling.UploadTaxInfomation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script src="Scripts/Upload/jquery-1.3.2.js" type="text/javascript"></script>
    <script src="Scripts/Upload/jquery.MultiFile.js" type="text/javascript"></script>
    <table>
    <tr>
     <td>
      <h2> <asp:Label ID="Label" class="divider top20 bottom10" runat="server" Text="Tax Information" ></asp:Label></h2>
     </td>
     <tr>
     <td>
     <h5>Kindly download the below Tax Information Sheet and upload it duly filled :</h5>
     </td>
     </tr>
     <tr>
     <td align="center">
     <asp:Button ID="BtnDownload" runat="server" Text="Download Tax Information Sheet" 
             ClientIDMode="Static" CssClass="button" onclick="BtnDownload_Click" 
             Width="235px"></asp:Button>
         <asp:Label ID="lbldownload" CssClass="error" runat="server"></asp:Label>
     </td>
     </tr>
     </tr>
     <tr>
     <td>
     <asp:Label ID="Label1" CssClass="error" runat="server" Text="" ></asp:Label>
     </td></tr>
       </table>
       <table>
           <tr>
                   <td align="center" style="width: 60px"> S.No </td>
                   <td align="center"> Browse </td>
                   <td align="center"> Remarks </td>  
                   <td align="center">  Upload</td>
                       </tr>

                       <tr>
                       <td style="width: 60px">1</td>
                       <td ><asp:FileUpload ID="FileUpload1" runat="server" class="multi" TabIndex="1" 
                               Width="229px" /> </td>                         
                       <td><asp:TextBox runat="server" Width="142px"  ID="txtTaxInformation" TextMode="MultiLine" 
                               TabIndex="2" Height="24px" ></asp:TextBox></td>
                                <td><asp:Button CssClass="button" Text="Upload" ID="btnTaxInfo" runat="server"  
                                OnClientClick="return ValidateFile" onclick="btnTaxInfo_Click" TabIndex="3" 
                                        Width="60px" /></td>  
                     
     <td>
         <asp:Button ID="btnView" CssClass="button" runat="server" onclick="btnView_Click" 
             onclientclick="return ValidateFile" Text="View" TabIndex="4" 
             Width="59px" />
                           </td>
                            </tr>
   <tr>
 <td style="width: 60px"> 
     <asp:Label ID="Label2" CssClass="error" runat="server" Text="Label"></asp:Label>
           </td>
           <td>
           </td>
           <td></td>
           <td align="right">
           <asp:HyperLink ID="hypnext" runat="server" Text="Next" CssClass="button"  Width="60px"
                   NavigateUrl="~/MyFileStatus.aspx"></asp:HyperLink>
           </td>
           </tr>
           </table>

 <asp:GridView ID="GdVwUploadTaxInformation" runat="server" AutoGenerateColumns="False"
        Height="75px" Width="100%" margin="10%" 
        onrowcommand="GdVwUploadTaxInformation_RowCommand" AllowPaging="True" 
        onpageindexchanging="GdVwUploadTaxInformation_PageIndexChanging"   CssClass="mGrid">
        <Columns>
<asp:TemplateField HeaderText="S.No.">
<ItemTemplate><%#Container.DataItemIndex+1 %><br />
</ItemTemplate>
</asp:TemplateField>
        
         <asp:BoundField DataField ="TaxSheetID"   Visible="false"/> 
      <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
      <asp:BoundField DataField="SheetName" HeaderText="Sheet Name" />
       <asp:BoundField DataField="CreatedOn" HeaderText="Uploaded Date&Time" 
                DataFormatString="{0:d}" HtmlEncode="False" />
         <asp:TemplateField>
         <ItemTemplate>

         <asp:LinkButton ID="BtnDownload" runat ="server" CommandName="Download" CommandArgument='<%# DataBinder.Eval(Container,"DataItem.TaxSheetID") %>' Text ="Download" ></asp:LinkButton>
         </ItemTemplate>
         </asp:TemplateField>
        </Columns>
    </asp:GridView>
    </asp:Content>