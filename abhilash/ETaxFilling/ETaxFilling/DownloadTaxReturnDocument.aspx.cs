﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ETaxDal;
using ETaxBO;
using System.IO;

namespace ETaxFilling
{
    public partial class DownloadTaxReturnDocument : System.Web.UI.Page
    {
        UploadDocumentsDAL objDocumentDal = new UploadDocumentsDAL();
       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Utility.Utility.checkLoginSession())
            {
                Response.Redirect("userlogin.aspx");
            }
            List<BO_tblTaxSheets> objtaxSheets = objDocumentDal.getReturnTaxpayerInformation(Utility.Utility.getLoginSession().TaxPayerID, Convert.ToInt32(MasterItems.TaxSheetSType.ReturnDocuments), (MasterItems.TaxSheetSource.Admin).ToString());
            if (objtaxSheets.Count!=0)
            {
                GdVDownloadDocuments.DataSource = objtaxSheets;
                GdVDownloadDocuments.DataBind();
            }
            else
           {
             lblStuas.Text = "No Download Recods Found";
             lblStuas.ForeColor = System.Drawing.Color.Red;
           }
        }
        protected void GdVDownloadDocuments_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Download")
            {
             BO_tblTaxSheets objTaxSheets = objDocumentDal.GetTaxPayerSheets(Convert.ToInt32(e.CommandArgument.ToString()));
             byte[] fileData = objTaxSheets.TaxSheet;
                Response.ClearContent();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + objTaxSheets.SheetName);
                BinaryWriter bw = new BinaryWriter(Response.OutputStream);
                bw.Write(fileData);
                bw.Close();
                Response.ContentType = "application/pdf";
                Response.End();
            }
        }
    }
}