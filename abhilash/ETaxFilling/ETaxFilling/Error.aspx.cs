﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

namespace ETaxFilling
{
    public partial class Error : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadError(Server.GetLastError().GetBaseException());
            }
        }
        private void LoadError(Exception exception)
        {
            string strError = "<b>Error Has Been Caught in Page_Error event</b><hr><br>" +
                        "<br><b>Error in: </b>" + Request.Url.ToString() +
                        "<br><b>Error Message: </b>" + exception.Message.ToString() +
                        "<br><b>Stack Trace:</b><br>" +
                              exception.StackTrace.ToString();
            ViewState.Add("LastError", strError.ToString());
            SendEmail();
            Server.ClearError();
        }
        private void SendEmail()
        { 
            try
            {
                Utility.MailMessage ms = new Utility.MailMessage();
                StringBuilder body = new StringBuilder();

                body.AppendLine("An unexcepted error has occurred.");
                body.AppendLine();

                body.AppendLine(ViewState["LastError"].ToString());

                ms.Subject = "Error";
                ms.Body = body.ToString();
                ms.isBodyHtml = true;
                ms.From = "info@dtptax.com";// admin 
                ms.To = "errors@dtptax.com";
                ms.SendMail();
               
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }           
    }
}