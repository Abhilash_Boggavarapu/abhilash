﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Client.master" AutoEventWireup="true" CodeBehind="Inbox.aspx.cs" Inherits="ETaxFilling.Inbox" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding-right:10px;" id="TABLE1" class="box">
  <tr>
    <td>
    <h2 Class="divider top20 bottom10"><asp:Label ID="lblTitle" runat="server" Text="Inbox" Width="100%"></asp:Label></h2></td>
  </tr>
 <%-- <tr>
    <td align="left"><uc1:MailMenu ID="MailMenu1" runat="server" /></td>
  </tr>--%>
  <tr>
  <td align="center"><asp:Label id="lblMessage" runat="server" cssclass="error"></asp:Label></td>
  </tr>
  <tr>
    <td class="content" align="center">
    <table width="100%" border="0" cellpadding="0" cellspacing="8">
	  <tr>
        <td style="width:100%; text-align:center;">
		<asp:GridView ID="gvInbox" runat="server" AllowPaging="True" AlternatingRowStyle-CssClass="alt"
        AutoGenerateColumns="False" CssClass="mGrid" PagerStyle-CssClass="pgr" 
                onrowcommand="gvInbox_RowCommand" onpageindexchanging="gvInbox_PageIndexChanging" 
                 >
<AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
        <Columns>
            <asp:TemplateField HeaderText="S.No.">
                <ItemTemplate>
                    <%#Container.DataItemIndex + 1%>
                </ItemTemplate>
            </asp:TemplateField>
            <%--<asp:BoundField DataField="From_Memberid" HeaderText="From_id" /--%>
            <asp:BoundField DataField="Emailid" HeaderText="From Emailid" />
            <asp:BoundField DataField="Subject" HeaderText="Subject" />
            <asp:BoundField DataField="Date" HeaderText="Date" />
            <asp:BoundField DataField="Isread" HeaderText="Isread" />
            <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkView" runat="server" Text="View" CssClass="link" CommandName="view" CommandArgument='<%# DataBinder.Eval(Container,"DataItem.ID") %>'>Show Mail</asp:LinkButton>
                                 <asp:Label ID="ID" Visible="false" runat="server" Text='<%# DataBinder.Eval (Container.DataItem, "ID") %>'></asp:Label>
                                <asp:Label ID="status" Visible="false" Text='<%# DataBinder.Eval (Container.DataItem, "Isread") %>'
                                    runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
            
        </Columns>

<PagerStyle CssClass="pgr"></PagerStyle>
    </asp:GridView>
	</td>
      </tr>
	  </table></td>
  </tr>
</table>
</asp:Content>
