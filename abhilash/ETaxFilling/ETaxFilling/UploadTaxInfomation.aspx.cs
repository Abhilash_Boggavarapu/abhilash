﻿namespace ETaxFilling
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using ETaxDal;
    using ETaxFilling;
    using ETaxBO;
    using System.IO;
    public partial class UploadTaxInfomation : System.Web.UI.Page
    {
        UploadDocumentsDAL objDocumentDal = new UploadDocumentsDAL();
        MasterDAL objMasterDal = new MasterDAL();
        string Remarks;
        long taxPayerID;
       
        protected void Page_Load(object sender, EventArgs e)
        {
            Label1.Text = "";
            Label2.Text = "";
            if (!Utility.Utility.checkLoginSession())
            {
                Response.Redirect("userlogin.aspx");
            }

            
             
            taxPayerID = Utility.Utility.getLoginSession().TaxPayerID;
        }

        protected void btnTaxInfo_Click(object sender, EventArgs e)
        {
            Remarks = txtTaxInformation.Text;
            fileExtension();
            txtTaxInformation.Text = "";
        }

        private void fileExtension()
        {
            if (FileUpload1.PostedFile.FileName == "")
            {
                Label1.ForeColor = System.Drawing.Color.Red;
                Label1.Text = "No file selected to upload";
            }
            else
            {
                string[] validFileTypes = { "zip", "rar", "doc", "docx", "pdf", "jpeg", "jpg", "xlsx" };
                string ext = System.IO.Path.GetExtension(FileUpload1.PostedFile.FileName);
                bool isValidFile = false;
                for (int i = 0; i < validFileTypes.Length; i++)
                {
                    if (ext == "." + validFileTypes[i])
                    {
                        isValidFile = true;
                        break;
                    }
                }
                if (!isValidFile)
                {
                    Label1.ForeColor = System.Drawing.Color.Red;
                    Label1.Text = "Invalid file. Please upload a file with extension " +
                    string.Join(",", validFileTypes);
                }
                else
                {
                    SaveDocuments();
                    Label1.ForeColor = System.Drawing.Color.Green;
                    Label1.Text = "File uploaded successfully.";

                }
            }
        }
        private void SaveDocuments()
        {
            try
            {
                BO_tblTaxSheets objSheets = new BO_tblTaxSheets();

                // Get the HttpFileCollection
                HttpFileCollection hfc = Request.Files;
         
                for (int i = 0; i < hfc.Count; i++)
                {
                    HttpPostedFile hpf = hfc[i];
                    if (hpf.ContentLength == 0)
                    {
                        Label1.ForeColor = System.Drawing.Color.Red;
                        Label1.Text = "Can't  upload empty files";
                    }
                    if (hpf.ContentLength > 0)
                    {
                        if (hpf.ContentLength == 0)
                        {
                            Label1.Text = "Can't upload empty files";
                            Label1.ForeColor = System.Drawing.Color.Red;

                        }
                        string fileName = hpf.FileName;
                        int fileSize = hpf.ContentLength;
                        byte[] documentBinary = new byte[fileSize];
                        hpf.InputStream.Read(documentBinary, 0, fileSize);
                        objSheets.Remarks = Remarks;
                        objSheets.SheetName = fileName;
                        objSheets.SheetsType = Convert.ToInt32(MasterItems.TaxSheetSType.TaxInformation);
                        objSheets.TaxSheet = documentBinary;
                        // objSheets.TaxPayerID = (long)Utility.Utility.getLoginSession().CurrentTaxPayer;
                        objSheets.TaxPayerID = taxPayerID;
                        objSheets.TaxSheetSource = MasterItems.TaxSheetSource.TaxPayer.ToString();
                        objDocumentDal.SaveTaxSheet(objSheets);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    
        protected void btnView_Click(object sender, EventArgs e)
        {

            Label1.Text = "";
            display();
        }


        private void display()
        {

            GdVwUploadTaxInformation.DataSource = null;
            GdVwUploadTaxInformation.DataBind();
            List<BO_tblTaxSheets> objsheet = objDocumentDal.GetTaxpayerInformation(taxPayerID, Convert.ToInt32(MasterItems.TaxSheetSType.TaxInformation), (MasterItems.TaxSheetSource.TaxPayer).ToString());
            if (objsheet.Count != 0)
            {
                GdVwUploadTaxInformation.DataSource = objsheet;
                GdVwUploadTaxInformation.DataBind();
            }
            else
            {
                Label2.ForeColor = System.Drawing.Color.Red;
               Label2.Text = "No files found ";
            }
            
        }
        protected void BtnDownload_Click(object sender, EventArgs e)
        {
            BO_tblTaxSheets objTaxSheets = objDocumentDal.getTaxpayerExelSheet(Convert.ToInt32(MasterItems.TaxSheetSType.TaxInformation),(MasterItems.TaxSheetSource.Admin).ToString());
            if (objTaxSheets != null)
            {
              byte[] fileData = objTaxSheets.TaxSheet;
               string fileName = objTaxSheets.SheetName;
                Response.ClearContent();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
                BinaryWriter bw = new BinaryWriter(Response.OutputStream);
                bw.Write(fileData);
                bw.Close();
                Response.ContentType = "application";
                Response.End();
            }
            else
            {
                lbldownload.Text = "No file has found";

            }
             
        }

        protected void GdVwUploadTaxInformation_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GdVwUploadTaxInformation.PageIndex = e.NewPageIndex;
            display();
        }

        protected void GdVwUploadTaxInformation_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Download")
            {
           BO_tblTaxSheets objTaxsheets   = objDocumentDal.GetTaxPayerSheets(Convert.ToInt32(e.CommandArgument.ToString()));
           byte[] fileData = objTaxsheets.TaxSheet;
                Response.ClearContent();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + objTaxsheets.SheetName);
                BinaryWriter bw = new BinaryWriter(Response.OutputStream);
                bw.Write(fileData);
                bw.Close();
                Response.ContentType = "application/pdf";
                Response.End();
            }
        
        }
 
    }
}