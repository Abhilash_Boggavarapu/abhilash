﻿<%@ Page Language="C#"  MasterPageFile="~/Client.master" AutoEventWireup="true" CodeBehind="MyFileStatus.aspx.cs" Inherits="ETaxFilling.MyFileStatus" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <asp:Panel ID="Panel1" runat="server">
            <table width="100%" align="center"  cellpadding="0" cellspacing="0" class="box">

               <tr>
        <td colspan="3">
        <h2 class="divider top20 bottom10">File Status</h2>
        </td>
        </tr>
                <tr>
                    <td align="center" style="font-size: medium; width: 307px;" >
                        <strong>File Status</strong></td>
                    <td align="center" style="font-size: medium" >&nbsp;</td>
                       
                    <td align="center" style="font-size: medium">
                        <strong> Date & Time</strong></td>
                </tr>
                <tr>
                    <td align="center" style="width: 307px" >
                        Personal Information</td>
                    <td align="center">
                        <asp:Label ID="lblPersonalInfo" runat="server"></asp:Label>
                    </td>
                    <td align="center">
                        <asp:Label ID="lblPinfoDate" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="width: 307px" >
                        Fill Tax Information</td>
                    <td align="center">
                        <asp:Label ID="lblFillTaxInfo" runat="server"></asp:Label>
                    </td>
                    <td align="center">
                        <asp:Label ID="lblFillTaxDate" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="width: 307px" >
                        Upload Tax Documents</td>
                    <td align="center">
                        <asp:Label ID="lblUploadDoc" runat="server"></asp:Label>
                    </td>
                    <td align="center">
                        <asp:Label ID="lblUploadDate" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                 <td align="center" style="width: 307px" >
                        <asp:Label ID="lblPrep" runat="server"></asp:Label>
                    </td>
                    <td align="center">
                        <asp:Label ID="lblPreparation" runat="server"></asp:Label>
                    </td>
                    <td align="center">
                        <asp:Label ID="lblPrpData" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="width: 307px">
                        <asp:Label ID="lblPay" runat="server"></asp:Label>
                    </td>
                    <td align="center">
                        <asp:Label ID="lblPayment" runat="server"></asp:Label>
                    </td>
                    <td align="center">
                        <asp:Label ID="lblPayDate" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                 <td align="center" style="width: 307px" >
                        <asp:Label ID="lblConfirm" runat="server"></asp:Label>
                    </td>
                    <td align="center">
                        <asp:Label ID="lblConformation" runat="server"></asp:Label>
                    </td>
                    <td align="center">
                        <asp:Label ID="lblCnfmDate" runat="server"></asp:Label>
                    </td>
                </tr>
               
                <tr>
                    <td align="center" style="width: 307px">
                        <asp:Label ID="lblEtax" runat="server"></asp:Label>
                    </td>
                    <td align="center">
                        <asp:Label ID="lblEtaxStus" runat="server"></asp:Label>
                    </td>
                    <td align="center">
                        <asp:Label ID="lblEtaxDate" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                <td align="center">
                    <asp:Label ID="lblComplt" runat="server"></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblCompltStaus" runat="server"></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="lblCompltDate" runat="server"></asp:Label>
                </td>
                </tr>
               
            </table>
         </asp:Panel>
         </asp:Content >    