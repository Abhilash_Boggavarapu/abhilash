﻿<%@ Page Language="C#" MasterPageFile="~/Client.master" AutoEventWireup="true" CodeBehind="ReferFriend.aspx.cs" Inherits="ETaxFilling.ReferFriend" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="divRFerr" style="color:#FF0000" runat="server"></div>
        <table width="100%" border="0px" cellpadding="0" cellspacing="6" class="box">
            <tr>
                <td>
                    <h2 class="divider top20 bottom10">
                        Refer A Friend</h2>
                </td>
            </tr>
            <tr>            <td>   <asp:Panel ID="Panel1" runat="server">
                <table width="100%" border="0px" cellpadding="0" cellspacing="6" class="box">
                    <tr>
                        <td align="right">
                           Referral Name : 
                        </td>
                        <td>
                            <asp:TextBox ID="txtReferalName" runat="server" 
                                MaxLength="50" TabIndex="1"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                             Email Id : 
                        </td>
                        <td>
                            <asp:TextBox ID="txtEmailId" runat="server" 
                                MaxLength="50" TabIndex="2"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                             Mobile Number :
                        </td>
                        <td>
                            <asp:TextBox ID="txtMobile"  runat="server" 
                                MaxLength="20" TabIndex="3"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                             Work Number :
                        </td>
                        <td>
                            <asp:TextBox ID="txtWork" runat="server" 
                                MaxLength="20" TabIndex="4"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td  >
                            <asp:Button ID="btnSaveReferal" runat="server" Text="Save" CssClass="button" 
                                OnClick="btnSaveReferal_Click" Height="23px" Width="109px" TabIndex="5"/>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            </td></tr>
            <tr>
            <td align="center">  <asp:Button ID="btnAddReferal" runat="server" Text="Add More Referral" OnClick="btnAddReferal_Click"
                        CssClass="button" Height="29px" Width="169px" />
                </td></tr>
            
            <%-- <Columns>
       <asp:TemplateField HeaderText="Email Id"></asp:TemplateField>
        </Columns>
       

        <Columns>
       <asp:TemplateField HeaderText="Mobile"></asp:TemplateField>
        </Columns>
         <Columns>
       <asp:TemplateField HeaderText="Work"></asp:TemplateField>
        </Columns>--%>
            <tr>
                <td>

                    &nbsp;</td>
            </tr>
            </table>
         


          <span align="center"> <asp:Label ID="lblMsg" runat="server" Text=" " ForeColor="#FF3300"></asp:Label> </span>

          <table width="100%" border="0px" cellpadding="0" cellspacing="6" class="box">
          <tr>
                <td>
                    <asp:GridView ID="grdReferal" runat="server" AutoGenerateColumns="False" 
                        Width="100%" AllowPaging="True" 
                        onpageindexchanging="grdReferal_PageIndexChanging" CssClass="mGrid">
                        <Columns>
                            <asp:TemplateField HeaderText="S.No.">
                                <ItemTemplate>
                                    <%#Container.DataItemIndex+1 %><br />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="ReferredID" Visible="false" />
                            <asp:BoundField DataField="ReferralName" HeaderText="Referral Name" SortExpression="EID" />
                            <asp:BoundField DataField="EmailID" HeaderText="EmailID " />
                            <asp:BoundField DataField="Phone1" HeaderText="Mobile No" />
                            <asp:BoundField DataField="Phone2" HeaderText="Work No" />
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
          </table>

</asp:Content>
