<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="entry7216form.aspx.cs" Inherits="ETaxFilling.entry7216from" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<div id="divConerr" style="color:#FF0000" runat="server"></div>
<title>E-Filing</title>
</head>



<style>
p{ font-family:Geneva, Arial, Helvetica, sans-serif, Times New Roman ; font-size:13px; padding-top:0px; padding-bottom:5px; line-height:20px; text-align:justify;}
.box {
 	font-family:Verdana, Arial, Helvetica, sans-serif; 
	font-weight:bold; 
	font-size:18px;
	background-color:#0066CC;
	color:#FFFFFF;
	width: 300px;
	height: 25px;
	margin: 0 auto 5px auto;
	padding: 8px;
	border: 1px solid #333333;
	-moz-border-radius: 11px;
	-webkit-border-radius: 11px;
	border-radius: 8px;
	behavior: url(border-radius.htc);
}
.headings{ font-family:Geneva, Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#0066CC; text-align:left;}

</style>

<body>

<div align ="center" >
<form id="Form1"  runat ="server">
<table width="650px" border="1" cellpadding="6" cellspacing="0" style="border:1px solid #CCCCCC;">
  <tr>
    <td align="center" style="padding-top:15px;"><h3 class="box" style="padding-top:25px;">7216 Consent Form</h3></td>
  </tr>
  <tr>
    <td><p style ="font-size :12px ;">Federal law requires this consent form be provided to you. Unless authorized by law, we cannot disclose, without your consent, your tax return information to third parties for purposes other than the preparation and filing of your tax return and, in certain limited circumstances, for purposes involving tax return preparation. If you consent to the disclosure of your tax return information, Federal law may not protect your tax return information from further use or distribution.
    <br /><br />
You are not required to complete this form. Because our ability to disclose your tax return information to another tax return preparer affects the service that we provide to you and its cost, we may decline to provide you with service or change the terms of service that we provide to you if you do not sign this form. If you agree to the disclosure of your tax return information, your consent is valid for the amount of time that you specify. If you do not specify the duration of your consent, your consent is valid for one year.
</p></td>
  </tr>
  <tr>
    <td>
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td class="headings">Duration of Consent : </td>
    <td> <asp:TextBox ID="txtDOC" runat="server" Width="213px" Text ="1" ></asp:TextBox> </td>
    
  </tr>
</table>
</td>
  </tr>
   <tr>
    <td><p> This consent to disclose may result in your tax return information being disclosed to a tax return preparer located outside the United States, including your personally identifiable information such as your Social Security Number (&#8220;SSN&#8221;). Both the tax return preparer in the United States that will disclose your SSN and the tax return preparer located outside the United States which will receive your SSN maintain an adequate data protection safeguard (as required by the regulations under 26 U.S.C. Section 7216) to protect privacy and prevent unauthorized access of tax return information. If you consent to the disclosure of your tax return information, Federal agencies may not be able to enforce U.S. laws that protect the privacy of your tax return information against a tax return preparer located outside of the U.S. to which the information is disclosed.
</p></td></tr>
   <tr>
    <td><p>I, <asp:Label ID="lblFullName" runat ="Server"></asp:Label> &nbsp;authorize Delta Tax Planners to disclose Form 1040 or as applied Form to Internal Revenue Service or any third party  (Eg. Lawyer who will represent the case in case of query) for the purpose of filing Income Tax Return or for representation as the case may be.

</p></td>
  </tr>
  <tr>
    <td><p style ="font-size :12px ;">If you believe your tax return information has been disclosed or used improperly in a manner unauthorized by law or without your permission, you may contact the Treasury Inspector General for Tax Administration (TIGTA) by telephone at 1-800-366-4484, or by email at <br /><br />
	<span class="headings">complaints@tigta.treas.gov</span>
</p></td>
  </tr>
  <tr>
    <td>
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td class="headings">Tax Payer Signature :  
        <asp:TextBox ID="txtSign" 
            runat="server" Width="214px" TabIndex="1" 
            ontextchanged="txtSign_TextChanged" style="margin-top: 0px" AutoPostBack="True" 
            ></asp:TextBox></td>
    <td class="headings">Date: <asp:Label ID="lbldate" runat ="server"></asp:Label></td>
  </tr>
</table>
</td>
  </tr>
</table>
<table>
<tr>
<td align ="center" >
<asp:Button  ID="btnBack" runat ="server" Text ="Back" onclick="btnBack_Click" 
        Width="58px" /></td>
<td></td><td></td>
<td>
    <asp:Button ID="btnSubmit" runat="server" Text="Confirm"
        onclick="btnSubmit_Click" /> 
</td></tr>
</table>
</form>
</div>
</body>
</html>


