﻿<%@ Page Language="C#"  MasterPageFile="~/DefaultMaster.Master" AutoEventWireup="true" CodeBehind="Resetpass.aspx.cs" Inherits="ETaxFilling.Resetpass" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script type="text/javascript">
        var fooElement = document.getElementById("#divFPerr");
        fooElement.style.color = "red";
    </script>
    <div style="margin-left: 0px">
    <div id="divFPerr" style="color:#FF0000" runat="server"></div>
        <table>
            <tr>
                <td style="font-size: x-large; text-align: center">
                    Forgot Password</td>
            </tr>
            <tr>
                <td style="font-size: x-large; text-align: center">
                    &nbsp;</td>
            </tr>
        </table>
        <table class="style2">
            <tr>
                <td class="style3">
                    <asp:Label ID="lblemail" runat="server" Text="Email ID" ></asp:Label>
                    <span style="color:red;">*</span>
                    &nbsp;:</td>
                <td>
                    <asp:TextBox ID="txtEmail" runat="server" MaxLength="50" 
                        CssClass="ValidationRequired email" AutoPostBack="True" 
                        Height="20px" Width="124px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style3">
                <asp:HyperLink ID="HyperLink1" runat="server" CssClass="button" 
                        NavigateUrl="~/UserLogIn.aspx">Back</asp:HyperLink>
                </td>
                <td>
                    <asp:Button ID="btnReset" runat="server" CssClass="button" Text="Submit" 
                        Width="108px" onclick="btnReset_Click" TabIndex="1" />
                </td>
                <td>
                    <br />
                </td> 
            </tr>
        </table>
    </div>
    <asp:Label ID="lblStatus" runat="server" ForeColor="#FF3300"></asp:Label>
    <br />
    </asp:Content>