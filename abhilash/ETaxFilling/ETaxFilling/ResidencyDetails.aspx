﻿<%@ Page Language="C#" MasterPageFile="~/Client.Master" AutoEventWireup="true" CodeBehind="ResidencyDetails.aspx.cs" Inherits="ETaxFilling.ResidencyDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  
    <script type="text/javascript">

        $(function () {
            $("#txtStartDate,#txtEndDate").datepicker({
                showOn: "button",
                buttonImage: "B_Images/calendar_icon.png",
                changeMonth: true,
                changeYear: true,
                buttonImageOnly: true
            });
        });

    </script>
    <div align ="center" > 
   <table width="100%" border="0" cellpadding="0" cellspacing="0" class="box">
       <tr align="center"> 
        <td>
        <div><ul class ="submenu" >
                        <li>  <a  href="EmployerDetails.aspx"   >Employer Details</a></li>
	       <li> <a href="ClientDetails.aspx" >Client Details</a> </li>
      <li> <a   style="color: ButtonHighlight "  href="#" >Residency Details</a></li>
      </ul>
     </div>
     </td>
     </tr>
     </table> 
     <table>
    <tr>
    <td>
    <asp:LinkButton runat="server" ID="lnkTaxPayer" Text="Tax Payer" CssClass="button" OnClick="lnkTaxPayer_Click"></asp:LinkButton>
            </td>
            <td>
                <asp:LinkButton runat="server" ID="lnkSpouse" Text="Spouse" CssClass="button" OnClick="lnkSpouse_Click"></asp:LinkButton>
            </td>
        </tr>
       
    </table>
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
               <tr>
        <td>
       <h3>
           <asp:Label ID="lblClientDetails" runat="server" Text=" Taxpayer Residency Details" />
       </h3>
        </td>
        </tr>
           </table>

     <div id="divRSDerror" style="color:#FF0000" runat="server"></div>
        <table>
            <tr>
            <td>
                <asp:Label Text="" CssClass="error" ID="lblSpouse" runat="server" />
            </td>
                <td>
                <h4>
                    <asp:Label Text="" CssClass="error" ID="lblMsg" runat="server" />
                </h4>
                </td>
            </tr>
        </table>
   <table width="100%" border="0" cellpadding="0" cellspacing="0" class="box">
        <tr>
            <td align="right" > <span style="color:red;">*</span>
                State Name:</td>
            <td align="left">
            <asp:TextBox ID="txtStateName" runat="server"  CssClass="ValidationRequired" 
                    MaxLength="50"  Width="150px" Height="30px"></asp:TextBox>
               </td>
        </tr>
        <tr>
            <td align="right" > <span style="color:red;">*</span>
                City:</td>
            <td align="left">
            <asp:TextBox ID="txtCity" runat="server"  CssClass="ValidationRequired" 
                    MaxLength="50" TabIndex="1"  Width="150px" Height="30px"></asp:TextBox>
               </td>
        </tr>
        <tr>
            <td align="right"> <span style="color:red;">*</span>
                Start Date:</td>
            <td align="left">
            <asp:TextBox ID="txtStartDate" ClientIDMode="Static" 
                    CssClass="ValidationRequired date" runat="server" 
                    TabIndex="2"  Width="150px" Height="30px"></asp:TextBox>
                </td>
        </tr>
        <tr>
            <td  align="right" > <span style="color:red;">*</span>
                End Date:</td>
            <td align="left" >
            <asp:TextBox ID="txtEndDate" ClientIDMode="Static" 
                    CssClass="ValidationRequired date" runat="server" 
                    TabIndex="3"  Width="150px" Height="30px"></asp:TextBox>

                </td>
        </tr>
        <tr>
            <td >
               </td>
            <td>
            <asp:Button ID="btnSave" runat="server" Text="Save" Width="90px" 
                    onclick="btnSave_Click" TabIndex="4" Height="30px" CssClass ="button" />
               
                </td>
        </tr>
       
    </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
        <td align ="center" >
            <asp:HyperLink ID="HyperLink2" runat="server" CssClass="button"  Height="25px" Width="50px"
                    TabIndex="6" NavigateUrl="~/ClientDetails.aspx">Back</asp:HyperLink>
        </td>
         <td align ="right" >
                <asp:HyperLink ID="HyperLink1" runat="server" CssClass="button"  Height="25px" Width="50px"
                    NavigateUrl="~/FillTaxConfirm.aspx" TabIndex="5">Next</asp:HyperLink>
            </td>
        </tr>
        <tr><td>&nbsp;</td></tr>
    </table>
    <div align="center" class="box">
    <asp:GridView ID="GrdResedentDetails" runat="server" AlternatingRowStyle-CssClass="alt"
        AutoGenerateColumns="False"
        PagerStyle-CssClass="pgr" Width="100%" 
            onrowcommand="GrdResedentDetails_RowCommand" HorizontalAlign="Center"  
            AllowPaging="True" onpageindexchanging="GrdResedentDetails_PageIndexChanging" 
            CssClass="mGrid">
<AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
<Columns>

<asp:TemplateField HeaderText="S.No.">
<ItemTemplate><%#Container.DataItemIndex+1 %><br />
</ItemTemplate>
</asp:TemplateField>
    
    <asp:BoundField DataField ="TaxPayerResidencyDetailsID"   Visible="false"/>      
<asp:BoundField DataField="CityName" HeaderText="City" SortExpression="EID"  />
<asp:BoundField DataField="StateName" HeaderText="State" />
<asp:BoundField DataField="StartDate" HeaderText="Start Date" 
        DataFormatString="{0:d}" HtmlEncode="False" />
<asp:BoundField DataField="EndDate" HeaderText="End Date" DataFormatString="{0:d}" 
        HtmlEncode="False"/>

<asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="lnkbtnEdit" runat="server" Text="Edit" CommandArgument='<%# DataBinder.Eval(Container,"DataItem.TaxPayerResidencyDetailsID") %>'
                        CommandName="EditROW" ></asp:LinkButton>
                       
                </ItemTemplate>
            </asp:TemplateField>
            
</Columns>

<PagerStyle CssClass="pgr"></PagerStyle>
</asp:GridView>
    </div>
    </div>
    </asp:Content>

