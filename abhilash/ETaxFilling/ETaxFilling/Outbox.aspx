﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Client.master" AutoEventWireup="true" CodeBehind="Outbox.aspx.cs" Inherits="ETaxFilling.Outbox" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding-right:10px;" id="TABLE1" class="box">
  <tr>
    <td>
    <h2 class="divider top20 bottom10">
    <asp:Label ID="lblTitle" runat="server" Text="Outbox" Width="100%" CssClass="divider top20 bottom10"></asp:Label></h2></td>
  </tr>
  <tr>
  <td class="error"><asp:Label id="lblMessage" runat="server" cssclass="error"></asp:Label></td>
  </tr>
  <tr>
    <td class="content" align="center">
    <table width="100%" border="0" cellpadding="0" cellspacing="8">
	  <tr>
        <td style="width:100%; text-align:right;">
		<asp:GridView ID="gvOutbox" runat="server" AllowPaging="True" AlternatingRowStyle-CssClass="alt"
        AutoGenerateColumns="False" CssClass="mGrid" GridLines="None" 
                PagerStyle-CssClass="pgr" onrowcommand="gvOutbox_RowCommand" 
                onpageindexchanging="gvOutbox_PageIndexChanging" >
<AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
        <Columns>
            <asp:TemplateField HeaderText="S.No.">
                <ItemTemplate>
                    <%#Container.DataItemIndex + 1%>
                </ItemTemplate>
            </asp:TemplateField>
            <%--<asp:BoundField DataField="From_Memberid" HeaderText="From_id" /--%>
            <asp:BoundField DataField="Emailid" HeaderText="To Email ID" />
            <asp:BoundField DataField="Subject" HeaderText="Subject" />
            <asp:BoundField DataField="Date" HeaderText="Date" />
            <asp:BoundField DataField="Isread" HeaderText="Is Read" />
            <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkView" runat="server" Text="View" CommandName="view" CommandArgument='<%# DataBinder.Eval(Container,"DataItem.ID") %>'>Show Mail</asp:LinkButton>
                                 <asp:Label ID="ID" Visible="false" runat="server" Text='<%# DataBinder.Eval (Container.DataItem, "ID") %>'></asp:Label>
                                <asp:Label ID="status" Visible="false" Text='<%# DataBinder.Eval (Container.DataItem, "Isread") %>'
                                    runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
            
        </Columns>

<PagerStyle CssClass="pgr"></PagerStyle>
    </asp:GridView>
	</td>
      </tr>
	  </table></td>
  </tr>
</table>
</asp:Content>
