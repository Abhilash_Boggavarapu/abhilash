﻿<%@ Page Language="C#" MasterPageFile="~/Client.Master" AutoEventWireup="true" CodeBehind="~/FillTaxConfirm.aspx.cs" Inherits="ETaxFilling.FillTaxConfirm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div >
<asp:Panel ID="Panel1" runat="server" Width="694px">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
         <tr>
            <td align="center">
                &nbsp;</td>  
                <td align="center">
                    <asp:Button Text="Confirm" ID="btnConfirm"  OnClientClick ='return confirm ("Are you sure You want to confirm?") ' OnClick="btnConfirm_Click" CssClass="button" runat="server" />
                </td>
                 <td align="center">
                &nbsp;</td>  
         </tr>
       </table>
   <table width="100%" border="0" cellpadding="0" cellspacing="0">
     <tr>
       <td>
          <h2 class="divider top20 bottom10">
             Taxpayer Employer Details </h2>
        </td>
        <td align="center">                            
             <asp:Button ID="btnEditEmpDetails" runat="server" Text="Edit" 
                 CssClass="button" onclick="btnEditEmpDetails_Click" Width="90px" Height="31px" />                             
           </td>
          </tr>
            <tr>
              <td colspan="2">
                <asp:Label Text="" CssClass="error" ID="lblEmpmsg" runat="server" /> 
              </td>
            </tr>
   </table>
    
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <asp:GridView ID="GrdEmployeeDetails" runat="server" 
        AllowPaging="False" AlternatingRowStyle-CssClass="alt"
AutoGenerateColumns="False"  GridLines="Vertical"
        PagerStyle-CssClass="pgr" Width="100%" CssClass="mGrid" 
           >
<AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
<Columns>
<asp:TemplateField HeaderText="S.No.">
<ItemTemplate><%#Container.DataItemIndex+1 %><br />
</ItemTemplate>
</asp:TemplateField>
   
    <asp:BoundField DataField ="TaxPayerEmployerID"  Visible="false"/>      
<asp:BoundField DataField="EmployerName" HeaderText="Employer Name" SortExpression="EID"  />
<asp:BoundField DataField="EmploymentStartDate" HeaderText="Employment Start Date"  DataFormatString="{0:d}" HtmlEncode="False"/>
<asp:BoundField DataField="EmploymentEndDate" HeaderText="Employment End Date"  DataFormatString="{0:d}" HtmlEncode="False" />
<asp:BoundField DataField="CountryName" HeaderText="Country"/>
<asp:BoundField  DataField="StateName"  HeaderText="State" />
<asp:BoundField  DataField="City"  HeaderText="City" />

</Columns>

<PagerStyle CssClass="pgr"></PagerStyle>
</asp:GridView>
    </table>
</asp:Panel>
    <br />
<asp:Panel ID="PanelClientDetails" runat="server">
                         <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="style9">
                                    <h2 class="divider top20 bottom10">
                                        Taxpayer Client Details    </h2>
                                </td>
                                 <td  align="center">
                                      <asp:Button ID="btnEditClientDetails" runat="server" Text="Edit" 
                                         CssClass="button" OnClick="btnEditClientDetails_Click"  
                                         Width="90px" Height="31px" />
                                </td>
                            </tr>    
                             <tr>
                                <td colspan="2">
                                   <asp:Label Text="" CssClass="error" ID="lblClientmsg" runat="server" />
                                </td>
                                </tr>
                                </table>       
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <asp:GridView ID="GrdClientlDetails" runat="server" 
        AllowPaging="False" AlternatingRowStyle-CssClass="alt"
AutoGenerateColumns="False"  GridLines="Vertical"
        PagerStyle-CssClass="pgr" Width="100%" 
            HorizontalAlign="Center" CssClass="mGrid">
<Columns>

<asp:TemplateField HeaderText="S.No.">
<ItemTemplate><%#Container.DataItemIndex+1 %><br />
</ItemTemplate>
</asp:TemplateField>
    
    <asp:BoundField DataField ="TaxPayerClientID"   Visible="false"/>      
<asp:BoundField DataField="ClientName" HeaderText="Client Name" SortExpression="EID"  />
<asp:BoundField DataField="ProjectStartDate" HeaderText="Project Start Date"  DataFormatString="{0:d}" HtmlEncode="False" />
<asp:BoundField DataField="ProjectEndDate" HeaderText="Project End Date"  DataFormatString="{0:d}" HtmlEncode="False" />
<asp:BoundField DataField="CountryName" HeaderText="Country"/>
<asp:BoundField  DataField="StateName"  HeaderText="State" />
<asp:BoundField  DataField="CityName"  HeaderText="City" />
</Columns>
</asp:GridView>
</table>
        </asp:Panel>

<br />

<asp:Panel ID="PanelResidentialDetails" runat="server">

 <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <h2 class="divider top20 bottom10">
                                         Taxpayer Residency Details</h2>
                                </td>
                                 <td  align="center">
                                    <asp:Button ID="btnEditResDetails" runat="server" Text="Edit" 
                                         CssClass="button" OnClick="btnEditResDetails_Click" 
                                         Width="90px" Height="31px" />
                                </td>
                            </tr>    
                             <tr>
                                <td colspan="2">
                                   <asp:Label CssClass="error" Text="" ID="lblResmsg" runat="server" />
                                </td>
                                </tr>
                                </table>       
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<asp:GridView ID="GrdResedentDetails" runat="server" 
        AllowPaging="False" AlternatingRowStyle-CssClass="alt"
AutoGenerateColumns="False"  GridLines="Vertical"
        PagerStyle-CssClass="pgr" Width="100%" CssClass="mGrid">
<Columns>

<asp:TemplateField HeaderText="S.No.">
<ItemTemplate><%#Container.DataItemIndex+1 %><br />
</ItemTemplate>
</asp:TemplateField>
    
    <asp:BoundField DataField ="TaxPayerResidencyDetailsID"   Visible="false"/>      
<asp:BoundField DataField="CityName" HeaderText="City Name" SortExpression="EID"  />
<asp:BoundField DataField="StateName" HeaderText="State Name"  />
<asp:BoundField DataField="StartDate" HeaderText="Start Date"  DataFormatString="{0:d}" HtmlEncode="False" />
<asp:BoundField DataField="EndDate" HeaderText="End Date"  DataFormatString="{0:d}" HtmlEncode="False"/>
          
</Columns>
</asp:GridView>
</table> 
</asp:Panel>
<asp:Panel ID="PanelTaxInfoSpouseDetails" runat="server">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="style6">
                                    <h2 class="divider top20 bottom10">
                                        Spouse Employer Details</h2>
                                </td>
                                 <td  align="center">
                                    <asp:Button ID="btnEditSpouseEmpDetails" runat="server" Text="Edit" 
                                         CssClass="button" OnClick="btnEditSpouseEmpDetails_Click" 
                                         Width="90px" Height="31px" />
                                </td>
                            </tr>    
                             <tr>
                                <td colspan="2">
                                   <asp:Label Text="" CssClass="error" ID="lblSpouseEmpmsg" runat="server" />
                                </td>
                                </tr>
                                </table>       
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<asp:GridView ID="GrdSpouseEmpDetails" runat="server" 
        AllowPaging="False" AlternatingRowStyle-CssClass="alt"
AutoGenerateColumns="False"  GridLines="Vertical"
        PagerStyle-CssClass="pgr" Width="100%" CssClass="mGrid">
<AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
<Columns>
<asp:TemplateField HeaderText="S.No.">
<ItemTemplate><%#Container.DataItemIndex+1 %><br />
</ItemTemplate>
</asp:TemplateField>
   
    <asp:BoundField DataField ="TaxPayerEmployerID"  Visible="false"/>      
<asp:BoundField DataField="EmployerName" HeaderText="Employer Name" SortExpression="EID"  />
<asp:BoundField DataField="EmploymentStartDate" HeaderText="Employment Start Date"  DataFormatString="{0:d}" HtmlEncode="False" />
<asp:BoundField DataField="EmploymentEndDate" HeaderText="Employment End Date"  DataFormatString="{0:d}" HtmlEncode="False"/>
<asp:BoundField DataField="CountryName" HeaderText="Country"/>
<asp:BoundField  DataField="StateName"  HeaderText="State" />
<asp:BoundField  DataField="City"  HeaderText="City" />

</Columns>

<PagerStyle CssClass="pgr"></PagerStyle>
</asp:GridView>  
</table>
</asp:Panel>
<asp:Panel ID="PanelSpouseClientDetails" runat="server">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <h2 class="divider top20 bottom10">
                                         Spouse Client Details</h2>
                                </td>
                                 <td  align="center">
                                    <asp:Button ID="btnSpouseClientDetails" runat="server" Text="Edit" 
                                         CssClass="button" OnClick="btnSpouseClientDetails_Click" 
                                         Width="90px" Height="31px" />
                                </td>
                            </tr>    
                             <tr>
                                <td>
                                   <asp:Label Text="" CssClass="error" ID="lblSpouseClientmsg" runat="server" />
                                </td>
                                </tr>
                                </table>

  <asp:GridView ID="GrdSpouseClientlDetails" runat="server" AllowPaging="False" 
            AlternatingRowStyle-CssClass="alt" AutoGenerateColumns="False" 
            GridLines="Vertical" PagerStyle-CssClass="pgr" 
            Width="100%" CssClass="mGrid">
            <Columns>
           <asp:TemplateField HeaderText="S.No.">
          <ItemTemplate><%#Container.DataItemIndex+1 %><br />
           </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="TaxPayerClientID" Visible="false" />
                <asp:BoundField DataField="ClientName" HeaderText="Client Name" 
                    SortExpression="EID" />
                <asp:BoundField DataField="ProjectStartDate" HeaderText="Project Start Date"   DataFormatString="{0:d}" HtmlEncode="False"/>
                <asp:BoundField DataField="ProjectEndDate" HeaderText="Project End Date"   DataFormatString="{0:d}" HtmlEncode="False"/>
                <asp:BoundField DataField="CountryName" HeaderText="Country" />
                <asp:BoundField DataField="StateName" HeaderText="State" />
                <asp:BoundField DataField="CityName" HeaderText="City" />
            </Columns>
        </asp:GridView>
     
    
        </asp:Panel>
        <asp:Panel ID="PanelSpouseDetails" runat="server">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <h2 class="divider top20 bottom10">
                                         Spouse Residency Details</h2>
                                </td>
                                 <td  align="center">
                                    <asp:Button ID="btnEditSpouseResidencyDetails" runat="server" Text="Edit" 
                                         CssClass="button" OnClick="btnEditSpouseDetails_Click" 
                                         Width="90px" Height="31px" />
                                </td>
                            </tr>    
                             <tr>
                                <td colspan="2">
                                   <asp:Label Text="" CssClass="error" ID="lblSouseResmsg" runat="server" />
                                </td>
                                </tr>
                                </table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<asp:GridView ID="GrdSpouseResidencyDetails" runat="server" 
        AllowPaging="False" AlternatingRowStyle-CssClass="alt"
AutoGenerateColumns="False"  GridLines="Vertical"
        PagerStyle-CssClass="pgr" Width="100%" CssClass="mGrid" 
           >
<Columns>

<asp:TemplateField HeaderText="S.No.">
<ItemTemplate><%#Container.DataItemIndex+1 %><br />
</ItemTemplate>
</asp:TemplateField>
    
    <asp:BoundField DataField ="TaxPayerResidencyDetailsID"   Visible="false"/>      
<asp:BoundField DataField="CityName" HeaderText="City Name" SortExpression="EID"  />
<asp:BoundField DataField="StateName" HeaderText="State Name" />
<asp:BoundField DataField="StartDate" HeaderText="Start Date"  DataFormatString="{0:d}" HtmlEncode="False" />
<asp:BoundField DataField="EndDate" HeaderText="End Date"  DataFormatString="{0:d}" HtmlEncode="False"/>          
</Columns>
</asp:GridView>
</table>
</asp:Panel>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
<td>
    <asp:label text="" id="lblmsg" CssClass="error" runat="server" />
</td>
<td align="center"  >             
    <asp:Button Text="Confirm" ID="btnConfirmDown"  OnClientClick ='return confirm ("Are you sure You want to confirm?") ' 
     CssClass="button" runat="server" Height="27px" onclick="btnConfirm_Click" />
                    </td>
                    <td>
                     <asp:Button runat="server" ID="btnNext" Text="Next"  OnClick="btnNext_Click"
                      CssClass="button" Height="30px" />
                    </td>
            </tr></table>
  </div>
 </asp:Content>