﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ETaxDal;
using ETaxBO;

namespace ETaxFilling
{
    public partial class ReferFriend : System.Web.UI.Page
    {
        long UserId;
        ReferralDal objDReferal = new ReferralDal();
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMsg.Text = string.Empty;
            if (!Utility.Utility.checkLoginSession())
            {
                Response.Redirect("userlogin.aspx");
            } 
            UserId = Utility.Utility.getLoginSession().UserID;
            if (!IsPostBack)
            {            
               Panel1.Visible = false;
               Display();
            }
        }

        protected void btnAddReferal_Click(object sender, EventArgs e)
        {
            Panel1.Visible = true;
            btnAddReferal.Visible = false;
        }

        protected void btnSaveReferal_Click(object sender, EventArgs e)
        {
           
            if (Validations.IsAlpha(txtReferalName.Text) && txtReferalName.Text !=string.Empty)
            {
                divRFerr.InnerText = Utility.Utility.ValReferralName;
                txtReferalName.Text = string.Empty;
                txtReferalName.Focus();
            }
           
            else if (!txtEmailId.Text.IsValidEmailID() && txtEmailId.Text !=string.Empty)
            {
                divRFerr.InnerText = Utility.Utility.ValNEmailId;
                txtEmailId.Text = string.Empty;
                txtEmailId.Focus();
            }
            
            else if (!Validations.IsNumeric(txtMobile.Text) && txtMobile.Text !=string.Empty)
            {

                divRFerr.InnerText = Utility.Utility.ValMobileNo;
                txtMobile.Text = string.Empty;
                txtMobile.Focus();
            }
            else if (!Validations.IsNumeric(txtWork.Text) && txtWork.Text !=string.Empty)
            {
                divRFerr.InnerText = Utility.Utility.ValWorkNo;
                txtWork.Text = string.Empty;
                txtWork.Focus();
            }
            else if (txtEmailId.Text == string.Empty && txtMobile.Text == string.Empty && txtReferalName.Text == string.Empty && txtWork.Text == string.Empty)
            {
                divRFerr.InnerText = Utility.Utility.ReqAnyField;
                txtReferalName.Text = string.Empty;
                txtReferalName.Focus();
            }
            else
            {
                divRFerr.InnerText = string.Empty;
                BO_tblUserReferral objReferal = new BO_tblUserReferral();
                int ReferralTypeID = Convert.ToInt32(MasterItems.ReferralTypes.ReferredfromClientLogin);
                objReferal.UserID = UserId;
                objReferal.ReferralTypeID = ReferralTypeID;
                objReferal.ReferralName = txtReferalName.Text;
                objReferal.EmailID = txtEmailId.Text;
                objReferal.Phone1 = txtMobile.Text;
                objReferal.Phone2 = txtWork.Text;
                objDReferal.SaveReferralDetails(objReferal);
                Display();
                lblMsg.Text = "Referral Information Successfully Saved";
                lblMsg.ForeColor = System.Drawing.Color.Green;
                txtEmailId.Text = txtMobile.Text = txtReferalName.Text = txtWork.Text = string.Empty;
                Panel1.Visible = false;
                btnAddReferal.Visible = true;
            }
        }
        private void Display()
        {
         grdReferal.DataSource = objDReferal.GetReferralDetails(UserId);
         grdReferal.DataBind();
        }

        protected void grdReferal_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdReferal.PageIndex = e.NewPageIndex;
            Display();
            lblMsg.Text = string.Empty;
        }
    }
}