﻿<%@ Page Language="C#" MasterPageFile="~/Client.Master" AutoEventWireup="true" CodeBehind="DownloadTaxReturnDocument.aspx.cs" Inherits="ETaxFilling.DownloadTaxReturnDocument" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="box">
<tr><td>
     <h2 class="divider top20 bottom10">Download Your Tax Return Documents :</h2>
    <asp:Label Text="" ID="lblStuas" runat="server" CssClass="error" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td class="box" align="center">

   <asp:GridView ID="GdVDownloadDocuments" runat="server" AutoGenerateColumns="False"
     Height="75px" Width="100%" OnRowCommand="GdVDownloadDocuments_RowCommand" >
        <Columns>
<asp:TemplateField HeaderText="S.No.">
<ItemTemplate><%#Container.DataItemIndex+1 %><br />
</ItemTemplate>
</asp:TemplateField>
        
         <asp:BoundField DataField ="TaxSheetID"   Visible="false"/> 
      <asp:BoundField DataField="SheetName" HeaderText="Document Name" />
      <asp:BoundField DataField="CreatedOn" HeaderText="Uploaded Date&Time" />
        <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
         <asp:TemplateField>
         <ItemTemplate>
         <asp:LinkButton ID="BtnDownload" runat ="server" CommandName="Download" CommandArgument='<%# DataBinder.Eval(Container,"DataItem.TaxSheetID") %>' Text ="Download" ></asp:LinkButton>
         </ItemTemplate>
         </asp:TemplateField>
        </Columns>
    </asp:GridView></td>
        </tr>
    </table>
    </td></tr></table>
</asp:Content>