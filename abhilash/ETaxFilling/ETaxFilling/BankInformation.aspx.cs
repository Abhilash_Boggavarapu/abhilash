﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ETaxBO;
using ETaxDal;
using ETaxFilling.Utility;

namespace ETaxFilling
{
    public partial class BankInformation1 : System.Web.UI.Page
    {
        BankInformDAL objDBank = new BankInformDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            lblDisplay.Text = string.Empty;
            if (!Utility.Utility.checkLoginSession())
            {
                Response.Redirect("userlogin.aspx");
            }            
            if (!IsPostBack)
            {
                bindData();
            }
        }

        protected void btnCDetails_Click(object sender, EventArgs e)
        {
           if (string.IsNullOrEmpty(txtAcNo.Text))
            {
                divBIerr.InnerText = Utility.Utility.ReqBDetails;
            }
            else if (!txtAcNo.Text.IsNumeric())
            {
                divBIerr.InnerText = Utility.Utility.ValBDetails;
                txtAcNo.Text = string.Empty;
                txtAcNo.Focus();
            }
            else if (string.IsNullOrEmpty(txtRoutingNo.Text))
            {
                divBIerr.InnerText = Utility.Utility.ReqRouting;
            }
            else if (!txtRoutingNo.Text.IsNumeric())
            {
                divBIerr.InnerText = Utility.Utility.ValRouting;
                txtRoutingNo.Text = string.Empty;
                txtRoutingNo.Focus();
            }
            else if (string.IsNullOrEmpty(txtBName.Text))
            {
                divBIerr.InnerText = Utility.Utility.ReqBName;
            }
            else if (txtBName.Text.IsAlpha())
            {
                divBIerr.InnerText = Utility.Utility.ValBName;
                txtBName.Text = string.Empty;
                txtBName.Focus();
            }
            else if (string.IsNullOrEmpty(txtAName.Text))
            {
                divBIerr.InnerText = Utility.Utility.ReqAName;
            }
            else if (txtAName.Text.IsAlpha())
            {
                divBIerr.InnerText = Utility.Utility.ValAName;
                txtAName.Text = string.Empty;
                txtAName.Focus();
            }
            else if (string.IsNullOrEmpty(ddlAType.Text))
            {
                divBIerr.InnerText = Utility.Utility.ReqAType;
            }
            else
            {
                divBIerr.InnerText = string.Empty;
                BO_tblTaxPayerBankDetail objTaxPayer = new BO_tblTaxPayerBankDetail();
                objTaxPayer.TaxPayerID = Utility.Utility.getLoginSession().TaxPayerID;
                objTaxPayer.BankName = txtBName.Text;
                objTaxPayer.AccountHolderName = txtAName.Text;
                objTaxPayer.AccountNo = Convert.ToInt64(txtAcNo.Text);
                objTaxPayer.RoutingNo = txtRoutingNo.Text;
                objTaxPayer.AccountHolderName = txtAName.Text;
                objTaxPayer.AccounTtype = (ddlAType.SelectedValue).ToString();
                objDBank.InsertUpdateBankInformation(objTaxPayer);
                lblStatus.Text = "Bank Information Saved Sucessfully";
                lblStatus.ForeColor = System.Drawing.Color.Green;
                bindData();
                Utility.Clear.ClearDependents(this.Page);
            }
        }
        private void bindData()
        {
            GrdBank.DataSource = objDBank.GetBankDetails(Utility.Utility.getLoginSession().TaxPayerID);
            GrdBank.DataBind();
        }
      
        protected void GrdBank_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GrdBank.PageIndex = e.NewPageIndex;
            bindData();
        }
    }
}