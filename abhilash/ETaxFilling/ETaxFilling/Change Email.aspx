﻿<%@ Page Language="C#" MasterPageFile="~/Client.master"  AutoEventWireup="true" CodeBehind="Change Email.aspx.cs" Inherits="ETaxFilling.Change_Email" %>

  <asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      <div> 
   <table align="center" cellpadding="0" cellspacing="8" border="0" width="100%" class="box">
        <tr class="box">
        <td colspan="3">
        <h2 class="divider top20 bottom10">Change Email ID</h2>
         <div id="divCEmailerr" style="color:#FF0000" runat="server"></div>
        </td>
        </tr>
                            <tr>
                                <td align="right" style=" width:40%;"><span style="color:red;">*</span>
                                  Email ID : 
                                </td>
                                <td>
                                 
                                    <asp:TextBox ID="txtEmailId" runat="server" CssClass="ValidationRequired email" 
                                        Width="179px" MaxLength="50" Height="25px" ReadOnly="true" TabIndex="1"></asp:TextBox>
                                  </td>
                            </tr>
                            <tr>
                                <td align="right"><span style="color:red;">*</span>
                                 New Email ID : 
                                </td>
                                <td>
                                   
                                   

                                    <asp:TextBox ID="txtNEmailId" runat="server" AutoPostBack="true"
                                       Width="179px" 
                                        ontextchanged="TxtNEmailId_TextChanged" 
                                        CssClass="ValidationRequired email" Height="25px" TabIndex="2"></asp:TextBox>
                                    <td align="left"><asp:Label ID="lblmsgNEmail" runat="server" BackColor = "White"
                                     EnableViewState = "false" ForeColor = "Black"></asp:Label></td>
                                             </td>
                                    <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="right"><span style="color:red;">*</span>
                                    Confirm Email ID : 
                                   
                                </td>
                                <td>
                                   
                                    <asp:TextBox ID="txtCEmailId" runat="server" 
                                        CssClass="ValidationRequired email" Width="179px" MaxLength="50" 
                                        Height="25px" TabIndex="3"></asp:TextBox>
                                   </td>
                            </tr>
                            <tr>
                                <td >
                                   </td>
                                <td>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:Button ID="btnSave" runat="server" onclick="BtnSave_Click" Text="Update" 
                                        Cssclass="button" Height="29px" Width="94px" TabIndex="4" />
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    <asp:HyperLink ID="HyperLink1" CssClass="button" runat="server" 
                                        NavigateUrl="~/ChangeSettings.aspx" TabIndex="5">Back</asp:HyperLink>
                                   </td>
                                <td>
                                  </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center">
                                  <asp:Label ID="lblMail" runat="server" CssClass="error" ForeColor="Black" TabIndex="6"></asp:Label>
                                    
                                </td>
                            </tr>
                        </table>
         
    </div>
</asp:Content>