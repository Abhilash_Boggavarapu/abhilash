﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ETaxBO;

namespace ETaxDal
{
    public class AssetsDAL
    {
        public int InserUpdateAssets(BO_tblAssetType pobjAssets)
        {
            try
            {
                using (ETaxEntities objTaxEntities = new ETaxEntities())
                {
                    var existAsset = from A in objTaxEntities.tblAssetTypes
                                     where A.AssetTypeID == pobjAssets.AssetTypeID
                                     select A;
                    if (existAsset.Any())
                    {
                        var Asset = existAsset.FirstOrDefault();
                        Asset.AssetName = pobjAssets.AssetName;
                    }
                    else
                    {
                        tblAssetType objAssets = new tblAssetType();
                        objAssets.AssetName = pobjAssets.AssetName;
                        objAssets.CreatedOn = DateTime.Now;
                        objTaxEntities.tblAssetTypes.AddObject(objAssets);

                    }
                    objTaxEntities.SaveChanges();

                    return 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public List<BO_tblAssetType> GetAssetTypesDetails()
        {
            try
            {
                using (ETaxEntities objETaxEntities = new ETaxEntities())
                {
                    objETaxEntities.Connection.Open();
                    List<BO_tblAssetType> objAssetDetails = (from n in objETaxEntities.tblAssetTypes

                                                             select new BO_tblAssetType

                                                             {

                                                                 AssetTypeID = n.AssetTypeID,
                                                                 AssetName = n.AssetName,

                                                             }).ToList();

                    return objAssetDetails;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public BO_tblAssetType GetAssetTypeDetails(int AssetTypeID)
        {
            try
            {
                using (ETaxEntities objETaxEntities = new ETaxEntities())
                {
                    objETaxEntities.Connection.Open();
                    BO_tblAssetType objAssetDetails = (from n in objETaxEntities.tblAssetTypes
                                                       where n.AssetTypeID == AssetTypeID


                                                       select new BO_tblAssetType

                                                       {

                                                           AssetTypeID = n.AssetTypeID,
                                                           AssetName = n.AssetName,

                                                       }).SingleOrDefault();

                    return objAssetDetails;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

      