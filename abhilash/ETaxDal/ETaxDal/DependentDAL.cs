﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ETaxBO;

namespace ETaxDal
{
    public class DependentDAL
    {
        public void InsertUpdateDependent(BO_tblTaxPayerDependent pobjDependent)
        {
            try
            {

                using (ETaxEntities objTaxEntities = new ETaxEntities())
                {
                    var existDependent = from s in objTaxEntities.tblTaxPayerDependents
                                         where s.TaxPayerDependentID == pobjDependent.TaxPayerDependentID
                                         select s;
                    if (existDependent.Any())
                    {
                        var Dependent = existDependent.FirstOrDefault();
                        Dependent.TaxPayerID = pobjDependent.TaxPayerID;
                        Dependent.FirstName = pobjDependent.FirstName;
                        Dependent.MiddleName = pobjDependent.MiddleName;
                        Dependent.LastName = pobjDependent.LastName;
                        Dependent.DOB = pobjDependent.DOB;
                        Dependent.RelationShipID = pobjDependent.RelationShipID;
                        Dependent.SSN = pobjDependent.SSN;
                        if (pobjDependent.VisaStatusID != 0)
                        {
                            Dependent.VisaStatusID = pobjDependent.VisaStatusID;
                        }
                        Dependent.USEntryDate = pobjDependent.USEntryDate;

                    }
                    else
                    {

                        tblTaxPayerDependent objDependent = new tblTaxPayerDependent();

                        objDependent.TaxPayerID = pobjDependent.TaxPayerID;
                        objDependent.FirstName = pobjDependent.FirstName;
                        objDependent.MiddleName = pobjDependent.MiddleName;
                        objDependent.LastName = pobjDependent.LastName;
                        objDependent.DOB = System.DateTime.Now;
                        objDependent.RelationShipID = pobjDependent.RelationShipID;
                        objDependent.SSN = pobjDependent.SSN;
                        if (pobjDependent.VisaStatusID != 0)
                        {
                            objDependent.VisaStatusID = pobjDependent.VisaStatusID;
                        }
                        objDependent.USEntryDate = System.DateTime.Now;
                        objTaxEntities.tblTaxPayerDependents.AddObject(objDependent);
                        objDependent.CreatedOn = System.DateTime.Now;
                    }
                    objTaxEntities.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void DeleteDependent(BO_tblTaxPayerDependent pobjDependent)
        {
            using (ETaxEntities objTaxEntities = new ETaxEntities())
            {
                var Dependent = from s in objTaxEntities.tblTaxPayerDependents
                                where s.TaxPayerDependentID == pobjDependent.TaxPayerDependentID
                                select s;
                if (Dependent.Any())
                {
                    objTaxEntities.tblTaxPayerDependents.DeleteObject(Dependent.FirstOrDefault());
                }
            }
        }

        public long TaxPayerDependentId(long TaxPayerID)
        {
            try
            {
                using (ETaxEntities objETaxEntites = new ETaxEntities())
                {
                    objETaxEntites.Connection.Open();
                    long TaxPayerDependentId = (from n in objETaxEntites.tblTaxPayerDependents
                                                where n.tblTaxPayerDetail.TaxPayerID == TaxPayerID
                                                select n.TaxPayerDependentID).SingleOrDefault();
                    return TaxPayerDependentId;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<BO_tblTaxPayerDependent> GetTaxPayerDependents(long taxPayerId)
        {
            try
            {
                using (ETaxEntities objETaxEntites = new ETaxEntities())
                {
                    objETaxEntites.Connection.Open();
                    List<BO_tblTaxPayerDependent> objTaxpayDependent = (from n in objETaxEntites.tblTaxPayerDependents
                                                                        where n.TaxPayerID == taxPayerId
                                                                        select new BO_tblTaxPayerDependent
                                                                        {TaxPayerDependentID =n.TaxPayerDependentID ,
                                                                            FirstName = n.FirstName,
                                                                            MiddleName = n.MiddleName,
                                                                            LastName = n.LastName,
                                                                            DOB = n.DOB,
                                                                            Relationship = n.tblRelationship.RelationShip,
                                                                            SSN = n.SSN,
                                                                            USEntryDate = n.USEntryDate,
                                                                            CreatedOn = n.CreatedOn,
                                                                         VisaStatusID =n.VisaStatusID,
                                                                         VisaStatus=n.tblVisaStatus.VisaStatus
                                                                         //select all values which are required for to bind the relevent page textboxes
                                                                           }).ToList();
                    return objTaxpayDependent;
                }
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }
        public BO_tblTaxPayerDependent GetTaxPayerDependent(long taxPayerDependentID)
        {
            try
            {
                using (ETaxEntities objETaxEntites = new ETaxEntities())
                {
                    objETaxEntites.Connection.Open();
                   BO_tblTaxPayerDependent objTaxpayDependent = (from n in objETaxEntites.tblTaxPayerDependents
                                                                 where n.TaxPayerDependentID == taxPayerDependentID
                                                                        select new BO_tblTaxPayerDependent
                                                                        {
                                                                            TaxPayerDependentID = n.TaxPayerDependentID,
                                                                            FirstName = n.FirstName,
                                                                            MiddleName = n.MiddleName,
                                                                            LastName = n.LastName,
                                                                            DOB = n.DOB,
                                                                            RelationShipID=n.RelationShipID,
                                                                            Relationship = n.tblRelationship.RelationShip,
                                                                            SSN = n.SSN,
                                                                            USEntryDate = n.USEntryDate,
                                                                            CreatedOn = n.CreatedOn,
                                                                            
                                                                            VisaStatusID = n.VisaStatusID,
                                                                            VisaStatus=n.tblVisaStatus.VisaStatus,
                                                                            
                                                                            //select all values which are required for to bind the relevent page textboxes
                                                                        }).SingleOrDefault();
                    return objTaxpayDependent;
                }
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }
    }
}

