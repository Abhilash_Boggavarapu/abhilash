﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ETaxBO;

namespace ETaxDal
{
   public  class RoleDAL
    {
       public int SaveRole(BO_tblRole pobjRole)
       {
             try
             {
                 using (ETaxEntities objTaxEntities = new ETaxEntities())
                 {
                     tblRole objRole = new tblRole();
                     objRole.RoleName = pobjRole.RoleName;
                     objRole.RoleDesc = pobjRole.RoleDesc;
                     objRole.CreatedOn = DateTime.Now;
                     objTaxEntities.tblRoles.AddObject(objRole);
                     objTaxEntities.SaveChanges();
                     return 1;
                 }

             }
             catch (Exception ex)
             {
                 throw ex;
             }
         }
    }
}
