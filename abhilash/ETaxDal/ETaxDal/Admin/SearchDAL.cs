﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ETaxBO;

namespace ETaxDal.Admin
{
   public  class SearchDAL
    {
        public BO_tblTaxPayerDetail GetTaxPayer(long UserID)
        {
            try
            {                
                using (ETaxEntities objETaxEntites = new ETaxEntities())
                {
                    int Spouse = (int)MasterItems.TaxPayerType.Spouse;
                    objETaxEntites.Connection.Open();

                    BO_tblTaxPayerDetail objTaxPayerObj = (from taxpayer in objETaxEntites.tblTaxPayerDetails
                                                           where taxpayer.tblUser.UserID == UserID
                                                           select new BO_tblTaxPayerDetail
                                                           {
                                                               UserID = taxpayer.UserID,
                                                               EmailId=taxpayer.tblUser.EmailID,
                                                               TaxPayerID = taxpayer.TaxPayerID,
                                                               TaxPayerSpouseID = (from a in objETaxEntites.tblTaxPayerDetails
                                                                                   where a.UserID == taxpayer.UserID &&
                                                                                   a.PayerType == Spouse
                                                                                   select a.TaxPayerID).FirstOrDefault(),
                                                               UserFirstName = taxpayer.tblUser.FirstName,
                                                           }).FirstOrDefault();
                    if (objTaxPayerObj != null)
                        objTaxPayerObj.CurrentTaxPayer = MasterItems.TaxPayerType.self;
                    return objTaxPayerObj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public BO_tblTaxPayerDetail GetTaxPayerDetails(long taxPayerID)
        {
            try
            {
                int Spouse = (int)MasterItems.TaxPayerType.Spouse;
                using (ETaxEntities objETaxEntites = new ETaxEntities())
                {
                    objETaxEntites.Connection.Open();
                    BO_tblTaxPayerDetail objTaxPayerObj = (from n in objETaxEntites.tblTaxPayerDetails
                                                           where n.TaxPayerID == taxPayerID
                                                           select new BO_tblTaxPayerDetail
                                                           {
                                                               TaxPayerSpouseID = (from a in objETaxEntites.tblTaxPayerDetails
                                                                                   where a.UserID == n.UserID &&
                                                                                   a.PayerType == Spouse
                                                                                   select a.TaxPayerID).FirstOrDefault(),
                                                               UserID=n.UserID,
                                                               UserFirstName = n.FirstName,
                                                               EmailId = n.tblUser.EmailID,
                                                               TaxPayerID = n.TaxPayerID

                                                           }).FirstOrDefault();
                    if (objTaxPayerObj != null)
                        objTaxPayerObj.CurrentTaxPayer = MasterItems.TaxPayerType.self;
                    return objTaxPayerObj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
