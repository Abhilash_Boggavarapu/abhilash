﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ETaxBO;

namespace ETaxDal
{
    public class AdminMasterDAL
    {
        public List<BO_tblTaxFillingStatusTypes> getFileStatusTypes()
        {
            try
            {
                using (ETaxEntities objETaxEntites = new ETaxEntities())
                {
                    objETaxEntites.Connection.Open();
                    List<BO_tblTaxFillingStatusTypes> objTaxFillingStatusTypes = (from fileStatus in objETaxEntites.tblTaxFillingStatusTypes
                                                                                  select new BO_tblTaxFillingStatusTypes
                                                                                  {
                                                                                      TaxFilingStatusType = fileStatus.TaxFilingStatusType,
                                                                                      TaxFilingStatusTypeID = fileStatus.TaxFilingStatusTypeID
                                                                                  }).ToList();
                    return objTaxFillingStatusTypes;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<BO_tblTaxPayerDetail> SearchPendingClients(int taxFilingStatusTypeID)
        {
            try
            {
                int currentStatus = 0;
                int payerType = Convert.ToInt32(MasterItems.TaxPayerType.self);

                if (taxFilingStatusTypeID == Convert.ToInt32(MasterItems.TaxFillingStatusTypes.PersonalInformation))
                {
                    currentStatus = Convert.ToInt32(MasterItems.TaxFillingStatusTypes.PersonalInformation);
                }
                else if (taxFilingStatusTypeID == Convert.ToInt32(MasterItems.TaxFillingStatusTypes.FillTaxInformation))
                {
                    currentStatus = Convert.ToInt32(MasterItems.TaxFillingStatusTypes.FillTaxInformation);
                }
                else if (taxFilingStatusTypeID == Convert.ToInt32(MasterItems.TaxFillingStatusTypes.UploadTaxDocuments))
                {
                    currentStatus = Convert.ToInt32(MasterItems.TaxFillingStatusTypes.UploadTaxDocuments);
                }
                else if (taxFilingStatusTypeID == Convert.ToInt32(MasterItems.TaxFillingStatusTypes.PaymentPendingClients))
                {
                    currentStatus = Convert.ToInt32(MasterItems.TaxFillingStatusTypes.PaymentPendingClients);
                }
                else if (taxFilingStatusTypeID == Convert.ToInt32(MasterItems.TaxFillingStatusTypes.ConformationPending))
                {
                    currentStatus = Convert.ToInt32(MasterItems.TaxFillingStatusTypes.ConformationPending);

                }
                else if (taxFilingStatusTypeID == Convert.ToInt32(MasterItems.TaxFillingStatusTypes.TaxPreparationPending))
                {
                    currentStatus = Convert.ToInt32(MasterItems.TaxFillingStatusTypes.TaxPreparationPending);
                }
                using (ETaxEntities objETaxEntites = new ETaxEntities())
                {
                    objETaxEntites.Connection.Open();

                    List<BO_tblTaxPayerDetail> Search = (from taxpayerDetails in objETaxEntites.tblTaxPayerDetails
                                                         where (
                                                         from a in objETaxEntites.TaxPayerTaxFilingStatusDetails
                                                         where taxpayerDetails.TaxPayerID == a.TaxPayerID && taxpayerDetails.PayerType==payerType
                                                         select a.TaxPayerID).Count() == 0
                                                                  ||
                                                               (
                                                         from a in objETaxEntites.TaxPayerTaxFilingStatusDetails
                                                         where taxpayerDetails.TaxPayerID == a.TaxPayerID && a.TaxFilingStatusTypeID == currentStatus
                                                         select a.TaxPayerID).Count() == 0

                                                         select new BO_tblTaxPayerDetail
                                                         {
                                                             TaxPayerID = taxpayerDetails.TaxPayerID,
                                                             UserID = taxpayerDetails.UserID,
                                                             EmailId=taxpayerDetails.tblUser.EmailID,
                                                             FirstName = taxpayerDetails.FirstName,
                                                             LastName = taxpayerDetails.LastName,
                                                             MiddleName = taxpayerDetails.MiddleName,
                                                             DOB = taxpayerDetails.DOB,
                                                             SSN = taxpayerDetails.SSN,
                                                             MaritalStatus = taxpayerDetails.tblMaritalStatus.MaritalStatus,
                                                             Occupation = taxpayerDetails.Occupation,
                                                             StreetNo = taxpayerDetails.StreetNo,
                                                             ApartmentNo = taxpayerDetails.ApartmentNo,
                                                             CityName = taxpayerDetails.tblCity.City,
                                                             StateName = taxpayerDetails.tblState.StateName,
                                                             //   CountryName = n.tblc,
                                                             MobileNumber = taxpayerDetails.MobileNumber,
                                                             HomeNumber = taxpayerDetails.HomeNumber,
                                                             LandlineNumber = taxpayerDetails.LandlineNumber,
                                                             PrimaryEMailID = taxpayerDetails.PrimaryEMailID,
                                                             SecondaryEMailID = taxpayerDetails.SecondaryEMailID,
                                                             VisaStatus = taxpayerDetails.tblVisaStatus.VisaStatus,
                                                             USEntryDate = taxpayerDetails.USEntryDate,
                                                             HasDependants = taxpayerDetails.HasDependants,
                                                             CreatedOn = taxpayerDetails.CreatedOn,
                                                         }).ToList();
                    return Search;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<BO_tblTaxPayerDetail> SearchCompletedClients()
        {
            try{
               using (ETaxEntities objETaxEntites = new ETaxEntities())
                {
                    objETaxEntites.Connection.Open();

                    List<BO_tblTaxPayerDetail> Search = (from taxpayerDetails in objETaxEntites.tblTaxPayerDetails
                                                         where (from a in objETaxEntites.TaxPayerTaxFilingStatusDetails
                                                                where taxpayerDetails.TaxPayerID==a.TaxPayerID && a.TaxFilingStatusTypeID !=1
                                                                && a.TaxFilingStatusTypeID != 2
                                                                && a.TaxFilingStatusTypeID != 3
                                                                select a.TaxPayerID).Count()==0
                                                         select new BO_tblTaxPayerDetail
                                                         {
                                                             TaxPayerID = taxpayerDetails.TaxPayerID,
                                                             UserID = taxpayerDetails.UserID,
                                                             FirstName = taxpayerDetails.FirstName,
                                                             LastName = taxpayerDetails.LastName,
                                                             MiddleName = taxpayerDetails.MiddleName,
                                                             DOB = taxpayerDetails.DOB,
                                                             ApartmentNo = taxpayerDetails.ApartmentNo,
                                                             CityName = taxpayerDetails.tblCity.City,
                                                             StateName = taxpayerDetails.tblState.StateName,
                                                             //   CountryName = n.tblc,
                                                             MobileNumber = taxpayerDetails.MobileNumber,
                                                             HomeNumber = taxpayerDetails.HomeNumber,
                                                             LandlineNumber = taxpayerDetails.LandlineNumber,
                                                             PrimaryEMailID = taxpayerDetails.PrimaryEMailID,
                                                            
                                                         }).ToList();
                    return Search;




               }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        public int SaveOrInsertTaxSummary(BO_TaxSummary pObjTaxSummary)
        {
            try
            {
                using (ETaxEntities objETaxEntites = new ETaxEntities())
                {
                    tblTaxSummary objTaxSummary = (from n in objETaxEntites.tblTaxSummaries
                                                   where n.TaxSummaryID == pObjTaxSummary.TaxSummaryId
                                                   select n).FirstOrDefault();
                    if (objTaxSummary != null)
                    {
                        objTaxSummary.TaxSummaryID = pObjTaxSummary.TaxSummaryId;
                        objTaxSummary.TaxSummary = pObjTaxSummary.TaxSummary;
                        objTaxSummary.TaxPayerID = pObjTaxSummary.TaxPayerID;
                        objTaxSummary.CreatedOn = System.DateTime.Now;
                    }
                    else
                    {
                         objTaxSummary = new tblTaxSummary();
                        objTaxSummary.TaxSummary = pObjTaxSummary.TaxSummary;
                        objTaxSummary.TaxPayerID = pObjTaxSummary.TaxPayerID;
                        objTaxSummary.CreatedOn = System.DateTime.Now;
                        objETaxEntites.tblTaxSummaries.AddObject(objTaxSummary);
                    }
                    objETaxEntites.SaveChanges();
                }
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       
        public   List< BO_TaxSummary> getTaxSummary(long taxpayerID)

        {
             try
            {
                using (ETaxEntities objETaxEntites = new ETaxEntities())
                {

                    List<BO_TaxSummary> objTaxSummary = (from n in objETaxEntites.tblTaxSummaries
                                                         where n.TaxPayerID == taxpayerID
                                                         select new BO_TaxSummary
                                                         {
                                                             TaxSummary = n.TaxSummary,
                                                             CreatedOn=n.CreatedOn
                                                         }).ToList();

                   
                    return objTaxSummary;
                }
                
             }
                 catch(Exception ex)
                {
                     throw ex;
                 
                 }
        }
        public int getTaxSummaryId(long taxpayerID)
        {
            try
            {
                using (ETaxEntities objETaxEntites = new ETaxEntities())
                {
                    int taxSummaryId = (from n in objETaxEntites.tblTaxSummaries
                                        where n.TaxPayerID == taxpayerID
                                        select n.TaxSummaryID).FirstOrDefault();
                    return taxSummaryId;
                }

            }
            catch (Exception ex)
            {
                throw ex;

            }
        }
        public int saveStaffComments(Bo_StaffComments pobjStaffComments)
        {
            try
            {
                using (ETaxEntities objETaxEntites = new ETaxEntities())
                {
                    tblStaaffComment objStaffComments = new tblStaaffComment();
                    objStaffComments.StaffComments = pobjStaffComments.StaffComments;
                    objStaffComments.CreatedOn = System.DateTime.Now;
                    objStaffComments.TaxpayerID = pobjStaffComments.TaxPayerID;
                    objETaxEntites.tblStaaffComments.AddObject(objStaffComments);
                    objETaxEntites.SaveChanges();
                }
                return 1;
            }             
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<Bo_StaffComments> getStaffComments(long taxpayerID)
        {
            try
            {
                using (ETaxEntities objETaxEntites = new ETaxEntities())
                {
                    List<Bo_StaffComments> objStaffComments = (from n in objETaxEntites.tblStaaffComments
                                                               where n.TaxpayerID == taxpayerID
                                                               select new Bo_StaffComments
                                                               {
                                                                   CreatedOn=n.CreatedOn,
                                                                   StaffComments=n.StaffComments,
                                                                   StaffCommentsID=n.StaffCommentsId
                                                               }).ToList();
                    return objStaffComments;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<BO_tblUser> getUserList(DateTime startDate, DateTime endDate)
        {
            try
            {

                using (ETaxEntities objEtaxEntity = new ETaxEntities())
                {
                    objEtaxEntity.Connection.Open();


                    List<BO_tblUser> objUsers = (from n in objEtaxEntity.tblUsers
                                                 where n.CreatedOn >= startDate &&
                                                 n.CreatedOn <= endDate
                                                 select new BO_tblUser
                                                 {
                                                     FirstName = n.FirstName,
                                                     LastName = n.LastName,
                                                     EmailID = n.EmailID,
                                                     Password = n.Password,
                                                     MobileNumber = n.MobileNumber,
                                                     CreatedOn = n.CreatedOn
                                                 }).ToList();
                    return objUsers;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<BO_tblUser> getUserUserCredentials(string FirstName, string EmailID)
        {
            try
            {
                using (ETaxEntities objEtaxEntity = new ETaxEntities())
                {
                    objEtaxEntity.Connection.Open();
                    List<BO_tblUser> objUser = (from n in objEtaxEntity.tblUsers
                                                where n.FirstName == FirstName ||
                                                n.EmailID == EmailID

                                                select new BO_tblUser
                                                {
                                                    FirstName = n.FirstName,
                                                    LastName = n.LastName,
                                                    EmailID = n.EmailID,
                                                    Password = n.Password,
                                                    CreatedOn = n.CreatedOn
                                                }).ToList();
                    return objUser;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
