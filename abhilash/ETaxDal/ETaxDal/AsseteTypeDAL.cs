﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ETaxBO;
namespace ETaxDal
{
    class AsseteTypeDAL
    {
        public List<BO_tblAssetType> GetAssetTypeDetails()
        {
            try
            {
                using (ETaxEntities objETaxEntities = new ETaxEntities())
                {
                    objETaxEntities.Connection.Open();
                    List<BO_tblAssetType> objAssetDetails = (from n in objETaxEntities.tblAssetTypes

                                                             select new BO_tblAssetType

                                                             {

                                                                 AssetTypeID = n.AssetTypeID,
                                                                 AssetName = n.AssetName,

                                                             }).ToList();

                    return objAssetDetails;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}
