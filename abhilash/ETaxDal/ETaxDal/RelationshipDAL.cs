﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ETaxBO;

namespace ETaxDal
{
     public class RelationshipDAL
    {
         public int SaveUpdateRelationship(BO_tblRelationship pobjRelation)
         {
             try
             {
                 using (ETaxEntities objTaxEntities = new ETaxEntities())
                 {
                     var existRelation = from E in objTaxEntities.tblRelationships

                                         where E.RelationShipID == pobjRelation.RelationShipID
                                         select E;
                     if (existRelation.Any())
                     {
                         var Refferal = existRelation.FirstOrDefault();
                         Refferal.RelationShip = pobjRelation.RelationShip;
                     }
                     else
                     {
                         tblRelationship objRel = new tblRelationship();
                         objRel.RelationShip = pobjRelation.RelationShip;
                         objRel.CreatedOn = DateTime.Now;
                         objTaxEntities.tblRelationships.AddObject(objRel);

                     }
                     objTaxEntities.SaveChanges();
                     return 1;
                 }
             }
             catch (Exception ex)
             {
                 throw ex;
             }
         }
         public List<BO_tblRelationship> GetRelationDetails()
         {
             try
             {

                 using (ETaxEntities objEtaxEntity = new ETaxEntities())
                 {

                     List<BO_tblRelationship> objRelationShipDetails = (from n in objEtaxEntity.tblRelationships
                                                                         select new BO_tblRelationship
                                                                         {
                                                                             RelationShipID=n.RelationShipID,
                                                                             RelationShip=n.RelationShip
                                                                         }).ToList();
                     return objRelationShipDetails;
                 }
             }
             catch (Exception ex)
             {
                 throw ex;
             }
         }
         public BO_tblRelationship GetRelationship(int RelationShipID)
         {
             try
             {

                 using (ETaxEntities objEtaxEntity = new ETaxEntities())
                 {

                     BO_tblRelationship objRelation = (from n in objEtaxEntity.tblRelationships
                                                            where n.RelationShipID == RelationShipID
                                                       select new BO_tblRelationship
                                                            {
                                                                RelationShipID = n.RelationShipID,
                                                                RelationShip = n.RelationShip
                                                            }).SingleOrDefault();
                     return objRelation;
                 }
             }
             catch (Exception ex)
             {
                 throw ex;
             }
         }
    }
}
