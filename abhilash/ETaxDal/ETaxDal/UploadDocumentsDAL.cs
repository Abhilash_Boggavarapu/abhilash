﻿// -----------------------------------------------------------------------
// <copyright file="UploadDocuments.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace ETaxDal
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using ETaxBO;
    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class UploadDocumentsDAL
    {

        public int SaveUpdateDocument(BO_tblDocument pobjDocument)
        {
            try
            {
                using (ETaxEntities objTaxEntities = new ETaxEntities())
                {
                    objTaxEntities.Connection.Open();
                    var existDocument = from D in objTaxEntities.tblDocuments
                                        where D.DocumentID == pobjDocument.DocumentID
                                        select D;
                    if (existDocument.Any())
                    {
                        var Document = existDocument.FirstOrDefault();
                        Document.FormName = pobjDocument.FormName;
                    }
                    else
                    {
                        tblDocument objDocument = new tblDocument();
                        objDocument.FormName = pobjDocument.FormName;
                        objDocument.CreatedOn = DateTime.Now;
                        objTaxEntities.tblDocuments.AddObject(objDocument);
                    }
                    objTaxEntities.SaveChanges();
                    return 1;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public BO_tblDocument GetFormName(int DocumentID)
        {
            try
            {
                using (ETaxEntities objETaxEntities = new ETaxEntities())
                {
                    objETaxEntities.Connection.Open();
                    BO_tblDocument Objdoc = (from n in objETaxEntities.tblDocuments
                                             where n.DocumentID == DocumentID
                                             select new BO_tblDocument
                                            {
                                                FormName = n.FormName,
                                            }).SingleOrDefault();
                    return Objdoc;



                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public List<BO_tblDocument> GetFormsName()
        {
            try
            {
                using (ETaxEntities objETaxEntities = new ETaxEntities())
                {
                    objETaxEntities.Connection.Open();
                    List<BO_tblDocument> Objdoc = (from n in objETaxEntities.tblDocuments
                                                   select new BO_tblDocument
                                             {
                                                 DocumentID = n.DocumentID,
                                                 FormName = n.FormName,
                                             }).ToList();
                    return Objdoc;



                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public long GetTaxpayerID(long UserId)
        {
            using (ETaxEntities objEtaxEntities = new ETaxEntities())
            {
                try
                {
                    long TaxPayerID = (from s in objEtaxEntities.tblTaxPayerDetails

                                       where s.tblUser.UserID == UserId
                                       select s.TaxPayerID).FirstOrDefault();
                    return TaxPayerID;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public int SaveTaxPayerDocuments(BO_tblTaxPayerDocument pObjTaxDocuments)
        {
            try
            {
                using (ETaxEntities objEtaxEntities = new ETaxEntities())
                {
                    objEtaxEntities.Connection.Open();
                    tblTaxPayerDocument objtaxDocument = new tblTaxPayerDocument();
                    objtaxDocument.TaxPayerID = pObjTaxDocuments.TaxPayerID;
                    objtaxDocument.FileName = pObjTaxDocuments.FileName;
                    objtaxDocument.DocumentID = pObjTaxDocuments.DocumentID;
                    objtaxDocument.FileContent = pObjTaxDocuments.FileContent;
                    objtaxDocument.Remarks = pObjTaxDocuments.Remarks;
                    objtaxDocument.CreatedOn = System.DateTime.Now;
                    objEtaxEntities.tblTaxPayerDocuments.AddObject(objtaxDocument);
                    objEtaxEntities.SaveChanges();

                }
                return 1;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<BO_tblTaxPayerDocument> GetTaxpayerDocuments(int documentID, long taxPayerID)
        {
            try
            {
                using (ETaxEntities objETaxEntites = new ETaxEntities())
                {
                    List<BO_tblTaxPayerDocument> objdocuments = (from n in objETaxEntites.tblTaxPayerDocuments
                                                                 where n.DocumentID == documentID && n.TaxPayerID == taxPayerID
                                                                 select
                                                                 new BO_tblTaxPayerDocument
                                                                 {
                                                                     TaxPayerDocumentID = n.TaxPayerDocumentID,
                                                                     Remarks = n.Remarks,
                                                                     FileName = n.FileName
                                                                 }
                                          ).ToList();
                    return objdocuments;
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public BO_tblTaxPayerDocument GetTaxpayerFile(int documentID)
        {
            try
            {
                using (ETaxEntities objETaxEntites = new ETaxEntities())
                {
                    BO_tblTaxPayerDocument objdocument = (from n in objETaxEntites.tblTaxPayerDocuments
                                                          where n.TaxPayerDocumentID == documentID
                                                          select new BO_tblTaxPayerDocument
                                                          {
                                                              FileContent = n.FileContent,
                                                              FileName = n.FileName
                                                          }).SingleOrDefault();

                    return objdocument;
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public int SaveTaxSheet(BO_tblTaxSheets pObjTaxDocuments)
        {
            try
            {
                using (ETaxEntities objEtaxEntities = new ETaxEntities())
                {
                    objEtaxEntities.Connection.Open();
                    tblTaxSheet objtaxSheet = new tblTaxSheet();
                    objtaxSheet.SheetsType = pObjTaxDocuments.SheetsType;
                    objtaxSheet.TaxPayerID = pObjTaxDocuments.TaxPayerID;
                    objtaxSheet.TaxSheet = pObjTaxDocuments.TaxSheet;
                    objtaxSheet.Remarks = pObjTaxDocuments.Remarks;
                    objtaxSheet.TaxSheetName = pObjTaxDocuments.SheetName;
                    objtaxSheet.TaxSheetSource = pObjTaxDocuments.TaxSheetSource;
                    objtaxSheet.CreatedOn = System.DateTime.Now;
                    objEtaxEntities.tblTaxSheets.AddObject(objtaxSheet);
                    objEtaxEntities.SaveChanges();
                }
                return 1;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int SaveSheet(BO_tblTaxSheets pObjTaxDocuments)
        {
            try
            {
                using (ETaxEntities objEtaxEntities = new ETaxEntities())
                {
                    objEtaxEntities.Connection.Open();
                    tblTaxSheet objtaxSheet = (from n in objEtaxEntities.tblTaxSheets
                                               where n.TaxSheetID == pObjTaxDocuments.TaxSheetID
                                               select n).FirstOrDefault();
                    if (objtaxSheet != null)
                    {
                        objtaxSheet.TaxSheetID = pObjTaxDocuments.TaxSheetID;
                        objtaxSheet.SheetsType = pObjTaxDocuments.SheetsType;
                        objtaxSheet.TaxSheet = pObjTaxDocuments.TaxSheet;
                        objtaxSheet.Remarks = pObjTaxDocuments.Remarks;
                        objtaxSheet.TaxSheetName = pObjTaxDocuments.SheetName;
                        objtaxSheet.TaxSheetSource = pObjTaxDocuments.TaxSheetSource;
                        objtaxSheet.CreatedOn = System.DateTime.Now;
                    }
                    else
                    {
                        tblTaxSheet objtaxSheetnew = new tblTaxSheet();
                        objtaxSheetnew.SheetsType = pObjTaxDocuments.SheetsType;
                        objtaxSheetnew.TaxSheet = pObjTaxDocuments.TaxSheet;
                        objtaxSheetnew.Remarks = pObjTaxDocuments.Remarks;
                        objtaxSheetnew.TaxSheetName = pObjTaxDocuments.SheetName;
                        objtaxSheetnew.TaxSheetSource = pObjTaxDocuments.TaxSheetSource;
                        objtaxSheetnew.CreatedOn = System.DateTime.Now;
                        objEtaxEntities.tblTaxSheets.AddObject(objtaxSheetnew);
                    }

                    objEtaxEntities.SaveChanges();
                }
                return 1;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int SaveConfirmTaxSheet(BO_tblTaxSheets pObjTaxDocuments)
        {
            try
            {
                using (ETaxEntities objEtaxEntities = new ETaxEntities())
                {
                    objEtaxEntities.Connection.Open();
                    tblTaxSheet objtaxSheet = new tblTaxSheet();
                    objtaxSheet.SheetsType = pObjTaxDocuments.SheetsType;
                    objtaxSheet.TaxPayerID = pObjTaxDocuments.TaxPayerID;
                    objtaxSheet.TaxSheet = pObjTaxDocuments.TaxSheet;
                    objtaxSheet.Remarks = pObjTaxDocuments.Remarks;
                    objtaxSheet.TaxSheetName = pObjTaxDocuments.SheetName;
                    objtaxSheet.TaxSheetSource = pObjTaxDocuments.TaxSheetSource;
                    objtaxSheet.CreatedOn = System.DateTime.Now;
                    objtaxSheet.ConfirmTaxDocStatus = pObjTaxDocuments.ConfirmTaxDocumentStatus;
                    objtaxSheet.EligibleForEfilingStatus = objtaxSheet.EligibleForEfilingStatus;
                    objEtaxEntities.tblTaxSheets.AddObject(objtaxSheet);
                    objEtaxEntities.SaveChanges();
                }
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //to get TaxSheetId
        public int TaxSheetID(int SheetsType, string taxSheetSource)
        {
            try
            {
                using (ETaxEntities objEtaxEntities = new ETaxEntities())
                {
                    objEtaxEntities.Connection.Open();
                    int taxSheetId = (from n in objEtaxEntities.tblTaxSheets
                                      where n.SheetsType == SheetsType &&
                                      n.TaxSheetSource == taxSheetSource
                                      select n.TaxSheetID).SingleOrDefault();
                    return taxSheetId;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BO_tblDocument> GetAllForms()
        {
            try
            {
                using (ETaxEntities objETaxEntites = new ETaxEntities())
                {
                    objETaxEntites.Connection.Open();
                    List<BO_tblDocument> objForm = (from n in objETaxEntites.tblDocuments
                                                    select new BO_tblDocument
                                                            {
                                                                DocumentID = n.DocumentID,
                                                                FormName = n.FormName,
                                                            }).ToList();
                    return objForm;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Get all TaxpayerUploaded Files using TaxPayerID
        public byte[] GetTaxpayerFiles(long TaxPayerID)
        {
            try
            {
                using (ETaxEntities objETaxEntites = new ETaxEntities())
                {
                    byte[] objFile = (from n in objETaxEntites.tblTaxPayerDocuments
                                      where n.tblTaxPayerDetail.TaxPayerID == TaxPayerID
                                      select n.FileContent).FirstOrDefault();
                    return objFile;
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<BO_tblTaxPayerDocument> GetTaxpayerAllDocuments(long TaxPayerID)
        {
            try
            {
                using (ETaxEntities objETaxEntites = new ETaxEntities())
                {
                    objETaxEntites.Connection.Open();
                    List<BO_tblTaxPayerDocument> objDocuments = (from n in objETaxEntites.tblTaxPayerDocuments
                                                                 where
                                                                     n.TaxPayerID == TaxPayerID
                                                                 select new BO_tblTaxPayerDocument
                                                                 {
                                                                     TaxPayerID = n.TaxPayerID,
                                                                     FormName = n.tblDocument.FormName,
                                                                     FileName = n.FileName,
                                                                     Remarks = n.Remarks
                                                                 }).ToList();

                    return objDocuments;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        //to get GetTaxPayerAllSheets using TaxSheetID
        public BO_tblTaxSheets GetTaxPayerSheets(long TaxSheetID)
        {
            try
            {
                using (ETaxEntities objETaxEntites = new ETaxEntities())
                {
                    BO_tblTaxSheets objSheets = (from n in objETaxEntites.tblTaxSheets
                                                 where n.TaxSheetID == TaxSheetID
                                                 select new BO_tblTaxSheets
                                                 {
                                                     SheetName = n.TaxSheetName,
                                                     TaxSheet = n.TaxSheet
                                                 }).SingleOrDefault();
                    return objSheets;
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<BO_tblTaxSheets> GetTaxpayerInformation(long TaxPayerID, int SheetType, string SheetSource)
        {
            try
            {
                using (ETaxEntities objETaxEntites = new ETaxEntities())
                {
                    List<BO_tblTaxSheets> objdocuments = (from n in objETaxEntites.tblTaxSheets
                                                          where n.TaxPayerID == TaxPayerID
                                                          && n.SheetsType == SheetType && n.TaxSheetSource == SheetSource

                                                          select new BO_tblTaxSheets
                                                          {
                                                              TaxSheetID = n.TaxSheetID,
                                                              Remarks = n.Remarks,
                                                              SheetName = n.TaxSheetName,
                                                              CreatedOn = n.CreatedOn,
                                                          }).ToList();


                    return objdocuments;

                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public BO_tblTaxSheets getTaxpayerExelSheet(int SheetType, string SheetSource)
        {
            try
            {
                using (ETaxEntities objETaxEntites = new ETaxEntities())
                {
                    BO_tblTaxSheets objdocuments = (from n in objETaxEntites.tblTaxSheets
                                                    where n.SheetsType == SheetType && n.TaxSheetSource == SheetSource
                                                    select new BO_tblTaxSheets
                                                    {
                                                        TaxSheetID = n.TaxSheetID,
                                                        Remarks = n.Remarks,
                                                        SheetName = n.TaxSheetName,
                                                        CreatedOn = n.CreatedOn,
                                                        TaxSheet = n.TaxSheet
                                                    }).FirstOrDefault();


                    return objdocuments;

                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        //to get return DocuMents
        public List<BO_tblTaxSheets> getReturnTaxpayerInformation(long TaxPayerID, int documentType, string SheetSource)
        {
            try
            {
                using (ETaxEntities objETaxEntites = new ETaxEntities())
                {
                    List<BO_tblTaxSheets> objdocuments = (from n in objETaxEntites.tblTaxSheets
                                                          where n.TaxPayerID == TaxPayerID &&
                                                         n.SheetsType == documentType && n.TaxSheetSource == SheetSource

                                                          select new BO_tblTaxSheets
                                                          {
                                                              TaxSheetID = n.TaxSheetID,
                                                              Remarks = n.Remarks,
                                                              SheetName = n.TaxSheetName,
                                                              CreatedOn = n.CreatedOn
                                                          }).ToList();


                    return objdocuments;

                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        //to get GetTaxPayerAllSheets using TaxpayerID
        public BO_tblTaxSheets GetTaxPayerAllSheets(long TaxpayerID, int SheetType)
        {
            try
            {
                using (ETaxEntities objETaxEntites = new ETaxEntities())
                {
                    BO_tblTaxSheets objSheets = (from n in objETaxEntites.tblTaxSheets
                                                 where n.TaxPayerID == TaxpayerID &&
                                                 n.SheetsType == SheetType
                                                 select new BO_tblTaxSheets
                                                 {
                                                     SheetName = n.TaxSheetName,
                                                     TaxSheet = n.TaxSheet
                                                 }).FirstOrDefault();
                    return objSheets;
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public int getDocumentCount(long taxpayerID, int documentID)
        {
            try
            {
                using (ETaxEntities objETaxEntites = new ETaxEntities())
                {
                    int Count = (from n in objETaxEntites.tblTaxPayerDocuments
                                 where n.TaxPayerID == taxpayerID && n.DocumentID == documentID
                                 select new BO_tblTaxPayerDocument
                                 {
                                     Count = (from a in objETaxEntites.tblTaxPayerDocuments
                                              where a.DocumentID == documentID && a.TaxPayerID == taxpayerID
                                              select a).Count(),


                                 }).Count();

                    return Count;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
         public List<BO_tblTaxPayerDocument> getDocumentsCount(long taxpayerID)
        {
            try
            {
                using (ETaxEntities objETaxEntites = new ETaxEntities())
                {
                    List<BO_tblTaxPayerDocument> objDocumentsCount = (from n in objETaxEntites.tblTaxPayerDocuments
                                                                      where n.TaxPayerID == taxpayerID
                                                                      group n by new { n.DocumentID } into m
                                                                      select new 
                                                                      BO_tblTaxPayerDocument
                                                                      {
                                                                          DocumentID=m.Key.DocumentID,
                                                                          
                                                                          Count=m.Count(),
                                                                      }).ToList();
                                                                

                    return objDocumentsCount;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
