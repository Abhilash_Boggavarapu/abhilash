﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ETaxBO;

namespace ETaxDal
{
     public class ReferralAdmin
    {
        public int InsertUpdateReferal(BO_tblReferralType pobjReferal)
        {
            try
            {
                using (ETaxEntities objTaxEntities = new ETaxEntities())
                {
                    var existRefferal = from E in objTaxEntities.tblReferralTypes

                                        where E.ReferralTypeID == pobjReferal.ReferralTypeID
                                        select E;
                    if (existRefferal.Any())
                    {
                        var Refferal = existRefferal.FirstOrDefault();
                        Refferal.ReferralType = pobjReferal.ReferralType;
                    }
                    else
                    {

                        tblReferralType objReferal = new tblReferralType();
                        objReferal.ReferralType = pobjReferal.ReferralType;
                        objReferal.CreatedOn = DateTime.Now;
                        objTaxEntities.tblReferralTypes.AddObject(objReferal);

                    }
                    objTaxEntities.SaveChanges();
                    return 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<BO_tblReferralType> GetReferralDetails()
        {
            try
            {

                using (ETaxEntities objEtaxEntity = new ETaxEntities())
                {
                    objEtaxEntity.Connection.Open();

                    List<BO_tblReferralType> objAdminReferralDetails = (from n in objEtaxEntity.tblReferralTypes
                                                                       select new BO_tblReferralType
                                                                       {
                                                                           ReferralTypeID=n.ReferralTypeID,
                                                                           ReferralType=n.ReferralType
                                                                       }).ToList();
                    return objAdminReferralDetails;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public BO_tblReferralType GetReferral(int ReferalId)
        {
            try
            {

                using (ETaxEntities objEtaxEntity = new ETaxEntities())
                {
                    objEtaxEntity.Connection.Open();

                    BO_tblReferralType objAdminReferral = (from n in objEtaxEntity.tblReferralTypes
                                                           where n.ReferralTypeID==ReferalId
                                                           select new BO_tblReferralType
                                                           {
                                                               ReferralTypeID = n.ReferralTypeID,
                                                               ReferralType = n.ReferralType
                                                           }).SingleOrDefault();
                    return objAdminReferral;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
