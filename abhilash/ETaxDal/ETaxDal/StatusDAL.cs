﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ETaxBO;

namespace ETaxDal
{
    public class StatusDAL
    {
        public List<BO_TaxPayerTaxFilingStatusDetails> getFilingStatusList(long taxPayerId)
        {
            try
            {
                using (ETaxEntities objETaxEntities = new ETaxEntities())
                {
                    objETaxEntities.Connection.Open();
                    List<BO_TaxPayerTaxFilingStatusDetails> objTaxFilingStatusDetails = (from filingStatusDetails in objETaxEntities.TaxPayerTaxFilingStatusDetails
                                                                                         where filingStatusDetails.TaxPayerID == taxPayerId
                                                                                         select new BO_TaxPayerTaxFilingStatusDetails
                                                                                         {
                                                                                             TaxFilingStatusTypeID = filingStatusDetails.TaxFilingStatusTypeID,
                                                                                             CreatedOn=filingStatusDetails.CreatedOn
                                                                                         }).ToList();


                    return objTaxFilingStatusDetails;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public BO_TaxPayerTaxFilingStatusDetails getFilingStatus(long taxPayerId, int TaxFilingStatusTypeID)
        {
            try
            {
                using (ETaxEntities objETaxEntities = new ETaxEntities())
                {
                    objETaxEntities.Connection.Open();
                    BO_TaxPayerTaxFilingStatusDetails objTaxFilingStatusDetails = (from filingStatusDetails in objETaxEntities.TaxPayerTaxFilingStatusDetails
                                                                                   where filingStatusDetails.TaxPayerID == taxPayerId &&
                                                                                  filingStatusDetails.TaxFilingStatusTypeID == TaxFilingStatusTypeID
                                                                                   select new BO_TaxPayerTaxFilingStatusDetails
                                                                                   {
                                                                                       TaxFilingStatusTransactionID = filingStatusDetails.TaxFilingStatusTransactionID,
                                                                                   }).FirstOrDefault();


                    return objTaxFilingStatusDetails;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public bool ConfirmFileStaus(BO_TaxPayerTaxFilingStatusDetails objConfirmFileStaus)
        {
            try
            {
                using (ETaxEntities objETaxEntities = new ETaxEntities())
                {
                    objETaxEntities.Connection.Open();
                    TaxPayerTaxFilingStatusDetail objTaxPayerTaxFilingStatusDetail = new TaxPayerTaxFilingStatusDetail();
                    objTaxPayerTaxFilingStatusDetail.TaxPayerID = objConfirmFileStaus.TaxPayerID;
                    objTaxPayerTaxFilingStatusDetail.TaxFilingStatusTypeID = objConfirmFileStaus.TaxFilingStatusTypeID;
                    objTaxPayerTaxFilingStatusDetail.CreatedOn = System.DateTime.Now;
                    objETaxEntities.TaxPayerTaxFilingStatusDetails.AddObject(objTaxPayerTaxFilingStatusDetail);
                    objETaxEntities.SaveChanges();
                }
                return true;
            }


            catch (Exception ex)
            {
                throw ex;
            }
        }

        public BO_TaxSummary getTaxSummary(int taxpayerID)
        {
            try
            {
                using (ETaxEntities objETaxEntities = new ETaxEntities())
                {
                    objETaxEntities.Connection.Open();
                    BO_TaxSummary objTaxSummary = (from n in objETaxEntities.tblTaxSummaries
                                                   where n.TaxPayerID == taxpayerID
                                                   select new BO_TaxSummary
                                                   {
                                                       TaxSummary = n.TaxSummary,
                                                       TaxSummaryId = n.TaxSummaryID
                                                   }).SingleOrDefault();
                    return objTaxSummary;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}
