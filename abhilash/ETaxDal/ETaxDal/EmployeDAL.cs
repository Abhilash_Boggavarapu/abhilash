﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ETaxBO;
using ETaxDal;

namespace ETaxDal
{
    public class EmployeDAL
    {
        public void InsertUpdateEmploye(BO_tblTaxPayerEmployerDetail pobjEmploye)
        {
            try
            {

                using (ETaxEntities objTaxEntities = new ETaxEntities())
                {
                    tblTaxPayerEmployerDetail objTaxPayerEmployerDetail = (from s in objTaxEntities.tblTaxPayerEmployerDetails
                                                                           where s.TaxPayerEmployerID == pobjEmploye.TaxPayerEmployerID
                                                                           select s).FirstOrDefault();
                                     
                    if (objTaxPayerEmployerDetail!=null)
                    {
                        objTaxPayerEmployerDetail.EmployerName = pobjEmploye.EmployerName;
                        objTaxPayerEmployerDetail.EmploymentStartDate = pobjEmploye.EmploymentStartDate;
                        objTaxPayerEmployerDetail.EmploymentEndDate = pobjEmploye.EmploymentEndDate;
                        objTaxPayerEmployerDetail.CountryID = pobjEmploye.CountryID;
                        objTaxPayerEmployerDetail.StateID = pobjEmploye.StateID;
                        objTaxPayerEmployerDetail.CityID = pobjEmploye.CityID;

                    }
                    else
                    {
                    objTaxPayerEmployerDetail = new tblTaxPayerEmployerDetail();
                    objTaxPayerEmployerDetail.TaxPayerID = pobjEmploye.TaxPayerID;
                    objTaxPayerEmployerDetail.EmployerName = pobjEmploye.EmployerName;
                    objTaxPayerEmployerDetail.EmploymentStartDate = pobjEmploye.EmploymentStartDate;
                    objTaxPayerEmployerDetail.EmploymentEndDate = pobjEmploye.EmploymentEndDate;
                    objTaxPayerEmployerDetail.CountryID = pobjEmploye.CountryID;
                    objTaxPayerEmployerDetail.StateID = pobjEmploye.StateID;
                    objTaxPayerEmployerDetail.CityID = pobjEmploye.CityID;
                    objTaxPayerEmployerDetail.CreatedOn = DateTime.Now;
                    objTaxEntities.tblTaxPayerEmployerDetails.AddObject(objTaxPayerEmployerDetail);
                    }
                    objTaxEntities.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       
     
        //  To get EmployeeDetails into List using TaxPayerEmployerID
        public BO_tblTaxPayerEmployerDetail GetEmployerDetails(long taxPayerEmployerID)
        {
            try
            {
                using (ETaxEntities objETaxEntities = new ETaxEntities())
                {
                    objETaxEntities.Connection.Open();
                    BO_tblTaxPayerEmployerDetail objEmpDetails = (from s in objETaxEntities.tblTaxPayerEmployerDetails
                                                                        where s.TaxPayerEmployerID == taxPayerEmployerID
                                                                        select new BO_tblTaxPayerEmployerDetail

                                                                        {
                                                                            TaxPayerEmployerID = s.TaxPayerEmployerID,
                                                                            TaxPayerID = s.TaxPayerID,
                                                                            EmployerName = s.EmployerName,
                                                                            EmploymentStartDate = s.EmploymentStartDate,
                                                                            EmploymentEndDate = s.EmploymentEndDate,
                                                                            CountryID =s.CountryID,
                                                                            StateID = s.StateID,
                                                                            CityID =s.CityID,
                                                                            CountryName =s.tblCountry.Country,
                                                                            City = s.tblCity.City,
                                                                            CreatedOn = s.CreatedOn,
                                                                            StateName =s.tblState.StateName,

                                                                        }).SingleOrDefault ();


                    return objEmpDetails;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        // To get TaxpayerEmloyer Details using TaxPayerID
      
        public List<BO_tblTaxPayerEmployerDetail> GetTaxPayerEmployersDetails(long taxPayerID)
        {
            try
            {
                using (ETaxEntities objETaxEntities = new ETaxEntities())
                {
                    objETaxEntities.Connection.Open();
                    List<BO_tblTaxPayerEmployerDetail> objEmpDetails = (from s in objETaxEntities.tblTaxPayerEmployerDetails
                                                                        where s.TaxPayerID == taxPayerID
                                                                        select new BO_tblTaxPayerEmployerDetail

                                                                        {
                                                                            TaxPayerEmployerID = s.TaxPayerEmployerID,
                                                                            EmployerName = s.EmployerName,
                                                                            EmploymentStartDate = s.EmploymentStartDate,
                                                                            EmploymentEndDate = s.EmploymentEndDate,
                                                                            CountryName =s.tblCountry.Country,
                                                                            City = s.tblCity.City,
                                                                            CreatedOn = s.CreatedOn,
                                                                            StateName = s.tblState.StateName,

                                                                        }).ToList();
                     

                    return objEmpDetails;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
