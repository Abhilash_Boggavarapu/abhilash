﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ETaxBO;

namespace ETaxDal
{
    public class ReferralDal
    {
       
        //for insert referral Details
        public int SaveReferralDetails(BO_tblUserReferral pUserReferral)
        {
            try
            {

                using (ETaxEntities objEtaxEntity = new ETaxEntities())
                {
                    objEtaxEntity.Connection.Open();
                    tblUserReferral objReferral = new tblUserReferral();
                    objReferral.ReferralTypeID = pUserReferral.ReferralTypeID;
                    objReferral.ReferralName = pUserReferral.ReferralName;
                    objReferral.UserID = pUserReferral.UserID;
                    objReferral.EmailID = pUserReferral.EmailID;
                    objReferral.Phone1 = pUserReferral.Phone1;
                    objReferral.Phone2 = pUserReferral.Phone2;
                    objReferral.ReferredName = pUserReferral.ReferredName;
                    objReferral.ReferredEmailID = pUserReferral.ReferredEmailID;
                    objReferral.ReferredPhone = pUserReferral.ReferredPhone;
                    objReferral.CreatedOn = System.DateTime.Now;
                    objEtaxEntity.tblUserReferrals.AddObject(objReferral);
                    objEtaxEntity.SaveChanges();
                    return 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    
        public List<BO_tblUserReferral> GetReferralDetails(long UserId)
        {
            try
            {

                using (ETaxEntities objEtaxEntity = new ETaxEntities())
                {
                    objEtaxEntity.Connection.Open();

                    List<BO_tblUserReferral> objUserReferralDetails=(from n in objEtaxEntity.tblUserReferrals
                                                                     where n.UserID== UserId
                                                                     select new  BO_tblUserReferral
                                                                     {
                                                                         ReferredID=n.ReferredID,
                                                                         ReferralName=n.ReferralName,
                                                                         EmailID=n.EmailID,
                                                                         Phone1=n.Phone1,
                                                                         Phone2=n.Phone2,
                                                                     }).ToList();
                    return objUserReferralDetails;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public long GetReferalID(int ReferralTypeID)
        {
            using (ETaxEntities objEtaxEntities = new ETaxEntities())
            {
                try
                {
                    long ReferalID = (from s in objEtaxEntities.tblUserReferrals

                                       where s.tblReferralType.ReferralTypeID == ReferralTypeID
                                       select s.ReferredID).FirstOrDefault();
                    return ReferalID;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public int SaveReferralFromHomeDetails(BO_tblUserReferral pReferralFromHome)
        {
            try
            {
                using (ETaxEntities objEtaxEntity = new ETaxEntities())
                {
                    objEtaxEntity.Connection.Open();
                    tblUserReferral objUserReferral = new tblUserReferral();
                    objUserReferral.ReferralTypeID = pReferralFromHome.ReferralTypeID;
                    objUserReferral.ReferralName = pReferralFromHome.ReferralName;
                    objUserReferral.EmailID = pReferralFromHome.EmailID;
                    objUserReferral.Phone1 = pReferralFromHome.Phone1;
                    objUserReferral.Phone2 = pReferralFromHome.Phone2;
                    objUserReferral.CreatedOn = System.DateTime.Now;
                    objEtaxEntity.tblUserReferrals.AddObject(objUserReferral);
                    objEtaxEntity.SaveChanges();

                }
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public List<BO_tblUserReferral> getReferalHomeDetails(long referalTypeID,DateTime startDate,DateTime endDate)
         {
            try
            {

                using (ETaxEntities objEtaxEntity = new ETaxEntities())
                {
                    objEtaxEntity.Connection.Open();
                    

                    List<BO_tblUserReferral> objUserReferralHomeDetails = (from n in objEtaxEntity.tblUserReferrals
                                                                           where n.ReferralTypeID==referalTypeID &&
                                                                           n.CreatedOn >= startDate && 
                                                                           n.CreatedOn <=endDate
                
                                                                           select new BO_tblUserReferral
                                                                           {
                                                                               //UserID = n.UserID,
                                                                               //UserName = n.tblUser.FirstName,
                                                                             
                                                                               //UserEmailID = n.tblUser.EmailID,
                                                                               //UserPhoneNo = n.tblUser.MobileNumber,
                                                                               //Count = (from g in objEtaxEntity.tblUserReferrals
                                                                               //         where g.UserID == n.UserID
                                                                               //         select g).Count(),
                                                                               ReferredID = n.ReferredID,
                                                                               ReferralName = n.ReferralName,
                                                                               EmailID = n.EmailID,
                                                                               Phone1 = n.Phone1,
                                                                               ReferredName = n.ReferredName,
                                                                               ReferredEmailID = n.ReferredEmailID,
                                                                               ReferredPhone = n.ReferredPhone,
                                                                               CreatedOn = n.CreatedOn
                                                                           }).Distinct().ToList();
                 

                    return objUserReferralHomeDetails;
                }
            }
              catch(Exception ex)
            {
                  throw ex;
              }
          }
        public List<BO_tblUserReferral> getReferalClientDetails(long referalTypeID,DateTime startDate,DateTime endDate)
        {
            try
            {

                using (ETaxEntities objEtaxEntity = new ETaxEntities())
                {
                    objEtaxEntity.Connection.Open();


                    List<BO_tblUserReferral> objUserReferralHomeDetails = (from n in objEtaxEntity.tblUserReferrals
                                                                           where n.ReferralTypeID == referalTypeID &&
                                                                              n.CreatedOn >= startDate && 
                                                                           n.CreatedOn <=endDate
                                                                           select new BO_tblUserReferral
                                                                           {
                                                                               UserID = n.UserID,
                                                                               UserName = n.tblUser.FirstName,

                                                                               UserEmailID = n.tblUser.EmailID,
                                                                               UserPhoneNo = n.tblUser.MobileNumber,
                                                                               Count = (from g in objEtaxEntity.tblUserReferrals
                                                                                        where g.UserID == n.UserID
                                                                                        select g).Count(),
                                                                              
                                                                           }).Distinct().ToList();


                    return objUserReferralHomeDetails;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BO_tblUserReferral> getReferalClientDetails(long userID, int referralTypeID)
        {
            try
            {

                using (ETaxEntities objEtaxEntity = new ETaxEntities())
                {
                    objEtaxEntity.Connection.Open();

                    List<BO_tblUserReferral> referalDetails = (from n in objEtaxEntity.tblUserReferrals
                                                               where n.UserID == userID &&
                                                               n.ReferralTypeID == referralTypeID
                                                               select new BO_tblUserReferral
                                                               {
                                                                   ReferralName=n.ReferralName,
                                                                   EmailID=n.EmailID,
                                                                   Phone1=n.Phone1,
                                                                   CreatedOn=n.CreatedOn
                                                               }).ToList();
                    return referalDetails;
                }
            }
          catch(Exception ex)
            {
              throw ex;
          }
      }
    }
}
