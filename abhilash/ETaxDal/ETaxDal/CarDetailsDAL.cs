﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ETaxDal;
using ETaxBO;
namespace ETaxDal
{
    class CarDetailsDAL
    {
        //To get TaxpayerId using UserID
        public long GetTaxpayerID(long UserId)
        {
            using (ETaxEntities objEtaxEntities = new ETaxEntities())
            {
                try
                {
                    long TaxPayerID = (from s in objEtaxEntities.tblTaxPayerDetails

                                       where s.tblUser.UserID == UserId
                                       select s.TaxPayerID).FirstOrDefault();
                    return TaxPayerID;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        // To get TaxPayercarDetailsID using TaxPayerID
        public long GetTaxPayerCarDetailsID(long TaxPayerID)
        {
            try
            {
                using (ETaxEntities objETaxEntites = new ETaxEntities())
                {
                    objETaxEntites.Connection.Open();
                    long TaxPayerCarDetailsID = (from n in objETaxEntites.tblTaxPayerCarDetails
                                                 where n.tblTaxPayerDetail.TaxPayerID == TaxPayerID
                                                 select n.TaxPayerCarDetailsID).FirstOrDefault();
                    return TaxPayerCarDetailsID;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        // TO get cardetails into List using TaxPayercarDetailsID
        public List<BO_tblTaxPayerCarDetail> GetCarDetails(long TaxPayerCarDetailsID)
        {
            try
            {
                using (ETaxEntities objETaxEntities = new ETaxEntities())
                {
                    objETaxEntities.Connection.Open();
                    List<BO_tblTaxPayerCarDetail> objCarDetails = (from s in objETaxEntities.tblTaxPayerCarDetails
                                                                   where s.TaxPayerCarDetailsID == TaxPayerCarDetailsID
                                                                   select new BO_tblTaxPayerCarDetail

                                                                   {
                                                                       CarMake_Model = s.CarMake_Model,
                                                                       PurchasedYear = s.PurchasedYear,
                                                                       CreatedOn = s.CreatedOn,
                                                                       MilesDriven = s.MilesDriven,
                                                                       TaxPayerCarDetailsID = s.TaxPayerCarDetailsID,
                                                                       TaxPayerID = s.TaxPayerID
                                                                   }).ToList();


                    return objCarDetails;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
