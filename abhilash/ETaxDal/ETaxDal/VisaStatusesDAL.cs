﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ETaxBO;
using ETaxDal;

namespace ETaxDal
{
    public class VisaStatusesDAL
    {
        public int SaveUpdateVisaStatuses(BO_tblVisaStatus pobjvisa)
            {
            try
            {

           using (ETaxEntities objTaxEntities = new ETaxEntities())
                {
                  objTaxEntities.Connection.Open();
                  
                    tblVisaStatus objVisa = (from n in objTaxEntities.tblVisaStatuses
                                                   where n.VisaStatusID == pobjvisa.VisaStatusID
                                              select n).FirstOrDefault();
                    if (objVisa != null)
                    {
                        objVisa.VisaStatus = pobjvisa.VisaStatus;
                        objVisa.CreatedOn = DateTime.Now;
                    }
                    else
                    {
                    objVisa = new tblVisaStatus();
                    objVisa.VisaStatus = pobjvisa.VisaStatus;
                    objVisa.CreatedOn = DateTime.Now;
                    objTaxEntities.tblVisaStatuses.AddObject(objVisa);
                    }
                objTaxEntities.SaveChanges();
                    return 1;
                  }
               }
    
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}

       
    

