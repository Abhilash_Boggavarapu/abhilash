﻿// -----------------------------------------------------------------------
// <copyright file="TaxPayerClientDetails.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace ETaxDal
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using ETaxBO;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class TaxPayerClientDetailsDAL
    {
        public int InsertUpdateClientInformation(BO_tblTaxPayerClientDetail pobjClient)
        {
            try
            {
                using (ETaxEntities objTaxEntities = new ETaxEntities())
                {
                    objTaxEntities.Connection.Open();

                    var objTaxPayerClient = from s in objTaxEntities.tblTaxPayerClientDetails
                                            where s.TaxPayerClientID == pobjClient.TaxPayerClientID
                                            select s;
                    if (objTaxPayerClient.Any())
                    {
                        var objClient = objTaxPayerClient.FirstOrDefault();
                        objClient.ClientName = pobjClient.ClientName;
                        objClient.ProjectStartDate = pobjClient.ProjectStartDate;
                        objClient.ProjectEndDate = pobjClient.ProjectEndDate;
                    }
                    else
                    {
                        tblTaxPayerClientDetail objTaxClient = new tblTaxPayerClientDetail();

                        objTaxClient.ClientName = pobjClient.ClientName;
                        objTaxClient.ProjectStartDate = pobjClient.ProjectStartDate;
                        objTaxClient.ProjectEndDate = pobjClient.ProjectEndDate;
                        objTaxClient.CreatedOn = System.DateTime.Now;

                        objTaxEntities.tblTaxPayerClientDetails.AddObject(objTaxClient);
                    }

                    objTaxEntities.SaveChanges();
                }
                return 1;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

  
        public List<BO_tblTaxPayerClientDetail> GetTaxpayrClientDetails(long TaxPayerClientID)
        {
             try
                {
            using(ETaxEntities objETaxEntities = new ETaxEntities())
            {
               objETaxEntities.Connection.Open();
               List<BO_tblTaxPayerClientDetail> objTaxPayrClientdetail =(from g in objETaxEntities.tblTaxPayerClientDetails
                                                                         where g.TaxPayerClientID == TaxPayerClientID
                                                                          select  new BO_tblTaxPayerClientDetail
                                                                           
                                                 {
                                                      ClientName = g.ClientName,
                                                      ProjectStartDate = g.ProjectStartDate,
                                                      ProjectEndDate = g.ProjectEndDate,
                                                      CityName = g.tblCity.City,
                                                     StateName = g.tblState.StateName,
                                                  
                                                  }).ToList();
   
                    return objTaxPayrClientdetail;
                
            }

        }
             catch (Exception ex)
             {
                 throw ex;
             }

      }
   }  
}
