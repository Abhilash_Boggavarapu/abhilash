﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ETaxBO;

namespace ETaxDal
{

     public class Form7216
    {
        
         public bool getConsentDurationStatus(long userID)
         {
             try
             {
                 using (ETaxEntities objETaxEntities = new ETaxEntities())
                 {
                     bool ConsentFormStatus= (from tbluser in objETaxEntities.tblUsers
                                           where tbluser.UserID == userID
                                           select tbluser.ConsentFormStatus).FirstOrDefault();
                     return ConsentFormStatus;
                 }
             }
             catch (Exception ex)
             {
                 throw ex;
             }
   
         }
         public BO_tblUser getConsentDurationDetails(long userID)
         {
             try
             {
                 using (ETaxEntities objETaxEntities = new ETaxEntities())
                 {
                     BO_tblUser objUserConsent = (from tbluser in objETaxEntities.tblUsers
                                                  where tbluser.UserID == userID
                                                  select new BO_tblUser
                                                  {
                                                      ConsentDuration=tbluser.ConsentDuration,
                                                      SignatureOnConsentForm =tbluser .SignatureOnConsentForm,
                                                      SignedOnConsentForm =tbluser .SignedOnConsentForm 
                                                  }).FirstOrDefault();
                                             
                     return objUserConsent;
                 }
             }
             catch (Exception ex)
             {
                 throw ex;
             }

         }
         public bool SetConsentDuration(long UserId, BO_tblUser pObjUser)
         {
             try
             {
                 using (ETaxEntities objETaxEntities = new ETaxEntities())
                 {
                     objETaxEntities.Connection.Open();
                     tblUser objUser = (from n in objETaxEntities.tblUsers
                                        where n.UserID == UserId
                                        select n).First();
                     objUser.ConsentFormStatus = pObjUser.ConsentFormStatus;
                     objUser.ConsentDuration = pObjUser.ConsentDuration;
                     objUser.SignedOnConsentForm = pObjUser.SignedOnConsentForm;
                     objUser.SignatureOnConsentForm = pObjUser.SignatureOnConsentForm;

                     objETaxEntities.SaveChanges();
                 }
                 return true;
             }
             catch (Exception ex)
             {
                 throw ex;
             }
         }
        
    }
}
