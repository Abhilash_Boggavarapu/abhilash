﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ETaxBO;

namespace ETaxDal
{
    public class BankInformDAL
    {
       
       public int InsertUpdateBankInformation(BO_tblTaxPayerBankDetail pobjBank)
       {
           try
           {
               using (ETaxEntities objTaxEntities = new ETaxEntities())
               {
                   objTaxEntities.Connection.Open();
                   //tblTaxPayerBankDetail objTaxPayerBankDetails = objEtaxEntities.tblTaxPayerBankDetails(var => var.BankDetailsID == pobjBankDtails.BankDetailsID);
                   var objTaxPayerBank = from s in objTaxEntities.tblTaxPayerBankDetails
                                         where s.BankDetailsID == pobjBank.BankDetailsID
                                         select s;
                   if (objTaxPayerBank.Any())
                   {
                       var objBank = objTaxPayerBank.FirstOrDefault();
                       objBank.TaxPayerID = pobjBank.TaxPayerID;

                       objBank.AccountNo = pobjBank.AccountNo;
                       objBank.RoutingNumber = pobjBank.RoutingNo;
                       objBank.AccountName = pobjBank.AccountHolderName;
                       objBank.AccountType = pobjBank.AccounTtype;
                       objBank.BankName = pobjBank.BankName;
                       objBank.TaxPayerID = pobjBank.TaxPayerID;
                   }
                   else
                   {
                       tblTaxPayerBankDetail objTaxBank = new tblTaxPayerBankDetail();
                       objTaxBank.TaxPayerID = pobjBank.TaxPayerID;
                       objTaxBank.AccountNo = pobjBank.AccountNo;
                       objTaxBank.RoutingNumber = pobjBank.RoutingNo;
                       objTaxBank.BankName = pobjBank.BankName;
                       objTaxBank.AccountName = pobjBank.AccountHolderName;
                       objTaxBank.AccountType = pobjBank.AccounTtype;
                       objTaxBank.CreatedOn = System.DateTime.Now;
                       objTaxEntities.tblTaxPayerBankDetails.AddObject(objTaxBank);

                   }

                   objTaxEntities.SaveChanges();
               }
               return 1;

           }
           catch (Exception ex)
           {
               throw ex;
           }
       }

       //to get the bankdetails id using userid
       public long getBankDetailsId(long UserId)
       {
           try
           {
               using (ETaxEntities objEtaxEntites = new ETaxEntities())
               {
                   objEtaxEntites.Connection.Open();
                   long BankDetailsID = (from n in objEtaxEntites.tblTaxPayerBankDetails
                                         where n.tblTaxPayerDetail.UserID == UserId
                                         select n.BankDetailsID).FirstOrDefault();
                   return BankDetailsID;

               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }
       public BO_tblTaxPayerBankDetail GetBankInform(long BankDetailsID)
       {
           try
           {
               using (ETaxEntities objEtaxEntities = new ETaxEntities())
               {
                   objEtaxEntities.Connection.Open();
                   BO_tblTaxPayerBankDetail objBank = (from s in objEtaxEntities.tblTaxPayerBankDetails
                                                       where s.BankDetailsID == BankDetailsID
                                                       select new BO_tblTaxPayerBankDetail

                                                       {
                                                           BankDetailsID = s.BankDetailsID,
                                                           TaxPayerID = s.TaxPayerID,
                                                          AccountHolderName = s.AccountName,
                                                           AccountNo=s.AccountNo,
                                                           RoutingNo=s.RoutingNumber,
                                                           AccounTtype = s.AccountType,
                                                           BankName = s.BankName,
                                                           CreatedOn = s.CreatedOn,

                                                       }).FirstOrDefault();


                   return objBank;
               }

           }
           catch (Exception ex)
           {
               throw ex;
           }
       }
       public List<BO_tblTaxPayerBankDetail> GetBankDetails(long TaxpayerId)
       {
           try
           {
               using (ETaxEntities objEtaxEntities = new ETaxEntities())
               {
                   objEtaxEntities.Connection.Open();
                   List<BO_tblTaxPayerBankDetail> objBank = (from s in objEtaxEntities.tblTaxPayerBankDetails
                                                             where s.TaxPayerID==TaxpayerId
                                                            
                                                             select new BO_tblTaxPayerBankDetail

                                                             {
                                                                 BankDetailsID = s.BankDetailsID,
                                                                 AccountHolderName = s.AccountName,
                                                                 AccounTtype = s.AccountType,
                                                                 RoutingNo=s.RoutingNumber,
                                                                 AccountNo=s.AccountNo,
                                                                 BankName = s.BankName,
                                                                 CreatedOn = s.CreatedOn,

                                                             }).ToList();


                   return objBank;
               }

           }
           catch (Exception ex)
           {
               throw ex;
           }
       }
    }
}