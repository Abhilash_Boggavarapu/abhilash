﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ETaxBO;

namespace ETaxDal
{
    public class TaxPayerDAL
    {
        public void InsertUpdateTaxPayer(BO_tblTaxPayerDetail pobjTaxPay, BO_tblTaxPayerDetail objTaxPayer, ref long? TaxPayerSpouseID)
        {
            try
            {
                using (ETaxEntities objTaxEntities = new ETaxEntities())
                {
                    tblTaxPayerDetail TaxPay = null;
                    if (objTaxPayer.CurrentTaxPayer == MasterItems.TaxPayerType.self)
                    {
                        TaxPay = (from s in objTaxEntities.tblTaxPayerDetails
                                  where s.TaxPayerID == objTaxPayer.TaxPayerID
                                  select s).FirstOrDefault();
                    }
                    else if (objTaxPayer.CurrentTaxPayer == MasterItems.TaxPayerType.Spouse)
                    {
                        if (objTaxPayer.TaxPayerSpouseID != null && objTaxPayer.TaxPayerSpouseID != 0)
                        {
                            TaxPay = (from s in objTaxEntities.tblTaxPayerDetails
                                      where s.TaxPayerID == objTaxPayer.TaxPayerSpouseID
                                      select s).FirstOrDefault();
                        }
                        else
                        {
                            TaxPay = new tblTaxPayerDetail();
                        }
                    }

                    //  tblTaxPayerDetail TaxPay = new tblTaxPayerDetail();                   
                    TaxPay.UserID = pobjTaxPay.UserID;
                    TaxPay.PayerType = pobjTaxPay.PayerType;
                    TaxPay.FirstName = pobjTaxPay.FirstName;
                    TaxPay.MiddleName = pobjTaxPay.MiddleName;
                    TaxPay.LastName = pobjTaxPay.LastName;
                    TaxPay.DOB = pobjTaxPay.DOB;
                    TaxPay.Gender = pobjTaxPay.Gender;
                    TaxPay.SSN = pobjTaxPay.SSN;
                    TaxPay.MaritalStatusID = pobjTaxPay.MaritalStatusID;
                    TaxPay.Occupation = pobjTaxPay.Occupation;
                    TaxPay.StreetNo = pobjTaxPay.StreetNo;
                    TaxPay.ApartmentNo = pobjTaxPay.ApartmentNo;
                    TaxPay.CountryID = pobjTaxPay.CountryID;
                    TaxPay.CityID = pobjTaxPay.CityID;
                    TaxPay.StateID = pobjTaxPay.StateID;
                    TaxPay.ZipCode = pobjTaxPay.ZipCode;
                    TaxPay.MobileNumber = pobjTaxPay.MobileNumber;
                    TaxPay.HomeNumber = pobjTaxPay.HomeNumber;
                    TaxPay.LandlineNumber = pobjTaxPay.LandlineNumber;
                    TaxPay.PrimaryEMailID = pobjTaxPay.PrimaryEMailID;
                    TaxPay.SecondaryEMailID = pobjTaxPay.SecondaryEMailID;
                    if (pobjTaxPay.VisaStatusID != 0)
                    {
                        TaxPay.VisaStatusID = pobjTaxPay.VisaStatusID;
                    }
                    TaxPay.USEntryDate = pobjTaxPay.USEntryDate;
                    TaxPay.HasDependants = pobjTaxPay.HasDependants;
                    TaxPay.CreatedOn = System.DateTime.Now;

                    if (objTaxPayer.CurrentTaxPayer == MasterItems.TaxPayerType.self)
                    {
                        TaxPay.TaxPayerID = objTaxPayer.TaxPayerID;
                        objTaxEntities.SaveChanges();
                    }
                    else if (objTaxPayer.CurrentTaxPayer == MasterItems.TaxPayerType.Spouse)
                    {
                        if (objTaxPayer.TaxPayerSpouseID != null && objTaxPayer.TaxPayerSpouseID != 0)
                        {
                            TaxPay.TaxPayerID = (int)objTaxPayer.TaxPayerSpouseID;
                        }
                        else
                        {
                            objTaxEntities.tblTaxPayerDetails.AddObject(TaxPay);

                        }
                        objTaxEntities.SaveChanges();
                        TaxPayerSpouseID = TaxPay.TaxPayerID;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        // to get taxpayerdeails with particular id
        public BO_tblTaxPayerDetail GetTaxpayerDeails(long taxpayerId)
        {
            try
            {
                using (ETaxEntities objETaxEntites = new ETaxEntities())
                {
                    objETaxEntites.Connection.Open();
                    BO_tblTaxPayerDetail objTaxpayerDeails = (from n in objETaxEntites.tblTaxPayerDetails
                                                              where n.TaxPayerID == taxpayerId
                                                              select new BO_tblTaxPayerDetail
                                                              {

                                                                  FirstName = n.FirstName,
                                                                  LastName = n.LastName,
                                                                  MiddleName = n.MiddleName,
                                                                  DOB = n.DOB,
                                                                  SSN = n.SSN,
                                                                  MaritalStatusID = n.MaritalStatusID,
                                                                  MaritalStatus = n.tblMaritalStatus.MaritalStatus,
                                                                  Occupation = n.Occupation,
                                                                  StreetNo = n.StreetNo,
                                                                  ApartmentNo = n.ApartmentNo,
                                                                  Gender = n.Gender,
                                                                  CountryName = n.tblCountry.Country,
                                                                  CountryID = n.CountryID,
                                                                  CityName = n.tblCity.City,
                                                                  CityID = n.CityID,
                                                                  StateName = n.tblState.StateName,
                                                                  StateID = n.StateID,
                                                                  ZipCode = n.ZipCode,
                                                                  MobileNumber = n.MobileNumber,
                                                                  HomeNumber = n.HomeNumber,
                                                                  LandlineNumber = n.LandlineNumber,
                                                                  PrimaryEMailID = n.PrimaryEMailID,
                                                                  SecondaryEMailID = n.SecondaryEMailID,
                                                                  VisaStatus = n.tblVisaStatus.VisaStatus,
                                                                  VisaStatusID = n.VisaStatusID,
                                                                  USEntryDate = n.USEntryDate,
                                                                  HasDependants = n.HasDependants,
                                                                  CreatedOn = n.CreatedOn,
                                                              }).First();
                    return objTaxpayerDeails;

                }

            }
            catch (Exception ex)
            {
                throw ex;

            }
        }
        //to get gender
        public string getGender(long taxpayerId)
        {
            try
            {
                using (ETaxEntities objETaxEntites = new ETaxEntities())
                {
                    objETaxEntites.Connection.Open();
                    string gender = (from n in objETaxEntites.tblTaxPayerDetails
                                     where n.TaxPayerID == taxpayerId
                                     select n.Gender).FirstOrDefault();
                    return gender;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblTaxPayerDetail objtblTaxPayerDetails { get; set; }
        public List<BO_tblTaxPayerDetail> SearchTaxPayer(long taxPayerID,string Name, string EmailID, DateTime? DOB,int payerType)
        {
            try
            {
                int EFilingCompleted = Convert.ToInt16(MasterItems.TaxFillingStatusTypes.EFilingCompleted);
                int TaxPreparationCompleted = Convert.ToInt16(MasterItems.TaxFillingStatusTypes.TaxPreparationPending);
                int CompletedClients = Convert.ToInt16(MasterItems.TaxFillingStatusTypes.CompletedClients);
                using (ETaxEntities objETaxEntites = new ETaxEntities())
                {
                    objETaxEntites.Connection.Open();
                    List<BO_tblTaxPayerDetail> Search = (from n in objETaxEntites.tblTaxPayerDetails

                                                         where (n.TaxPayerID == taxPayerID ||n.FirstName == Name || n.tblUser.EmailID == EmailID || (DOB != null && n.DOB == DOB))
                                                         && n.PayerType==payerType
                                              
                                                         select new BO_tblTaxPayerDetail
                                                         {
                                                             TaxPayerID = n.TaxPayerID,
                                                             EmailId=n.tblUser.EmailID,
                                                             UserID = n.UserID,
                                                             FirstName = n.FirstName,
                                                             LastName = n.LastName,
                                                             MiddleName = n.MiddleName,
                                                             DOB = n.DOB,
                                                             SSN = n.SSN,
                                                             MaritalStatus = n.tblMaritalStatus.MaritalStatus,
                                                             Occupation = n.Occupation,
                                                             StreetNo = n.StreetNo,
                                                             ApartmentNo = n.ApartmentNo,
                                                             CityName = n.tblCity.City,
                                                             StateName = n.tblState.StateName,
                                                             //   CountryName = n.tblc,
                                                             MobileNumber = n.MobileNumber,
                                                             HomeNumber = n.HomeNumber,
                                                             LandlineNumber = n.LandlineNumber,
                                                             PrimaryEMailID = n.PrimaryEMailID,
                                                             SecondaryEMailID = n.SecondaryEMailID,
                                                             VisaStatus = n.tblVisaStatus.VisaStatus,
                                                             USEntryDate = n.USEntryDate,
                                                             HasDependants = n.HasDependants,
                                                             CreatedOn = n.CreatedOn,
                                                             TaxPreparationCompletedEnabled = (from a in objETaxEntites.TaxPayerTaxFilingStatusDetails
                                                                                               where a.TaxPayerID==n.TaxPayerID && a.TaxFilingStatusTypeID == TaxPreparationCompleted
                                                                                               select a.TaxPayerID).Count() == 0 ? true : false,
                                                             EFilingCompletedEnabled = (from a in objETaxEntites.TaxPayerTaxFilingStatusDetails
                                                                                        where a.TaxPayerID == n.TaxPayerID && a.TaxFilingStatusTypeID == EFilingCompleted
                                                                                        select a.TaxPayerID).Count() == 0 ? true : false,
                                                             CompletedEnabled = (from a in objETaxEntites.TaxPayerTaxFilingStatusDetails
                                                                                 where a.TaxPayerID == n.TaxPayerID && a.TaxFilingStatusTypeID == CompletedClients
                                                                                 select a.TaxPayerID).Count() == 0 ? true : false,

                                                         }).ToList();
                    return Search;

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

}

