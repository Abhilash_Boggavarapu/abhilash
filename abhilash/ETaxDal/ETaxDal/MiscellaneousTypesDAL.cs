﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ETaxDal;
using ETaxBO;

namespace ETaxDal
{
   public class MiscellaneousTypesDAL
    {
       public int SaveMiscellaneous(BO_tblMiscellaneousType pobjMisc)
       {
           try
           {

               using (ETaxEntities objTaxEntities = new ETaxEntities())
               {
                   objTaxEntities.Connection.Open();
                   tblMiscellaneousType objMisc = new tblMiscellaneousType();
                   objMisc.MiscellaneousType = pobjMisc.MiscellaneousType;
                   objMisc.CreatedOn = DateTime.Now;
                   objTaxEntities.tblMiscellaneousTypes.AddObject(objMisc);
                   objTaxEntities.SaveChanges();
                   return 1;
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }

       }
       //Get MiscellaneousType
       public List<BO_tblMiscellaneousType> GetMiscellaneousType()
       {
           try
           {
               using (ETaxEntities objETaxEntities = new ETaxEntities())
               {
                   objETaxEntities.Connection.Open();
                   List<BO_tblMiscellaneousType> objMiscellaneousType = (from n in objETaxEntities.tblMiscellaneousTypes

                                                                         select new BO_tblMiscellaneousType

                                                                         {

                                                                             MiscellaneousTypeID = n.MiscellaneousTypeID,
                                                                             MiscellaneousType = n.MiscellaneousType,

                                                                         }).ToList();


                   return objMiscellaneousType;
               }

           }
           catch (Exception ex)
           {
               throw ex;
           }
       }
    }
}
