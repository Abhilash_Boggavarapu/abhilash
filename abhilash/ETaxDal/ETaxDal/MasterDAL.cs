﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ETaxBO;
using System.Data.Common;
using System.Transactions;


namespace ETaxDal
{
    public class MasterDAL
    {
        //to get UserId,RoleId,FirstName
        public BO_tblTaxPayerDetail Checklogin(string EmailId, string Password, ETaxBO.MasterItems.Roles Role)
        {
            try
            {
                int role = (int)Role;
                using (ETaxEntities objETaxEntites = new ETaxEntities())
                {
                    int Spouse = (int)MasterItems.TaxPayerType.Spouse;
                    objETaxEntites.Connection.Open();

                    BO_tblTaxPayerDetail objTaxPayerObj = (from taxpayer in objETaxEntites.tblTaxPayerDetails
                                                           where taxpayer.tblUser.EmailID == EmailId &&
                                                            taxpayer.tblUser.Password == Password &&
                                                            taxpayer.tblUser.RoleID == role
                                                           select new BO_tblTaxPayerDetail
                                                           {
                                                               UserID = taxpayer.UserID,
                                                               TaxPayerID = taxpayer.TaxPayerID,
                                                               TaxPayerSpouseID = (from a in objETaxEntites.tblTaxPayerDetails
                                                                                   where a.UserID == taxpayer.UserID &&
                                                                                   a.PayerType == Spouse
                                                                                   select a.TaxPayerID).FirstOrDefault(),
                                                               UserFirstName = taxpayer.tblUser.FirstName,
                                                               ConsentFormStatus = taxpayer.tblUser.ConsentFormStatus
                                                           }).FirstOrDefault();
                    if (objTaxPayerObj != null)
                        objTaxPayerObj.CurrentTaxPayer = MasterItems.TaxPayerType.self;
                    return objTaxPayerObj;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //to save the user details
        public int saveUserDetails(BO_tblUser pObjUser)
        {
            try
            {
               using (TransactionScope ts = new TransactionScope())
                {
                    using (ETaxEntities objEtaxEntites = new ETaxEntities())
                    {
                        tblUser objUser = new tblUser();
                        objUser.RoleID = pObjUser.RoleID;
                        objUser.EmailID = pObjUser.EmailID;
                        objUser.Password = pObjUser.Password;
                        objUser.FirstName = pObjUser.FirstName;
                        objUser.LastName = pObjUser.LastName;
                        objUser.Gender = pObjUser.Gender;
                        objUser.ZipCode = pObjUser.ZipCode;
                        objUser.CountryID = pObjUser.CountryID;
                        objUser.CityID = pObjUser.CityID;
                        objUser.StateID = pObjUser.StateID;
                        objUser.MobileNumber = pObjUser.MobileNumber;
                        objUser.HomeNumber = pObjUser.HomeNumber;
                        objUser.WorkNubmer = pObjUser.WorkNubmer;
                        objUser.CreatedOn = System.DateTime.Now;
                        
                        //changed by deepu-to hide the consent form
                        objUser.ConsentFormStatus = true;

                        objEtaxEntites.tblUsers.AddObject(objUser);

                        objEtaxEntites.SaveChanges();
                        tblTaxPayerDetail objTaxPayerObj = new tblTaxPayerDetail()
                        {
                            UserID = objUser.UserID,
                            PayerType = Convert.ToInt32(MasterItems.TaxPayerType.self),
                            FirstName = string.Empty,
                            MiddleName = string.Empty,
                            LastName = string.Empty,
                            DOB = System.DateTime.Now,
                            Gender = pObjUser.Gender,
                            //ZipCode = pObjUser.ZipCode,
                            ZipCode =string.Empty,
                            SSN = string.Empty,
                            MaritalStatusID = Convert.ToInt32(MasterItems.MaritalStatuses.Single),
                            Occupation = string.Empty,
                            StreetNo = string.Empty,
                            ApartmentNo = string.Empty,
                            CountryID = pObjUser.CountryID,
                            CityID = pObjUser.CityID,
                            StateID = pObjUser.StateID,
                            MobileNumber = string.Empty,
                            HomeNumber = string.Empty,
                            LandlineNumber = string.Empty,
                            PrimaryEMailID = string.Empty,
                            SecondaryEMailID = string.Empty,
                         //   VisaStatusID = Convert.ToInt32(MasterItems.VisaStatuses.H1),
                            VisaStatusID = null,
                            USEntryDate = System.DateTime.Now,
                            CreatedOn = System.DateTime.Now
                        };
                        objEtaxEntites.tblTaxPayerDetails.AddObject(objTaxPayerObj);
                        objEtaxEntites.SaveChanges();
                        ts.Complete();
                        return 1;
                    }
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string checkEmailId(string emailId)
        {
            try
            {
                using (ETaxEntities objEtaxEntities = new ETaxEntities())
                {
                    string EmailId = (from tblUser in objEtaxEntities.tblUsers
                                      where tblUser.EmailID == emailId
                                      select tblUser.EmailID).FirstOrDefault();
                    return EmailId;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        //to get taxpayer id by using Emailid
        public long GetUseridbyMailid(string emailId)
        {
            try
            {
                using (ETaxEntities objEtaxEntities = new ETaxEntities())
                {
                    long Userid = (from tblUser in objEtaxEntities.tblUsers
                                   where tblUser.EmailID == emailId
                                   select tblUser.UserID).FirstOrDefault();
                    return Userid;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        // get userid by taxpayerid
        public long GetuserIDbyTaxpayerId(long TaxPayerID)
        {
            try
            {
                using (ETaxEntities objEtaxEntities = new ETaxEntities())
                {

                    long UserID = (from s in objEtaxEntities.tblTaxPayerDetails

                                       where s.TaxPayerID  == TaxPayerID 
                                       select s.UserID ).FirstOrDefault();
                    return UserID;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

          

        }
        //to get taxpayer id by using userid
        public long GetTaxpayerID(long UserId, int payerType)
        {
            try
            {
                using (ETaxEntities objEtaxEntities = new ETaxEntities())
                {

                    long TaxPayerID = (from s in objEtaxEntities.tblTaxPayerDetails

                                       where s.tblUser.UserID == UserId && s.PayerType == payerType
                                       select s.TaxPayerID).FirstOrDefault();
                    return TaxPayerID;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public long GetTaxpayerID(long UserId)
        {
            using (ETaxEntities objEtaxEntities = new ETaxEntities())
            {
                try
                {
                    long TaxPayerID = (from s in objEtaxEntities.tblTaxPayerDetails

                                       where s.tblUser.UserID == UserId
                                       select s.TaxPayerID).FirstOrDefault();
                    return TaxPayerID;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public List<BO_tblVisaStatus> getVisaStatus()
        {
            try
            {

                using (ETaxEntities objETaxEntites = new ETaxEntities())
                {
                    objETaxEntites.Connection.Open();
                    List<BO_tblVisaStatus> objVisaStatus = (from tblVisaStatus in objETaxEntites.tblVisaStatuses
                                                            select new BO_tblVisaStatus
                                                            {
                                                                VisaStatusID = tblVisaStatus.VisaStatusID,
                                                                VisaStatus = tblVisaStatus.VisaStatus,
                                                            }).ToList();
                    return objVisaStatus;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public BO_tblVisaStatus GetVisa(int VisaStatusID)
        {
            try
            {
                using (ETaxEntities objETaxEntities = new ETaxEntities())
                {
                    objETaxEntities.Connection.Open();
                    BO_tblVisaStatus objvisa = (from n in objETaxEntities.tblVisaStatuses
                                                where n.VisaStatusID == VisaStatusID
                                                select new BO_tblVisaStatus
                                                {
                                                    VisaStatusID = n.VisaStatusID,
                                                    VisaStatus = n.VisaStatus,
                                                }).SingleOrDefault();
                    return objvisa;

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }



        //to get the Employee Details
        public List<BO_tblTaxPayerEmployerDetail> getEmployeerDetails(long taxPayerID)
        {
            try
            {

                using (ETaxEntities objETaxEntites = new ETaxEntities())
                {
                    List<BO_tblTaxPayerEmployerDetail> objtaxPayerEmployeeDetails = (from n in objETaxEntites.tblTaxPayerEmployerDetails
                                                                                     where n.TaxPayerID == taxPayerID
                                                                                     select
                                                                                     new BO_tblTaxPayerEmployerDetail
                                                                                     {
                                                                                         EmployerName = n.EmployerName,
                                                                                         TaxPayerEmployerID = n.TaxPayerEmployerID,
                                                                                     }).ToList();

                    return objtaxPayerEmployeeDetails;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public int SaveMaritalStatuses(BO_tblMaritalStatus pobjMarital)
        {

            try
            {
                using (ETaxEntities objTaxEntities = new ETaxEntities())
                {
                    tblMaritalStatus objMarital = new tblMaritalStatus();
                    objMarital.MaritalStatus = pobjMarital.MaritalStatus;
                    objMarital.CreatedOn = DateTime.Now;
                    objTaxEntities.tblMaritalStatuses.AddObject(objMarital);
                    objTaxEntities.SaveChanges();
                    return 1;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //to get the Maritalstatus
        public List<BO_tblMaritalStatus> getMaritalStatus()
        {
            try
            {
                using (ETaxEntities objETaxEntites = new ETaxEntities())
                {
                    objETaxEntites.Connection.Open();
                    List<BO_tblMaritalStatus> objMaritalStatus = (from tblMaritalStatus in objETaxEntites.tblMaritalStatuses
                                                                  select new BO_tblMaritalStatus
                                                                  {
                                                                      MaritalStatusID = tblMaritalStatus.MaritalStatusID,
                                                                      MaritalStatus = tblMaritalStatus.MaritalStatus,

                                                                  }).ToList();
                    return objMaritalStatus;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public BO_tblMaritalStatus GetMarstatus(int MaritalStatusID)
        {
            try
            {
                using (ETaxEntities objETaxEntities = new ETaxEntities())
                {
                    objETaxEntities.Connection.Open();
                    BO_tblMaritalStatus Objmar = (from n in objETaxEntities.tblMaritalStatuses
                                                  where n.MaritalStatusID == MaritalStatusID
                                                  select new BO_tblMaritalStatus
                                                  {
                                                      MaritalStatusID = n.MaritalStatusID,
                                                      MaritalStatus = n.MaritalStatus,
                                                  }).SingleOrDefault();
                    return Objmar;



                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }




        //to get the countries
        public List<BO_tblCountry> getCountries()
        {
            try
            {
                using (ETaxEntities objETaxEntites = new ETaxEntities())
                {
                    objETaxEntites.Connection.Open();
                    List<BO_tblCountry> objCountry = (from tblCountry in objETaxEntites.tblCountries
                                                      select new BO_tblCountry
                                                      {
                                                          CountryID = tblCountry.CountryID,
                                                          Country = tblCountry.Country,
                                                          CountrySmallName = tblCountry.CountrySmallName,
                                                      }).ToList();
                    return objCountry;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        // to get the states in each country
        public List<BO_tblState> getStates(int CountryId)
        {
            try
            {
                using (ETaxEntities objETaxEntites = new ETaxEntities())
                {
                    objETaxEntites.Connection.Open();
                    List<BO_tblState> objState = (from tblState in objETaxEntites.tblStates
                                                  where tblState.CountryID == CountryId
                                                  select new BO_tblState
                                                  {
                                                      StateID = tblState.StateID,
                                                      StateName = tblState.StateName,
                                                      StateSmallName = tblState.StateSmallName,
                                                  }).ToList();
                    return objState;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //to get the cities in each states
        public List<BO_tblCity> getCities(int StateId)
        {
            try
            {
                using (ETaxEntities objETaxEntites = new ETaxEntities())
                {
                    objETaxEntites.Connection.Open();
                    List<BO_tblCity> objCity = (from tblCity in objETaxEntites.tblCities
                                                where tblCity.StateID == StateId
                                                select new BO_tblCity
                                                {
                                                    CityID = tblCity.CityID,
                                                    City = tblCity.City,
                                                    CitySmallName = tblCity.CitySmallName,
                                                }).ToList();
                    return objCity;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //to get the relation ships
        public List<BO_tblRelationship> getRelationShips()
        {
            try
            {
                using (ETaxEntities objETaxEntites = new ETaxEntities())
                {
                    objETaxEntites.Connection.Open();
                    List<BO_tblRelationship> objRelationShips = (from tblRelationship in objETaxEntites.tblRelationships
                                                                 select new BO_tblRelationship
                                                                 {
                                                                     RelationShipID = tblRelationship.RelationShipID,
                                                                     RelationShip = tblRelationship.RelationShip,
                                                                     CreatedOn = tblRelationship.CreatedOn,
                                                                 }).ToList();
                    return objRelationShips;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public BO_tblUser CheckloginAdmin(string EmailId, string Password, ETaxBO.MasterItems.Roles Role)
        {
            try
            {
                int role = (int)Role;
                using (ETaxEntities objETaxEntites = new ETaxEntities())
                {
                    objETaxEntites.Connection.Open();
                    BO_tblUser objuser = (from n in objETaxEntites.tblUsers
                                          where n.EmailID == EmailId && n.Password == Password
                                          && n.RoleID == role
                                          //&& n.RoleID==RoleId

                                          select new BO_tblUser
                                          {

                                              UserID = n.UserID,
                                              RoleID = n.RoleID,
                                              FirstName = n.FirstName,
                                          }).SingleOrDefault();
                    return objuser;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //for checking matrial status
        public string checkMatrialStatus(long taxPayerId)
        {
            try
            {
                using (ETaxEntities objETaxEntites = new ETaxEntities())
                {
                    objETaxEntites.Connection.Open();
                    string MatrialStatus = (from n in objETaxEntites.tblTaxPayerDetails
                                            where n.TaxPayerID == taxPayerId
                                            select n.tblMaritalStatus.MaritalStatus).SingleOrDefault();


                    return MatrialStatus;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public BO_TaxSummary getTaxSummary(long taxPayerID)
        {
            try
            {
              using (ETaxEntities objETaxEntites = new ETaxEntities())
                {
                    BO_TaxSummary objtaxSummary = (from n in objETaxEntites.tblTaxSummaries
                                                   where n.TaxPayerID == taxPayerID
                                                   select new BO_TaxSummary
                                                   {
                                                       TaxSummary=n.TaxSummary
                                                   }).SingleOrDefault();
                    return objtaxSummary;
              }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int insertCity(BO_tblCity pobjCity)
        {
            try
            {
                using (ETaxEntities objETaxEntites = new ETaxEntities())
                {
                    int CityID = (from n in objETaxEntites.tblCities where n.City == pobjCity.City && n.StateID == pobjCity.StateID select n.CityID).FirstOrDefault();
                    if (CityID ==0)
                    {
                        tblCity objCity = new tblCity();
                        objCity.City = pobjCity.City;
                        objCity.CountryID = pobjCity.CountryID;
                        objCity.StateID = pobjCity.StateID;
                        objCity.CreatedOn = System.DateTime.Now;
                        objETaxEntites.tblCities.AddObject(objCity);
                        objETaxEntites.SaveChanges();
                        CityID = objCity.CityID;
                    }
                    return CityID;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int getCityId(string cityname)
        {
            try
            {
                using (ETaxEntities objETaxEntites = new ETaxEntities())
                {
                    objETaxEntites.Connection.Open();
                    int CityID = (from n in objETaxEntites.tblCities
                                  where n.City == cityname
                                  select n.CityID).FirstOrDefault();
                    return CityID;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
