﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ETaxBO;

namespace ETaxDal
{
    public class ChangeSettingsDAL
    {
        public string GetUseremailid(long UserId)
        {
            try
            {
                using (ETaxEntities objETaxEntities = new ETaxEntities())
                {
                    objETaxEntities.Connection.Open();

                    var x = (from n in objETaxEntities.tblUsers
                             where n.UserID == UserId
                             select
                                 n.EmailID).SingleOrDefault();

                    return x.ToString();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public int EditEmailId(long UserId, BO_tblUser pObjUser)
        {
            try
            {
                using (ETaxEntities objETaxEntities = new ETaxEntities())
                {
                    objETaxEntities.Connection.Open();


                    tblUser objUser = (from n in objETaxEntities.tblUsers
                                       where n.UserID == UserId
                                       select n).First();
                    objUser.EmailID = pObjUser.EmailID;
                    objETaxEntities.SaveChanges();
                }
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetPassword(long UserId)
        {
            try
            {
                using (ETaxEntities objETaxEntities = new ETaxEntities())
                {
                    objETaxEntities.Connection.Open();
                    var y = (from r in objETaxEntities.tblUsers
                             where r.UserID == UserId
                             select
                              r.Password).SingleOrDefault();

                    return y.ToString();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public int UpdateNewPassword(long UserId, BO_tblUser pObjUser)
        {
            try
            {
                using (ETaxEntities objETaxEntities = new ETaxEntities())
                {
                    objETaxEntities.Connection.Open();
                    tblUser objUser = (from n in objETaxEntities.tblUsers
                                       where n.UserID == UserId
                                       select n).First();
                    objUser.Password = pObjUser.Password;
                    objETaxEntities.SaveChanges();
                }
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //for reset the password
        public int ResetNewPassword(string emailID, BO_tblUser pObjUser)
        {
            try
            {
                using (ETaxEntities objETaxEntities = new ETaxEntities())
                {
                    objETaxEntities.Connection.Open();
                    tblUser objUser = (from tblUser in objETaxEntities.tblUsers
                                       where tblUser.EmailID == emailID
                                       select tblUser).FirstOrDefault();
                    objUser.Password = pObjUser.Password;
                    objETaxEntities.SaveChanges();
                }
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string UpdateMobNo(long UserId, BO_tblUser pObjUser)
        {
            try
            {
                using (ETaxEntities objETaxEntities = new ETaxEntities())
                {
                    objETaxEntities.Connection.Open();
                    tblUser objUser = (from n in objETaxEntities.tblUsers
                                       where n.UserID == UserId
                                       select n).First();
                    objUser.MobileNumber = pObjUser.MobileNumber;
                    objUser.HomeNumber = pObjUser.HomeNumber;
                    objETaxEntities.SaveChanges();

                }
                return "Mobile Number Updated Successfully";
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        //get emailid to match new emailid
        public string GetEmailId(long UserID, string EmailId, BO_tblUser pObjUser)
        {
            try
            {
                using (ETaxEntities objETaxEntities = new ETaxEntities())
                {

                    tblUser objUser = (from n in objETaxEntities.tblUsers
                                       where n.UserID == UserID && n.EmailID == EmailId
                                       select n).First();
                   if(objUser!=null)
                   {
                       objUser.ToString();
                   }
                    else
                   {


                     
                        objUser.EmailID = pObjUser.EmailID;
                        objETaxEntities.SaveChanges();

                    }
                }
                return EmailId;
            }
            
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
        
            
        

