﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ETaxBO;
using ETaxDal;
namespace ETaxDal
{
   public class ResidentDetailsDAL
    {
        // To get TaxpayerResidencyDetailsID using TaxPayerID 
        public long TaxPayerResidencyDetailsID(long TaxPayerID)
        {
            try
            {
                using (ETaxEntities objETaxEntities = new ETaxEntities())
                {
                    objETaxEntities.Connection.Open();
                    long TaxPayerResidencyDetailsID = (from s in objETaxEntities.tblTaxPayerResidencyDetails
                                                       where s.tblTaxPayerDetail.TaxPayerID == TaxPayerID
                                                       select s.TaxPayerResidencyDetailsID).FirstOrDefault();


                    return TaxPayerResidencyDetailsID;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public long insertOrUpdateResidencyDetails(BO_tblTaxPayerResidencyDetail pobjPayerResidencyDetail)
        {
            try
            {
                using (ETaxEntities objETaxEntities = new ETaxEntities())
                {
                    objETaxEntities.Connection.Open();
                    tblTaxPayerResidencyDetail objPayerResidencyDetail = (from residencyDetail in objETaxEntities.tblTaxPayerResidencyDetails
                                                                          where residencyDetail.TaxPayerResidencyDetailsID == pobjPayerResidencyDetail.TaxPayerResidencyDetailsID
                                                                          select residencyDetail).FirstOrDefault();
                    if (objPayerResidencyDetail != null)
                    {
                        objPayerResidencyDetail.TaxPayerID = pobjPayerResidencyDetail.TaxPayerID;
                        objPayerResidencyDetail.StateName = pobjPayerResidencyDetail.StateName;
                        objPayerResidencyDetail.CityName = pobjPayerResidencyDetail.CityName;
                        objPayerResidencyDetail.StartDate = pobjPayerResidencyDetail.StartDate;
                        objPayerResidencyDetail.EndDate = pobjPayerResidencyDetail.EndDate;
                        objPayerResidencyDetail.CreatedOn = System.DateTime.Now;
                    }
                    else
                    {
                        objPayerResidencyDetail = new tblTaxPayerResidencyDetail();
                        objPayerResidencyDetail.TaxPayerID = pobjPayerResidencyDetail.TaxPayerID;
                        objPayerResidencyDetail.StateName = pobjPayerResidencyDetail.StateName;
                        objPayerResidencyDetail.CityName = pobjPayerResidencyDetail.CityName;
                        objPayerResidencyDetail.StartDate = pobjPayerResidencyDetail.StartDate;
                        objPayerResidencyDetail.EndDate = pobjPayerResidencyDetail.EndDate;
                        objPayerResidencyDetail.CreatedOn = System.DateTime.Now;
                        objETaxEntities.tblTaxPayerResidencyDetails.AddObject(objPayerResidencyDetail);
                    }
                    objETaxEntities.SaveChanges();
                }
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<BO_tblTaxPayerResidencyDetail> getResidencyDetails(long taxPayerId)
        {
            using (ETaxEntities objEtaxEntities = new ETaxEntities())
            {
                try
                {
                    List<BO_tblTaxPayerResidencyDetail> objtblTaxPayerResidencyDetail = (from residencyDetail in objEtaxEntities.tblTaxPayerResidencyDetails
                                                                                         where residencyDetail.TaxPayerID == taxPayerId
                                                                                         select new BO_tblTaxPayerResidencyDetail
                                                                                         {
                                                                                             TaxPayerResidencyDetailsID = residencyDetail.TaxPayerResidencyDetailsID,
                                                                                             CityName = residencyDetail.CityName,
                                                                                             StateName = residencyDetail.StateName,
                                                                                             StartDate = (DateTime)residencyDetail.StartDate,
                                                                                             EndDate = (DateTime)residencyDetail.EndDate

                                                                                         }).ToList();
                    return objtblTaxPayerResidencyDetail;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public BO_tblTaxPayerResidencyDetail getResidencySingleRecord(long taxPayerResidencyDetailsID)
        {
            using (ETaxEntities objEtaxEntities = new ETaxEntities())
            {
                try
                {
                    BO_tblTaxPayerResidencyDetail objtblTaxPayerResidencyDetail = (from residencyDetail in objEtaxEntities.tblTaxPayerResidencyDetails
                                                                                   where residencyDetail.TaxPayerResidencyDetailsID == taxPayerResidencyDetailsID
                                                                                   select new BO_tblTaxPayerResidencyDetail
                                                                                   {
                                                                                       TaxPayerID=residencyDetail.TaxPayerID,
                                                                                       CityName = residencyDetail.CityName,
                                                                                       StateName = residencyDetail.StateName,
                                                                                       StartDate = (DateTime)residencyDetail.StartDate,
                                                                                       EndDate = (DateTime)residencyDetail.EndDate

                                                                                   }).FirstOrDefault();
                    return objtblTaxPayerResidencyDetail;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
}
