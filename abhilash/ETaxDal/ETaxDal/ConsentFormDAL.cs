﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ETaxDal;
using ETaxBO;

namespace ETaxDal
{
    public class ConsentFormDAL
    {
        public void SaveConsentForm(BO_tblConsentForm pobjConsent)
        {
            try
            {
                int ConsentFormID = (int)MasterItems.ConsentForm.ConsentFormID;
                bool IsNew = false;
                using (ETaxEntities objTaxEntities = new ETaxEntities())
                {
                    tblConsentForm objConsent = (from a in objTaxEntities.tblConsentForms where a.ConsentFormID == ConsentFormID select a).FirstOrDefault();
                    if (objConsent == null)
                    {
                        IsNew = true;
                        objConsent = new tblConsentForm();
                        objConsent.ConsentFormID =ConsentFormID;
                    }
                    objConsent.Description1 = pobjConsent.Description1;
                    objConsent.Description2 = pobjConsent.Description2;
                    objConsent.Description3 = pobjConsent.Description3;
                    objConsent.Description4 = pobjConsent.Description4;

                    if (!IsNew)
                    {
                        objConsent.UpdatedOn = System.DateTime.Now;
                    }
                    else if (IsNew)
                    {
                        objConsent.CreatedOn = System.DateTime.Now;
                        objConsent.UpdatedOn = System.DateTime.Now;
                        objTaxEntities.tblConsentForms.AddObject(objConsent);
                    }
                    objTaxEntities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tblConsentForm GetConsentForm( )
        {
            try
            {
                int ConsentFormID = (int)MasterItems.ConsentForm.ConsentFormID;
                tblConsentForm objConsent;
                using (ETaxEntities objTaxEntities = new ETaxEntities())
                {
                    objConsent = (from a in objTaxEntities.tblConsentForms where a.ConsentFormID == ConsentFormID select a).FirstOrDefault();
                }
                return objConsent;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
