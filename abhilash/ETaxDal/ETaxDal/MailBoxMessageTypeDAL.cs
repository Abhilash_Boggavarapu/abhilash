﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ETaxBO;
using ETaxDal;

namespace ETaxDal
{
    public class MailBoxMessageTypeDAL
    {
        public int InsertUpdateMailBoxMessageType(BO_tblMailBoxMessageType pobjMessage)
        {

            try
            {
                using (ETaxEntities objTaxEntities = new ETaxEntities())
                {
                    var existMailBoxType = from A in objTaxEntities.tblMailBoxMessageTypes
                                           where A.MessageTypeID == pobjMessage.MessageTypeID
                                           select A;
                    if (existMailBoxType.Any())
                    {
                        var MailBoxType = existMailBoxType.FirstOrDefault();
                        MailBoxType.MessageType = pobjMessage.MessageType;
                    }
                    else
                    {
                        tblMailBoxMessage objMessage = new tblMailBoxMessage();
                        objMessage.Message = pobjMessage.MessageType;
                        objMessage.CreatedOn = DateTime.Now;
                        objTaxEntities.tblMailBoxMessages.AddObject(objMessage);
                    }
                    objTaxEntities.SaveChanges();
                    return 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<BO_tblMailBoxMessageType> GetMailBoxMessageTypes()
        {
            try
            {
                using (ETaxEntities objETaxEntities = new ETaxEntities())
                {
                    objETaxEntities.Connection.Open();
                    List<BO_tblMailBoxMessageType> objMailBoxTypeDetails = (from s in objETaxEntities.tblMailBoxMessageTypes

                                                                      select new BO_tblMailBoxMessageType
                                                                      {
                                                                          MessageTypeID=s.MessageTypeID,
                                                                          MessageType=s.MessageType
                                                             }).ToList();

                    return objMailBoxTypeDetails;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public BO_tblMailBoxMessageType GetMailBoxMessageType(int MessageTypeID)
          {
              try
              {
                  using (ETaxEntities objETaxEntities = new ETaxEntities())
                  {
                      objETaxEntities.Connection.Open();
                      BO_tblMailBoxMessageType objMailBoxTypeDetails = (from n in objETaxEntities.tblMailBoxMessageTypes
                                                         where n.MessageTypeID == MessageTypeID


                                                                  select new BO_tblMailBoxMessageType

                                                         {

                                                             MessageTypeID = n.MessageTypeID,
                                                             MessageType = n.MessageType
                                                         }).SingleOrDefault();

                      return objMailBoxTypeDetails;

                  }
              }
              catch (Exception ex)
              {
                  throw ex;
              }
          }
    }
}
