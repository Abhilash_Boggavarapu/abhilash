﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ETaxBO;
using ETaxDal;

namespace ETaxDal
{
    public class MaritalStatusesDAL
    {
        public int SaveUpdateMaritalStatuses(BO_tblMaritalStatus pobjMarital)
        {

            try
            {
                using (ETaxEntities objTaxEntities = new ETaxEntities())
                {
                  
                  
                    tblMaritalStatus objMarital = (from n in objTaxEntities.tblMaritalStatuses
                                                   where n.MaritalStatusID == pobjMarital.MaritalStatusID
                                              select n).FirstOrDefault();
                    if (objMarital != null)
                    {
                        objMarital.MaritalStatus = pobjMarital.MaritalStatus;
                        objMarital.CreatedOn = DateTime.Now;
                    }
                    else
                    {
                        objMarital.MaritalStatus = pobjMarital.MaritalStatus;
                        objMarital.CreatedOn = DateTime.Now;
                        objTaxEntities.tblMaritalStatuses.AddObject(objMarital);
                    }
                    objTaxEntities.SaveChanges();
                    return 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
