﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ETaxBO;

namespace ETaxDal
{
    public class ExpencesTypeDAL
    {
        public int InsertUpdateExpence(BO_tblExpencesType pobjExpence)
        {
            try
            {
                using (ETaxEntities objTaxEntities = new ETaxEntities())
                {
                    var existExpence = from E in objTaxEntities.tblExpencesTypes
                                       where E.ExpenseTypeID == pobjExpence.ExpenseTypeID
                                       select E;
                    if (existExpence.Any())
                    {
                        var Expence = existExpence.FirstOrDefault();
                        Expence.ExpenseType = pobjExpence.ExpenseType;
                    }
                    else
                    {
                        tblExpencesType objExpence = new tblExpencesType();
                        objExpence.ExpenseType = pobjExpence.ExpenseType;
                        objExpence.CreatedOn = DateTime.Now;
                        objTaxEntities.tblExpencesTypes.AddObject(objExpence);

                    }
                    objTaxEntities.SaveChanges();
                    return 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //Get Expences
        public List<BO_tblExpencesType> GetExpences()
        {
            try
            {
                using (ETaxEntities objETaxEntities = new ETaxEntities())
                {
                    List<BO_tblExpencesType> objExpences = (from n in objETaxEntities.tblExpencesTypes

                                                            select new BO_tblExpencesType

                                                            {
                                                                ExpenseTypeID = n.ExpenseTypeID,
                                                                ExpenseType = n.ExpenseType,


                                                            }).ToList();


                    return objExpences;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public BO_tblExpencesType GetExpence(int ExpenceId)
        {
            try
            {
                using (ETaxEntities objETaxEntities = new ETaxEntities())
                {
                    objETaxEntities.Connection.Open();
                    BO_tblExpencesType objExpences = (from n in objETaxEntities.tblExpencesTypes
                                                      where n.ExpenseTypeID==ExpenceId

                                                      select new BO_tblExpencesType

                                                      {
                                                          ExpenseTypeID = n.ExpenseTypeID,
                                                          ExpenseType = n.ExpenseType,


                                                      }).SingleOrDefault();


                    return objExpences;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
