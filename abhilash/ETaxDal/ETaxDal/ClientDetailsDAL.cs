﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ETaxBO;
using ETaxDal;

namespace ETaxDal
{
    public class ClientDetailsDAL
    {
        public int InsertUpdateClientInformation(BO_tblTaxPayerClientDetail pobjClient)
        {
            try
            {
                using (ETaxEntities objTaxEntities = new ETaxEntities())
                {
                    objTaxEntities.Connection.Open();

                    tblTaxPayerClientDetail objTaxPayerClientDetails = (from s in objTaxEntities.tblTaxPayerClientDetails
                                                                        where s.TaxPayerClientID == pobjClient.TaxPayerClientID
                                                                        select s).FirstOrDefault();
                    if (objTaxPayerClientDetails != null)
                    {
                        objTaxPayerClientDetails.TaxPayerID = pobjClient.TaxPayerID;
                        //objTaxPayerClientDetails.TaxPayerEmployerID = pobjClient.EmployerID;
                        objTaxPayerClientDetails.ClientName = pobjClient.ClientName;
                        objTaxPayerClientDetails.ProjectStartDate = pobjClient.ProjectStartDate;
                        objTaxPayerClientDetails.ProjectEndDate = pobjClient.ProjectEndDate;
                        objTaxPayerClientDetails.CountryID = pobjClient.CountryID;
                        objTaxPayerClientDetails.StateID = pobjClient.StateID;
                        objTaxPayerClientDetails.CityID = pobjClient.CityID;
                    }
                    else
                    {
                        objTaxPayerClientDetails = new tblTaxPayerClientDetail();
                        objTaxPayerClientDetails.TaxPayerID = pobjClient.TaxPayerID;
                        objTaxPayerClientDetails.TaxPayerEmployerID = pobjClient.EmployerID;
                        objTaxPayerClientDetails.ClientName = pobjClient.ClientName;
                        objTaxPayerClientDetails.ProjectStartDate = pobjClient.ProjectStartDate;
                        objTaxPayerClientDetails.ProjectEndDate = pobjClient.ProjectEndDate;
                        objTaxPayerClientDetails.CityID = pobjClient.CityID;
                        objTaxPayerClientDetails.StateID = pobjClient.StateID;
                        objTaxPayerClientDetails.CountryID = pobjClient.CountryID;
                        objTaxPayerClientDetails.CreatedOn = System.DateTime.Now;

                        objTaxEntities.tblTaxPayerClientDetails.AddObject(objTaxPayerClientDetails);
                    }

                    objTaxEntities.SaveChanges();
                }
                return 1;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<BO_tblTaxPayerClientDetail> GetTaxpayrClientsDetails(long taxPayerID)
        {
            try
            {
                using (ETaxEntities objETaxEntities = new ETaxEntities())
                {
                    objETaxEntities.Connection.Open();
                    List<BO_tblTaxPayerClientDetail> objTaxPayrClientdetail = (from g in objETaxEntities.tblTaxPayerClientDetails
                                                                               where g.TaxPayerID == taxPayerID
                                                                               select new BO_tblTaxPayerClientDetail

                                                                               {
                                                                                   TaxPayerClientID = g.TaxPayerClientID,
                                                                                   ClientName = g.ClientName,
                                                                                   ProjectStartDate = g.ProjectStartDate,
                                                                                   ProjectEndDate = g.ProjectEndDate,
                                                                                   CountryName = g.tblCountry.Country,
                                                                                   CityName = g.tblCity.City,
                                                                                   StateName = g.tblState.StateName,
                                                                                   CreatedOn = g.CreatedOn
                                                                               }).ToList();



                    return objTaxPayrClientdetail;

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public BO_tblTaxPayerClientDetail GetTaxpayrClientDetails(long taxPayerClientID)
        {
            try
            {
                using (ETaxEntities objETaxEntities = new ETaxEntities())
                {
                    objETaxEntities.Connection.Open();
                    BO_tblTaxPayerClientDetail objTaxPayrClientdetail = (from g in objETaxEntities.tblTaxPayerClientDetails
                                                                         where g.TaxPayerClientID == taxPayerClientID
                                                                         select new BO_tblTaxPayerClientDetail

                                                                         {
                                                                             TaxPayerClientID = g.TaxPayerClientID,
                                                                             ClientName = g.ClientName,
                                                                             ProjectStartDate = g.ProjectStartDate,
                                                                             ProjectEndDate = g.ProjectEndDate,
                                                                             CountryID = g.CountryID,
                                                                             CityID = g.CityID,
                                                                             StateID = g.StateID,
                                                                             EmployerID=g.tblTaxPayerEmployerDetail.TaxPayerEmployerID,
                                                                             EmployerName=g.tblTaxPayerEmployerDetail.EmployerName,
                                                                             CountryName = g.tblCountry.Country,
                                                                             CityName = g.tblCity.City,
                                                                             StateName =g.tblCountry.Country,
                                                                             CreatedOn = g.CreatedOn
                                                                         }).SingleOrDefault();



                    return objTaxPayrClientdetail;

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}