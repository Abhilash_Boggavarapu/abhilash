﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETaxBO
{
   public class BO_tblTaxPayerResidencyDetail
    {
        public long TaxPayerResidencyDetailsID { get; set; }
        public long TaxPayerID { get; set; }
        public string CityName { get; set; }
        public string StateName { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public virtual BO_tblTaxPayerDetail tblTaxPayerDetail { get; set; }

        public System.DateTime StartDate { get; set; }

        public System.DateTime EndDate { get; set; }
    }
}
