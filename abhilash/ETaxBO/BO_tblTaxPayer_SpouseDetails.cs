﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETaxBO
{
  public  class BO_tblTaxPayer_SpouseDetails
    {
        public long TaxPayer_SpouseID { get; set; }
        public long TaxPayerID { get; set; }
        public long SpouseID { get; set; }
        public System.DateTime CreatedOn { get; set; }

        public virtual BO_tblTaxPayerDetail tblTaxPayerDetail { get; set; }
        public virtual BO_tblTaxPayerDetail tblTaxPayerDetail1 { get; set; }
    }
}
