﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETaxBO
{
   public class BO_tblMailBoxMessageType
    {
       public BO_tblMailBoxMessageType()
    {
        this.tblMailBoxMessageTransactions = new HashSet<BO_tblMailBoxMessageTransaction>();
    }

    public int MessageTypeID { get; set; }
    public string MessageType { get; set; }
    public System.DateTime CreatedOn { get; set; }

    public virtual ICollection<BO_tblMailBoxMessageTransaction> tblMailBoxMessageTransactions { get; set; }
    }
}
