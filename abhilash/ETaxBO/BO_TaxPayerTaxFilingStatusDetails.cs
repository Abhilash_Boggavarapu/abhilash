﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETaxBO
{
    public class BO_TaxPayerTaxFilingStatusDetails
    {
        public int TaxFilingStatusTransactionID { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public virtual ICollection<BO_tblUser> tblUsers { get; set; }
        public virtual ICollection<BO_tblTaxFillingStatusTypes> tblTaxFillingStatusTypes { get; set; }
        public long TaxPayerID { get; set; }
        public int TaxFilingStatusTypeID{ get; set; }

    }
}
