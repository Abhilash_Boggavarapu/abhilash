﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETaxBO
{
    public static class MasterItems
    {
        public enum AssetTypes
        {
            Desktop = 1,
            Laptop = 2,
            HardwareAccessories = 3,
            Printer = 4,
            Software = 5,
            Scanner = 6,
            Fax = 7,
            Copier = 8,
            ComputerFurniture = 9,
            CellPhone = 10,
            MissAssets = 11

        }

        public enum Documents
        {
            WageTaxStatement = 1, // W2 Form 
            MiscellaneousIncomeStatement = 2, // Form 1099 
            InterestIncomeStatement = 3, //1099 Int
            StateTaxRefund = 4, //1099G
            individualRetirementArrangement = 5, //1099- R
            HomeMortgageStatement = 6, //1098 
            TuitionfeeStatement = 7, //1098-T
            EducationInterestLoan = 8,//1098- E
            Partnership = 9,  //1065 K 
            Uploadotherdocument

        }

        public enum ExpencesTypes
        {
            MealsExpensesPerDay = 1,
            MonthlyStayExpenses = 2,
            LaundryExpensesPerMonth = 3,
            ModeofCommuting = 4,
            OneWayDistancetooffice = 5,
            CommutingExpensesPerMonth = 6,
            IncidentalExpenses = 7,
            RelocationExpenses = 8,
            VisaExpenses = 9,
            JobHuntingExpenses = 10,
            MobileExpensesPerMonth = 11,
            IternetExpensesPerMonth = 12,
            JobTrainingExpenses = 13
        }

        public enum MailBoxMessageTypes
        {
            Compose = 1,
            Reply = 2,
            Forword = 3
        }

        public enum MaritalStatuses
        {
            Single = 1,
            Married = 2,
            Divorsed = 3,
            wedowed = 4,
            NeverMarried = 5,
            Separated = 6
        }

        public enum MiscellaneousTypes
        {
            Mortgage_Interest = 1,//Are you Paying any Mortgage_Interest
            Retirement_Savings = 2,//have you made any contributions to Retirement Savings
            interest_Income = 3,//Have you earned any interest Income
            dividend_Income = 4,//Have you earned any dividend Income
            Stocks_during_TY2011 = 5,//Have you sold any Stocks during TY2011
            Carryforward_Broughtforward = 6,//Do you have any Carry forward / Brought forward
            Loss_amount = 7,//Loss amount from Stock sales in previous years
            State_Tax_refund,//Have you received any  State Tax refund during TY2011
            Health_Savings_Account = 9,//Any Contribution amount paid towards Health Savings Account
            Medical_expenses = 10, //have you incurred any Medical expenses
            Spouse_Maternity = 11,//Have you Incurred any expenses towards your  Spouse Maternity
            Charitable_NonCharitableContributions = 12 // Have you made any Charitable/Non Charitable Contributions
        }

        public enum ReferralTypes
        {
            ReferredfromHomePage = 1,
            ReferredfromClientLogin = 2
        }

        public enum Relationships
        {
            Brother = 1,
            Sister = 2,
            Friend = 3,
            Cousin = 4,
            Niece = 5,
            Nephew = 6
        }

        public enum Roles
        {
            Admin = 1,
            User = 2
        }

        public enum VisaStatuses
        {
            GreenCard = 1,
            H1 = 2,
            L1 = 3,
            F1OPT = 4,
            F1CPT = 5,
            Other = 6
        }

        public enum TaxSheetSource
        {
            TaxPayer,
            Admin
        }
        public enum TaxSheetSType
        {
            ReturnDocuments=1,
            TaxInformation=2,
            ConfirmTaxDocuments=3

        }

        public enum TaxFillingStatusTypes
        {
            PersonalInformation = 1,
            FillTaxInformation = 2,
            UploadTaxDocuments = 3,
            TaxPreparationPending = 4,
            PaymentPendingClients = 5,
            ConformationPending = 6,
            EFilingCompleted = 7,
            CompletedClients=8
        }

        public enum ConsentForm
        {
            ConsentFormID=1
        }
        public enum TaxPayerType
        {
            self=1,
            Spouse=2,
        }
        public enum Statustypes
        {
            Pending=1,
            Completed=2,

        }
    }
}

