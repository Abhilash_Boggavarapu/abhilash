﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETaxBO
{
   public class BO_tblVisaStatus
    {
       public BO_tblVisaStatus()
    {
        this.tblTaxPayerDependents = new HashSet<BO_tblTaxPayerDependent>();
        this.tblTaxPayerDetails = new HashSet<BO_tblTaxPayerDetail>();
    }

    public int VisaStatusID { get; set; }
    public string VisaStatus { get; set; }
    public System.DateTime CreatedOn { get; set; }

    public virtual ICollection<BO_tblTaxPayerDependent> tblTaxPayerDependents { get; set; }
    public virtual ICollection<BO_tblTaxPayerDetail> tblTaxPayerDetails { get; set; }
    }
}
