﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETaxBO
{
  public  class BO_tblTaxPayerMiscellaneousExpencesDetail
    {
        public long TaxPayerMiscellaneousExpencesDetailsID { get; set; }
        public long TaxPayerID { get; set; }
        public int MiscellaneousTypeID { get; set; }
        public bool TaxPayer { get; set; }
        public decimal Amount { get; set; }
        public string Remarks { get; set; }
        public System.DateTime CreatedOn { get; set; }

        public virtual BO_tblMiscellaneousType tblMiscellaneousType { get; set; }
        public virtual BO_tblTaxPayerDetail tblTaxPayerDetail { get; set; }
    }
}
