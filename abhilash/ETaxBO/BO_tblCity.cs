﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETaxBO
{
    public class BO_tblCity
    {
        public BO_tblCity()
    {
        this.tblTaxPayerClientDetails = new HashSet<BO_tblTaxPayerClientDetail>();
        this.tblTaxPayerDetails = new HashSet<BO_tblTaxPayerDetail>();
        this.tblTaxPayerEmployerDetails = new HashSet<BO_tblTaxPayerEmployerDetail>();
        this.tblTaxPayerResidencyDetails = new HashSet<BO_tblTaxPayerResidencyDetail>();
        this.tblUsers = new HashSet<BO_tblUser>();
    }

    public int CityID { get; set; }
    public int CountryID { get; set; }
    public int StateID { get; set; }
    public string City { get; set; }
    public string CitySmallName { get; set; }
    public System.DateTime CreatedOn { get; set; }

    public virtual ICollection<BO_tblTaxPayerClientDetail> tblTaxPayerClientDetails { get; set; }
    public virtual ICollection<BO_tblTaxPayerDetail> tblTaxPayerDetails { get; set; }
    public virtual ICollection<BO_tblTaxPayerEmployerDetail> tblTaxPayerEmployerDetails { get; set; }
    public virtual ICollection<BO_tblTaxPayerResidencyDetail> tblTaxPayerResidencyDetails { get; set; }
    public virtual ICollection<BO_tblUser> tblUsers { get; set; }
    }
}
