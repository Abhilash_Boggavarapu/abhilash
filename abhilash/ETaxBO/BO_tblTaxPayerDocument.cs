﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETaxBO
{
   public class BO_tblTaxPayerDocument
    {
        public long TaxPayerDocumentID { get; set; }
        public long TaxPayerID { get; set; }
        public int DocumentID { get; set; }
        public byte[] FileContent { get; set; }
        public string Remarks { get; set; }
        public string FileName { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public int Count { get; set; }
        public virtual BO_tblDocument tblDocument { get; set; }
        public virtual BO_tblTaxPayerDetail tblTaxPayerDetail { get; set; }

        public string FormName { get; set; }
    }
}
