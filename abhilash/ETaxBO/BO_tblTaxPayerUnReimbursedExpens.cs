﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETaxBO
{
   public class BO_tblTaxPayerUnReimbursedExpens
    {
        public long TaxPayerUnReimbursedExpenseID { get; set; }
        public long TaxPayerID { get; set; }
        public int ExpenseID { get; set; }
        public long TaxPayerClientID { get; set; }
        public decimal Price { get; set; }
        public string Remarks { get; set; }
        public System.DateTime CreatedOn { get; set; }

        public virtual BO_tblExpencesType tblExpencesType { get; set; }
        public virtual BO_tblTaxPayerClientDetail tblTaxPayerClientDetail { get; set; }
        public virtual BO_tblTaxPayerDetail tblTaxPayerDetail { get; set; }
    }
}
