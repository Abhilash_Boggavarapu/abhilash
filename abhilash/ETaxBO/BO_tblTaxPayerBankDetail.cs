﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETaxBO
{
   public class BO_tblTaxPayerBankDetail
    {
        public long BankDetailsID { get; set; }
        public long TaxPayerID { get; set; }
        public long AccountNo { get; set; }
        public string RoutingNo { get; set; }
        public string BankName { get; set; }
        public string AccountHolderName { get; set; }
        public string AccounTtype { get; set; }
        public System.DateTime CreatedOn { get; set; }
    }
}
