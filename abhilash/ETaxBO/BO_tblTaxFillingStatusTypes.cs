﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETaxBO
{
   public class BO_tblTaxFillingStatusTypes
    {
        public int TaxFilingStatusTypeID { get; set; }
        public string TaxFilingStatusType { get; set; }
        public System.DateTime CreatedOn { get; set; }
    }
}
