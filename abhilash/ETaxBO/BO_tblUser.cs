﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETaxBO
{
  public  class BO_tblUser
    {
      public BO_tblUser()
    {
        this.tblMailBoxMessageTransactions = new HashSet<BO_tblMailBoxMessageTransaction>();
        this.tblMailBoxMessageTransactions1 = new HashSet<BO_tblMailBoxMessageTransaction>();
        this.tblTaxPayerDetails = new HashSet<BO_tblTaxPayerDetail>();
        this.tblUserReferrals = new HashSet<BO_tblUserReferral>();
    }

    public long UserID { get; set; }
    public int RoleID { get; set; }
    public string EmailID { get; set; }
    public string Password { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Gender { get; set; }
    public string ZipCode { get; set; }
    public int CityID { get; set; }
    public int StateID { get; set; }
    public int CountryID { get; set; }
    public string MobileNumber { get; set; }
    public string WorkNubmer { get; set; }
    public string HomeNumber { get; set; }
    public string EmployerName { get; set; }
    public System.DateTime CreatedOn { get; set; }
    public bool ConsentFormStatus { get; set; }
    public byte? ConsentDuration { get; set; }
    public string SignatureOnConsentForm { get; set; }
    public DateTime? SignedOnConsentForm { get; set; }


    public virtual BO_tblCity tblCity { get; set; }
    public virtual ICollection<BO_tblMailBoxMessageTransaction> tblMailBoxMessageTransactions { get; set; }
    public virtual ICollection<BO_tblMailBoxMessageTransaction> tblMailBoxMessageTransactions1 { get; set; }
    public virtual BO_tblState tblState { get; set; }
    public virtual ICollection<BO_tblTaxPayerDetail> tblTaxPayerDetails { get; set; }
    public virtual ICollection<BO_tblUserReferral> tblUserReferrals { get; set; }
    }
}
