﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETaxBO
{
    public class BO_tblAssetType
    {
        public BO_tblAssetType()
    {
        this.tblTaxPayerAssetsDetails = new HashSet<BO_tblTaxPayerAssetsDetail>();
    }

    public int AssetTypeID { get; set; }
    public string AssetName { get; set; }
    public System.DateTime CreatedOn { get; set; }

    public virtual ICollection<BO_tblTaxPayerAssetsDetail> tblTaxPayerAssetsDetails { get; set; }
    }
}
