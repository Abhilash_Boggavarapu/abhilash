﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETaxBO
{
   public class BO_tblRelationship
    {
       public BO_tblRelationship()
    {
        this.tblTaxPayerDependents = new HashSet<BO_tblTaxPayerDependent>();
    }

    public int RelationShipID { get; set; }
    public string RelationShip { get; set; }
    public System.DateTime CreatedOn { get; set; }

    public virtual ICollection<BO_tblTaxPayerDependent> tblTaxPayerDependents { get; set; }
    }
}
