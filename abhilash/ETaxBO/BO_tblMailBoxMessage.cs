﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETaxBO
{
     public class BO_tblMailBoxMessage
    {
        public long MailBoxMessageID { get; set; }
        public string Message { get; set; }
        public System.DateTime CreatedOn { get; set; }
    }
}
