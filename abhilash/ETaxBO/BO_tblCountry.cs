﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETaxBO
{
     public class BO_tblCountry
    {
         public BO_tblCountry()
    {
        this.tblStates = new HashSet<BO_tblState>();
    }

    public int CountryID { get; set; }
    public string Country  { get; set; }
    public string CountrySmallName { get; set; }
    public System.DateTime CreatedDate { get; set; }

    public virtual ICollection<BO_tblState> tblStates { get; set; }
    }
}
