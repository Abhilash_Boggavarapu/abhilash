﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETaxBO
{
   public class BO_tblMaritalStatus
    {
       public BO_tblMaritalStatus()
    {
        this.tblTaxPayerDetails = new HashSet<BO_tblTaxPayerDetail>();
    }

    public int MaritalStatusID { get; set; }
    public string MaritalStatus { get; set; }
    public System.DateTime CreatedOn { get; set; }

    public virtual ICollection<BO_tblTaxPayerDetail> tblTaxPayerDetails { get; set; }
    }
}
