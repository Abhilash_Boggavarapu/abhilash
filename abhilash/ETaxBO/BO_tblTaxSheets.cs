﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace ETaxBO
{
  public  class BO_tblTaxSheets
    {
        public int TaxSheetID { get; set; }
        public long TaxPayerID { get; set; }
        public bool EligibleForEfilingStatus { get; set; }
        public bool ConfirmTaxDocumentStatus { get; set; }

        public string TaxSheetSource { get; set; }
        public byte[] TaxSheet { get; set; }
        public string Remarks { get; set; }
        public string SheetName { get; set; }
        public int SheetsType { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public virtual BO_tblTaxPayerDetail tblTaxPayerDetails { get; set; }
    }
}
