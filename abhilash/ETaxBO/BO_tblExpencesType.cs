﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETaxBO
{
   public  class BO_tblExpencesType
    {
       public BO_tblExpencesType()
    {
        this.tblTaxPayerUnReimbursedExpenses = new HashSet<BO_tblTaxPayerUnReimbursedExpens>();
    }

    public int ExpenseTypeID { get; set; }
    public string ExpenseType { get; set; }
    public System.DateTime CreatedOn { get; set; }

    public virtual ICollection<BO_tblTaxPayerUnReimbursedExpens> tblTaxPayerUnReimbursedExpenses { get; set; }
    }
}
