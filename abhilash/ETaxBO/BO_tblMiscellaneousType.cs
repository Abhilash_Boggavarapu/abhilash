﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETaxBO
{
   public class BO_tblMiscellaneousType
    {
       public BO_tblMiscellaneousType()
    {
        this.tblTaxPayerMiscellaneousExpencesDetails = new HashSet<BO_tblTaxPayerMiscellaneousExpencesDetail>();
    }

    public int MiscellaneousTypeID { get; set; }
    public string MiscellaneousType { get; set; }
    public System.DateTime CreatedOn { get; set; }

    public virtual ICollection<BO_tblTaxPayerMiscellaneousExpencesDetail> tblTaxPayerMiscellaneousExpencesDetails { get; set; }
    }
}
