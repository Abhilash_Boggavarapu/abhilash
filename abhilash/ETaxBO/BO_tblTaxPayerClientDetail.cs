﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETaxBO
{
    public class BO_tblTaxPayerClientDetail
    {
        public BO_tblTaxPayerClientDetail()
        {
            this.tblTaxPayerResidencyDetails = new HashSet<BO_tblTaxPayerResidencyDetail>();
            this.tblTaxPayerUnReimbursedExpenses = new HashSet<BO_tblTaxPayerUnReimbursedExpens>();
        }

        public long TaxPayerClientID { get; set; }
        public long TaxPayerID { get; set; }
        public string ClientName { get; set; }
        public System.DateTime ProjectStartDate { get; set; }
        public System.DateTime ProjectEndDate { get; set; }
        public string CityName { get; set; }
        public string CountryName { get; set; }
        public string StateName { get; set; }
        public int CountryID { get; set; }
        public int CityID { get; set; }
        public int StateID { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public long EmployerID { get; set; }
        public string EmployerName { get; set; }

        public virtual BO_tblCity tblCity { get; set; }
        public virtual BO_tblState tblState { get; set; }
        public virtual BO_tblTaxPayerDetail tblTaxPayerDetail { get; set; }
        public virtual ICollection<BO_tblTaxPayerResidencyDetail> tblTaxPayerResidencyDetails { get; set; }
        public virtual ICollection<BO_tblTaxPayerUnReimbursedExpens> tblTaxPayerUnReimbursedExpenses { get; set; }
        public virtual ICollection<BO_tblTaxPayerEmployerDetail> tblTaxPayerEmployerDetails { get; set; }
    }
}
