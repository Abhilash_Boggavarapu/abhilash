﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETaxBO
{
   public class BO_TaxPayerAdditionalInformation
    {

        public long TaxPayerAdditionalInfoID { get; set; }
        public long TaxPayerID { get; set; }
        public string Information { get; set; }
        public System.DateTime CreatedOn { get; set; }

        public virtual BO_tblTaxPayerDetail tblTaxPayerDetail { get; set; }
    }
}
