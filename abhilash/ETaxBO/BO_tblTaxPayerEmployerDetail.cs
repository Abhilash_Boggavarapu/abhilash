﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETaxBO
{
   public class BO_tblTaxPayerEmployerDetail
    {
        public long TaxPayerEmployerID { get; set; }
        public long TaxPayerID { get; set; }
        public string EmployerName { get; set; }
        public System.DateTime EmploymentStartDate { get; set; }
        public System.DateTime EmploymentEndDate { get; set; }
        public int CityID { get; set; }
        public string CountryName { get; set; }
        public int CountryID { get; set; }
        public int StateID { get; set; }
        public System.DateTime CreatedOn { get; set; }

        public virtual BO_tblCity tblCity { get; set; }
        public virtual BO_tblState tblState { get; set; }
        public virtual BO_tblTaxPayerDetail tblTaxPayerDetail { get; set; }
        public string City { get; set; }
        public string StateName { get; set; }
    }
}
