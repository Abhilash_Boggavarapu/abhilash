﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETaxBO
{
   public class BO_TaxSummary
    {
        public int TaxSummaryId { get; set; }
        public string TaxSummary { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public long TaxPayerID { get; set; }
        public virtual ICollection<BO_tblTaxPayerDetail> tblUserReferrals { get; set; }
    }
}
