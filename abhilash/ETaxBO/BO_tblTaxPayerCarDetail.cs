﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETaxBO
{
   public class BO_tblTaxPayerCarDetail
    {
        public long TaxPayerCarDetailsID { get; set; }
        public long TaxPayerID { get; set; }
        public string CarMake_Model { get; set; }
        public decimal PurchasedYear { get; set; }
        public decimal MilesDriven { get; set; }
        public System.DateTime CreatedOn { get; set; }

        public virtual BO_tblTaxPayerDetail tblTaxPayerDetail { get; set; }
    }
}
