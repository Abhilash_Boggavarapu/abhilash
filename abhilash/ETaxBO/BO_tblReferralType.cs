﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETaxBO
{
   public class BO_tblReferralType
    {
       public BO_tblReferralType()
    {
        this.tblUserReferrals = new HashSet<BO_tblUserReferral>();
    }

    public int ReferralTypeID { get; set; }
    public string ReferralType { get; set; }
    public System.DateTime CreatedOn { get; set; }

    public virtual ICollection<BO_tblUserReferral> tblUserReferrals { get; set; }
    }
}
