﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETaxBO
{
   public class Bo_StaffComments
    {
        public int StaffCommentsID { get; set; }
        public string StaffComments { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public long TaxPayerID { get; set; }
        public virtual ICollection<BO_tblTaxPayerDetail> tblUserReferrals { get; set; }
    }
}
