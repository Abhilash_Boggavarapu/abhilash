﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETaxBO
{
   public class BO_tblState
    {
       public BO_tblState()
    {
        this.tblTaxPayerClientDetails = new HashSet<BO_tblTaxPayerClientDetail>();
        this.tblTaxPayerDetails = new HashSet<BO_tblTaxPayerDetail>();
        this.tblTaxPayerEmployerDetails = new HashSet<BO_tblTaxPayerEmployerDetail>();
        this.tblTaxPayerResidencyDetails = new HashSet<BO_tblTaxPayerResidencyDetail>();
        this.tblUsers = new HashSet<BO_tblUser>();
    }

    public int StateID { get; set; }
    public int CountryID { get; set; }
    public string StateName { get; set; }
    public string StateSmallName { get; set; }
    public System.DateTime CreatedDate { get; set; }

    public virtual BO_tblCountry tblCountry { get; set; }
    public virtual ICollection<BO_tblTaxPayerClientDetail> tblTaxPayerClientDetails { get; set; }
    public virtual ICollection<BO_tblTaxPayerDetail> tblTaxPayerDetails { get; set; }
    public virtual ICollection<BO_tblTaxPayerEmployerDetail> tblTaxPayerEmployerDetails { get; set; }
    public virtual ICollection<BO_tblTaxPayerResidencyDetail> tblTaxPayerResidencyDetails { get; set; }
    public virtual ICollection<BO_tblUser> tblUsers { get; set; }
    }
}
