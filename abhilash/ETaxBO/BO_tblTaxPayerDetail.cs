﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETaxBO
{
    public class BO_tblTaxPayerDetail
    {
        public BO_tblTaxPayerDetail()
        {
            this.TaxPayerAdditionalInformations = new HashSet<BO_TaxPayerAdditionalInformation>();
            this.tblTaxPayer_SpouseDetails = new HashSet<BO_tblTaxPayer_SpouseDetails>();
            this.tblTaxPayer_SpouseDetails1 = new HashSet<BO_tblTaxPayer_SpouseDetails>();
            this.tblTaxPayerCarDetails = new HashSet<BO_tblTaxPayerCarDetail>();
            this.tblTaxPayerClientDetails = new HashSet<BO_tblTaxPayerClientDetail>();
            this.tblTaxPayerDependents = new HashSet<BO_tblTaxPayerDependent>();
            this.tblTaxPayerDocuments = new HashSet<BO_tblTaxPayerDocument>();
            this.tblTaxPayerEmployerDetails = new HashSet<BO_tblTaxPayerEmployerDetail>();
            this.tblTaxPayerMiscellaneousExpencesDetails = new HashSet<BO_tblTaxPayerMiscellaneousExpencesDetail>();
            this.tblTaxPayerResidencyDetails = new HashSet<BO_tblTaxPayerResidencyDetail>();
            this.tblTaxPayerUnReimbursedExpenses = new HashSet<BO_tblTaxPayerUnReimbursedExpens>();
        }

        public long TaxPayerID { get; set; }
        public long? TaxPayerSpouseID { get; set; }
        public long UserID { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public System.DateTime DOB { get; set; }
        public int PayerType { get; set; }
        public string Gender { get; set; }
        public string SSN { get; set; }
        public string MaritalStatus { get; set; }
        public string CityName { get; set; }
        public string StateName { get; set; }
        public string CountryName { get; set; }
        public string VisaStatus { get; set; }
        public int MaritalStatusID { get; set; }
        public string Occupation { get; set; }
        public string StreetNo { get; set; }
        public string ApartmentNo { get; set; }
        public int CityID { get; set; }
        public int StateID { get; set; }
        public int CountryID { get; set; }
        public string  ZipCode { get; set; }
        public string MobileNumber { get; set; }
        public string HomeNumber { get; set; }
        public string LandlineNumber { get; set; }
        public string PrimaryEMailID { get; set; }
        public string SecondaryEMailID { get; set; }
        public int? VisaStatusID { get; set; }
        public System.DateTime? USEntryDate { get; set; }
        public bool? HasDependants { get; set; }
        public System.DateTime CreatedOn { get; set; }
      
       

        public virtual ICollection<BO_TaxPayerAdditionalInformation> TaxPayerAdditionalInformations { get; set; }
        public virtual BO_tblCity tblCity { get; set; }
        public virtual BO_tblCountry tblCountry { get; set; }
        public virtual BO_tblMaritalStatus tblMaritalStatus { get; set; }
        public virtual BO_tblState tblState { get; set; }
        public virtual ICollection<BO_tblTaxPayer_SpouseDetails> tblTaxPayer_SpouseDetails { get; set; }
        public virtual ICollection<BO_tblTaxPayer_SpouseDetails> tblTaxPayer_SpouseDetails1 { get; set; }
        public virtual ICollection<BO_tblTaxPayerCarDetail> tblTaxPayerCarDetails { get; set; }
        public virtual ICollection<BO_tblTaxPayerClientDetail> tblTaxPayerClientDetails { get; set; }
        public virtual ICollection<BO_tblTaxPayerDependent> tblTaxPayerDependents { get; set; }
        public virtual BO_tblUser tblUser { get; set; }
        public virtual BO_tblVisaStatus tblVisaStatus { get; set; }
        public virtual ICollection<BO_tblTaxPayerDocument> tblTaxPayerDocuments { get; set; }
        public virtual ICollection<BO_tblTaxPayerEmployerDetail> tblTaxPayerEmployerDetails { get; set; }
        public virtual ICollection<BO_tblTaxPayerMiscellaneousExpencesDetail> tblTaxPayerMiscellaneousExpencesDetails { get; set; }
        public virtual ICollection<BO_tblTaxPayerResidencyDetail> tblTaxPayerResidencyDetails { get; set; }
        public virtual ICollection<BO_tblTaxPayerUnReimbursedExpens> tblTaxPayerUnReimbursedExpenses { get; set; }

        public string UserFirstName { get; set; }
        public bool ConsentFormStatus { get; set; }

        public MasterItems.TaxPayerType CurrentTaxPayer { get; set; }

        public bool TaxPreparationCompletedEnabled { get; set; }

        public bool EFilingCompletedEnabled { get; set; }

        public string EmailId { get; set; }

        public bool CompletedEnabled { get; set; }
    }
}
