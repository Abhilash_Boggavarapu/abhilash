﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETaxBO
{
   public class BO_tblUserReferral
    {
        public long ReferredID { get; set; }
        public int ReferralTypeID { get; set; }
        public string ReferralName { get; set; }
        public string EmailID { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string ReferredName { get; set; }
        public string ReferredEmailID { get; set; }
        public string ReferredPhone { get; set; }
        public Nullable<long> UserID { get; set; }
        public System.DateTime CreatedOn { get; set; }

        public virtual BO_tblReferralType tblReferralType { get; set; }
        public virtual BO_tblUser tblUser { get; set; }

        public string UserName { get; set; }

        public string UserEmailID { get; set; }

        public string UserPhoneNo { get; set; }

        public int Count { get; set; }
    }
}
