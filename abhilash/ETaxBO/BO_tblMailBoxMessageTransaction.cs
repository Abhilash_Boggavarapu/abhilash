﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETaxBO
{
   public class BO_tblMailBoxMessageTransaction
    {
        public long MessageTransactionID { get; set; }
        public int MessageTypeID { get; set; }
        public long MailBoxMessageID { get; set; }
        public long FromUserID { get; set; }
        public long ToUserID { get; set; }
        public bool IsRead { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreatedOn { get; set; }

        public virtual BO_tblMailBoxMessageType tblMailBoxMessageType { get; set; }
        public virtual BO_tblMailBoxMessageTransaction tblMailBoxMessageTransactions1 { get; set; }
        public virtual BO_tblMailBoxMessageTransaction tblMailBoxMessageTransaction1 { get; set; }
        public virtual BO_tblUser tblUser { get; set; }
        public virtual BO_tblUser tblUser1 { get; set; }
    }
}
