﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETaxBO
{
   public class BO_tblTaxPayerAssetsDetail
    {
        public long TaxPayerAssetDetailsID { get; set; }
        public long TaxPayerID { get; set; }
        public int AssetTypeID { get; set; }
        public System.DateTime PurchasedDate { get; set; }
        public decimal PercentageUsedInBusiness { get; set; }
        public decimal AcquisitionCost { get; set; }
        public Nullable<decimal> AnyReimbursement { get; set; }
        public System.DateTime CreatedOn { get; set; }

        public virtual BO_tblAssetType tblAssetType { get; set; }
    }
}
