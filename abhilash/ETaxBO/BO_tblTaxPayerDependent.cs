﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETaxBO
{
   public class BO_tblTaxPayerDependent
    {
        public long TaxPayerDependentID { get; set; }
        public long TaxPayerID { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public System.DateTime DOB { get; set; }
        public int RelationShipID { get; set; }
        public string SSN { get; set; }
        public int? VisaStatusID { get; set; }
        public string VisaStatus { get; set; }
        public System.DateTime? USEntryDate { get; set; }
        public System.DateTime CreatedOn { get; set; }
       
        public virtual BO_tblRelationship tblRelationship { get; set; }
        public virtual BO_tblTaxPayerDetail tblTaxPayerDetail { get; set; }
        public virtual BO_tblVisaStatus tblVisaStatus { get; set; }

        public string Relationship { get; set; }
    }
}
