﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETaxBO
{
   public  class BO_tblDocument
    {
    //   public BO_tblDocument()
    //{
    //   // this.tblTaxPayerDocuments = new HashSet<BO_tblTaxPayerDocument>();
    //}

    public int DocumentID { get; set; }
    public string FormName { get; set; }
    public System.DateTime CreatedOn { get; set; }

    public virtual ICollection<BO_tblTaxPayerDocument> tblTaxPayerDocuments { get; set; }
    }
}
