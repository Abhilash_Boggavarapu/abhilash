﻿

Imports Microsoft.VisualBasic

Public Class IntMail
    Private MyMaster As New Master()

    Public Function send_Internal_mail(ByVal fromid As String, ByVal Toid As String, ByVal subject As String, ByVal message As String)
        Dim MailstatusArray(1) As String
        MailstatusArray(0) = "insert into Tbl_InternalMail(From_Memberid,To_Memberid,Subject,Message,Date,Isread) values('" + fromid + "','" + Toid + "','" + subject + "','" + message + "',getdate(),'0')"
        MailstatusArray(1) = "insert into Tbl_InternalMail_Outbox(From_Memberid,To_Memberid,Subject,Message,Date,Isread) values('" + fromid + "','" + Toid + "','" + subject + "','" + message + "',getdate(),'0')"
        Return MyMaster.ReturnTransExecuteNoneQuery(MailstatusArray)

    End Function
    Public Function send_Internal_mail_TOALL(ByVal fromid As String, ByVal Toid As String, ByVal subject As String, ByVal message As String, ByVal isread As String)
        Dim status As String = ""
        Dim sendmess(1) As String
        sendmess(0) = "insert into Tbl_InternalMail(From_Memberid,To_Memberid,Subject,Message,Date,Isread) values('" + fromid + "','" + Toid + "','" + subject + "','" + message + "',getdate(),'" + isread + "')"
        sendmess(1) = "insert into Tbl_InternalMail_Outbox(From_Memberid,To_Memberid,Subject,Message,Date,Isread) values('" + fromid + "','" + Toid + "','" + subject + "','" + message + "',getdate(),'" + isread + "')"
        status = MyMaster.ReturnTransExecuteNoneQuery(sendmess)
        Return status
    End Function
    'InBox
    Public Function Mail_Inbox(ByVal Memberid As String)
        Return MyMaster.ReturnDataTable("select TIM.Id,TIM.From_Memberid,TblUser.Emailid,TIM.Subject,TIM.Message,convert(varchar,TIM.Date,101) as date,case TIM.Isread when 1 then 'Read' when 0 then 'Unread' end as Isread from Tbl_InternalMail TIM,tblUsers TblUser " & _
        " where TIM.From_Memberid=TblUser.userid and TIM.To_Memberid='" + Memberid + "' order by TIM.Date desc")
    End Function
    'Inbox Count
    Public Function Mail_Inbox_count(ByVal memberid As String)
        Return MyMaster.ReturnExecuteScalar("select count(*) from Tbl_InternalMail where To_Memberid='" + memberid + "' and Isread='False'")
    End Function
    'OutBox
    Public Function Mail_Outbox(ByVal memberid) As Data.DataTable
        'Return MyMaster.ReturnDataTable("select * from Tbl_InternalMail_Outbox where From_Memberid='" + memberid + "' order by date")
        Return MyMaster.ReturnDataTable("select TIM.Id,TIM.To_Memberid,TblUser.Emailid,TIM.Subject,TIM.Message,convert(varchar,TIM.Date,101) as date,case TIM.Isread when 1 then 'Read' when 0 then 'Unread' end as Isread from Tbl_InternalMail_Outbox TIM,tblUsers TblUser " & _
        " where TIM.To_Memberid=TblUser.userid and TIM.From_Memberid='" + memberid + "' order by TIM.Date desc")

    End Function
    'Full Inbox or outbox Mail description
    Public Function Mail_Description(ByVal memberid As String, ByVal MsgID As String, ByVal type As String) As Data.DataTable
        'here 0 means inbox and 1 means outbox

        If type = "0" Then
            'Return MyMaster.ReturnDataTable("select * from Tbl_InternalMail where Id='" + ID + "'")
            Return MyMaster.ReturnDataTable("select TIM.Id,TIM.From_Memberid,TblUser.Emailid as ToEmailid,TIM.Subject,TIM.Message, convert(varchar,TIM.Date,101) as date,TIM.Isread from Tbl_InternalMail TIM,tblUsers TblUser " & _
            " where TIM.From_Memberid=TblUser.userid and TIM.To_Memberid='" + memberid + "' and TIM.Id='" + MsgID + "' order by date")
        ElseIf (type = "1") Then
            ' Return MyMaster.ReturnDataTable("select * from Tbl_InternalMail_Outbox where Id='" + ID + "'")
            Return MyMaster.ReturnDataTable("select TIM.Id,TIM.to_Memberid,TblUser.Emailid as FromEmailid,TIM.Subject,TIM.Message,convert(varchar,TIM.Date,101) as date,TIM.Isread from Tbl_InternalMail_Outbox TIM,tblUsers TblUser " & _
            " where TIM.From_Memberid=TblUser.userid and TIM.From_Memberid='" + memberid + "' and TIM.Id='" + MsgID + "' order by date")

        End If

    End Function

    'delete Inbox Items
    Public Function Mail_Inbox_delete(ByVal id As String)
        Return MyMaster.ReturnExecuteNoneQuery("DELETE from Tbl_InternalMail WHERE ID IN (" + id + ")")
    End Function
    'delete Outbox Items
    Public Function Mail_Outbox_delete(ByVal id As String)
        Return MyMaster.ReturnExecuteNoneQuery("DELETE from Tbl_InternalMail_Outbox WHERE ID IN (" + id + ")")
    End Function
    'Inbox Mail status update
    Public Function Mail_Inbox_MessageRead(ByVal id As String)
        Return MyMaster.ReturnExecuteNoneQuery("update Tbl_InternalMail set Isread='1' WHERE ID = " + id + "")
    End Function
    'outbox Mail status update
    Public Function Mail_Outbox_MessageRead(ByVal id As String)
        Return MyMaster.ReturnExecuteNoneQuery("update Tbl_InternalMail_Outbox set Isread='1' WHERE ID = " + id + "")
    End Function
    Public Function Convertdatetime(ByVal datetime As Date)
        Return MyMaster.ReturnExecuteScalar("Select Convert(varchar ," + datetime + ",101) + ' ' + Convert(varchar , " + datetime + ",108)+ ' ' + SUBSTRING(CONVERT(varchar(20), " + datetime + ", 22), 18, 3)")
    End Function

End Class
