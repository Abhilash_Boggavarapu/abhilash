﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Data.SqlClient

''' <summary>
''' Summary description for Master
''' </summary>
Public Class Master

    Private SqlCon As New SqlConnection(ConfigurationSettings.AppSettings("Intmail"))
    Private SqlCmd As SqlCommand
    Private ds As DataSet
    Private SqlDap As SqlDataAdapter
    Private dt As DataTable
    Private status As Integer
    Private strStatus As String
    Private trns As SqlTransaction
    Private dr As SqlDataReader

    Public Sub New()

        '
        ' TODO: Add constructor logic here
        '
    End Sub
    Public Function ManageQoute(ByVal str As String) As String
        Return str.Replace("'", "''")
    End Function
    Public Sub getCon()
        If SqlCon.State = ConnectionState.Closed Then
            SqlCon.Open()
        End If
    End Sub
    Public Function ReturnDataSet(ByVal Query As String) As DataSet
        Try
            'getCon();
            SqlDap = New SqlDataAdapter(Query, SqlCon)
            ds = New DataSet()
            SqlDap.Fill(ds)
            Return ds
        Catch ex As Exception
            Return ErrorDataSet(ex.Message)
        Finally
            SqlCon.Close()
            '  SqlCmd.Dispose();
            SqlDap.Dispose()
            ds.Dispose()

        End Try
    End Function
    Public Function ReturnDataTable(ByVal Query As String) As DataTable
        Try
            'getCon();

            SqlDap = New SqlDataAdapter(Query, SqlCon)
            ds = New DataSet()
            SqlDap.Fill(ds)

            If (ds.Tables.Count > 0) Then
                Return ds.Tables(0)
            End If
            Return Nothing
        Catch ex As Exception
            Return ErrorDataTable(ex.Message)
        Finally
            SqlCon.Close()
            ' SqlCmd.Dispose();
            SqlDap.Dispose()
            ds.Dispose()
        End Try
    End Function
    Public Function ReturnExecuteNoneQuery(ByVal Query As String) As String
        Try
            getCon()
            SqlCmd = New SqlCommand(Query, SqlCon)
            strStatus = SqlCmd.ExecuteNonQuery().ToString()
            Return strStatus
        Catch ex As Exception
            Return ex.Message
        Finally
            SqlCon.Close()
            SqlCmd.Dispose()
        End Try
    End Function
    Public Function ReturnTransExecuteNoneQuery(ByVal Query As String()) As String
        Try
            getCon()

            trns = SqlCon.BeginTransaction()
            SqlCmd = New SqlCommand()
            SqlCmd.Transaction = trns
            SqlCmd.Connection = SqlCon

            For i As Integer = 0 To Query.Length - 1
                If (Query(i) <> "") Then
                    SqlCmd.CommandText = Query(i)
                    strStatus = SqlCmd.ExecuteNonQuery().ToString()
                End If
            Next

            trns.Commit()
            Return "1"

        Catch ex As Exception
            trns.Rollback()
            Return ex.Message
        Finally
            SqlCon.Close()
            SqlCmd.Dispose()
        End Try
    End Function


    'Public Function Reg_ReturnTransExecuteNoneQuery(ByVal Query As String(), ByVal Sponsorid As String, ByVal PackageID As String, ByVal regmemberid As String) As String
    '    Try
    '        getCon()

    '        trns = SqlCon.BeginTransaction()
    '        SqlCmd = New SqlCommand()
    '        SqlCmd.Transaction = trns
    '        SqlCmd.Connection = SqlCon

    '        For i As Integer = 0 To Query.Length - 1
    '            If (Query(i) <> "") Then
    '                SqlCmd.CommandText = Query(i)
    '                strStatus = SqlCmd.ExecuteNonQuery().ToString()
    '                Dim zeroquery As String
    '                If strStatus = "0" Then
    '                    zeroquery = Query(i)

    '                ElseIf (strStatus <> "0") Then
    '                    zeroquery = Query(i)
    '                End If
    '            End If

    '        Next


    '        If regSponsorbns = "1" Then
    '            trns.Commit()
    '            Return "1"
    '        Else
    '            trns.Rollback()
    '            ' Return ex.Message
    '        End If

    '    Catch ex As Exception
    '        trns.Rollback()
    '        Return ex.Message
    '    Finally
    '        SqlCon.Close()
    '        SqlCmd.Dispose()
    '    End Try
    'End Function
    Public Function ReturnExecuteScalar(ByVal Query As String) As String
        Try
            Dim sts
            getCon()
            SqlCmd = New SqlCommand(Query, SqlCon)
            sts = SqlCmd.ExecuteScalar()
            If IsDBNull(sts) Then
                Return ""
            Else
                Return sts
            End If
        Catch ex As Exception
            Return ex.Message
        Finally
            SqlCon.Close()
            SqlCmd.Dispose()
        End Try
    End Function

    Public Function ReturnExecuteScalarAsDecimal(ByVal Query As String) As Decimal
        Try
            getCon()

            SqlCmd = New SqlCommand(Query, SqlCon)
            strStatus = SqlCmd.ExecuteScalar().ToString()

            Return strStatus
        Catch ex As Exception
            Return ex.Message
        Finally
            SqlCon.Close()
            SqlCmd.Dispose()
        End Try
    End Function
    Public Function ReturnExecuteReader(ByVal Query As String) As String
        Try
            getCon()

            SqlCmd = New SqlCommand(Query, SqlCon)
            dr = SqlCmd.ExecuteReader()
            If dr.HasRows Then
                dr.Read()
                strStatus = dr(0).ToString()

            Else
                Return strStatus
            End If
            dr.Close()
            Return strStatus
        Catch ex As Exception
            Return ex.Message
        Finally
            SqlCon.Close()
            SqlCmd.Dispose()
        End Try
    End Function

    Public Function ErrorDataSet(ByVal ErrMsg As String) As DataSet
        Try
            Dim dt As DataTable = CreateDataTable()
            Dim row As DataRow = dt.NewRow()
            row("ErrMsg") = ErrMsg
            dt.Rows.Add(row)
            ds = New DataSet()
            ds.Tables.Add(dt)
            Return ds
        Catch ex As Exception
            Return Nothing
        Finally
            ds.Dispose()
        End Try
    End Function
    Public Function CreateDataTable() As DataTable
        Try
            dt = New DataTable()
            Dim myDataColumn As New DataColumn()
            myDataColumn.DataType = Type.[GetType]("System.String")
            myDataColumn.ColumnName = "ErrMsg"
            dt.Columns.Add(myDataColumn)
            Return dt
        Catch ex As Exception
            Return Nothing
        Finally
        End Try
    End Function
    Public Function ErrorDataTable(ByVal ErrMsg As String) As DataTable
        dt = CreateDataTable()
        Dim row As DataRow = dt.NewRow()
        row("ErrMsg") = ErrMsg
        dt.Rows.Add(row)
        Return dt


    End Function


    'Public Function ReturnExecutescalarWithnull(ByVal query As String)
    '    Try

    '        'object oScalarReturned = new Object();
    '        'oScalarReturned = command.ExecuteScalar();
    '        'if (oScalarReturned is DBNull || oScalarReturned == null)
    '        'scalarReturned = "";
    '        '            Else
    '        'scalarReturned = oScalarReturned.ToString();

    '        getCon()
    '        Dim oScalarReturned As Object = New Object()
    '        oScalarReturned = SqlCmd.ExecuteScalar()
    '        If (oScalarReturned Is DBNull.Value Or oScalarReturned = Nothing) Then
    '            strStatus = ""
    '        Else
    '            strStatus = oScalarReturned.ToString()

    '        End If


    '        'SqlCmd = New SqlCommand(query, SqlCon)
    '        'strStatus = SqlCmd.ExecuteScalar()
    '        'If strStatus Is DBNull.Value Then
    '        '    strStatus = "0"
    '        'End If
    '        Return strStatus
    '    Catch ex As Exception
    '        Return ex.Message
    '    Finally
    '        SqlCon.Close()
    '        SqlCmd.Dispose()
    '    End Try
    'End Function
   
End Class
